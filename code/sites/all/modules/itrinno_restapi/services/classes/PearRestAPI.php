<?php

class PearRestAPI implements IRestAPI {

  function executeRequest($service_data) {

    if (!$service_data->GetServiceURL()) {
      // deal with service registry cache
      $cid = _service_registry_get_cache_id($service_data);
      $cachedData = get_service_registry_cache($cid);
      if ($cachedData) {
        $service_url = $cachedData->data;
      }
      else {
        $service_url = json_decode($this->GetServices($service_data));
        // set the service registry response in the cache only if it is valid
        if (!is_null($service_url)) {
          set_service_registry_cache($service_url, $cid);
        }
      }

      // after consulting the service registry cache or the service registry
      // response we should have a valid service url
      // if not, errors have been logged in GetServices()
      if (!is_null($service_url)) {
        $service_data->SetServiceURL($service_url);
      }
      else {
        return NULL;
      }
    }
    
    // modify service url according to modproxy configuration
    $service_data->SetServiceURL($this->getFormattedServiceURL($service_data->GetServiceURL()));

    $req = new HTTP_Request($service_data->GetServiceURL());
    $req->setMethod($service_data->GetMethod());

    switch (trim($service_data->GetMethod())) {
      case "PUT":
        $req->SetBody('ServiceReq' . "=" . urlencode($service_data->GetPostedData()));
        break;

      case "POST":
        $req->addPostData('ServiceReq', $service_data->GetPostedData());
        break;

      case "GET":
        $req->clearPostData();
        break;
    }

    // contextService depends on framework_1_5 module
    $siteAuthKey = ContextService::getSiteAuthorizationKey();
    $SiteId = ContextService::getSiteId();

    // set authorization HTTP headers
    $req->addHeader('Authorization', 'Basic ' . base64_encode($SiteId . ":" . $siteAuthKey));
    $req->addHeader('SITEID', ContextService::getSiteId());
    $req->addHeader('Debug', "true");

    // send the HTTP request
    $req->sendRequest();

    $code = $req->getResponseCode();

    if ($code != 200 && $code != 201 && $code != 202 && $code != 203) {
      $error_msg = array();
      $error_msg[] = print_r($req->_requestHeaders, TRUE);
      if (method_exists($service_data, 'GetPostedData')) {
        $error_msg[] = print_r($service_data->GetPostedData(), TRUE);
      }
      $response_msg = $this->getStatusMessage($code);

      watchdog('ITrInno RestAPI',
          'Error in Service response: [@errCode] [%url] [%errMsg] [%siteId] [%serviceName] [%serviceIdty] [%authKey]<br />Request made:<br />@error_msg',
          array(
            '@errCode' => $code,
            '%url' => $service_data->GetServiceURL(),
            '%errMsg' => $response_msg,
            '%siteId' => $SiteId,
            '%serviceName' => $service_data->GetServiceName(),
            '%serviceIdty' => $service_data->GetServiceIdentity(),
            '%authKey' => $siteAuthKey,
            '@error_msg' => $error_msg),
          WATCHDOG_ERROR);

      if (method_exists($service_data, 'set_error_message')) {
        $service_data->set_error_message("Error in service URL: " . $response_msg);
      }
      return FALSE;
    }

    return $req->getResponseBody();
  }

  public function GetServices($service_data) {
    $site_id = ContextService::getSiteId();
    $service_name = $service_data->GetServiceName();
    $service_identity = $service_data->GetServiceIdentity();

    // get the registry base url
    $service_registry_url = GlobalSettings::getServiceRegistryUrl();

    // modify registry base to use modproxy in case it's configured
    $service_registry_url = $this->getFormattedServiceURL($service_registry_url);

    // set the request parameters
    $req = new HTTP_Request($service_registry_url . "{$site_id}/{$service_name}/{$service_identity}");
    $req->setMethod(HTTP_REQUEST_METHOD_GET);

    $site_auth_key = ContextService::getSiteAuthorizationKey();

    // HTTP Authentication headers
    $req->addHeader('Authorization', 'Basic ' . base64_encode($site_id . ":" . $site_auth_key));
    $req->addHeader('SITEID', $site_id);
    $req->addHeader('Debug', "true");

    $req->sendRequest();

    $code = $req->getResponseCode();

    // capture the request headers and body
    ob_start();
    print_r($req->_requestHeaders);
    echo "\n";
    print_r($req->_body);
    $err_body = ob_get_contents();
    ob_end_clean();

    if ($code != 200) {
      $response_msg = $this->getStatusMessage($code);
      watchdog('ITrInno RestAPI',
          'Error in Service Registry response: [@errCode] [%url] [%errMsg] [%siteId] [%serviceName] [%serviceIdty] [%authKey]<br />Request made:<br />@err_body',
          array(
            '@errCode' => $code,
            '%url' => $service_registry_url,
            '%errMsg' => $response_msg,
            '%siteId' => $site_id,
            '%serviceName' => $service_name,
            '%serviceIdty' => $service_identity,
            '%authKey' => $site_auth_key,
            '@err_body' => $err_body),
          WATCHDOG_ERROR);

      if (method_exists($service_data, 'set_error_message')) {
        $service_data->set_error_message("Error in Service Registry response: " . $response_msg);
      }
      return NULL;
    }

    return $req->getResponseBody();
  }

  private function getStatusMessage($code) {

    switch ($code) {
      case 404:
        $err_msg = t("Error Code 404: Document not found");
        break;

      case 403:
        $err_msg = t("Error Code 403: Forbidden");
        break;

      case 401:
        $err_msg = t("Error Code 401: Authentication error");
        break;

      case 500:
        $err_msg = t("Error Code 500: Internal Server error");
        break;

      case 200:
      default:
        $err_msg = t("OK");
        break;
    }

    return $err_msg;
  }

  /**
   * Format the service URL (both registry and endpoint URLs) depending on where
   * the call will be originated (Internal J&J or Modproxy).
   * 
   * @param string $service_url Service URL.
   * @return string Altered service URL (if configured to use modproxy).
   */
  private function getFormattedServiceURL($service_url) {
    // don't change the service url if we're in a jnj environment
    if (variable_get('itrinno_restapi_call_method', 'IJ') == 'IJ') {
      return $service_url;
    }

    $modproxy_hostname = trim(variable_get('itrinno_restapi_modproxy_url', ''));
    if (empty($modproxy_hostname)) {
      // modproxy hostname should not be empty if it's configured to be used
      watchdog(
          'ITrInno RestAPI',
          'RestAPI Modproxy URL is empty. Please configure it.',
          array(),
          WATCHDOG_ERROR,
          l('RestAPI settings page', RESTAPI_CONFIG_PAGE_URL));

      return $service_url;
    }

    // separate the service url between scheme and the rest
    $service_url_parts = explode('://', $service_url);
    // disregard the scheme and separate the rest by slashes
    $service_url_parts = explode('/', $service_url_parts[1], 2);
    // replace the the hostname with modproxy scheme + hostname
    return $modproxy_hostname . '/' . $service_url_parts[1];
  }

}

?>