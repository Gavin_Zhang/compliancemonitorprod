<?php

/**
 * Rest API interface
 */
class RestApiFactory {

  /**
   * Create a new Rest API object according to the "type" property in the
   * restAPI properties file.
   * 
   * @return object A new service object. 
   */
  function CreateInstance() {
    $classname = RestApiFactory::getClassName();
    include_once "classes/$classname.php";

    if (!class_exists($classname)) {
      trigger_error('Class File Not found', E_USER_ERROR);
      return NULL;
    }

    @$obj = new $classname;

    return $obj;
  }

  function getClassName() {
    return RestApiFactory::getProperties('type');
  }

  function getProperties($param = NULL) {
    $name = drupal_get_path('module', 'itrinno_restapi') . '/services/restAPI.properties';
    $fp = fopen($name, 'rb');

    while (!feof($fp)) {
      $asd = fgets($fp);
      if ($asd{0} != ';') {
        $Parameters = explode(':', $asd);
        $properties[$Parameters[0]] = $Parameters[1];
      }
    }
    if ($param != NULL) {
      return $properties[$param];
    }
    else {
      return $properties;
    }
  }

}

?>