/**
 * @file
 * Automatically posts the credentials on the CAS autologin form.
 */

(function($) {
  Drupal.behaviors.casAutoLoginForm = {
    attach: function(context, settings) {
      // submit the form once the loading gif has shown up  
      $(window).load(function() {
        $('form#-cas-autologin-form', context).trigger('submit');
      });
    }
  }
})(jQuery);