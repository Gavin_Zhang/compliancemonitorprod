/**
 * @file
 * CAS admin page related functionalities.
 */

(function($) {
  Drupal.behaviors.casAccessWithMixedModeWarning = {
    attach: function(context, settings) {

      /**
       * Check Redirection Settings when using CAS Mixed Mode.
       */
      var checkRedirectionSettings = function () {
        // set some "constants" so they can be more easily referred to
        var cas_login_form_values = {
          'CAS_NO_LINK': '0',
          'CAS_ADD_LINK': '1',
          'CAS_MAKE_DEFAULT': '2',
          'CAS_MIXED_MODE': '3'
        }
        var cas_access_values = {
          'SPECIFIC_PAGES': '0',
          'ALL_BUT_LISTED': '1'
        }
        
        // 'specific pages' or 'all but specific pages' setting
        var cas_access = $('input[name="cas_access"]', context);
        // list of pages for which to alter the login form behavior
        // (blacklist or whitelist depending on cas_access)
        var cas_pages = $('textarea[name="cas_pages"]', context);
        // mixed_mode or some standard CAS login mode
        var cas_login_mode_option = $('input[name="cas_login_form"]', context).filter(':checked');

        if (cas_login_form_values['CAS_MIXED_MODE'] == cas_login_mode_option.val()) {
          if ((cas_access_values['SPECIFIC_PAGES'] == cas_access.filter(':checked').val()) && ($.trim(cas_pages.val()).length) == 0) {
            return false;
          }
        }
        
        return true;
      }

      // action taken when 'mixed mode' is chosen
      $('input[name="cas_login_form"]', context).change(function() {
        if (!checkRedirectionSettings()) {
          // display the warning message
          $('div#cas-access-default-value-message').fadeIn(350);
        }
        else {
          // remove any previously set error messages
          $('div#cas-access-default-value-message').fadeOut(350);
        }
      });
      
      // action taken when specific pages is chosen
      $('input[name="cas_access"]', context).change(function() {
        if (!checkRedirectionSettings()) {
          // display the warning message
          $('div#cas-login-mode-option-message').fadeIn(350);
        }
        else {
          // remove any previously set error messages
          $('div#cas-login-mode-option-message').fadeOut(350);
        }
      });
    }
  }
  
  Drupal.behaviors.dismissMessageLink = {
    attach: function(context, settings) {
      $('a.dismiss-message', context).click(function(e) {
        e.preventDefault();
        $(this).parent('div.messages').fadeOut(350);
      });
    }
  }
})(jQuery);