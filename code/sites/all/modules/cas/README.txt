Contents
========

This README file is divided in the following sections:
  - Introduction
  - Requirements
  - Installation
  - Configuration & Workflow
  - 

Introduction
============

Central Authentication Services (CAS) is a commonly used Single Sign-On
protocol used by many universities and large organizations. For a brief
introduction, please see the Jasig website: http://www.jasig.org/cas/about

The Drupal CAS project has two modules:

 * CAS:
     Drupal acts as a CAS client, allowing users to authenticate with a
     separate single sign-on CAS server.

 * CAS Server:
     Drupal acts as a CAS server.

Do NOT enable both modules at the same time, as it may lead to unpredictable
results.

The following README.txt covers the CAS module only and the alterations that
have been made for the ITrInno platform.

Requirements
============
PHP 5 with the following modules:
  curl, openssl, dom, zlib, and xml
phpCAS version 1.0.0 or later.

Installation
============

* Place the cas folder in your Drupal modules directory. It comes with the
  phpCAS library modified version that is required in case CAS Mixed Mode is
  used (see details in the "ITrInno Specific Notes" section).

* Go to Administer > Modules and enable this module.

* Go to Administer > Configuration > People > CAS to configure the CAS module.
  Depending on where and how you installed the phpCAS library, you may need
  to configure the path to CAS.php. The current library version will be
  displayed if the library is found.

Configuration & Workflow
========================

For the purposes of this example, assume the following configuration:
 * https://auth.example.com/cas - Your organization's CAS server
 * http://site.example.com/ - This Drupal site using the CAS module

Configure the CAS module:
 * Log in to the Drupal site and navigate to Admin > Configuration > People >
   Central Authentication Services.
 * Point the CAS module at the CAS server:
     - Hostname: auth.example.com
     - Port: 443
     - URI: /cas
 * Configure user accounts:
     - Decide if you want to automatically create Drupal user accounts for each
       CAS-authenticated user. If you leave this option deselected, you will
       have to manually add a paired Drupal account for every one of your users
       in advance.
     - Hide the Drupal password field if your users will never know (or need to
       know) their Drupal password.
 * Configure the login form(s):
     - There are four ways that a user can start the CAS authentication
       process:
         1. Visit http://site.example.com/cas
              This option is always available and is good for embedding a text
              "Login" link in your theme. (See the note to themers below).

         2. Click on a CAS Login menu link.
              The menu item is disabled by default, but may be enabled in
              Admin > Structure > Menus. You should find the link in the
              "Navigation" menu.

         3. Select the CAS login option on the Drupal login form.
              The CAS login option needs to be added to the login form in the
              CAS settings.

         4. Use the CAS login block.
              The CAS login block may be enabled in Admin > Structure > Blocks.

Note to Themers
===============

You may want to include a text CAS "Login" link in your theme. If you simply
link to "/cas", you will find that your users are redirected to the site
frontpage after they are authenticated. To redirect your users to the page
they were previously on, instead use:

  <?php
    print l(t('Login'), 'cas', array('query' => drupal_get_destination()));
  ?>

Upgrading from 6.x-2.x / Associating CAS usernames with Drupal users
=====================================================================

The following options have been depreciated:
* "Is Drupal also the CAS user repository?"
* "If Drupal is not the user repository, should CAS hijack users with the same name?"

The CAS module uses a lookup table (cas_user) to associate CAS usernames with
their corresponding Drupal user ids. The depreciated options bypassed this
lookup table and let users log in if their CAS username matched their Drupal
name. The update.php script has automatically inserted entries into the lookup
table so that your users will continue to be able to log in as before.

You can see the results of the update script and manage CAS usernames on the
"Administration >> People" (admin/people) page. A new column displays CAS
usernames, and the bulk operations drop-down includes options for rapidly
creating and removing CAS usernames. The "Create CAS username" option will
assign a CAS username to each selected account that matches their Drupal name.
The "Remove CAS usernames" option will remove all CAS usernames from the
selected accounts.

API Changes Since 6.x-2.x
=========================
The hooks hook_auth_name() and hook_auth_filter() were combined and renamed
to hook_cas_user_alter(). See cas.api.php.

ITrInno Specific Notes
======================

1) CAS Login Modes
By default CAS implements 3 login modes - described below - that will, at some
point, redirect the user to the CAS server login page. As an addition to those
modes CAS Mixed Mode has been introduced.

These modes work as follows:

- Login through Drupal using CAS as a fallback (mixed mode): Uses Drupal as the
  user authentication backend and fallbacks to CAS transparently when needed.
  For further information on CAS Mixed Mode please refer to the next section.
- Do not add link to login forms: Uses Drupal as the user authentication backend
  only, never resorting to the CAS server as an authentication mechanism.
- Add link to login forms: Adds a link that allows users to choose between
  logging in through Drupal or CAS (Drupal is default).
- Make CAS login default on login forms: Same as above, except CAS is default.

2) CAS Mixed Mode

As an addition to the CAS community module features ITrInno CAS also implements
what has been called "CAS Mixed Mode". When in mixed mode user credentials will
be first checked against Drupal and, if authentication is not possible, they'll
be checked against CAS without having the user to retype or resubmit his/hers
login credentials.

2.1) Security Concerns

The CAS module implements that by altering the submit functions for
"user_login" and "user_login_block" form IDs. That means that if CAS Mixed
Mode is chosen user credentials (user name and password) WILL BE SENT BACK TO
THE USER'S BROWSER IN ORDER TO BE AUTOMATICALLY RESUBMITTED TO THE CAS SERVER.
Because of that please make sure that the aforementioned login forms are ALWAYS
served in a secure fashion, that is, using HTTPS, so that user credentials
cannot be intercepted. For more information and/or resources please refer to
http://drupal.org/https-information.

If connection security cannot be ensured between the client's browser and Drupal
please consider using some login mode other than CAS Mixed Mode.

2.2) Redirection settings

CAS allows the admin user to set a list of URLs where CAS login is forced
(either by setting a "white list" where CAS login should be enforced or a "black
list" where it should never be used). These settings are available under 
"Redirection" in the configuration page (in admin/config/people/cas). CAS Mixed
Mode uses that setting to determine whether CAS will eventually be used as a
fallback in case it's not possible to authenticate the user with Drupal.

CAS Mixed Mode will NOT redirect the user to the CAS server login page to ask
for login credentials. Changes have been made to the phpCAS library that is
shipped with the module to allow this behavior, so updating this library is NOT
RECOMMENDED.

When in mixed mode if "Require CAS login for" is set to "specific pages" and the
"Specific pages" list of pages is left empty, a warning will be issued. That's
because with that combination of settings mixed mode would never be actually
applied to any of the login forms, thus being invalid.