<?php

/**
 * @file
 * CAS module settings UI.
 */

/**
 * Provides settings pages.
 */
function cas_admin_settings() {

  $form['library'] = array(
    '#type' => 'fieldset',
    '#title' => t('Library (phpCAS)'),
    '#collapsible' => TRUE,
  );
  if (module_exists('libraries')) {
    // If Libraries API is enabled, print an information item.
    $form['library']['cas_library_dir'] = array(
      '#type' => 'item',
      '#title' => t('Library directory'),
      '#value' => t('Using <a href="@url">Libraries API</a>.', array('@url' => 'http://drupal.org/project/libraries')),
      '#description' => t('Please ensure phpCAS is installed in a location compatible with Libraries API. For example, install phpCAS so that <em>sites/all/libraries/CAS/CAS.php</em> exists. See README.txt for more information.'),
      '#after_build' => array('cas_library_version_check'),
    );
  }
  else {
    // If Libraries API is not installed, display path settings.
    $form['library']['cas_library_dir'] = array(
      '#type' => 'textfield',
      '#title' => t('Library directory'),
      '#default_value' => variable_get('cas_library_dir', 'CAS'),
      '#description' => t('Specify the path to the directory the CAS.php file resides in. Leave blank to load cas from your phpinclude path.'),
      '#after_build' => array('cas_library_version_check'),
    );
  }


  $form['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('CAS Server'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  // set default values or get them from framework_1_5, if available
  $cas_server_default_values = array('cas_server' => 'signon.itrinno.com', 'cas_port' => '443', 'cas_uri' => 'casv20');
  if (class_exists('GlobalSettings')) {
    $cas_server_default_values = array(
      'cas_server' => GlobalSettings::getCASDomain(),
      'cas_port' => GlobalSettings::getCASPort(),
      'cas_uri' => GlobalSettings::getCASUri(),
    );
  }

  $form['server']['cas_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Hostname'),
    '#default_value' => variable_get('cas_server', $cas_server_default_values['cas_server']),
    '#size' => 30,
    // Hostnames can be 255 characters long.
    '#maxlength' => 255,
    '#description' => t('Hostname or IP Address of the CAS server.'),
  );

  $form['server']['cas_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('cas_port', $cas_server_default_values['cas_port']),
    '#size' => 5,
    // The maximum port number is 65536, 5 digits.
    '#maxlength' => 5,
    '#description' => t('443 is the standard SSL port. 8443 is the standard non-root port for Tomcat.'),
  );

  $form['server']['cas_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('URI'),
    '#default_value' => variable_get('cas_uri', $cas_server_default_values['cas_uri']),
    '#size' => 30,
    '#description' => t('If CAS is not at the root of the host, include a URI (e.g., /cas).'),
  );

  $form['login'] = array(
    '#type' => 'fieldset',
    '#title' => t('Login form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  
  $description_items = theme('item_list', array(
    'items' => array(
      t('Use Drupal as the user authentication backend and fallback to CAS transparently.'),
      t('Use Drupal only as the user authentication backend.'),
      t('Add a link to allow users to choose between logging in through Drupal or CAS (Drupal is default).'),
      t('Same as above, except CAS is default.'),
    )
  ));
  
  $cas_login_mode_option_user_notice = t(
      'CAS mixed mode will not work properly when <strong>!setting_name</strong> is set to <strong>!setting_value</strong> and <strong>!pages_list_name</strong> list is empty. (!dismiss_link)',
      array(
        '!setting_name' => 'Require CAS login for',
        '!setting_value' => 'specific pages',
        '!pages_list_name' => 'Specific pages',
        '!dismiss_link' => l(t('dismiss'), '#', array('html' => TRUE, 'attributes' => array('class' => 'dismiss-message'))),
      )
    );
  
  $form['login']['cas_login_form'] = array(
    '#type' => 'radios',
    '#title' => t('CAS login mode'),
    '#default_value' => variable_get('cas_login_form', CAS_MIXED_MODE),
    '#options' => array(
      CAS_MIXED_MODE => t('Login through Drupal using CAS as a fallback (<strong>mixed mode</strong>)'),
      CAS_NO_LINK => t('Do not add link to login forms'),
      CAS_ADD_LINK => t('Add link to login forms'),
      CAS_MAKE_DEFAULT => t('Make CAS login default on login forms'),
    ),
    '#description' => t('The modes above will operate in the following ways:!description_items', array('!description_items' => $description_items)),
    '#prefix' => '<div id="cas-access-default-value-message" class="messages error" style="display: none;">' . $cas_login_mode_option_user_notice . '</div>',
  );

  $form['login']['cas_login_invite'] = array(
    '#type' => 'textfield',
    '#title' => t('CAS Login invitation'),
    '#default_value' => variable_get('cas_login_invite', CAS_LOGIN_INVITE_DEFAULT),
    '#description' => t('Message users will see to invite them to log in with CAS credentials.'),
  );

  $form['login']['cas_login_drupal_invite'] = array(
    '#type' => 'textfield',
    '#title' => t('Drupal login invitation'),
    '#default_value' => variable_get('cas_login_drupal_invite', CAS_LOGIN_DRUPAL_INVITE_DEFAULT),
    '#description' => t('Message users will see to invite them to log in with Drupal credentials.'),
  );

  $form['login']['cas_login_redir_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirection notification message'),
    '#default_value' => variable_get('cas_login_redir_message', CAS_LOGIN_REDIR_MESSAGE),
    '#description' => t('Message users see at the top of the CAS login form to warn them that they are being redirected to the CAS server.'),
  );

  // Setting for message displayed to user upon successfull login
  $form['login']['cas_login_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Successful login message'),
    '#default_value' => variable_get('cas_login_message', 'Logged in via CAS as %cas_user_identifier.'),
    '#description' => t('Message displayed when a user logs in successfully. <em>%cas_user_identifier</em> will be replaced with the user identifier value.'),
  );
  
  // which value should be displayed in user login message as the user identifier?
  $form['login']['cas_user_login_message_identifier'] = array(
    '#type' => 'select',
    '#title' => t('User identifier to be displayed in login message'),
    '#options' => array(
      'name' => t('User name'),
      'mail' => t('E-mail'),
    ),
    '#default_value' => variable_get('cas_user_login_message_identifier', 'name'),
    '#description' => t('Defines the user identifier value to be shown in the successful login message.'),
  );
  
  // Setting to show the password expiration
  $form['login']['cas_password_expiration_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Password expiration message'),
    '#default_value' => variable_get('cas_password_expiration_message', t('Your password will expire in %days_to_expiration day(s)')),
    '#size' => 62,
    '#maxlength' => 150,
    '#description' => t("Password expiration message shown when the user logs in. <em>%days_to_expiration</em> will be replaced with the days left to password expiration."),
  );
  
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('User accounts'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['account']['cas_user_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically create Drupal accounts'),
    '#default_value' => variable_get('cas_user_register', 1),
    '#description' => t('Whether a Drupal account is automatically created the first time a CAS user logs into the site. If disabled, you will need to pre-register Drupal accounts for authorized users.'),
  );

  $form['account']['cas_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address'),
    '#field_prefix' => t('username@'),
    '#default_value' => variable_get('cas_domain', 'jnj.com'),
    '#size' => 30,
    // Hostnames can be 255 characters long.
    '#maxlength' => 255,
    '#description' => t("If provided, automatically generate each new user's e-mail address. If omitted, the e-mail field will not be populated. Other modules may be used to populate the e-mail field from CAS attributes or LDAP servers."),
  );

  // Taken from Drupal's User module.
  $roles = array_map('check_plain', user_roles(TRUE));
  $checkbox_authenticated = array(
    '#type' => 'checkbox',
    '#title' => $roles[DRUPAL_AUTHENTICATED_RID],
    '#default_value' => TRUE,
    '#disabled' => TRUE,
  );
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  $form['account']['cas_auto_assigned_role'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#description' => t('The selected roles will be automatically assigned to each CAS user on login. Use this to automatically give CAS users additional privileges or to identify CAS users to other modules.'),
    '#default_value' => variable_get('cas_auto_assigned_role', array()),
    '#options' => $roles,
    '#access' => user_access('administer permissions'),
    DRUPAL_AUTHENTICATED_RID => $checkbox_authenticated,
  );

  $form['account']['cas_hide_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Users cannot change email address'),
    '#default_value' => variable_get('cas_hide_email', 0),
    '#description' => t('Hide email address field on the edit user form.'),
  );

  $form['account']['cas_hide_password'] = array(
    '#type' => 'checkbox',
    '#title' => t('Users cannot change password'),
    '#default_value' => variable_get('cas_hide_password', 0),
    '#description' => t('Hide password field on the edit user form. This also removes the requirement to enter your current password before changing your e-mail address.'),
  );

  if (module_exists('persistent_login')) {
    $form['account']['cas_allow_rememberme'] = array(
      '#type' => 'checkbox',
      '#title' => t('Users can stay logged in between sessions'),
      '#default_value' => variable_get('cas_allow_rememberme', 0),
      '#description' => t('If Persistent Login is enabled, users can choose to stay logged in between browser sessions'),
      );
  }

  $form['pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Redirection'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['pages']['cas_check_first'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check with the CAS server to see if the user is already logged in?'),
    '#default_value' => variable_get('cas_check_first', 0),
    '#description' => t('This implements the <a href="@cas-gateway">Gateway feature</a> of the CAS Protocol. The check is only performed the first time a user visits your site, so that the local drupal logout is still useful for site admins.', array('@cas-gateway' => 'https://wiki.jasig.org/display/CAS/gateway')),
  );
  
  $form['pages']['cas_access'] = array(
    '#type' => 'radios',
    '#title' => t('Require CAS login for'),
    '#default_value' => variable_get('cas_access', 0),
    '#options' => array(t('specific pages'), t('all pages except specific pages')),
    '#prefix' => '<div id="cas-login-mode-option-message" class="messages error" style="display: none;">' . $cas_login_mode_option_user_notice . '</div>',
  );

  $form['pages']['cas_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Specific pages'),
    '#default_value' => variable_get('cas_pages', _cas_pages_default_value()),
    '#cols' => 40,
    '#rows' => 5,
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are '<em>blog</em>' for the blog page and '<em>blog/*</em>' for every personal blog. '<em>&lt;front&gt;</em>' is the front page."),
  );

  $form['pages']['cas_exclude'] = array(
    '#type' => 'textarea',
    '#title' => t('Excluded Pages'),
    '#default_value' => variable_get('cas_exclude', CAS_EXCLUDE),
    '#cols' => 40,
    '#rows' => 5,
    '#description' => t("Indicates which pages will be ignored (no login checks). Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are '<em>blog</em>' for the blog page and '<em>blog/*</em>' for every personal blog. '<em>&lt;front&gt;</em>' is the front page."),
  );


  $form['misc'] = array(
    '#type' => 'fieldset',
    '#title' => t('Login/Logout Destinations'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Settings for redirection upon first login
  $form['misc']['cas_login_destination'] = array(
    '#type' => 'textfield',
    '#title' => t('Login destination'),
    '#default_value' => variable_get('cas_login_destination', ''),
    '#size' => 40,
    '#maxlength' => 255,
    '#description' => t("Drupal path or URL. Enter a destination if you want the user to be redirected to this page on CAS login. An example path is <em>blog</em> for the blog page, <em>&lt;front&gt;</em> for the front page, or <em>user</em> for the user's page."),
  );

  // Setting for page to return to after a CAS logout
  $form['misc']['cas_logout_destination'] = array(
    '#type' => 'textfield',
    '#title' => t('Logout destination'),
    '#default_value' => variable_get('cas_logout_destination', ''),
    '#size' => 40,
    '#maxlength' => 255,
    '#description' => t("Drupal path or URL. Enter a destination if you want a user to be directed to this page after logging out of CAS, or leave blank to direct users back to the previous page. An example path is <em>blog</em> for the blog page or <em>&lt;front&gt;</em> for the front page."),
  );
  
  // invoke cas_logout when logging out of drupal? (single-sign-out)
  $form['misc']['cas_single_sign_out'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do a single sign-out when user logs out of Drupal'),
    '#default_value' => variable_get('cas_single_sign_out', TRUE),
    '#description' => t('Invoke the CAS sign-out process when user logs out of Drupal.'),
  );

  $form['misc']['cas_changePasswordURL'] = array(
    '#type' => 'textfield',
    '#title' => t('Change password URL'),
    '#default_value' => variable_get('cas_changePasswordURL', ''),
    '#maxlength' => 255,
    '#description' => t('The URL users should use for changing their password.  Leave blank to use the standard Drupal page.'),
  );

  $form['misc']['cas_registerURL'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration URL'),
    '#default_value' => variable_get('cas_registerURL', ''),
    '#maxlength' => 255,
    '#description' => t('The URL users should use for changing registering.  Leave blank to use the standard Drupal page.'),
  );

  $form['cas_debugfile'] = array(
    '#type' => 'textfield',
    '#title' => t('CAS debug filename'),
    '#default_value' => variable_get('cas_debugfile', ''),
    '#maxlength' => 255,
    '#description' => t("Name for the CAS debug file. Leave empty if CAS debugging should be off.
        This setting depends on configuration of a !privatefs_link to work properly.
        <br /><strong>It is NOT recommended to leave this option enabled in production environments
        as it might capture sensitive data such as user names.</strong>",
        array('!privatefs_link' => l(t('Private file system path'), 'admin/config/media/file-system'))),
  );
  
  $form['#attached']['js'][] = drupal_get_path('module', 'cas') . '/cas.admin.js';

  return system_settings_form($form);
}

/**
 * Form validation handler for cas_admin_settings().
 */
function cas_admin_settings_validate($form, &$form_state) {
  $values = &$form_state['values'];
  
  // when informed, cas_domain should not include '@' (e-mail setting)
  if (!(empty($values['cas_domain'])) && !(valid_email_address('username@' . $values['cas_domain']))) {
    form_set_error('cas_domain', t('Please specify a valid suffix for E-mail address.'));
  }
  
  $values['cas_pages'] = trim($values['cas_pages']);
  // if cas mixed mode is chosen with cas_access set to "specific pages" and no
  // pages are listed, cas mixed mode wouldn't work as expected
  if (($values['cas_login_form'] == CAS_MIXED_MODE) && (($values['cas_access'] == 0) && (empty($values['cas_pages'])))) {
    $error_message = t(
      '<strong>CAS mixed mode</strong> will not work properly when <strong>%setting_name</strong> is set to <strong>%setting_value</strong> and <strong>%pages_list_name</strong> list is empty.',
      array(
        '%setting_name' => 'Require CAS login for',
        '%setting_value' => 'specific pages',
        '%pages_list_name' => 'Specific pages',
      )
    );
    form_set_error('cas_access', $error_message);
  }
}

/**
 * Checks that the library is installed in the location specified by loading the
 * class and extracting the version.
 *
 * @param $element
 *   The form element containing the "library" fieldset.
 * @param $form_state
 *   An array containing the form's state information.
 *
 * @return
 *   The modified form element containing the "library" fieldset.
 */
function cas_library_version_check($element, &$form_state) {
  $path = module_exists('libraries') ? NULL : $element['#value'];
  // Suppress errors if phpCAS cannot be loaded.
  if ($version = @cas_phpcas_load($path)) {
    $element['#suffix'] = '<div class="ok messages">' . t('phpCAS version %version successfully loaded.', array('%version' => $version)) . '</div>';
  }
  else {
    $element['#suffix'] = '<div class="error messages">' . t('The phpCAS library was not found or could not be loaded.') . '</div>';
  }
  return $element;
}
