<div class="info-mess">
	<?php print t('病棟名を選び、年月を入力した後、参照ボタンよりCSVファイルデータをインポートしてください。'); ?></div><div id="form-view">
		<div class="block themed-block" id="form-view-filter">
		<div class="panel-content">
			<div class="left hospital-field">
				<span>病院名：</span>
				<?php print drupal_render($form['hospital']); ?>
			</div>
			<div class="left ward-field">
				<span>病棟名：</span>
				<?php print drupal_render($form['ward']); ?>
			</div>
			<div class="left first-item">
				<span>年月：<span class="small-text">例：2011/09</span></span>
				<div id="edit-date-wrapper">
					<?php print drupal_render($form['date']); ?>
				</div>
			</div>
			<div class="left csv-upload">
				<span>CSVファイル：</span>
				<?php print drupal_render($form['import']); ?>
			</div>
			<div class="left ward-annually-data-import-upload">
				<?php print drupal_render($form['upload']); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<?php print drupal_render_children($form); ?>
</div>