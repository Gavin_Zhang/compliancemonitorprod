<?php print drupal_render($form['mail_wico_notice']); ?>
<div class="info-mess"><span class="form-required">*</span>は入力必須項目です</div>
<div class="form-item">
	<label>WEBサイト版コンプライアンスモニターを使用していますか？<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<?php print drupal_render($form['choose']); ?>
	<div class="clear"></div>
</div>
<div id="question-type-yes" class="form-item <?php echo $_SESSION['question_type_yes_class'];?>">
	<label>お問い合わせ内容：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<?php print drupal_render($form['question_type_yes']); ?>
	<div class="clear"></div>
</div>
<div id="question-type-no" class="form-item <?php echo $_SESSION['question_type_no_class'];?>">
	<label>お問い合わせ内容：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<?php print drupal_render($form['question_type_no']); ?>
	<div class="clear"></div>
</div>
<div class="form-item">
	<label>お名前（漢字）：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<span class="field-prefix"><strong>姓</strong></span>
	<?php print drupal_render($form['first_name']); ?>
	<span class="field-prefix"><strong>名</strong></span>
	<?php print drupal_render($form['last_name']); ?>
	<div class="clear"></div>
</div>
<div class="form-item">
	<label>お名前（カタカナ）：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<span class="field-prefix"><strong>セイ</strong></span>
	<?php print drupal_render($form['first_name_phonetic']); ?>
	<span class="field-prefix"><strong>メイ</strong></span>
	<?php print drupal_render($form['last_name_phonetic']); ?>
	<div class="clear"></div>
</div>
<div class="form-item">
	<label>勤務先名称：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<?php print drupal_render($form['employer_name']); ?>
	<div class="clear"></div>
</div>
<div class="form-item">
	<label>部署名：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<?php print drupal_render($form['department_name']); ?>
	<div class="clear"></div>
</div>
<div class="form-item">
	<label>職種：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<?php print drupal_render($form['positions']); ?>
	<div class="clear"></div>
</div>
<div class="form-item">
	<label>勤務先住所：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<span class="field-prefix zip-field-prefix"><strong>郵便番号</strong></span>
	<?php print drupal_render($form['zip_first']); ?>
	<span class="dash">-</span>
	<?php print drupal_render($form['zip_second']); ?>
	<span class="des same-line-des">半角でご入力ください。</span><span class="des new-line-des address-des"><strong>住所</strong> ※都道府県名から入力してください。 （例：東京都千代田区西神田3丁目5-2）</span>
	<?php print drupal_render($form['address']); ?>
</div>
<div class="phone-info form-item">
	<label>ご連絡先電話番号（半角）：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<span class="des phone-des">※市外局番から半角でご入力ください。（例：03-1234-5678）</span>
	<div class="phone-block">
		<?php print drupal_render($form['phone_1']); ?>
		<span class="dash phone-dash phone-dash-1">-</span>
		<?php print drupal_render($form['phone_2']); ?>
		<span class="dash phone-dash">-</span>
		<?php print drupal_render($form['phone_3']); ?>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="form-item email-item">
	<label>メールアドレス（半角）：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<span class="des">半角でご入力ください。</span>
	<?php print drupal_render($form['email_1']); ?>
	<span class="des new-line-des">確認の為再度ご入力ください。</span>
	<?php print drupal_render($form['email_2']); ?>
</div>
<!--  
<div class="form-item">
	<label>お問い合わせの標題：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<?php print drupal_render($form['title']); ?>
</div>
-->
<div class="form-item">
	<label>お問い合わせの内容：<span title="<?php print t('This field is required.'); ?>" class="form-required">*</span></label>
	<?php print drupal_render($form['message']); ?>
</div>
<div class="contact-button-block">
	<?php print drupal_render($form['reset']) ?>
	<?php print drupal_render($form['form_submit']) ?>
</div>

<?php print drupal_render_children($form);?>