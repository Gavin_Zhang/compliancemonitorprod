<div class="info-mess">
	<?php print t('参照ボタンよりCSVファイルデータをインポートしてください。'); ?></div><div id="form-view">
		<div class="block themed-block" id="form-view-filter">
		<div class="panel-content">
			<div class="left csv-upload">
				<span>CSVファイル：</span>
				<?php print drupal_render($form['import']); ?>
			</div>
			<div class="left">
				<?php print drupal_render($form['upload']); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<?php print drupal_render_children($form); ?>
</div>