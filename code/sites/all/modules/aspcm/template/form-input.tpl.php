<div id="form-input">
	<div class="block-goback" id="top-link-back"><?php print drupal_render($form['Return']); ?></div>
	<div class="block themed-block" id="form-main-panel">
		<div class="panel-content">
			<div class="left hospital-field">
				<span>病院名：</span>
				<?php print drupal_render($form['hospital']); ?>
			</div>
			<div class="left ward-field">
				<span>病棟名：</span>
			    <?php print drupal_render($form['ward']); ?>
			</div>
			<div class="left first-item">
				<span>年月：<span class="small-text">例：2011/09</span></span>
				<div id="edit-date-wrapper">
					<?php print drupal_render($form['date']); ?>
					<!--<a class="lnk-datepicker"></a>-->
				</div>
			</div>
	 <?php
	   $user_obj = get_user_profile();
       if ( $user_obj->dishos || $user_obj->disward || $user_obj->uid == 1) {
         $display = "";
       }
       else {
         $display = "none";
       }
	 ?>
			<div class="left last-item" style="display: <?php print $display?>">
				<!--<a class="lnk-empty-butt" href="#">Search</a>-->
				<?php print drupal_render($form['Search']); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>


	<?php if (isset($message)){ ?><div class="messages error"><?php print $message; ?></div><?php } ?>
	<div class="form-set" style="display:<?php if (empty($form['ward']['#options'])) {
	        print 'none';
	      }
	      else {
	        print 'block';
	      } ?>;">

	
		<h3>払い出し本数／使用量</h3>
		<?php if(isset($form['product'])){ ?>
			<div id="product-frm-input">
				<?php print drupal_render($form['product']); ?>
				<div class="clear"></div>
			</div>
		<?php }else{  ?>
		
			<p class="error">この病棟には製品がありません。</p>
			
		<?php } ?>

		<h3>患者数・スタッフ数</h3>

		<div id="sev-level-block">
			<div class="tabled-input">
				<?php print drupal_render($form['total_patient']); ?>
				<?php $flag = unserialize($form['estimated']['nid']['#value']);?>
				<div class ="<?php print !empty($flag)?'':'hidden';?>">
					 <?php  print drupal_render($form['severity_level']);?>
					<div class="form-comment info-mess actual-comment">月間延べ人数データが取得できない場合には「数日間の実績を予測し入力」を選択し月間人数を算出してください。（遵守率を算出しない場合には、入力は不要です。）</div>
					<div class="form-comment info-mess estimated-comment">重症度レベル別患者数を数日間入力すると、月間延べ人数を予測して算出することができます。</div>
					<div class="clear"></div>
				</div>
				
			</div>
			
			<div  class="estimated-comment error">このフィールドに数値を入力すると、『月間人数を直接入力』で入力したデータは上書きされますのでご注意ください。</div>
			
			<div id="estimated" class ="<?php print !empty($flag)?'':'hidden';?>">
			<h5 style="display:none;" class="print-headers">数日間の実績を入力し予測:</h5>
			<table cellspacing="2" cellpadding="0" border="0" class="form-table">
            <tbody><tr name="rsprd"><th>&nbsp;</th><th>1日目</th><th>2日目</th>
            <th>3日目</th><th>4日目</th><th>5日目</th><th>6日目</th><th>7日目</th>
            <th class="bg_total_th">合計</th><th class="bg_total_th">指定日数</th><th class="bg_total_th">月間人数<br/>(予測)</th></tr>
            <?php 
            //var_dump($form['estimated']['nid']['#value']);die();
            foreach (unserialize($form['estimated']['nid']['#value']) as $key => $value) {
              $td[] = array();
              for($i = 0 ; $i < 11 ; $i++) {
                $name = 'estimated_' . $value . '_' . $i ;
                $td[$i] = drupal_render($form['estimated'][$name]);
             }
              print "<tr class=\"level5\"> 
                <td class=\"sev-level-name\">".$td['0']."</td>
                <td>".$td['1']."</td>
                <td>".$td['2']."</td>
                <td>".$td['3']."</td>
                <td>".$td['4']."</td>
                <td>".$td['5']."</td>
                <td>".$td['6']."</td>
                <td>".$td['7']."</td>
                <td class='bg-total'>".$td['8']."</td>
                <td class='bg-total'>".$td['9']."</td>
                <td class='bg-total'>".$td['10']."</td>
                </tr> ";
            }
            ?>
            <tr class="level-total">
            <td class="sev-level-num tar b">合計</td>
            <td><input name="total_1" type="text" value="" disabled="disabled" class="small form-text estimated-day-total" /></td>
            <td><input name="total_2" type="text" value="" disabled="disabled" class="small form-text estimated-day-total" /></td>
            <td><input name="total_3" type="text" value="" disabled="disabled" class="small form-text estimated-day-total" /></td>
            <td><input name="total_4" type="text" value="" disabled="disabled" class="small form-text estimated-day-total" /></td>
            <td><input name="total_5" type="text" value="" disabled="disabled" class="small form-text estimated-day-total" /></td>
            <td><input name="total_6" type="text" value="" disabled="disabled" class="small form-text estimated-day-total" /></td>
            <td><input name="total_7" type="text" value="" disabled="disabled" class="small form-text estimated-day-total" /></td>
            <td class="total_last"><input name="total_8" type="text" value="" disabled="disabled" class="small form-text estimated-total-total" /> 人</td>
            <td class="total_last"><input name="total_9" type="text" value="" disabled="disabled" class="small form-text estimated-count-total" /> 日</td>
            <td class="total_last"><input name="total_10" type="text" value="" disabled="disabled" class="small form-text estimated-avg-total" /> 人</td>
            </tr>
            </tbody>
            </table>

		</div>
		<div id="actual" style="display:none;" class ="<?php print !empty($flag)?'':'hidden';?>">
		<h5 style="display:none;" class="print-headers">月間人数を直接入力:</h5>
				<div class="tabled-input">
					<?php print drupal_render($form['actual']); ?>
				</div>
		</div>
		<?php // } ?>
			<div class="tabled-input">
				<?php print drupal_render($form['Mandays']); ?>		
			</div>		
		<div id="disease-block" style="display:
			<?php 
				if(isset($form['disease'])){
						print 'block'; 
				}else{
						print 'none';
				}
				?>	;">

			<h3 class="disease-first">病原体</h3>
			
			<div class="tabled-input" id="disease-list">
				<?php print drupal_render($form['disease']);?>
				<?php 
				
				if (!$user_obj->disward) {
				?>
				<!--<input id="add-disease" class="form-submit" type="button" value="病原体追加"/>-->
				<?php }?>
			</div>
			<!--<div id="popup-rate" style="display:none;"><?php print drupal_render($form['add_rate']); ?></div>-->
		
			</div>
		
			<!-- <h3>目標遵守率</h3> -->
			<h3>遵守率</h3>
    	<div class="tabled-input">
				<p class="form-item" style="float:left; width:465px; margin-top:0;margin-bottom:1em"><?php print drupal_render($form['rate']); ?></p>
				<div class="form-comment info-mess" style="margin-top:3px; width:auto;"><?php print t('目標遵守率を変更したい場合には、病棟情報ページから変更してください。'); ?></div>
			</div>
			<div class="tabled-input" >
			  <p class="form-item" style="float:left; width:466px; margin-top:0;"><?php print drupal_render($form['compliance_rate']); ?><input type="button" value="<?php print t('Calculate'); ?>" id="calc-compliance-rate" /></p>
        <div class="form-comment info-mess" style="margin-top:3px; width:auto;"><?php print t('Click the button to calculate the value.'); ?></div>
			</div>
			<div class="clear"></div>
		<!-- 
		<div class="block-goback" id="bottom-link-back"><a href="#" class="link-back">入力一覧画面に戻る</a></div>
		 -->
	 <div class="block-goback" id="bottom-link-back"><?php print drupal_render($form['return_bottom']); ?></div>
	 
		<div id="panel-buttons" style="display: <?php print $display?>">
		    <?php print drupal_render($form['export_csv']);?>
			<?php
			  print drupal_render($form['Previous and Save']); 
			?>
			<?php print drupal_render($form['Save']); ?>
			<?php 
			  print drupal_render($form['Next and Save']); 
			?>
		</div>

		<div id="popup-confirmation">
			<p>
			保存ボタンを押していない場合、入力した情報は保存されません。入力一覧に戻りますか？
			</p>
		</div>
		<div id="hidden-buttons">
			<?php print drupal_render($form['save_and_redirect']); ?>
			<?php print drupal_render($form['count_days']); ?>
		</div>

	
	</div>
	<?php print drupal_render_children($form); ?>
</div>
</div>
 <script type="text/javascript">
         function calc_clicked() {
           
           $.post("get/compliance-rate", 
                 $("#aspcm-ward-monthly-data-input-form").serialize(),
                 function(data){
            alert("Data Loaded: " + data);
               });
         }
         </script>         
