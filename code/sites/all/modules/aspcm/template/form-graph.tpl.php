<div id="form-input-block">
	<div id="form-input">
	<div  class="info-mess"><?php print '表示させたい病棟を選び、「グラフを表示」ボタンをクリックしてください。'; ?></div>
		<div id="form-main-panel" class="block themed-block">
				<div class="panel-content">
					<div class="left hospital-field">
						<span>病院名：</span>
						<?php print drupal_render($form['hospital']); ?>
					</div>
					<div class="left ward-field">
						<span>病棟名：<span class="fs-small">病棟名を複数選択するには、Ctrlボタンを押しながら病棟を選択してください。</span></span>
						<?php print drupal_render($form['ward']); ?>
						<ul class="select-wards">
							<li><a href="#" class="lnk-arrow lnk-empty-butt form-submit" id="select-all-wards">選択</a></li>
							<li><a href="#" class="lnk-arrow lnk-empty-butt form-submit" id="reset-all-wards">リセット</a></li>
						</ul>
                        <div class="ward-selected">
                        <?php print drupal_render($form['ward2']); ?>
                        </div>
						<div class="clear"></div>
						<!-- @auther lxl  -->

					</div>
					<div class="left period-field">
                                            <span>表示させる期間を選びます：</span>	
                                            <div class="clear"></div>
                                            <?php print drupal_render($form['horizontal_start']);?>
                                            <?php print drupal_render($form['horizontal_end']);?>
                                            <div class="clear"></div>
					</div>
                                    <!-- add by yueshuo -->
                                    <div class="clear"></div>
									<br>
                                    <div>
									<strong>表示させるグラフを選びます：</strong>
                                        <?php print drupal_render($form['presetRadios']); ?>
                                    </div>
                                    <!-- end -->
					<div class="btns-block">
							<?php print drupal_render($form['generate_main']); ?>
					</div>
					<div class="clear"></div>
				</div>
		</div>
		<div  class="info-mess"><?php print '確認したいグラフの種類を選んでください。'; ?></div>
		<div id="preset-panel" class="tabs" style='display: none'>
			<ul class="tabs primary">
                                <!-- modified by yueshuo-->
				<?php //print drupal_render($form['presetA']); ?>
				<?php //print drupal_render($form['presetB']); ?>
				<?php //print drupal_render($form['presetC']); ?>
                                <!-- end -->
			</ul>
		</div>

	</div>
	<div id="form-graph" <?php /*if((empty($form['hospital']['#default_value']))||(empty($form['ward']['#default_value']))) { print "style='display:none;'";}*/ ?>>
		<div>
			<div class="info-mess left"><?php print 'グラフを変更したい場合には、主軸、副軸および横軸ボタンをクリックし、表示させたい項目を選びます。'; ?></div>
			<div id="buttons-block" class="right">
				<div class="btn-long-container"><input type="submit" class="form-submit" value="設定を保存する" id="edit-save-confirm" name="op"></div>
				<div class="btn-long-container"><input type="submit" class="form-submit" value="設定をリセットする" id="edit-reset-confirm" name="op"></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div id="graph-tab-container">
			<div id="major-link-block"><a href="#">主軸</a></div>
			<div id="img-graph">
				<?php print drupal_render($form['image']); ?>
			</div>
			<div id="minor-link-block"><a href="#">副軸</a></div>
			<div id="horiz-link-block"><a href="#">横軸</a></div>
			<div id="graph-download"><?php print drupal_render($form['download']); ?></div>
			<div class="clear"></div>	
		</div>
                <!-- 主轴 -->
		<div id="popup-major" style="display:none;">
			<div class="messages error hidden"></div>
			<h4>棒グラフに表示する項目を選びます：</h4>
			<div id="major-axis" class="radio-box">
					<?php print drupal_render($form['major_value']); ?>
					<?php print drupal_render($form['major_wash_number']); ?>
					<?php print drupal_render($form['major_antisepic_volume']); ?>
					<?php print drupal_render($form['major_infection_rate']); ?>
					<div class="clear"></div>
			</div>
			<h4>手指衛生剤表示形式を選びます：</h4>
			<div id="major-axis-bars" class="radio-box">			
					<?php print drupal_render($form['major_single_bar']); ?>
			</div>
			<h4>グラフの主軸に表示する最大値を選びます：</h4>
			<div id="max-major-axis" class="radio-box">
				<?php print drupal_render($form['major_maximum_value']); ?>
				<?php print drupal_render($form['major_fixed_value']); ?>
			</div>
			<div class="buttons-block">
				<?php print drupal_render($form['generate_major']); ?>
			</div>
		</div>
		<!-- 副轴 -->
		<div id="popup-minor" style="display:none;">
			<div class="messages error hidden"></div>
			<h4>折れ線グラフに表示する項目を選びます：</h4>
			<div id="minor-axis" class="radio-box">
				<?php print drupal_render($form['minor_value']); ?>
				<?php print drupal_render($form['minor_wash_number']); ?>
				<?php print drupal_render($form['minor_antisepic_volume']); ?>
				<?php print drupal_render($form['minor_infection_rate']); ?>
				<div class="clear"></div>
			</div>
			<h4>手指衛生剤表示形式を選びます：</h4>
			<div id="minor-axis-bars" class="radio-box">			
					<?php print drupal_render($form['minor_single_line']); ?>
			</div>
			<h4>グラフの副軸に表示する最大値を選びます：</h4>
			<div id="max-minor-axis" class="radio-box">
				<?php print drupal_render($form['minor_maximum_value']); ?>
				<?php print drupal_render($form['minor_fixed_value']); ?>
			</div>
			<div class="buttons-block">
				<?php print drupal_render($form['generate_minor']); ?>
			</div>		
		</div>	
		<div id="popup-horiz" style="display:none;">
			<div class="messages error hidden"></div>
		  	<h4>期間または病棟のどちらかを選びます：</h4>
			<div id="horiz-axis" class="radio-box">
					<?php print drupal_render($form['horizontal_axis']);?>	
					<div class="horiz-unit">
						<label>表示月単位を選択します：</label>
						<?php print drupal_render($form['unit_of_period_displayed']);?>
					</div>
					<div class="clear"></div>
			</div>

			<div class="buttons-block">
				<?php print drupal_render($form['generate_horizontal']);?>
			</div>
		</div>
	</div>
  	<div id="popup-confirmation-save">
		<p>常に表示されるグラフとして設定しますか？</p>
	</div>
	<div id="popup-confirmation-reset">
		<p>これまで保存していたグラフの設定内容をリセットしますか？</p>
	</div>
	<div id="hidden-buttons">
		<?php print drupal_render($form['save']); ?>
		<?php print drupal_render($form['reset']); ?>
		<?php print drupal_render($form['graph_path']); ?>
	</div>
</div>
<?php print drupal_render_children($form);?>