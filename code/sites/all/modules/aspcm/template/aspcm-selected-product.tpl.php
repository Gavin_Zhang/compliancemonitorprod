<table id="selected-products-view">
	<thead>
		<tr><th><?php print t('Company Name'); ?></th><th><?php print t('Alcohol/Soap'); ?></th><th><?php print t('Product'); ?></th><th><?php print t('Default PerPush Volume'); ?></th><th><?php print t('Custom PerPush Volume'); ?></th></tr>
	</thead>
	<tbody>
		<?php $i=0; foreach ($product as $value) : ?>
		<tr class=<?php print $i%2 == 0 ? 'odd':'even'; ?>>
			<td><?php print $value['cname']; ?></td>
			<td><?php print $value['category_name']; ?></td>
			<td><?php print $value['pname']; ?></td>
			<td><?php if (!empty($value['perpush'])) print $value['perpush']. ' ml'; ?></td>
			<td><?php if (!empty($value['custom_perpush'])) print $value['custom_perpush']. ' ml'; ?></td>
		</tr>
		<?php $i++; ?>
		<?php endforeach; ?>
	</tbody>
</table>