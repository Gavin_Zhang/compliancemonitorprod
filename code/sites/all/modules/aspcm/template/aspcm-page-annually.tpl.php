<div id="form-view">
	<div class="info-mess">参照したい病棟名と、年度を入力し、検索ボタンをクリックしてください。</div>
	<div class="block themed-block" id="form-view-filter">
		<div class="panel-content">
			<div class="left hospital-field">
				<span>病院名：</span>
					<?php print drupal_render($form['hospital']); ?>
			</div>
			<div class="left ward-field">
				<span>病棟名：</span>
				<?php print drupal_render($form['ward']); ?>
			</div>
			<div class="left first-item">
				<span>年度：</span>
				<div id="edit-date-wrapper">
					<?php print drupal_render($form['date']); ?>
				</div>
			</div>
			<div class="left last-item">
			<?php print drupal_render($form['search']); ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="form-set">
	<?php if(isset($form['product'])) {
	  $startmonth = $form['startmonth']['#markup'];
	  ?>
		<div class="info-mess">入力したい月をクリックして入力画面に進んで下さい。ピンクにハイライトされたセルから進むことができます。</div>
		<div class="info-mess">各項目の詳細は、<span class="icon-plus"></span>ボタンをクリックすると確認できます。</div>
		<table cellspacing="2" cellpadding="0" border="0" class="form-table" id="first-table">
			<tr name="rsprd" class="head-row">
				<th class="first-column align-left">総使用量(ml)</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				  $year = $form['date']['#default_value'];
				  $mon = $result<10? '0'. $result:$result;
				  if ($i/12>1) $year = $form['date']['#default_value']+1;
				  if ($form['permission']['#value']) {
				    print "<th><a href=/ward-monthly-data-input?date=".$year.'/'.$mon."&old=".$form['date']['#default_value'].">".$result."月</a></th>";
				  }
				  else {
				    print "<th>".$result."月</th>";
				  }
				}
				?>
				<th class="last-total01 nowrap">合計使用量</th>
				<!--  <th class="last-total">合計本数</th> -->
			</tr>
			<?php
			$block_id = 0;
			foreach ($form['product'] as $category => $types) {
			  if(false === strpos($category,'#')){
			    ?>
			<tr class="product-volumn-total">
				<th class="align-left nowrap"><?php print $category;?>総使用量</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				  print '<td>'.number_format($form['product'][$category]['#product_category_volume']['#value'][$result]).'</td>';
				}
				?>
				<td class="bg-total">
				<?php
				  print number_format($form['product'][$category]['#product_category_volume']['#value']['volumes']);
				?>
				</td>
				<!-- 
				<td class="bg-total"><?php 	
				print number_format($form['product'][$category]['#product_category_volume']['#value']['bottles']);
				?>
				</td> -->
			</tr>
			<?php
			foreach ($types as $key => $value) {
			  if(false === strpos($key,'#')){
			    $block_id++; ?>
			<tr class="product-category1" id="block<?php print $block_id?>">
				<th class="align-left clickable collapsed"><a href="#"><?php print $key;?>総使用量</a>
				</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				  print '<td>'.number_format($value['#product_volume']['#value'][$result]).'</td>';
				}
				?>
				<td class="bg-total"><?php 	
				print number_format($value['#product_volume']['#value']['volumes']);
				?>
				</td><!-- 
				<td class="bg-total"><?php 
				print number_format($value['#product_volume']['#value']['bottles']);
				?>
				</td> -->
			</tr>
			<?php foreach ($value as $product_key=>$product_value) {
			  if(false === strpos($product_key,'#')){?>
			<tr class="product-category1 hidden sub-block<?php print $block_id?>">
				<th class="text-indent align-left"><?php print $product_value['#value']['title']?>
				</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				  print '<td class="last-total">'.number_format($product_value['#value'][$result]).'</td>';
				}
				?>
				<td class="bg-total"><?php 
				print number_format($product_value['#value']['volumes']);
				?>
				</td> <!-- 
				<td class="bg-total"><?php 
				print number_format($product_value['#value']['bottles']);
				?>
				</td>-->
			</tr>
			<?php } ?>
			<?php } //product loop end?>
			<?php } ?>
			<?php } //product types loop end?>
			<?php } ?>
			<?php } //product category loop end?>
		</table>
		<?php } ?>		
		<?php print drupal_render($form['severity_table']);?>

		<?php if(isset($form['product'])) {
		  $startmonth = $form['startmonth']['#markup'];
		  ?>
		<table cellspacing="2" cellpadding="0" border="0" class="form-table">
			<tr name="rsprd" class="head-row">
				<th class="first-column align-left">製品総使用回数(回)</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				  print "<th>".$result."月</th>";
				}
				?>
				<th class="last-total">合計</th>
			</tr>
			<tr id="block05">
				<th class="align-left clickable collapsed"><a href="#">1患者/day 総使用回数</a>
				</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				    print '<td>'.number_format($form['product']['#total_patient_times_total']['#value'][$result],1).'</td>';
				}
				?>
				<?php 
				 print '<td class="bg-total">'.number_format($form['product']['#total_patient_times_total']['#value']['total'],1).'</td>';
				?>
			</tr>
			<?php
			foreach ($form['product']['#total_patient_times'] as $key => $value) {
			  if(false === strpos($key,'#')){?>
			<tr class="hidden sub-block05">
				<th class="text-indent align-left"><?php print $value['#value']['title'];?>使用回数</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				    print '<td>'. number_format($value['#value'][$result],1).'</td>';
				}?>
				<?php
				   print '<td class="bg-total">'. number_format($value['#value']['total'],1).'</td>';
				?>
			</tr>
			<?php  } ?>
			<?php } //end loop?>
			<tr id="block06">
				<th class="align-left clickable collapsed"><a href="#">1スタッフ/day
						総使用回数</a>
				</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				   print '<td>'.number_format($form['product']['#total_staff_times_total']['#value'][$result],1).'</td>';
				}?>
				<?php 
				  print '<td class="bg-total">'.number_format($form['product']['#total_staff_times_total']['#value']['total'],1).'</td>';
				?>
			</tr>
			<?php
			if (isset($form['product']['#total_staff_times'])) {
			foreach ($form['product']['#total_staff_times'] as $key => $value) {
			  if(false === strpos($key,'#')){?>
			<tr class="hidden sub-block06">
				<th class="text-indent align-left"><?php print $value['#value']['title'];?>使用回数</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				  print '<td>'. number_format($value['#value'][$result],1).'</td>';
				}?>
				<?php 
				 print '<td class="bg-total">'.number_format($value['#value']['total'],1).'</td>';
				?>
			</tr>
			<?php  } ?>
			<?php }} //end loop?>
		</table>

		<table cellspacing="2" cellpadding="0" border="0" class="form-table">
			<tr name="rsprd" class="head-row">
				<th class="first-column align-left">製品総使用量(ml)</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				  print "<th>".$result."月</th>";
				}
				?>
				<th class="last-total">合計</th>
			</tr>
			<tr id="block07">
				<th class="align-left clickable collapsed"><a href="#">1患者/day 総使用量</a>
				</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				    print '<td>'.number_format($form['product']['#total_patient_volumes_total']['#value'][$result],1).'</td>';
				}?>
				<?php
				   print '<td class="bg-total">'.number_format($form['product']['#total_patient_volumes_total']['#value']['total'],1).'</td>';
				 ?>
			</tr>
			<?php
			foreach ($form['product']['#total_patient_volumes'] as $key => $value) {
			  if(false === strpos($key,'#')){?>
			<tr class="hidden sub-block07">
				<th class="text-indent align-left"><?php print $value['#value']['title'];?>使用量</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				   print '<td>'.number_format($value['#value'][$result],1).'</td>';
				}?>
				<?php
				 print '<td class="bg-total">'.number_format($value['#value']['total'],1).'</td>';
				 ?>
			</tr>
			<?php  } ?>
			<?php } //end loop?>
			<tr id="block08">
				<th class="align-left clickable collapsed"><a href="#">1スタッフ/day
						総使用量</a>
				</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				  print '<td>'.number_format($form['product']['#total_staff_volumes_total']['#value'][$result],1).'</td>';
				}?>
				<?php
				 print '<td class="bg-total">'.number_format($form['product']['#total_staff_volumes_total']['#value']['total'],1).'</td>';
				?>
			</tr>
			<?php
			if (isset($form['product']['#total_staff_volumes'])) {
			foreach ($form['product']['#total_staff_volumes'] as $key => $value) {
			  if(false === strpos($key,'#')){?>
			<tr class="hidden sub-block08">
				<th class="text-indent align-left"><?php print $value['#value']['title'];?>使用量</th>
				<?php
				for ($i = $startmonth; $i < 12 + $startmonth; $i++) {
				  $result = ($i%12 == 0)? 12 : $i%12;
				  print '<td>'.number_format($value['#value'][$result],1).'</td>';
				}?>
				<?php 
				print '<td class="bg-total">'.number_format($value['#value']['total'],1).'</td>';
				?>
			</tr>
			<?php  } ?>
			<?php }} //end loop?>
		</table>
		<?php } ?>		
		<?php print drupal_render($form['total_ward']);?>		
		<?php print drupal_render($form['disease']); ?>
	</div>
	<div id="hidden-buttons">
	<?php print drupal_render($form['rolename']);?>	
	</div>
</div>
		<?php print drupal_render_children($form);?>
