	<div id="form-input">
		<div  class="info-mess"><?php print '比較したい病院の条件を選択してください。'; ?></div>
		<div id="form-main-panel" class="block themed-block">
				<div class="panel-content">
					<div class="left last-item">
					    <?php if ($form['hospital']['#access']) : ?>
					    <h4>病院名：</h4>
					    <?php print drupal_render($form['hospital']); ?>
					    <?php endif; ?>
						<div class="clear"></div>
						<h4>主軸（棒グラフ）に表示する項目を選びます：</h4>
						<?php print drupal_render($form['major_value']); ?>
                                                <!-- add by yueshuo-->
						<h4>副軸（棒グラフ）に表示する項目を選びます：</h4>
                                                <?php print drupal_render($form['minor_value']);?>
                                                <!--<select id='minor_selected_flag' name='minor_selected_flag'>
                                                    <option value=1 <?php // echo (isset($_POST['minor_selected_flag']) && $_POST['minor_selected_flag'] == 1) ? 'selected' : '' ?>>手指衛生回数</option>
                                                    <option value=2 <?php // echo (isset($_POST['minor_selected_flag']) && $_POST['minor_selected_flag'] == 2) ? 'selected' : '' ?>>手指衛生剤使用量</option>
                                                    <option value=3 <?php // echo (isset($_POST['minor_selected_flag']) && $_POST['minor_selected_flag'] == 3) ? 'selected' : '' ?>>遵守率</option>
                                                    <option value=4 <?php // echo (isset($_POST['minor_selected_flag']) && $_POST['minor_selected_flag'] == 4) ? 'selected' : '' ?>>なし</option>
                                                </select>-->
                                                <!-- end -->
						<?php if (isset($form['own_government_dept']) && isset($form['own_clinical_dept'])) : ?>
              <h4>自施設の診療科を選びます：</h4>
              <span class="fs-small">診療科を複数選択するには、Ctrlボタンを押しながら診療科を選択してください。</span>
              <?php print drupal_render($form['own_government_dept']); ?>
              <?php print drupal_render($form['own_clinical_dept']); ?>
            <?php endif; ?>
            
						<h4>表示させる期間を選びます：</h4>
						<?php print drupal_render($form['horizontal_start']); ?>
						<?php print drupal_render($form['horizontal_end']); ?>
						
					</div>	
					<div class="left spec">
						<div class="line-spec">
						<!-- @author_lxl_start -->
						<?php 
							$is_checked_radio_yes = (isset($_SESSION['comparison_graph_radio']) &&$_SESSION['comparison_graph_radio']=='radio_yes')?'checked="checked"':'';
							$is_checked_radio_no = (isset($_SESSION['comparison_graph_radio']) &&$_SESSION['comparison_graph_radio']=='radio_no')?'checked="checked"':'';
							if(!$is_checked_radio_yes && !$is_checked_radio_no){
								$is_checked_radio_yes = 'checked="checked"';
							}
							
						?>
						<input type="radio" name="comparison_graph_radio" id="comparison_graph_radio_no" value="radio_no"  <?php echo $is_checked_radio_no;?>/> 全ての病院と比較する <br><br>
						<input type="radio" name="comparison_graph_radio" id="comparison_graph_radio_yes" value="radio_yes" <?php echo $is_checked_radio_yes;?>/> 条件を合わせる比較する
						<div class="" id="comparison_graph_grey"></div>
						<!-- @author_lxl_end -->
<!--							<div  class="info-mess">--><?php //print '全ての病院と比較する場合には、条件を選択せずにグラフ表示ボタンをクリックしてください。 '; ?><!--</div>-->
						<!-- @author_lxl_start -->
							<div class="mt20" id="mt20"></div>
							<?php
								unset($_SESSION['comparison_graph_radio']);
							?>
						<!-- @author_lxl_end -->
							<?php print drupal_render($form['hospital_beds']); ?>
							<div id="gov-dept"><?php print drupal_render($form['government_dept']); ?></div>
							<div id="clinical-dept"><?php print drupal_render($form['clinical_dept']); ?></div>					
							<?php print drupal_render($form['area']); ?>
							<?php print drupal_render($form['select_all_above_conditions']); ?>
							<div class="buttons-block">
								<?php print drupal_render($form['generate_main']); ?>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
		</div>
		<div  class="info-mess"><?php print 'グラフを変更したい場合には、主軸、副軸および横軸ボタンをクリックし、表示させたい項目を選びます。'; ?></div>
	</div>
	<div id="form-graph">
		<div id="major-link-block"><a href="#" class=" lnk-arrow">主軸</a></div>
		<div id="img-graph">
		<?php print drupal_render($form['image']); ?>
		<!--<img src="/comparison-chart" width="750" />-->
		</div>
		<div id="minor-link-block"><a href="#" class="lnk-arrow">副軸</a></div>
		<div id="horiz-link-block"><a href="#" class="lnk-arrow">横軸</a></div>
		<div id="graph-download"><?php print drupal_render($form['download']); ?></div>
		<div class="clear"></div>		
		
		<div id="popup-major" style="display:none;">
			<h4>棒グラフに表示する項目を選びます：</h4>
			<div id="major-axis">
				<div class="form-radios">
					<div id="edit-major-value-hand-washing-wrapper" class="form-item">
						<label class="option">
							<input type="radio" id="major_hand_washing" class="form-radio" value="hand_washing" name="major_value" disabled="disabled" /> 手指衛生回数
						</label>
					</div>
					<div id="edit-major-value-antiseptic-used-wrapper" class="form-item major_antiseptic_used">
						<label class="option">
							<input type="radio" id="major_antiseptic_used"  class="form-radio" value="antiseptic_used" name="major_value" disabled="disabled" /> 手指衛生剤使用量
						</label>
					</div>
					<div id="edit-major-value-compliance-rate-wrapper" class="form-item major_compliance_rate">
						<label class="option">
							<input type="radio" id="major_compliance_rate" class="form-radio" value="compliance_rate" name="major_value" disabled="disabled" /> 遵守率
						</label>
					</div>
				</div>
				<?php print drupal_render($form['major_wash_number']); ?>
				<?php print drupal_render($form['major_antisepic_volume']); ?>
				<div class="clear"></div>
			</div>	
			
			<h4>グラフの主軸に表示する最大値を選びます：</h4>
			<div id="max-major-axis">
				<?php print drupal_render($form['major_maximum_value']); ?>
				<?php print drupal_render($form['major_fixed_value']); ?>
			</div>
			<div class="buttons-block">
				<?php print drupal_render($form['generate_major']); ?>
			</div>
		</div>
		
		<div id="popup-minor" style="display:none;">
			<h4>折れ線グラフに表示する項目を選びます：</h4>
			<div id="minor-axis">
                            
                            <?php //add by yueshuo 201604?>
				<?php //print drupal_render($form['minor_value']); ?>
                            <div class="form-radios">
                                <div class="form-item form-type-radio form-item-minor-value">
                                    <input type="radio" id="minor_value_flag_0"  class="form-radio" disabled="disabled" /> 
                                    <label class="option">手指衛生回数 </label>
                                </div>
                                <div class="form-item form-type-radio form-item-minor-value">
                                    <input type="radio" id="minor_value_flag_1"  class="form-radio" disabled="disabled" /> 
                                    <label class="option">手指衛生剤使用量   </label>
                                </div>
                                <div class="form-item form-type-radio form-item-minor-value">
                                    <input type="radio" id="minor_value_flag_2"  class="form-radio" disabled="disabled" /> 
                                    <label class="option">遵守率 </label>
                                </div>
                                <div class="form-item form-type-radio form-item-minor-value">
                                    <input type="radio" id="minor_value_flag_3"  class="form-radio" disabled="disabled" /> 
                                    <label class="option">なし</label>
                                </div>
			</div>
                            <?php //end?>
                            
                	<?php print drupal_render($form['minor_wash_number']); ?>
				<?php print drupal_render($form['minor_antisepic_volume']); ?>
				<div class="clear"></div>
			</div>
			<h4>グラフの副軸に表示する最大値を選びます：</h4>
			<div id="max-minor-axis">
				<?php print drupal_render($form['minor_maximum_value']); ?>
				<?php print drupal_render($form['minor_fixed_value']); ?>
			</div>
			<div class="buttons-block">
				<?php print drupal_render($form['generate_minor']); ?>
			</div>		
		</div>	
		<div id="popup-horiz" style="display:none;">
		  	<h4>表示させる期間を選びます：</h4>
			<div id="horiz-axis">
					<div class="horiz-unit">
						<label>表示月単位を選択します：</label>
						<?php print drupal_render($form['unit_of_period_displayed']);?>
					</div>
					<div class="clear"></div>
			</div>
			<div class="buttons-block">
				<?php print drupal_render($form['generate_horizontal']); ?>
			</div>
		</div>		
	</div>
	<div id="hidden-buttons">
		<?php print drupal_render($form['graph_path']); ?>
	</div>
<?php print drupal_render_children($form);?>