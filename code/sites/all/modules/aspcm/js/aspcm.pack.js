/**
 * @file: aspcm.pack.js
 * @description: add sepcific JS function for supporting the project to achieve new features or fixing bugs
 */
jQuery(document).ready(function() {
	// Comparison graph page, searching my deparments by hospital name
	// use clicked the search icon, will triggle this event
	jQuery(".gcomp-search-autoc").bind('click', function(){
		departments_search();
		return false;
	});
	
	// Clinical-Goverment Hospitals (logic for Compare Graph Page  - Selectbox)
	// Switcher 
	jQuery("#edit-major-value").bind('change', function(){
		gcomp_change_my_depts_display(jQuery(this).val());
	});
});

function departments_search() {
	hospital = jQuery("#edit-hospital").val();
	
	kod ='';
	kod += "hospital=" + encodeURI(hospital) + "&ajax=1";
	kod += "&form_build_id="+jQuery("input:hidden[name='form_build_id']").val() + "&form_id=aspcm-comparison-graph-form";
	kod += "&major_value="+jQuery("#edit-major-value").val();
	//if(ew!=null){ kod += "&ward[]=" + ew[0];}
	
	if(jQuery("input:hidden[name='form_token']").length>0){ 
		kod += "&form_token="+jQuery("input:hidden[name='form_token']").val(); 
	}
	jQuery("#edit-own-government-dept").attr("disabled","disabled");
	jQuery("#edit-own-clinical-dept").attr("disabled","disabled");
	jQuery("#edit-hospital").css('background-position','100% -17px');
	
	jQuery.ajax({
			type : "POST",
			url : "/my/departments/ajax",
			dataType: "json",
			data: kod,
			success : function(matches) {
					if(matches['status']){
						jQuery('.form-item-own-government-dept').replaceWith(matches['data']['own_government_dept']);
						jQuery('.form-item-own-clinical-dept').replaceWith(matches['data']['own_clinical_dept']);
					}
					jQuery("#edit-hospital").val(hospital).css('background-position','100% 2px');
					
				return false;
			},
			complete : function(XMLHttpRequest, textStatus) {
			  return false;
			},
			error : function() {
			  // request error
			}
	});
}

function gcomp_change_my_depts_display(name){
	switch(name){
		case 'compliance_rate': 
			jQuery("#edit-own-government-dept-wrapper, #edit-own-government-dept").hide();
			jQuery("#edit-own-clinical-dept-wrapper, #edit-own-clinical-dept").show();	
			break;
		default: 
			jQuery("#edit-own-government-dept-wrapper, #edit-own-government-dept").show();
			jQuery("#edit-own-clinical-dept-wrapper, #edit-own-clinical-dept").hide();	
	}
}

