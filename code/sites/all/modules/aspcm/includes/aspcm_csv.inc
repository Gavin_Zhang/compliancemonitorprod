<?php 
// $Id$

/**
 * @file aspcm_csv.inc
 * generate a csv file
 */

module_load_include('inc', 'aspcm', 'includes/aspcm_safe_storage.class');
module_load_include('inc', 'aspcm', 'includes/aspcm_common');
module_load_include('inc', 'aspcm', 'includes/aspcm_ward_monthly_data_input');

/**
 * export the csv file
 * @param $form
 * @param $form_state
 */
function export_csv($form, $form_state) {
  $filename = get_export_data($form, $form_state, $filename);
  
  $ua = $_SERVER["HTTP_USER_AGENT"];
  
  drupal_add_http_header("Content-type", "text/csv; charset=Shift-JIS");
  if (preg_match("/MSIE/", $ua) || preg_match("/rv:11.0/", $ua)) {
    //$encoded_filename = mb_convert_encoding($filename, 'Shift-JIS', 'UTF-8');
    //$encoded_filename = iconv("UTF-8", "Shift-JIS", $filename);
    $encoded_filename = urlencode($filename);//ie6 hava encode issue so need to urlencode
    $encoded_filename = str_replace("+", "%20", $encoded_filename);
    drupal_add_http_header("Content-Disposition", "attachment;filename=\"" . $encoded_filename ."\"");
  }
  else {
    drupal_add_http_header("Content-Disposition", "attachment;filename=\"" . $filename ."\"");
  }
  
  if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) {
    drupal_add_http_header("Cache-Control", "private");
    drupal_add_http_header("Pragma", "private");
  }
  

  
  exit(); 
}

/**
 * get data generate the csv file
 * @param $form
 * @param $form_state
 * @return $filename
 */
function get_export_data($form, $form_state) {
  ob_clean();
  $filename = date('YmdHis')  .  ".csv";
  //get current month data
  $form_state = aspcm_input_form_load($form_state);  
  $data = $form_state['values'];
  $output .= get_export_header_data($data, $filename);
  $output .= get_export_product_data($data);
  $output .= get_export_patient_data($data);
  $output .= get_export_disease_data($data);

  //original code delete by Benny for ACMSUPP-17 20111122 
  //$output = iconv("UTF-8", "Shift-JIS", $output);
  
  //add by Benny for ACMSUPP-17 20111122
  //$output = iconv("UTF-8", "Windows-31J", $output);
  //ADD BY jaguar
  $format = variable_get('aspcm_code_format', 'UTF-8');
  $output = iconv("UTF-8", $format, $output);

  echo chr(0xEF).chr(0xBB).chr(0xBF).$output;
  return $filename;
}

/**
 * get hospital,ward,date
 * @param $data
 * @param &$filename
 * @return $output
 */
function get_export_header_data($data, &$filename) {
  //get hospital name by id
  $hospital = get_storage_array('aspcm_hospital', $data['hospital']);
  //get ward name by id
  $ward = get_storage_array('aspcm_ward', $data['ward']);
  $filename = $ward['title'] .  ".csv";  
  $output .= export_one_line(array('病院名', $hospital['title']));
  $output .= export_one_line(array('病棟名', $ward['title']));
 // $output .= export_one_line(array('年月', $data['date'])); 
  $output .= export_one_line(array('年月', date_to_string($data['date'])));  //new fix
  $output .= "\r\n";
  return $output;
}

/**
 * get product data
 * @param $data
 * @return $output
 */
function get_export_product_data($data) {
  //get all products
  
  $pro_list = get_products_by_hid($data['hospital'], $data['ward']);
  
  //Author:linda 2012/03/19
  //TODO: the unit of alcohol and soap is separated and can setup individually
  //      so show the alcohol and soap unit individually
  if (!empty($pro_list)) {
    $pro_list = get_products_order($pro_list);
	
    foreach ($pro_list as $cat => $product) {	
	  if ($cat == 'alcohol' && count($product) > 0) {
	    $output .= export_one_line(array('アルコール払い出し本数／使用量', t($data['alcoholunit'])));		
	  } 
	  else if ($cat == 'soap' && count($product) > 0) {
	    $output .= export_one_line(array('石けん払い出し本数／使用量', t($data['soapunit'])));    
	  }
	  
	  if (count($product) > 0) {
	    foreach ($product as $p) {
		  $name = 'product_'  .  $p['nid'];
		  if ($p['available'] == 1) {
		    if ($cat == 'alcohol' && $data['alcoholunit'] == 'bottle') {
			  $d = $data[$name] / $p['volume'];		  
			} 
			else if ($cat == 'soap' && $data['soapunit'] == 'bottle') {
			  $d = $data[$name] / $p['volume'];			  
			} 
			else {
			  $d = $data[$name];
			}
			
            $output .= export_one_line(array($p['title'], $d));			
		  }
		}
	  }
      
    }
  }
  $output .= "\r\n";
  return $output;
}

/**
 * get patient data
 * @param $data
 * @return $output
 */
function get_export_patient_data($data) {    
  $output .= export_one_line(array('患者数・スタッフ数', '人'));
  
  //original code delete by Benny for ACMSUPP-17 201124
  //$output .= export_one_line(array('述べ患者数', $data['total_patient']));
  //$output .= export_one_line(array('述べスタッフ数', $data['Mandays']));
  
  //add by Benny for ACMSUPP-17 20111124
  $output .= export_one_line(array('延べ患者数', $data['total_patient']));
  $output .= export_one_line(array('延べスタッフ数', $data['Mandays']));
  
  $output .= export_one_line(array('重症度別患者数の入力方法', '月間'));
  //get all severities
  $severity_list = get_severities($data['ward']);
  if (!empty($severity_list)) {
    foreach ($severity_list as $value) {
      $name = 'actual_'  .  $value['nid'];
      $output .= export_one_line(array($value['node_title'], $data[$name]));
    }
  }
  $output .= "\r\n";
  return $output;
}

/**
 * get disease data
 * @param $data
 * @return $output
 */
function get_export_disease_data($data) {
  $ward_id = $data['ward'];
  //get disease store in mysql
  $general_disease = get_general_disease($ward_id);
  //get disease store in mssql
  $private_disease = get_private_disease($ward_id);  
  $output .= export_one_line(array('病原体', '人'));
  if (!empty($general_disease)) {
    foreach ($general_disease as $key => $value) {
      $name = 'disease_'  .  $key;
      $output .= export_one_line(array($value, $data[$name]));
    }
  }
  if (!empty($private_disease)) {
    foreach ($private_disease as $key => $value) {
      $name = 'disease_'  .  $key;
      $output .= export_one_line(array($value, $data[$name]));
    }
  }
  return $output;
}

/**
 * generate one row by data
 * @param $row
 * @return $output
 */
function export_one_line($row = NULL) {
  if (empty($row)) {
    return NULL;
  }
  //$output .= implode(',', $row);  
  foreach ($row as $value) {
    /*if (strpos($value, ',') === FALSE) {
      $output .= $value .',';
    }
    else {
      $output .= "\"$value\"" .',';       
    }*/
    $output .= "\"$value\"" .',';
  }
  //$pos = strrpos($output, ',');
  //$output = rtrim($output, ',');
  $output = substr($output, 0, -1);
  $output .= "\r\n";
  return $output;
}

/**
 * products order by order field
 * @param $pro_list
 */
function get_products_order($pro_list) {
  $alcohol_tmp = array();
  $soap_tmp = array();
  if (!empty($pro_list)) {
    foreach ($pro_list as $item) {
	  if ($item['category'] == ALCOHOL_JP) {
	    $alcohol_tmp[$item['order']] = $item;
	  } 
	  else {
	    $soap_tmp[$item['order']] = $item;
	  }
	}
	ksort($alcohol_tmp);
	ksort($soap_tmp);
  }
  return array('alcohol' => $alcohol_tmp, 'soap' => $soap_tmp);
}
/**
 * remove '/'  2011/08
 * 
 */
function date_to_string($date) {
  if (strpos($date, '/')) {
    $arr = explode('/', $date);
    if (count($arr) == 2) {
      $dt = $arr[0] . $arr[1];
    }
    return $dt;
  }
  else {
    return $date;
  }
}
/**
 * string to date  add '/'
 * */
function string_to_date($str) {
  if (strlen($str) == 6 ) {
    return substr($str, 0, 4) .'/'. substr($str,4);
  }
  else if (strlen($str) == 5){
    return substr($str, 0, 4) .'/0'. substr($str,4); 
  }
  else {
    return $str;
  }
}

?>