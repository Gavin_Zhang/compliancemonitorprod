<?php

// $Id$
/**
 * @file 
 * aspcm_safe_storage.inc
 * A class that provides some generic methods that allow data to be saved in different locations
 */

module_load_include('inc', 'aspcm', 'includes/aspcm_storage_factory.class');
module_load_include('inc', 'aspcm', 'includes/aspcm_storage_generic.class');

class SafeStorage {
  const MAX_INFO_IDS_PER_REQUEST = 100;

  protected $content_type;
  protected $protected_fields;
  protected $site_id;
  protected $type_id = NULL;
  protected $max_info_ids_per_request;
  
  function SafeStorage($content_type, $site_id) {

    if (empty($content_type) || empty($site_id)) {
      throw new Exception("Content Type or Site Id cannot be empty");
    }
    
    $this->set_save_location(variable_get('aspcm_data_save_options', ASPCM_PROD_SAVE_LOCATION));
    $this->max_info_ids_per_request = self::MAX_INFO_IDS_PER_REQUEST;
    $this->content_type = $content_type;
    $this->site_id = $site_id;
    $this->set_protected_fields();
  }

  function get_protected_fields() {
    return empty($this->protected_fields[$this->content_type]) ? FALSE : $this->protected_fields[$this->content_type];
  }
  
  function save_data($info_id, $data, $store_format = StorageGeneric::ECD_BASE64) {
    $initial_time = time();
    
    // encode the data
    $storage_generic = new StorageGeneric(ASPCM_PRINT_INFO);
    $data = $storage_generic->encode_data($data, $store_format);
    
    // get the object based on the option in the administrative area for the module
    $obj = StorageFactory::getStorage($this->save_location);
    
    // set the necessary ids for the class to work
    $obj->set_ids($this->site_id, $this->type_id, $this->question_control_id, $this->question_control_answer_id, $this->question_data_id, $info_id);
    
    $this->show_time_spent($initial_time, time(), __FUNCTION__);
    
    // save the data and return the information
    return $obj->save_data($data);
  }
  
  function load_data($info_id) {
    $initial_time = time();
  
    if (!$this->protected_fields_exists() || empty($info_id)) {
      return FALSE;
    }
    
    $cache_key = "aspcm-safestorage-{$info_id}";
    
    if ($data = cache_get($cache_key)) {
      print_info("load_data: retrieving value cached for $cache_key");
      $this->show_time_spent($initial_time, time(), __FUNCTION__, "[cached]");
      return $data->data;
    }
    
    
    print_info("loading id #{$info_id} data from safe storage");
    
    $obj = StorageFactory::getStorage($this->save_location);
    $obj->set_ids($this->site_id, $this->type_id, $this->question_control_id, $this->question_control_answer_id, $this->question_data_id, $info_id);
    $result = $obj->load_data($info_id);
    
    print_info("load_data: creating cache for $cache_key");
    //original code comment by Benny for ACMSUPP-35 20120411
    //cache_set($cache_key, $result, 'cache', CACHE_TEMPORARY);
    
    //add by Benny for ACMSUPP-35 20120411
    if (!empty($result)){
    	cache_set($cache_key, $result, 'cache', CACHE_TEMPORARY);
    }
    
    $this->show_time_spent($initial_time, time(), __FUNCTION__);
    
    return $result;
  }
  
  /**
   * Return the content stored into safe storage according to the informed node ids
   * 
   * $node_ids is expected to be an array of node ids.
   * thi is the format of the expetected array:
   * <code>
   * array( 100, 157, 77, 388 );
   * </code>
   * 
   * Notice that all the elements in the array will be converted to integer.
   * 
   * If no content is found, an empty array will be returned.
   * If one of the informed id is not found, the result will put an empty array on that position
   * 
   * Here is an example of the returned array. In this case, the node id #77 wasn't found in the
   * safe storage.
   * <pre>
   * array (
   *   100 => array('field 1' => 'value', 'field 2' => 'value 2'),
   *   157 => array('field 1' => 'value', 'field 2' => 'value 2', 'field 3' => 'value 3'),
   *   77  => array(),
   * )
   * </pre>
   * 
   * @param array $info_ids
   * @return array
   */
  function load_data_ids($info_ids) {
    $initial_time = time();
    
    // check if the node_ids has the correct format
    if (!is_array($info_ids)) {
      $msg = "The parameter must be an array.";
      print_info(__METHOD__ .": ERROR: $msg");
      return FALSE;
    }
    
    // make sure the keys are a numerical sequence
    $info_ids = array_values($info_ids);

    if (!$this->protected_fields_exists()) {
      return FALSE;
    }
    
    $info_ids_count = count($info_ids);
    
    print_info("loading {$info_ids_count} items from safe storage");
    
    // get the correct object to save the data
    $obj = StorageFactory::getStorage($this->save_location);
    
    // send the expected ids
    $obj->set_ids($this->site_id, $this->type_id, $this->question_control_id, $this->question_control_answer_id, $this->question_data_id);
    
    // get how many requests should be done
    $chunks = array_chunk($info_ids, $this->max_info_ids_per_request, TRUE);
    $requests = count($chunks);
    print_info("TOTAL REQUESTS: $requests");
    
    // get the values from the safe storage
    $result = array();
    $cached_chunks = 0; // no cache at all
    foreach($chunks as $piece => $chunk) {
      print_info("request: " . ($piece+1));
      
      // generate the cache key for the chunk
      $cache_key = "aspcm-safestorage-ids-";
      $nids = implode(',', $chunk);
      $cache_key .= md5($nids);
      
      // try to get the data from the cache
      if ($data = cache_get($cache_key)) {
        print_info("load_data_ids: retrieving value cached for $cache_key");
        $partial_result = json_decode($data->data, TRUE);
        $cached_chunks++;
      }
      else {
        // cache not found, get from safe storage
        $partial_result = $obj->load_data_ids($chunk);
        
        //db_query("SELECT * FROM comments"); // to avoid 'mysql has gone away' problem, send a dummy query to mysql
        
        // cache the returned value
        $cached_value = json_encode($partial_result); // have to encode with JSON to avoid issues with the php serialize function used by Drupal
        $cache_size = round((mb_strlen($cached_value))/1024, 2);
        print_info("load_data_ids: creating cache for $cache_key (size:  {$cache_size} KBytes)");
        //original code comment by Benny for ACMSUPP-35 20120411
        //cache_set($cache_key, $cached_value, 'cache', time() + ASPCM_CACHE_TTL);
       	
        //add by Benny for ACMSUPP-35 20120411
        if (!$partial_result){
        	cache_set($cache_key, $cached_value, 'cache', time() + ASPCM_CACHE_TTL);
        }
      }
      if (!empty($partial_result)) {
	    $result = array_merge($result, $partial_result);
	  }
      
    }
    $result = @array_combine($info_ids, $result);

    if (is_array($result)) {
      // clear the positions that wasn't found in the service
      foreach ($result as $key => $value) {
        if (!is_array($value)) {
          $result[$key] = NULL;
        }
      }
    }
    else {
      $result = array();
    }
    
    $cache_info = "";
    if ($cached_chunks == $requests) {
      $cache_info = "[cached] ";
    }
    elseif($cached_chunks > 0) {
      $cache_info = "[partially cached] ";
    }
    
    $this->show_time_spent($initial_time, time(), __FUNCTION__, "{$cache_info} [{$info_ids_count} items]");
    
    return $result;

  }
  
  /**
   * Populate the protected_fields to get the settings from drupal variable table
   * Meanwhile, the type_id will be asigned also
   * 
   * e.g.:
   * <pre>
   * 'aspcm_hospital' => array(
   *   'field_1', 'field_2', 'field_3'
   * );
   * </pre>
   */
  function set_protected_fields() {
  
    // check if the fields are already set for this content type
    if (isset($this->protected_fields[$this->content_type])) {
      return TRUE;
    }
  
    $fields = variable_get('aspcm_protected_fields', '');
    $cnt_types = explode("\r\n", $fields);

    // Loop each content types
    foreach ($cnt_types as $cnt_type) {
      $cnt_type_prop = explode('|', $cnt_type);
      if (count($cnt_type_prop) == 6) {
        if ($this->content_type == $cnt_type_prop[0]) {
          $this->type_id = (int) $cnt_type_prop[1];
          $this->question_control_id = (int) $cnt_type_prop[2];
          $this->question_control_answer_id = (int) $cnt_type_prop[3];
          $this->question_data_id = (int) $cnt_type_prop[4];
           
          $this->protected_fields = array(
            $this->content_type => explode(',', $cnt_type_prop[5]),
          );
          
          break;
        }
      }
      else {
        print_info(__METHOD__ .": ERROR: Protected fields don't have the right property settings.");
      }
    }

    return TRUE;
  }
  
  function protected_fields_exists() {
    if (count($this->protected_fields) > 0) {
      return TRUE;
    }
    return FALSE;
  }
  
  function set_save_location($location) {
    $this->save_location = StorageFactory::DRUPALDB_SAVE_LOCATION;
//    if ($location == ASPCM_PROD_SAVE_LOCATION) {
//      $this->save_location = StorageFactory::WS_SAVE_LOCATION;
//    }
  }
  
  function show_time_spent($initial_timestamp, $end_timestamp, $method, $extra = '') {
    global $user;
    $total_time = $end_timestamp - $initial_timestamp;
    $str = "It took $total_time seconds to execute $method.";
    $str .= empty($extra) ? '' : " $extra";
    
    $print = variable_get('aspcm_data_message_errorlog', ASPCM_DONT_PRINT_INFO);
    if ($print == ASPCM_PRINT_INFO && $user->uid == 1) {
      drupal_set_message($str);
    }
  }
  
}