<?php

class QueueHandler {

  protected $queue = array();
  protected $uniqueValue = FALSE;
  protected $associative = FALSE;
  
  function isAssociative() {
    $this->associative = TRUE;
  }
  
  function isUnique() {
    $this->uniqueValue = TRUE;
  }
  
  function add($value, $key = NULL) {
    
    if ($this->associative && is_null($key)) {
      throw new Exception("This is an associative queue, so you need to specify a key for it");
    }
    
    if ($this->uniqueValue && in_array($value, $this->queue)) {
      return FALSE;
    }   
  
    if (empty($this->queue)) {
      if ($this->associative) {
        $this->queue = array($key => $value);
      } 
      else {
        $this->queue = array($value);
      }
      return 1;
    }
    
    if ($this->associative) {
      $this->queue[$key] = $value;
      $count = count($this->queue);
    } 
    else {
      $count = array_push($this->queue, $value);
    }
    
    return $count;
  
  }
  
  function pop() {
    return array_shift($this->queue);
  }
  
  function search($value) {
    return array_search($value, $this->queue);
  }
  
  function searchKey($key) {
    if (array_key_exists($key, $this->queue)) {
      return $this->queue[$key];
    }
    
    return FALSE;
  }
  
  function remove($key) {
    unset($this->queue[$key]);
  }
  
  function getAllElements() {
    return $this->queue;
  }
}