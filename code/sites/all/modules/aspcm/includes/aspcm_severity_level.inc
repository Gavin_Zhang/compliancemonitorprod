<?php
// $Id$
/**
 * @file severity.inc
 *
 */

/**
 * Severity Level Index
 */
function severity_level_index() {
  $header = '<div class="menu-links"><a class="back_to_severity" href="#">重症度に係る評価票</a>
			 <a class="navi_to_result" href="#">5段階レベル</a>
			 <a class="next_to_nurse current" href="#">重症度・看護必要度に係る評価票</a>
			 <a class="back_to_nurse" href="#">重症度・看護必要度に係る評価票</a></div>';
  $header_mess = '<div class="severity-message severity-class">重症度に係る評価票</div>
			 <div class="severity-message nursing-class" style="display:none">重症度・看護必要度に係る評価票</div>
			 <div class="severity-message result-class" style="display:none">5段階レベル</div>';
  $page1_output = severity_level_generate();
  $page1_output = '<div id="severity_severity_page">'. $page1_output .'</div>';
  $page2_output = nursing_level_generate();
  $page2_output = '<div id="severity_nursing_page">'. $page2_output .'</div>';
  $page3_output = final_result_generate();
  $page3_output = '<div id="severity_result_page">'. $page3_output .'</div>';
  return '<div class="serverity-menu">'. $header . $header_mess . '<div id="main_content" class="level-tables">'. $page1_output . $page2_output . $page3_output .'</div><div class="footer-mess">' . $header . '</div></div>';
}

/**
 * Implement severity_level_generate()
 */
function severity_level_generate() {
  //$header .='<div class="severity-message">重症度に係る評価票</div>';
  $severity_mt_table = get_severity_mt_content();
  $severity_mt_table_sore = '<div class="score_region"><span>Ａ得点：  </span><span class="ascore">0</span></div>';
  $severity_cond_table = get_severity_cond_content();
  $severity_cond_table_sore = '<div class="score_region"><span>Ｂ得点：  </span><span class="bscore">0</span></div>';
  $result = $severity_mt_table . $severity_mt_table_sore . $severity_cond_table . $severity_cond_table_sore;
  return  $result;
}

/**
 * Generate nursing_level
 */
function nursing_level_generate() {
  $nursing_mt_table = get_nursing_mt_content();
  $nursing_mt_table_sore = '<div class="score_region"><span>Ａ得点：  </span><span class="nurascore">0</span></div>';
  $nursing_cond_table = get_nursing_cond_content();
  $nursing_cond_table_sore = '<div class="score_region"><span>Ｂ得点：  </span><span class="nurbscore">0</span></div>';
  $result = $nursing_mt_table . $nursing_mt_table_sore . $nursing_cond_table . $nursing_cond_table_sore;
  return $result;
}
 
/**
 * Generate result_level
 */
function final_result_generate() {
  $output = '';
  $severity_table = '<table class="views-table"><tr><td rowspan="2" class="severity-category">重症度に係る評価票</td>
					 <td class="score-title">A得点</td><td><span class="ascore">0</span></td></tr>
					 <tr><td class="score-title">B得点</td><td><span class="bscore">0</span></td></tr></table>';
  $nursing_table = '<table class="views-table"><tr><td rowspan="2" class="severity-category">重症度・看護必要度に係る評価票</td>
					<td class="score-title">A得点</td><td><span class="nurascore">0</span></td></tr>
					<tr><td class="score-title">B得点</td><td><span class="nurbscore">0</span></td></tr></table>';
  $final_score = '<div class="belong-to"><span>確定分類：   </span><span id="final_classify"></span></div>';
  
  $final_catagory = get_category_content();
  
  $output = $severity_table . $nursing_table . $final_score . $final_catagory;
  return $output;
} 

/**
 * Get catagory content
 */
function get_category_content() {
  $output = '';
  $table_head = '<table class="views-table level-criterion"><thead><tr><th colspan="2">患者の看護必要度による５分類</th></tr></thead>';
  $table_body = '<tbody class="criterion_body">';
  // Severity Category
  $severity_category = array(
    'ICUレベルの中でも最重症',
    'ICUレベルの重症度',
    'HCUレベルの重症度',
    '一般レベル',
    '一般レベルの中でも最軽症'
  );
  $index = 5;
  for ($i = 0; $i < 5; $i++) {
    $table_row = '<tr><td>'. $index .'</td><td>'. $severity_category[$i] .'</td></tr>';
    $table_body .= $table_row;
    $index --;
  }  
  $table_body .= '</tbody>';
  $table_end = '</table>';
  
  $output = $table_head . $table_body . $table_end;
  return $output;
}
 
/**
 *  Get content_type_severity_treatment content
 */
function get_severity_mt_content() {
  $result = db_query("SELECT * FROM {node} AS n
                    INNER JOIN {field_data_field_aspcm_sev_treat_zero} AS stz ON n.nid = stz.entity_id
                    INNER JOIN {field_data_field_aspcm_sev_treat_one} AS sto ON n.nid = sto.entity_id
                    WHERE n.type = 'aspcm_treatment' ORDER BY n.nid");
  
  $table = '<form id="severity_mt"><table class="views-table">';
  $table_header = '<thead><tr><th class="serious-level-item">モニタリング及び処置等</th><th class="point-0-1">0点</th><th class="point-0-1">1点</th></tr></thead>';
  $table_body ='';
  foreach ($result as $row) {
  
    $table_row = '<tr><td>'. htmlspecialchars($row->title) .'</td><td><input class="severity_mt_radio" value="0" name="'. $row->nid .'" id="severity-mt-'.$row->nid.'" type="radio"
	/><label for="severity-mt-'.$row->nid.'">'. htmlspecialchars($row->field_aspcm_sev_treat_zero_value) .'</lable></td><td><input class="severity_mt_radio" value="1" name="'. $row->nid .'" id="severity-mt-'.$row->nid.'-1" type="radio" /><label for="severity-mt-'.$row->nid.'-1">'. htmlspecialchars($row->field_aspcm_sev_treat_one_value) .'</label></td></tr>';
    $table_body .= $table_row;

  }
  $table_end='</table></form>';
  $result = $table . $table_header . $table_body . $table_end;
  return $result;
}

/**
 *  Get severity_cond content
 */
function get_severity_cond_content() {
  $query = db_query("SELECT * FROM node AS n
                    LEFT JOIN field_data_field_aspcm_sev_cond_zero AS scz ON n.nid = scz.entity_id
                    LEFT JOIN field_data_field_aspcm_sev_cond_one AS sco ON n.nid = sco.entity_id
                    LEFT JOIN field_data_field_aspcm_sev_cond_two AS sct ON n.nid = sct.entity_id
                    WHERE n.type = 'aspcm_condition' ORDER BY n.nid");
  $table = '<form id="severity_cond"><table class="views-table">';
  $table_header = '<thead><tr><th class="patient-state-item"> 患者の状況等</th><th class="point-0-1-2">0点</th><th class="point-0-1-2">1点</th><th class="point-0-1-2">2点</th></tr></thead>';
  $table_body ='';
  foreach ($query as $row) {
    
    $table_row = '<tr><td>'. htmlspecialchars($row->title) .'</td><td><input class="severity_cond_radio" value="0" name="'. $row->nid .'" id="severity-cond-'.$row->nid.'" type="radio" /><label for="severity-cond-'.$row->nid.'">'
                 . htmlspecialchars($row->field_aspcm_sev_cond_zero_value) .'</label></td>
                 <td><input class="severity_cond_radio" value="1" name="'. $row->nid .'" id="severity-cond-'.$row->nid.'-1" type="radio"  /><label for="severity-cond-'.$row->nid.'-1">'. htmlspecialchars($row->field_aspcm_sev_cond_one_value) .'</label></td>';
          
    if ($row->field_aspcm_sev_cond_two_value == '') {
      $table_row .= '<td>&nbsp;</td></tr>';
    }
    
    else {
      $table_row .= '<td><input class="severity_cond_radio" value="2" name="'. $row->nid .'" id="everity-cond-'.$row->nid.'-2" type="radio"  /><label for="severity-cond-'.$row->nid.'-2">'. htmlspecialchars($row->field_aspcm_sev_cond_two_value) .'</label></td></tr>';
    }
    
    $table_body .= $table_row;
      
  }
  
  $table_end = '</table></form>';
  $result = $table . $table_header . $table_body . $table_end;
  return $result;
}

/**
 *  Get content_type_nursing_treatment content
 */
function get_nursing_mt_content() {
  $query = db_query("SELECT * FROM node AS n
                    LEFT JOIN field_data_field_aspcm_nur_treat_zero AS ntz ON n.nid = ntz.entity_id
                    LEFT JOIN field_data_field_aspcm_nur_treat_one AS nto ON n.nid = nto.entity_id
                    WHERE n.type = 'aspcm_nurse_treatment' ORDER BY n.nid");
  $table = '<form id="nursing_mt"><table class="views-table">';
  $table_header = '<thead><tr><th class="serious-level-item">モニタリング及び処置等</th><th class="point-0-1">0点</th><th class="point-0-1">1点</th></tr></thead>';
  $table_body ='';
  foreach ($query as $row) {
  
    $table_row = '<tr><td>'. htmlspecialchars($row->title) .'</td><td><input class="nursing_mt_radio" value="0" name="'. $row->nid .'" id="nursing-mt-'.$row->nid.'" type="radio" /><label for="nursing-mt-'.$row->nid.'">'.
    htmlspecialchars($row->field_aspcm_nur_treat_zero_value) .'</label></td><td><input class="nursing_mt_radio" value="1" name="'. $row->nid .'" id="nursing-mt-'.$row->nid.'-1" type="radio" /><label for="nursing-mt-'.$row->nid.'-1">'.
    htmlspecialchars($row->field_aspcm_nur_treat_one_value) .'</label></td></tr>';
    $table_body .= $table_row;

  }
  $table_end = '</table></form>';
  $result = $table . $table_header . $table_body . $table_end;
  return $result;
}

/**
 *  Get content_type_nursing_cond content
 */
function get_nursing_cond_content() {
  $query = db_query("SELECT * FROM node AS n
          LEFT JOIN field_data_field_aspcm_nur_cond_zero AS ncz ON n.nid = ncz.entity_id
          LEFT JOIN field_data_field_aspcm_nur_cond_one AS nco ON n.nid = nco.entity_id
          LEFT JOIN field_data_field_aspcm_nur_cond_two AS nct ON n.nid = nct.entity_id
          WHERE n.type = 'aspcm_nurse_condition' ORDER BY n.nid");
  $table='<form id="nursing_cond"><table class="views-table">';
  $table_header = '<thead><tr><th class="patient-state-item"> 患者の状況等</th><th class="point-0-1-2">0点</th><th class="point-0-1-2">1点</th><th class="point-0-1-2">2点</th></tr></thead>';
  $table_body='';
  foreach ($query as $row) {
  
    $table_row = '<tr><td>'. htmlspecialchars($row->title) .'</td><td><input class="nursing_cond_radio" value="0" name="'. $row->nid .'" id="nursing-mt-'.$row->nid.'" type="radio" /><label for="nursing-mt-'.$row->nid.'">'
                 . htmlspecialchars($row->field_aspcm_nur_cond_zero_value) .'</label></td>
                 <td><input class="nursing_cond_radio" value="1" name="'. $row->nid .'" id="nursing-mt-'.$row->nid.'-1" type="radio" /><label for="nursing-mt-'.$row->nid.'-1">'. htmlspecialchars($row->field_aspcm_nur_cond_one_value) .'</label></td>';
    if ($row->field_aspcm_nur_cond_two_value == '') {
      $table_row .= '<td>&nbsp;</td></tr>';
    }
    else{
      $table_row .= '<td><input class="nursing_cond_radio" value="2" name='. $row->nid .' id="nursing-mt-'.$row->nid.'-2" type="radio"/><label for="nursing-mt-'.$row->nid.'-2">'. htmlspecialchars($row->field_aspcm_nur_cond_two_value) .'</label></td></tr>';
    }

    $table_body .= $table_row;

  }
  $table_end='</table></form>';
  $result = $table . $table_header . $table_body . $table_end;
  return $result;
}


/**
 * Normal severity level page 
 */
function severity_level_normal_index() {
    $output = normal_severity_level_generate();
    $level = normal_final_result_page();
    return '<div id="normal_severity_page">'. $output . $level .'</div>';
}

/**
 * Generate normal severity level
 */
function normal_severity_level_generate() {
  $normal_severity_mt_table = get_normal_nursing_mt_content();
  $normal_severity_mt_table_sore = '<br /><div class="score_region"><span>Ａ得点：  </span><span class="ascore">0</span></div><br />';
  $normal_severity_cond_table = get_normal_nursing_cond_content();
  $normal_severity_cond_table_sore = '<br /><div class="score_region"><span>Ｂ得点：  </span><span class="bscore">0</span></div><br />';
  $result = $normal_severity_mt_table . $normal_severity_mt_table_sore . $normal_severity_cond_table . $normal_severity_cond_table_sore;
  return $result;
}

/**
 * Get normal severity level treatment content
 */
function get_normal_nursing_mt_content() {
  $query = db_query("SELECT * FROM node AS n
                    LEFT JOIN field_data_field_aspcm_nor_nur_treat_zero AS anntz ON n.nid = anntz.entity_id
                    LEFT JOIN field_data_field_aspcm_nor_nur_treat_one AS annto ON n.nid = annto.entity_id
                    LEFT JOIN field_data_field_aspcm_nor_nur_treat_two AS anntt ON n.nid = anntt.entity_id
                    WHERE n.type = 'aspcm_normal_nurse_treatment' ORDER BY n.nid");
  $table = '<form id="normal_nursing_treat"><table class="views-table">';
  $table_header = '<thead><tr><th class="normal-level-item">モニタリング及び処置等</th><th class="point-0-1-2">0点</th><th class="point-0-1-2">1点</th><th class="point-0-1-2">2点</th></tr></thead>';
  $table_body ='';
  foreach ($query as $row) {
    
    $table_row = '<tr><td>'. htmlspecialchars($row->title) .'</td><td><input class="normal_nursing_mt_radio" value="0" name="'. $row->nid .'" id="normal-nursing-mt-'.$row->nid.'" type="radio" /><label for="normal-nursing-mt-'.$row->nid.'">'
                 . htmlspecialchars($row->field_aspcm_nor_nur_treat_zero_value) .'</label></td>';
    
    if ($row->field_aspcm_nor_nur_treat_one_value == '') {
      $table_row .= '<td>&nbsp;</td>';
    }
    
    else {
      $table_row .= '<td><input class="normal_nursing_mt_radio" value="1" name="'. $row->nid .'" id="normal-nursing-mt-'.$row->nid.'-1" type="radio"  /><label for="normal-nursing-mt-'.$row->nid.'-1">'. htmlspecialchars($row->field_aspcm_nor_nur_treat_one_value) .'</label></td>';
    }

    if ($row->field_aspcm_nor_nur_treat_two_value == '') {
      $table_row .= '<td>&nbsp;</td>';
    }
    
    else {
      $table_row .= '<td><input class="normal_nursing_mt_radio" value="2" name="'. $row->nid .'" id="normal-nursing-mt-'.$row->nid.'-2" type="radio"  /><label for="normal-nursing-mt-'.$row->nid.'-2">'. htmlspecialchars($row->field_aspcm_nor_nur_treat_two_value) .'</label></td></tr>';
    }

    $table_body .= $table_row;
      
  }
  
  $table_end = '</table></form>';
  
  $text = '<div  class="info-mess">遵守率を算出する際に必要な患者の看護必要度による5分類を判定することができます。</div>';
  $result = $text . $table . $table_header . $table_body . $table_end;
  return $result;
}

/**
 * Get normal severity level treatment content
 */
function get_normal_nursing_cond_content() {
  $query = db_query("SELECT * FROM node AS n
                    LEFT JOIN field_data_field_aspcm_nor_nur_cond_zero AS anncz ON n.nid = anncz.entity_id
                    LEFT JOIN field_data_field_aspcm_nor_nur_cond_one AS annco ON n.nid = annco.entity_id
                    LEFT JOIN field_data_field_aspcm_nor_nur_cond_two AS annct ON n.nid = annct.entity_id
                    WHERE n.type = 'aspcm_normal_nurse_condition' ORDER BY n.nid");
  $table = '<form id="normal_nursing_cond"><table class="views-table">';
  $table_header = '<thead><tr><th class="patient-state-item">患者の状況等</th><th class="point-0-1-2">0点</th><th class="point-0-1-2">1点</th><th class="point-0-1-2">2点</th></tr></thead>';
  $table_body ='';
  foreach ($query as $row) {
    
    $table_row = '<tr><td>'. htmlspecialchars($row->title) .'</td><td><input class="normal_nursing_cond_radio" value="0" name="'. $row->nid .'" id="normal-nursing-cond-'.$row->nid.'" type="radio" /><label for="normal-nursing-cond-'.$row->nid.'">'
                 . htmlspecialchars($row->field_aspcm_nor_nur_cond_zero_value) .'</label></td>
                 <td><input class="normal_nursing_cond_radio" value="1" name="'. $row->nid .'" id="normal-nursing-cond-'.$row->nid.'-1" type="radio"  /><label for="normal-nursing-cond-'.$row->nid.'-1">'. htmlspecialchars($row->field_aspcm_nor_nur_cond_one_value) .'</label></td>';

    if ($row->field_aspcm_nor_nur_cond_two_value == '') {
      $table_row .= '<td>&nbsp;</td></tr>';
    }
    
    else {
      $table_row .= '<td><input class="normal_nursing_cond_radio" value="2" name="'. $row->nid .'" id="normal-nursing-cond-'.$row->nid.'-2" type="radio"  /><label for="normal-nursing-cond-'.$row->nid.'-2">'. htmlspecialchars($row->field_aspcm_nor_nur_cond_two_value) .'</label></td></tr>';
    }
    
    $table_body .= $table_row;
      
  }
  
  $table_end = '</table></form>';
  $result = $table . $table_header . $table_body . $table_end;
  return $result;
}


/**
 * Normal final result page
 */
function normal_final_result_page() {
  $output = '';
  $severity_table = '<table class="views-table"><tr><td rowspan="2" class="severity-category">一般病棟の看護必要度重症度に係る評価票</td>
					 <td class="score-title">A得点</td><td><span class="ascore">0</span></td></tr>
					 <tr><td class="score-title">B得点</td><td><span class="bscore">0</span></td></tr></table>';
  $final_score = '<div class="belong-to"><span>確定分類：   </span><span id="final_classify"></span></div>';
  
  $final_catagory = get_category_content();
  
  $output = $severity_table . $final_score . $final_catagory;
  return $output;
}