<?php

require_once('sites/all/modules/survey/survey.php');
require_once('sites/all/modules/survey/surveydata.php');

class ExtendedSurveyData extends SurveyData {

  protected $userIds;
  
  public function set_user_ids($userIds){
    if (is_array($userIds)) {
      $userIds = implode(",", $userIds);
    }
		$this->userIds = $userIds;
	}
  
  public function get_user_ids(){
		return $this->userIds;
	}
  
  /**
   * Overrides the current implementation of SetServiceURL, so we can create the URL for the new service
   * 
   * @see sites/all/module/survey/surveydata.php
   */
  public function SetServiceURL($ServiceURL) {
    // check if this is a call for the new service
    if (is_object($ServiceURL) && $ServiceURL->Action == 'usersRetake') {
      $ServiceURL = $ServiceURL->ServiceUrl.'?evaluationId='.$this->get_evaluation_id() . 
        '&pageNumber=' . $this->get_current_page_no() . 
        '&pageSize='.$this->get_no_of_questions_per_page() .
        '&userIds=' . $this->get_user_ids();
    }
    
    // for any other service, call the current implementation
    parent::SetServiceURL($ServiceURL);
    
    print_info("Setting service URL: " . ($this->GetServiceURL()));
  }
}