<?php
/**
 * @file 
 * aspcm_graph_default_configuration.inc
 * 
 * A bundle of graph default configurations.
 * 
 */

// For some purpose, the month of the year does not always start from Jan.
// we call this current year  
define ('DEFAULT_YEAR_STARTING_MONTH', '04');
define ('DEFAULT_YEAR_ENDING_MONTH', '03');

// Define a gobal variable to handle the default settings for the chart
global $default_settings;

// Get the system year and month
$year_now = date('Y');
$month_now = date('m');

// If current month is larger than the default starting month
// we can be sure that the data belongs to current year 
if ($month_now >= (int)DEFAULT_YEAR_STARTING_MONTH) {
  $year = $year_now;
}
// Otherwise, the data belongs to last year
else {
  $year = $year_now - 1;
}

// Default options for Preset A (遵守状況グラフ)
$default_settings['major_value'][1] = 'hand_washing';
$default_settings['major_maximum_value'][1] = 'automatic';
$default_settings['major_fixed_value'][1] = '';
$default_settings['major_wash_number'][1] = 'all_patient';
$default_settings['major_antisepic_volume'][1] = 'alcohol_patient';
$default_settings['minor_value'][1] = 'compliance_rate';
$default_settings['minor_maximum_value'][1] = 'automatic';
$default_settings['minor_fixed_value'][1] = '';
$default_settings['minor_wash_number'][1] = 'alcohol_staff';
$default_settings['minor_antisepic_volume'][1] = 'alcohol_patient';
$default_settings['horizontal_axis'][1] = 'period';
$default_settings['horizontal_start'][1] = $year .'/'. DEFAULT_YEAR_STARTING_MONTH;
$default_settings['horizontal_end'][1] = ($year + 1) .'/'. DEFAULT_YEAR_ENDING_MONTH;
$default_settings['unit_of_period_displayed'][1] = 1;
$default_settings['major_single_bar'][1] = 'multiple';
$default_settings['minor_single_line'][1] = 'single';

// Default options for Preset B (感染率推移グラフ)
$default_settings['major_value'][2] = 'hand_washing';
$default_settings['major_maximum_value'][2] = 'automatic';
$default_settings['major_fixed_value'][2] = '';
$default_settings['major_wash_number'][2] = 'all_patient';
$default_settings['major_antisepic_volume'][2] = 'alcohol_patient';
$default_settings['minor_value'][2] = 'infection_rate';
$default_settings['minor_maximum_value'][2] = 'automatic';
$default_settings['minor_fixed_value'][2] = '';
$default_settings['minor_wash_number'][2] = 'alcohol_staff';
$default_settings['minor_antisepic_volume'][2] = 'alcohol_patient';
$default_settings['horizontal_axis'][2] = 'period';
$default_settings['horizontal_start'][2] = $year .'/'. DEFAULT_YEAR_STARTING_MONTH;
$default_settings['horizontal_end'][2] = ($year + 1) .'/'. DEFAULT_YEAR_ENDING_MONTH;
$default_settings['unit_of_period_displayed'][2] = 1;
$default_settings['major_single_bar'][2] = 'multiple';
$default_settings['minor_single_line'][2] = 'single';

// Default options for Preset C (病棟別グラフ)
$default_settings['major_value'][3] = 'hand_washing';
$default_settings['major_maximum_value'][3] = 'automatic';
$default_settings['major_fixed_value'][3] = '';
$default_settings['major_wash_number'][3] = 'all_patient';
$default_settings['major_antisepic_volume'][3] = 'alcohol_patient';
$default_settings['minor_value'][3] = 'compliance_rate';
$default_settings['minor_maximum_value'][3] = 'automatic';
$default_settings['minor_fixed_value'][3] = '';
$default_settings['minor_wash_number'][3] = 'alcohol_staff';
$default_settings['minor_antisepic_volume'][3] = 'alcohol_patient';
$default_settings['horizontal_axis'][3] = 'ward';
$default_settings['horizontal_start'][3] = $year .'/'. DEFAULT_YEAR_STARTING_MONTH;
$default_settings['horizontal_end'][3] = ($year + 1) .'/'. DEFAULT_YEAR_ENDING_MONTH;
$default_settings['unit_of_period_displayed'][3] = 1;
$default_settings['major_single_bar'][3] = 'multiple';
$default_settings['minor_single_line'][3] = 'single';

