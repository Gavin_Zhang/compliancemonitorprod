<?php

interface IASPCMStorage {
  public function save_data($data);
  public function load_data($node_id);
  public function load_data_ids($node_ids);
  public function set_ids($site_id, $type_id, $question_control_id, $question_control_answer_id, $question_data_id, $info_id = NULL);
}