<?php
// $Id$
/**
 * @file 
 * aspcm_graph.class.inc
 * A class of graph.
 */

module_load_include('inc', 'aspcm', 'includes/aspcm_common');

define ('DEFAULT_FONT_SIZE', 7);


class ASPCM_Graph {
  
  protected $width;
  protected $height;
  protected $xaxis;
  protected $yaxis;
  protected $y2axis;
  protected $attributes;
  protected $scale;
  protected $data;
  protected $name;
  protected $graph_data;
  
  /**
   * Specify what scale we want to use
   * The library supports linear, integer, logarithmic, text and date scales
   * The scale is specified as a string where the first half of the string denominates the X-axis scale and the second half denominates the Y-axis scale. 
   * 
   * @param string $scale_type
   * "intint", represents for integer scale for the X-axis and integer scale for the Y-axis
   * 
   * @param array $size
   * array('width' => 450, 'height' => 450)
   * @param string $title
   * @param array $coords
   * array('xaxis' => array(
   *   'title' => 'X-axis',
   *   'labels' => array(2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016),
   *   ...
   *  ), 
   *  'yaxis' => array(
   *    'title' => 'Y2-axis',
   *    ...
   *  ), 
   *  'y2axis' => array(
   *    'scale_type' => 'lin',
   *    'max' => 2,
   *    'min' => 0,
   *    ...
   *  )
   * )
   * @param array $attributes
   */
  function ASPCM_Graph($size, $scale_type = 'textint', $coords = array(), $attributes = array()) {
    module_load_include('php', 'aspcm', 'library/jpgraph/jpgraph');
    module_load_include('php', 'aspcm', 'library/jpgraph/jpgraph_bar');
    module_load_include('php', 'aspcm', 'library/jpgraph/jpgraph_line');
    module_load_include('php', 'aspcm', 'library/jpgraph/jpgraph_table');
    module_load_include('php', 'aspcm', 'library/jpgraph/jpgraph_text');
  
    $this->width = $size['width'];
    $this->height = $size['height'];
    $this->scale = $scale_type;
    
    if (!empty($coords)) {
      
      if (isset($coords['xaxis'])) {
        $this->xaxis = $coords['xaxis'];
      }
      
      if (isset($coords['yaxis'])) {
        $this->yaxis = $coords['yaxis'];
      }
      
      if (isset($coords['y2axis'])) {
        $this->y2axis = $coords['y2axis'];
      }
    }
    
    if (!empty($attributes)) {
      $this->attributes = $attributes;
    }
    
  }

  /**
   * Create the graph
   */
  function create_graph() {
    // Create a Graph object from JpGraph library
    $graph = new Graph($this->width, $this->height);   
    
    $graph->SetScale('textlin');
    $graph->SetBox(FALSE);
    $graph->SetFrame(FALSE);
    $graph->title->SetFont(FF_PGOTHIC, FS_NORMAL, 15);
    //$graph->SetImgFormat('png',100);
    
    if (!empty($this->yaxis)) {
      self::_set_yaxis_property($graph);
    }
    
    if (isset($this->attributes['title'])) {
      $graph->title->Set($this->attributes['title']);
    }
    
    // Specify X scale
    if (!empty($this->xaxis)) {
      self::_set_xaxis_property($graph);
    }
    
    if (!empty($this->y2axis)) {
      self::_set_y2axis_property($graph);
      $graph->SetY2OrderBack(FALSE);
    }
    
    //$graph->SetMarginColor('white');
    //$graph->SetColor('silver');
    
    if (!empty($this->attributes)) { 
      
      if (isset($this->attributes['margin'])) {
        $margin = $this->attributes['margin'];
        $graph->SetMargin($margin['lm'], $margin['rm'], $margin['tm'], $margin['bm']);
      } 
      
      if (isset($this->attributes['legend'])) {
        
        $legend = $this->attributes['legend'];
        
        $graph->legend->SetColumns(1);
        
        $graph->legend->SetFrameWeight(0);
        
        $graph->legend->SetShadow(FALSE);
        
        $graph->legend->Pos($legend['pos']['xcoord'], $legend['pos']['ycoord'], $legend['pos']['halign'], $legend['pos']['valign']);
        
        $graph->legend->SetHColMargin(10);
                
        $graph->legend->SetFillColor('white');
        
        $graph->legend->SetFont(FF_PGOTHIC, FS_NORMAL, 7.5);
                
        if ($this->height > 500) {
          $graph->legend->SetLineSpacing(10);
          $graph->legend->SetPos(0.01, 0.89, 'left', 'center');
        }
        else {
          $graph->legend->SetLineSpacing(8);
          $graph->legend->SetPos(0.01, 0.90, 'left', 'center');
        }
      }
      
    }
    
    // Use the data to create the graph 
    self::_create_graph($graph, $this->data, $this->graph_data);
    
      $max_y = 0;
      $max_y2 = 0;
      foreach((array)$this->data as $val){
          $pos = array_search(max($val['values']), $val['values']);
          if($val['axis'] == 'y'){
            $max_y = max($val['values'][$pos],$max_y);
          }elseif($val['axis'] == 'y2'){
              $max_y2 = max($val['values'][$pos],$max_y2);
          }
      }
    $path = create_folder_for_current_user();
    if (!empty($path)) {
        if($max_y == $max_y2){
            $graph->Stroke($path .'/'. $this->name .'.png',1);
        }else{
            $graph->Stroke($path .'/'. $this->name .'.png');
        }
    }
  }
  
  function _create_graph(&$graph, $data, $graph_data) {
    
    $bplots = array();
    // Put an empty array at the begining of the table (first row)
    // This will be used to algin the right labels of X-axis
    $tbl_data = array(array());
    
    foreach ($graph_data as $pinfo) {
      if (isset($pinfo['type'])) {
        if ($pinfo['type'] == 'line') {
          $lplot = new LinePlot($pinfo['values']);          
          
          if (isset($pinfo['axis']) && $pinfo['axis'] == 'y2') {
            $graph->AddY2($lplot);
          }
          else {
            $graph->Add($lplot);
          }
          
          self::_create_lineplot($lplot, $pinfo['attributes']);
          //$tbl_data[] = $pinfo['values'];
        }
        elseif ($pinfo['type'] == 'bar') {
          $bplots[] = new BarPlot($pinfo['values']);
          $attributes[] = $pinfo['attributes'];
          
          // User wants to not show major bar, only see lines
          // we use 0 to fill the Yaxis, since jpgraph needs to set Yaxis, but we don't show it
          /*if ($this->yaxis['hide'] != TRUE) {
            $tbl_data[] = $pinfo['values'];
          }*/
        }        
      }
    }
    
    if (!empty($bplots)) {
      // For multiple bars in the same charts, we need to use GroupBarPlots
      if (count($bplots) > 1) {
        
        $barplots = $bplots;
        
        // Finally, the bar plots will be grouped
        $gpPlots = new GroupBarPlot($barplots);
        //$gpPlots->SetColor(array('#ef74b3', '#73b8df', '#f9b785', '#68c8c6'));
        // Add to the graph
        $graph->Add($gpPlots);
      }
      else {
        $graph->Add($bplots[0]);
      }
      $i = 0;
      foreach ($bplots as $bplot) {
        self::_create_barplot($bplot, $attributes[$i]);
        $i++;
      }
    }
    
    foreach ($data as $pinfo) {
      if (isset($pinfo['type'])) {
        if ($pinfo['type'] == 'line') {
          $tbl_data[] = $pinfo['values'];
        }
        elseif ($pinfo['type'] == 'bar') {
          if ($this->yaxis['hide'] != TRUE) {
            $tbl_data[] = $pinfo['values'];
          }
        }
      }
    }

    // Table format
    $table = new GTextTable();
    
    // The position of the table is calculated by the properties of the whole chart
    // If the table only contains one row of data, to correct the position of the table by doing this
    if (count($tbl_data) == 2) {
      $top = $this->height - $this->attributes['margin']['bm'] - 10;
    }
    else {
      $top = $this->height - $this->attributes['margin']['bm'] + 20;
    }
    $left = $this->attributes['margin']['lm'];
    $tbl_width = $this->width - $this->attributes['margin']['lm'] - $this->attributes['margin']['rm'];
    
    $table->SetPos($left, $top);
    if ($this->height > 500) {
      $top = 0.89 * ($this->height - 11) - count($data) / 2 * 22;
      $table->SetPos($left, $top);
    }
    else {
      $top = 0.90 * ($this->height - 11) - count($data) / 2 * 20;
      $table->SetPos($left, $top - 5);
    }
    // Remove the outer-border of the table 
    $table->SetBorder(FALSE);
    // Set the number format
    $table->SetNumberFormat('%0.1f');
    
    //$table->SetFont(FF_DEFAULT, FS_NORMAL, DEFAULT_FONT_SIZE);
    
    $table->Set($tbl_data); 
    $table->SetAlign('center');
    
    // Since all the data has the same number of values, so we just get the second one
    // DO NOT use $tbl_data[0], because this is an empyt array
    $cols = count($tbl_data[1]);
    
    for ($i = 1; $i < $cols; $i++ ) {
      $table->SetColGrid($i, 0);
    }
    
    $table->SetRowGrid(1, 0);
    $table->SetMinRowHeight(22);
    $table->SetMinRowHeight(0, 0);
    $table->SetMinColWidth($tbl_width/$cols);
    $graph->Add($table);
    
    // Possible creations of text objects 
    if (!empty($this->text)) {
      $array = explode("\n", $this->text);
      $i = 0;
      foreach($array as $line) {
        $array1 = explode("：", $line, 2);

        if(count($array1) == 2) {
          $txt = new Text(($array1[0] . '：'), 13, 40 + $i*15); 
          $txt->SetFont(FF_PGOTHIC, FS_NORMAL, 10);
          $graph->AddText($txt);
          $txt1 = new Text($array1[1], $txt->Getwidth($graph->img) +18, 40 + $i*15); 
          $txt1->SetFont(FF_PGOTHIC, FS_NORMAL, 10);
          $txt1->SetColor('#535353');
          $graph->AddText($txt1);
        }
        else {
          $txt1 = new Text($array1[0], 60, 40 + $i*15); 
          $txt1->SetFont(FF_PGOTHIC, FS_NORMAL, 10);
          $txt1->SetColor('#535353');
          $graph->AddText($txt1);
        }
        
        $i++;
      }
    }
  }
  
  function _set_xaxis_property(&$graph) {
    if (isset($this->xaxis['title'])) {
      $graph->xaxis->title->Set($this->xaxis['title']);
      $graph->xaxis->title->SetFont(FF_PGOTHIC);
      $graph->xaxis->SetTitleMargin(25);
    }
    
    if (isset($this->xaxis['labels'])) {
      $graph->xaxis->SetTickLabels($this->xaxis['labels']);
      $graph->xaxis->SetFont(FF_PGOTHIC, FS_NORMAL, DEFAULT_FONT_SIZE);
    }
  }
  
  function _set_yaxis_property(&$graph) {
    $yscale = $this->scale;
    // Specify what scale we want to use
    if (isset($this->yaxis['max'])) {
      if ($this->yaxis['max'] == 0) {
        $this->yaxis['max'] = 1;
      }
      $graph->SetScale($yscale, 0, $this->yaxis['max']);
    }
    $graph->SetScale("textlin");
    if (isset($this->yaxis['mark'])) {
      $graph->yaxis->SetLabelFormat($this->yaxis['mark']);
    }
    
    if (isset($this->yaxis['hide']) && $this->yaxis['hide']) {
      $graph->yaxis->hide();
    }
      
    if (isset($this->yaxis['title']) && !empty($this->yaxis['title'])) {
      $graph->yaxis->title->Set($this->yaxis['title']);
      $graph->yaxis->title->SetFont(FF_PGOTHIC);
      $graph->yaxis->SetTitlemargin(50);
    }

    $graph->yaxis->SetFont(FF_DEFAULT, FS_NORMAL, DEFAULT_FONT_SIZE);
  }
  
  function _set_y2axis_property(&$graph) {
    // We have to define the scale before using y2axis object
    if (isset($this->y2axis['scale_type'])) {
      
      // Specifiy Y2 scale
      $y2scale = isset($this->y2axis['scale_type']) ? $this->y2axis['scale_type'] : 'lin';
      
      if (isset($this->y2axis['max'])) {
        if ($this->y2axis['max'] == 0) {
          $this->y2axis['max'] = 1;
        }
        $graph->SetY2Scale($y2scale, 0, $this->y2axis['max']);
      }
      else {
        $graph->SetY2Scale($y2scale, 0);
      }  
      //lxl_change
      $graph->y2axis->scale->SetGrace(3);
      if (isset($this->y2axis['mark'])) {
        $graph->y2axis->SetLabelFormat($this->y2axis['mark']);
      }
      
      if (isset($this->y2axis['title'])) {
        $graph->y2axis->title->Set($this->y2axis['title']);
        $graph->y2axis->title->SetFont(FF_PGOTHIC);
        $graph->y2axis->SetTitlemargin(65);
      }
      
      $graph->y2axis->SetFont(FF_DEFAULT, FS_NORMAL, DEFAULT_FONT_SIZE);
    }
  }
  
  function _create_lineplot($lineplot, $attributes) {    
    
    if (isset($attributes['color'])) {
      $lineplot->SetColor($attributes['color']);
    }
        
    if (isset($attributes['style'])) {
      $lineplot->SetStyle($attributes['style']);
    }
    
    if (isset($attributes['align']) && $attributes['align'] == 'center') {
      $lineplot->SetBarCenter();
    }
    
    if (isset($attributes['legend'])) {
      $legend = $attributes['legend'];
      $lineplot->SetLegend($legend);
    }
    
    if (isset($attributes['weight'])) {
      $lineplot->SetWeight($attributes['weight']);
    }
    
    if (isset($attributes['mark'])) {
      $lineplot->mark->SetType($attributes['mark']);
      if (isset($attributes['color'])) {
        $lineplot->mark->SetFillColor($attributes['color']);
      }
    }
    
    return $lineplot;
  }

  function _create_barplot($barplot, $attributes) {
    //$barplot = new BarPlot($data);
    
    if (isset($attributes['fillcolor'])) {
      $barplot->SetFillColor($attributes['fillcolor']);
    }
    
    if (isset($attributes['color'])) {
      $barplot->SetColor($attributes['color']);
    }
    
    if (isset($attributes['width'])) {
      $barplot->SetWidth($attributes['width']);
    }
    
    if (isset($attributes['align'])) {
      $barplot->SetAlign($attributes['align']);
    }
    
    if (isset($attributes['legend']) && $attributes['legend']) {
      $legend = $attributes['legend'];
      $barplot->SetLegend($legend);
    }
    
    if (isset($attributes['weight'])) {
      $barplot->SetWeight($attributes['weight']);
    }
    
    return $barplot;
  }
   
  function set_data($data) {
    $this->data = $data;
  }
  
  function set_name($name) {
    $this->name = $name;
  }
  
  function set_graph_data($graph_data) {
    $this->graph_data = $graph_data;
  }
  
  function set_text_data($text) {
    $this->text = $text;
  }
}