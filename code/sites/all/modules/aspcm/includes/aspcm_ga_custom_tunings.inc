<?php
/**
 * Coordinate with googleanaylitics module to generate some Javascript vairable objects
 * And add into the page
 */

// Call the function to generate custom variables
aspcm_ga_customvar_values();

/**
 * 
 * Call drupal_add_js to append Drupal.settings to include some custom variable values used by Google Analytics
 * 
 * Tracking user role.
 */
function aspcm_ga_customvar_values() {
  global $user;
  
  $user_role = 'anonymous user';
  
  if (in_array(ASPCM_ADMIN, $user->roles)) {
    $user_role = ASPCM_ADMIN;
  }
  elseif (in_array(ASPCM_SALES, $user->roles)) {
    $user_role = ASPCM_SALES;
  }
  elseif (in_array(ASPCM_REGULAR_USER, $user->roles)) {
    $user_role = ASPCM_REGULAR_USER;
  }
  elseif (in_array(ASPCM_SUPER_USER, $user->roles)) {
    $user_role = ASPCM_SUPER_USER;
  }
  
  drupal_add_js(array('aspcm' => array('userRole' => $user_role)), 'setting', 'header', FALSE, FALSE);
}