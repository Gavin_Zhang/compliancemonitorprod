<?php
/**
 * INC000006324309
 * @author Bala
 * 
 * Product class
 */
class Product {
  private static $instance;
  protected $products;
  
  function Product() {
    
  }
  
  /**
   * Retrieve all available products with provided parameters.
   * The second parameter, options, is used to filter the results with any of following fields: 
   * company name, product form (alcohol, soap), product name and either JnJ or Non-JnJ products.
   * 
   * @param mixed $nid, array or integer
   * @param array $options
   */
  function get_products($nid, $options = array()) {
    $results = array();
    
    if (empty($nid) && empty($options)) {
      return $results;
    }
    
    $query = "SELECT 
              fac.field_aspcm_company_value AS cname,
              n.nid AS pid,
              n.title AS pname,
              apcd.field_aspcm_product_category_des_value AS category_name,
              fapt.field_aspcm_product_type_value AS ptype,
              fapv.field_aspcm_product_volume_value AS volume,
              aijp.field_aspcm_is_jnj_product_value AS isjnj,
              avpp.field_aspcm_volume_per_push_value AS perpush,
              fapo.field_aspcm_product_order_value AS weight     
               FROM node AS n
              LEFT JOIN field_data_field_aspcm_company AS fac ON n.nid = fac.entity_id
              LEFT JOIN field_data_field_aspcm_ref_product_category AS arpc ON n.nid = arpc.entity_id
              LEFT JOIN field_data_field_aspcm_product_type AS fapt ON n.nid = fapt.entity_id
              LEFT JOIN field_data_field_aspcm_product_volume AS fapv ON n.nid = fapv.entity_id
              LEFT JOIN field_data_field_aspcm_is_jnj_product AS aijp ON n.nid = aijp.entity_id
              LEFT JOIN field_data_field_aspcm_product_order AS fapo ON n.nid = fapo.entity_id
              LEFT JOIN field_data_field_aspcm_product_available AS fapa ON n.nid = fapa.entity_id
              LEFT JOIN field_data_field_aspcm_volume_per_push AS avpp ON n.nid = avpp.entity_id
              INNER JOIN field_data_field_aspcm_product_category_des AS apcd ON apcd.entity_id = arpc.field_aspcm_ref_product_category_nid
              WHERE n.type = 'aspcm_product' AND fapa.field_aspcm_product_available_value = 'start'";
    
    if (!empty($nid)) {
      if (is_array($nid)) {
        $nid = implode(',', $nid);
        $query .= " AND n.nid IN ({$nid})";
      }
      else {
        $query .= " AND n.nid = {$nid}";
      }
    }
    else {
      if (isset($options['cname']) && !empty($options['cname'])) {
        $query .= " AND fac.field_aspcm_company_value = '{$options['cname']}'";
      }
      
      if (isset($options['pgroup']) && !empty($options['pgroup'])) {
        $query .= " AND apcd.field_aspcm_product_category_des_value = '{$options['pgroup']}'";
      }
      
      if (isset($options['pname']) && !empty($options['pname'])) {
        $query .= " AND n.title LIKE '{$options['pname']}%'";
      }
      
      if (isset($options['isjnj']) && !empty($options['isjnj'])) {
        $query .= " AND aijp.field_aspcm_is_jnj_product_value = '{$options['isjnj']}'";
      }
    }
    $query .= ' ORDER BY weight ASC';
    
    $result = db_query($query);
    
    while ($item = $result->fetchAssoc()) {
      if ($item['cname'] == 'JnJ') {
        $item['cname'] = 'ALJ';//update by lina
      }
      $results[$item['pid']] = $item;
    }
    
    return $results;
  }
    
  /**
   * Get companies list with provide company name. Use stristr to match the names.
   */  
  function get_product_companies() {
    $items = array();

    $sql = "SELECT data 
            FROM field_config
            WHERE field_name = 'field_aspcm_company'";      
    $result = db_query($sql);
    if ($value = $result->fetchObject()) {
      $settings = @unserialize($value->data);
      $set = $settings["settings"]['allowed_values'];
      $list = array_map('trim', $set);
      $items = array_filter($list, 'strlen');
    }
    return $items;
  }
  
  /**
   * Get products list from database with the provided ward node id,
   * first will find the hospital node id, then find the products belongs to this hospital. 
   * 
   * @param $wid
   *  Ward
   * 
   * @return an array of products with product node id, name, category, standard volume, jnj/no-jnj, perpush value, available, weight(ordervalue), ptype, measurement unit and so on. 
   */
  function get_products_by_ward($wid) {
    $products = array();
	
	//get product category
    $product_category_arr = get_product_category();
	
	//get ward product unit
	$ward_product_unit = get_ward_product_unit($wid);
	
  //Region begin: Fixed for ticket NO.28.
  $sql = "SELECT ward.field_aspcm_ref_hospital_nid,hosp.entity_id,n.nid AS nid,
          arpc.field_aspcm_ref_product_category_nid AS catid, 
          n.title AS title,   
          apv.field_aspcm_product_volume_value AS volume,
          aijp.field_aspcm_is_jnj_product_value AS jnj,
          avpp.field_aspcm_volume_per_push_value AS perpush,
          apa.field_aspcm_product_available_value AS available,
          apo.field_aspcm_product_order_value AS ordervalue,
          apt.field_aspcm_product_type_value AS ptype,
          hosp.field_aspcm_product_per_push AS hid_perpush
          FROM node AS n
        LEFT JOIN field_data_field_aspcm_ref_product_category AS arpc ON n.nid = arpc.entity_id
        LEFT JOIN field_data_field_aspcm_is_jnj_product AS aijp ON n.nid = aijp.entity_id
        LEFT JOIN field_data_field_aspcm_volume_per_push AS avpp ON n.nid = avpp.entity_id
        LEFT JOIN field_data_field_aspcm_product_volume AS apv ON n.nid = apv.entity_id
        LEFT JOIN field_data_field_aspcm_product_available AS apa ON n.nid = apa.entity_id
        LEFT JOIN field_data_field_aspcm_product_order AS apo ON n.nid = apo.entity_id
        LEFT JOIN field_data_field_aspcm_product_type AS apt ON n.nid = apt.entity_id
        INNER JOIN field_data_field_aspcm_ref_products AS hosp ON hosp.field_aspcm_ref_products_nid = n.nid 
        INNER JOIN field_data_field_aspcm_ref_hospital AS ward ON ward.field_aspcm_ref_hospital_nid = hosp.entity_id
        WHERE (n.type = 'aspcm_product' AND ward.entity_id = :ward_nid AND apa.field_aspcm_product_available_value = 'start')
        ORDER BY ordervalue, hosp.entity_id";
  //Region end: Fixed for ticket NO.28.
  
    $result = db_query($sql, array(':ward_nid' => $wid));
    foreach ($result as $row) {
      $product_cat = isset($product_category_arr[$row->catid]) ? $product_category_arr[$row->catid] : '';
	  
      if ($product_cat == ALCOHOL_JP) {
        $product_cat = ALCOHOL_JP;
        $type = $ward_product_unit['alcohol'] == 'bottle' ? 'bottle' : 'ml';//t('bottle') : t('ml');
      } 
      else {
        $product_cat = SOAP_JP;
        $type = $ward_product_unit['soap'] == 'bottle' ? 'bottle' : 'ml';
      }
           
      array_push($products, 
        array(
          'nid' => $row->nid,
          'title' => check_plain($row->title),
          'category' => check_plain($product_cat),
          'type' => $type,
          'jnj' => ($row->jnj == 'yes')? 1 : 0,
          'available' => ($row->available == 'start') ? 1 : 0,
          'order' => $row->ordervalue,
          'ptype' => check_plain($row->ptype),
          'volume' => $row->volume,
          //Region begin: Fixed for ticket NO.28
          //'perpush' => $row->perpush
          'perpush' => isset($row->hid_perpush) ? $row->hid_perpush : $row->perpush
          //Region end: Fixed for ticket NO.28
        ));
    }
    
    return $products;
  }

  function get_products_by_hid($hid) {
    $products = array();

    if (!empty($hid)) {
      $query = "SELECT n.nid AS nid,
                ac.field_aspcm_company_value AS cname,
                cat.field_aspcm_product_category_des_value AS catename,
                n.title AS pname,
                avpp.field_aspcm_volume_per_push_value AS perpush,
                hosp.field_aspcm_product_per_push AS custom_perpush
                FROM node AS n
                LEFT JOIN field_data_field_aspcm_company AS ac ON n.nid = ac.entity_id
                LEFT JOIN field_data_field_aspcm_ref_product_category AS arpc ON n.nid = arpc.entity_id
                LEFT JOIN field_data_field_aspcm_volume_per_push AS avpp ON n.nid = avpp.entity_id
                LEFT JOIN field_data_field_aspcm_product_order AS apo ON n.nid = apo.entity_id
                LEFT JOIN field_data_field_aspcm_product_available AS apa ON n.nid = apa.entity_id
                INNER JOIN field_data_field_aspcm_ref_products AS hosp ON hosp.field_aspcm_ref_products_nid = n.nid  
                INNER JOIN field_data_field_aspcm_product_category_des AS cat ON cat.entity_id = arpc.field_aspcm_ref_product_category_nid   
                WHERE (n.type = 'aspcm_product' AND hosp.entity_id = :nid AND apa.field_aspcm_product_available_value = 'start')
                ORDER BY apo.field_aspcm_product_order_value ASC";

      $result = db_query($query, array(':nid' => $hid));
      while ($row = $result->fetchAssoc()) {
        if ($row['cname'] == 'JnJ') {
          $row['cname'] = 'ALJ';//update by lina
        }
        $products[$row['nid']] = array(
          'cname' => $row['cname'],
          'category_name' => $row['catename'],
          'pname' => $row['pname'],
          'perpush' => $row['perpush'],
          'custom_perpush' => $row['custom_perpush'],
          'pid' => $row['nid'],
        );
      }
    }
    return $products;
  }
  
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new Product();
    }
    
    return self::$instance;
  }
  /*
  function get_hospitals_used_products($hosp_ids) {
    $results = array();
    
    $storage = get_storage_instance('aspcm_hospital');
    
    if ($storage) {
      $data = $storage->load_data_ids($hosp_ids);
      
      if ($data) {
        $results = array_map('_get_hospital_only_products', $data);
      }
    }
 
    return $results;
  }
  
  private function _get_hospital_only_products($var) {
    return $var['nid'] = $var['seletect_products'];
  }
  */
}