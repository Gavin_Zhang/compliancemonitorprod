<?php

module_load_include('inc', 'aspcm', 'includes/aspcm_extended_survey_data.class');

class SurveyClient {
  
  protected $survey_id;
  protected $site_id;
  protected $user_id;
  protected $user_ids;
  protected $session_id;
  protected $current_page_no;
  protected $question_per_page;
  protected $answer_obj;

  function SurveyClient($survey_id, $site_id) {
    $this->survey_id = $survey_id;
    $this->site_id = $site_id;
    
    // for the service we are using (retake), we will never need to change these values
    $this->current_page_no = 1;
    $this->question_per_page = 100;
  }
  
  function get_user_id () {
    return $this->user_id;
  }
  
  function set_user_id($id) {
    if (!is_int($id)) {
      throw new Exception('User ID must be integer');
    }
    
    $this->user_id = (int) $id;
    return TRUE;
  }
  
  function set_user_ids($ids) {
    $this->user_ids = $ids;
  }
  
  function get_user_ids() {
    return $this->user_ids;
  }
  
  function get_session_id() {
   if (empty($this->session_id)) {
      $session_value = md5(time());
      $this->session_id = $session_value;
   }
   
   return $this->session_id;
  }
  
  function set_session_id($id) {
    $this->session_id = (string)$id;
    return TRUE;
  }
  
  function set_answer_data($data) {
    if (!is_array($data)) {
      throw new Exception('Data must be an array');
    }

    // Answers that will be stored in the Survey
    foreach ($data as $idx => $item) {
    
      if (!is_array($item) || !isset($item['question']) || !isset($item['answer'])) {
        throw new Exception('Data is not using the correct format');
      }
    
      $obj->EvaluationResult->ResultInfo[$idx]->Question_id = $item['question']; // question id
      $obj->EvaluationResult->ResultInfo[$idx]->Answer_id = 0; // if the answer is a text, this will be zero
      $obj->EvaluationResult->ResultInfo[$idx]->Score = 0; // just leave as zero. not sure why this is needed
      $obj->EvaluationResult->ResultInfo[$idx]->type = ''; // just leave as empty. not sure why this is needed
      $obj->EvaluationResult->ResultInfo[$idx]->ResponseText = $item['answer']; // value of the answer (usually text)
      
      // check if the answer is just a number...
      if (is_numeric($item['answer']) ){
        //... if so, the answer ID will be this number, not zero.
        $obj->EvaluationResult->ResultInfo[$idx]->Answer_id = $item['answer'];
        
        // and the responseText is not a text, so empty will be fine
        $obj->EvaluationResult->ResultInfo[$idx]->ResponseText = '';
        
        
        /*
        Why is this needed?
        This is needed because some answers will have a predefined value in the dashboard.
        For example, if you add a Radion Button question type in the dashboard, you can add the
        answers for it. Each answer can have a text associated, but the dashboard will create a number
        ID for this answer. This ID will be the value in the HTML.
        So you will have something like this HTML:
        <input type='radio' value='1234'> Yes
        <input type='radio' value='6789'> No
        
        So, when the user answer yes, dashboard will expect the integer value 1234, not the string
        'Yes'
        */
      }
    }

    // Common data
    $obj->EvaluationResult->Evaluation_id = $this->survey_id;
    $obj->EvaluationResult->UserSession_id = $this->get_session_id();
    $obj->EvaluationResult->User_id = $this->get_user_id();
    $obj->EvaluationResult->firstName = 'null';
    $obj->EvaluationResult->lastName = 'null';
    $obj->EvaluationResult->page = $this->current_page_no;
    $obj->EvaluationResult->type = 'null';
    $obj->EvaluationResult->Status = TRUE;
    
    $this->answer_obj = $obj;
    
    return TRUE;

  }
  
  function save_answer() {
  
    if (empty($this->answer_obj)) {
      throw new Exception('No answer informed. Use set_answer_data before you call ' . __FUNCTION__);
    }
  
    // Create the SurveyData object that will transmit the information to e-mkt dashboard
    $survey_data = $this->get_survey_data_object('save');
    
    // pass the answers to the SurveyData object
    $survey_data->SetPostedData($survey_data->DataEncode($this->answer_obj));    
    
    // Create a new Survey object and send the data through it
    $survey = new Survey();
    return $survey->create_evaluation($survey_data);
  }
  
  function retrieve_answer() {
    $survey_data = $this->get_survey_data_object('retake');
    
    $survey = new Survey();
    $res = $survey->get_evaluation_retake_answers($survey_data);
    
    return $res; 
  }
  
  function retrieve_answer_list() {
    print_info("retrieving answer list");
    $survey_data = $this->get_survey_data_object('usersRetake');

    $survey = new Survey();
    $res = $survey->get_evaluation_retake_answers($survey_data);

    return $res; 
  }
  
  function get_survey_data_object($operation) {
    $obj = new ExtendedSurveyData();
    $obj->SetServiceName('evaluation'); // this is how the service call an 'answer'
    $obj->SetSiteId($this->site_id); // the ID of this site, created inside the dashboard
    
    switch ($operation) {
      case 'save':
        $obj->SetServiceIdentity('create'); // the operation we are going to do
        $obj->SetMethod('post');            // method of the data transmission
      $service_url = GlobalSettings::getServiceRegistryUrl();
      if (!empty($service_url)) {
          print_info('setting the service URL directly');
          // use this way if the service that discovers usersRetake method is not being able to find it.
          //$base_url = "http://jnjdevjbs02.cit:8080/fwk_Evaluation_API_v2.0/usersRetake";

          $obj->SetServiceURL($service_url);
      }
        break;
        
      case 'retake':
        $obj->SetServiceIdentity('retake'); // the operation we are going to do
        $obj->SetMethod('get');                             // method of the data transmission
        $obj->set_evaluation_id($this->survey_id);          // survey id created in the dashboard
        $obj->set_current_page_no($this->current_page_no);
        $obj->set_user_id($this->get_user_id());
        $obj->set_no_of_questions_per_page($this->question_per_page);
        $obj->set_user_operation('retake');
      $service_url = variable_get('aspcm_data_ws_location', '');
      if (!empty($service_url)) {
          print_info('setting the service URL directly');
          // use this way if the service that discovers usersRetake method is not being able to find it.
          //$base_url = "http://jnjdevjbs02.cit:8080/fwk_Evaluation_API_v2.0/usersRetake";
          $service_url .= '?evaluationId='.$this->survey_id .
              '&userIds=' . $this->get_user_id();
          $obj->SetServiceURL($service_url);
      }
        break;
        
      case 'usersRetake':
        $obj->SetServiceIdentity('usersRetake'); // the operation we are going to do
        $obj->SetMethod('get');                             // method of the data transmission
        $obj->set_evaluation_id($this->survey_id);          // survey id created in the dashboard
        $obj->set_current_page_no($this->current_page_no);
        $obj->set_user_ids($this->get_user_ids());
        $obj->set_no_of_questions_per_page($this->question_per_page);
        
        $service_url = variable_get('aspcm_data_ws_location', '');
        if (!empty($service_url)) {
          print_info('setting the service URL directly');
          // use this way if the service that discovers usersRetake method is not being able to find it.
          //$base_url = "http://jnjdevjbs02.cit:8080/fwk_Evaluation_API_v2.0/usersRetake";
          $service_url .= '?evaluationId='.$this->survey_id . 
            '&pageNumber=' . $this->current_page_no . 
            '&pageSize=' . $this->question_per_page .
            '&userIds=' . $this->get_user_ids();
          $obj->SetServiceURL($service_url);
        }
        
        break;
        
      default:
        throw new Exception("Operation not supported: $operation");
    }
    
    return $obj;
  }

}