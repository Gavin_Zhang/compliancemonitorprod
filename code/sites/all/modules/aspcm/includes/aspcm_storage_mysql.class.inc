<?php
module_load_include('inc', 'aspcm', 'includes/aspcm_storage_generic.class');

class ASPCMMysql extends StorageGeneric {
  const TABLENAME = "safe_table";
  
  function ASPCMMysql ($print_info) {
    $this->table_name = self::TABLENAME;
    $this->print_info = $print_info;
  }  
  
  function save_data($data) {
    // check if all necessary data was given
    if (!$this->check_set_ids(__FUNCTION__)) {
      return FALSE;
    }
  
    // check if the safe table already exists
    $tables = $this->db_get_tables();
    if (!in_array($this->table_name, $tables)) {
      // try to create the safe table
      if (!$this->db_create_safe_table()) {
        return FALSE;
      }
    }
  
    // check if the data already exists in the database
    $sql = "
      SELECT *
      FROM {$this->table_name}
      WHERE site_id = :sid AND type_id = :tid AND info_id = :iid";
    $res = db_query($sql, array(':sid' => $this->site_id, 
      ':tid' => $this->type_id, 'iid' => $this->info_id));    

    if ($row = $res->fetchObject()) {
      // update the existing data
      $sql = "
        UPDATE {$this->table_name}
        SET data = :data
        WHERE site_id = :sid AND type_id = :tid AND info_id = :iid";
        
    } else {
    
      // save the data into safe table
      $sql = "
        INSERT INTO {$this->table_name} (data, site_id, type_id, info_id)
        VALUES (:data, :sid, :tid, :iid)
      ";
    }

    if (!db_query($sql, array(':data' => $data, 
      ':sid' => $this->site_id, ':tid' => $this->type_id, ':iid' => $this->info_id))) {
      $this->debug_log(__METHOD__ . ": ERROR: Failed to save the data into the {$this->table_name}\n$sql");
      return FALSE;
    }
    
    return TRUE;
  }
  
  function load_data($info_id) { 
    // check if all necessary data was given
    if (!$this->check_set_ids(__FUNCTION__)) {
      return FALSE;
    }
    
    $sql = "
      SELECT *
      FROM {$this->table_name}
      WHERE site_id = :sid AND type_id = :tid AND info_id = :iid";
    $res = db_query($sql, array(':sid' => $this->site_id,
      ':tid' => $this->type_id, ':iid' => $info_id));

    if ($row = $res->fetchObject()) {
      $row->data = $this->decode_data($row->data);
    } else {
      $debug_sql = "
        SELECT *
        FROM %s
        WHERE site_id = %d AND type_id = %d AND info_id = %d";
      $debug_sql = sprintf($debug_sql, $this->table_name, $this->site_id, $this->type_id, $info_id);
      $this->debug_log(__METHOD__ . ": ERROR: NO DATA FOUND for node ID #{$info_id}\nContent Type: $this->content_type\nDebug SQL:\n$debug_sql");
      return FALSE;
    }

    return $row->data;
  }
  
  function load_data_ids($info_ids) {
    // check if all necessary data was given
    if (!$this->check_set_ids(__FUNCTION__)) {
      return FALSE;
    }
  
    // get the content based on the ids
    $content = array(); // this is the default value if nothing is found
    
    
    $sql = "
      SELECT *
      FROM {$this->table_name}
      WHERE site_id = :site_id AND type_id = :type_id AND info_id IN (:info_id)";
    try {
    $res = db_query($sql, array(':site_id' => $this->site_id, 
      'type_id' => $this->type_id, ':info_id' => $info_ids));
    }
    //The \ just denotes that PDOException is in the root namespace, require php 5.3.0
    catch (\PDOException $e) {
      $error = $e->getMessage();
      $debug_sql = "
        SELECT *
        FROM %s
        WHERE site_id = %d AND type_id = %d AND info_id IN (%s)";
      $debug_sql = sprintf($debug_sql, $this->table_name, $this->site_id, $this->type_id, implode(',', $info_ids));
      $this->debug_log(__METHOD__ . ": ERROR: NO DATA FOUND for node ID #{$info_ids}\nContent Type: $this->content_type\nDebug SQL:\n$debug_sql");
      return FALSE;
    }
    
    // although a new variable could be created to receive the array_flip content, is better from
    //  from the memory utilization point of view use this way.
    $info_ids = array_flip($info_ids);
    foreach ($res as $row) {
      $info_ids[$row->info_id] = $this->decode_data($row->data);
    }
    
    // return the content found
    return $info_ids;
  }

  function db_get_tables() {
    $tables = array();
    
    $db_name = $this->db_get_current_db();
    
    $sql = "SELECT TABLE_NAME, TABLE_COMMENT
            FROM information_schema.TABLES
            WHERE TABLE_SCHEMA='{$db_name}'";
    $res = db_query($sql);
    
    foreach ($res as $row) {
      $tables[] = $row->TABLE_NAME;
    }
    
    return $tables;
  }
  
  function db_get_current_db($refresh = FALSE) {
    if (!empty($this->db_name) && !$refresh) {
      return $this->db_name;
    }
    
    $sql = "SELECT DATABASE() AS db_name";
    $res = db_query($sql);
    $row = $res->fetchObject();
    $this->db_name = $row->db_name;
    
    return $this->db_name;
  }
  
  function db_create_safe_table() {
    $db_name = $this->db_get_current_db();
    
    $sql = "CREATE TABLE `{$db_name}`.`{$this->table_name}` (
              `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
              `site_id` INT UNSIGNED ,
              `type_id` INT UNSIGNED ,
              `info_id` INT UNSIGNED ,
              `data` TEXT ,
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC";
            
    if (!db_query($sql)) {
      $this->debug_log(__METHOD__ . ": ERROR: Failed to create safe table");
      return FALSE;
    }
    
    return TRUE;
  }
}