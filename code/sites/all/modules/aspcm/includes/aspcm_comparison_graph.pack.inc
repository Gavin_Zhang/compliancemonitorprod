<?php
/**
 * Add goveronment and clinical departments component to comparison generation page.
 *  
 * User will be able to choose selected hospital's departments when comparing with the hospitals.
 */

/**
 * Define form elements of my departments 
 * 
 * @param $form
 * @param $form_state
 * @param $params, pass additional parameters using this array
 */
function aspcm_comparison_my_departments_form_elements(&$form, $form_state, $params = array()) {
  //debug_write_log(print_r($form_state, 1));
  // Show the elements only for J&J Sales and Super User 
  if (!in_array(ASPCM_SALES, $params['user_profile']->roles) && !in_array(ASPCM_SUPER_USER, $params['user_profile']->roles)) { 
    return;
  }
  
  // The user is a sales man
  if (in_array(ASPCM_SALES, $params['user_profile']->roles)) {
    //$params['user_profile']->asp_hospitals_management
    if(isset($form_state['values']['hospital']) && !empty($form_state['values']['hospital'])) {
      $hospital_nid = get_hospital_id_by_hospitalname($form_state['values']['hospital']);
    }
  }
  else {
    $hospital_nid = $params['user_profile']->asp_hospital_id;
  }
  
  $own_gov_depts = array();
  $own_clin_depts = array();
  $depts = array();
  
  if (!empty($hospital_nid)) {
    $ward_nids = get_ward_id_by_hos_id($hospital_nid);
    $wards = get_wards_by_nid($ward_nids);
    $store_dept_wards = array();
    
    foreach ($wards as $k => $w) {
      $store_dept_wards[$w['field_aspcm_ref_clindeptmnt']['0']['nid']][] = $k; // ward node id
      $store_dept_wards[$w['field_aspcm_criterial_level']['0']['nid']][] = $k;
      $own_gov_depts[$w['field_aspcm_ref_clindeptmnt']['0']['nid']] = $params['gov_depts'][$w['field_aspcm_ref_clindeptmnt']['0']['nid']];
      $own_clin_depts[$w['field_aspcm_criterial_level']['0']['nid']] = $params['clin_depts'][$w['field_aspcm_criterial_level']['0']['nid']];
    }
  }
  // Sorting
  // Use the order of original entries which were selected from database sorted by weight 
  $own_gov_depts = array_intersect($params['gov_depts'], $own_gov_depts);
  $own_clin_depts = array_intersect($params['clin_depts'], $own_clin_depts);
  
  /* If you selected nothing, all the options will be selected by default */
  $own_gov_depts = array_unique($own_gov_depts);
  $own_gov_depts = array_filter($own_gov_depts); // remove empty value item
  
  // If submitted
  if (isset($form_state['values']['major_value'])) {
    $gov_depts_disp = ($form_state['values']['major_value'] == 'compliance_rate') ? 'none' : 'block';
  }
  // not submitted
  else if (isset($form['major_value']['#default_value'])) {
    $gov_depts_disp = ($form['major_value']['#default_value'] == 'compliance_rate') ? 'none' : 'block';
  }

  $form['own_government_dept'] = array(
    '#type' => 'select',
    '#attributes' => array('style' => 'display:'. $gov_depts_disp),
    //'#title' => t('Choose my departments'), // the title is defined in the template file, see template/aspcm-form-comparison-graph.tpl.php
    '#options' => $own_gov_depts,
    '#multiple' => TRUE,
    '#default_value' => !isset($form_state['values']['own_government_dept']) ? array_keys($own_gov_depts) : array_values($form_state['values']['own_government_dept']),
  ); 
  
  $own_clin_depts = array_unique($own_clin_depts);
  $own_clin_depts = array_filter($own_clin_depts);
  
  $form['own_clinical_dept'] = array(
    '#type' => 'select',
    '#attributes' => array('style' => 'display:'. ($gov_depts_disp == 'block' ? 'none' : 'block')),
    //'#title' => t('Choose my departments'), 
    '#options' => $own_clin_depts, 
    '#multiple' => TRUE,
    '#default_value' => !isset($form_state['values']['own_clinical_dept']) ? array_keys($own_clin_depts) : array_values($form_state['values']['own_clinical_dept']),
  );
  
  // This is overwriting the hospital elements definition by adding ahah and suffix
  // see: forms/aspcm_comparison_graph.inc
  //
  // Autocomplete, when text changing will triggle ajax call to get my hospital's departments
  $form['hospital']['#ajax'] = array(
    'event' => '', // don't put any values, because this event trigger is customized
    'path' => 'my/departments/ajax',
    'wrapper' => 'edit-ward-wrapper',
    'method' => 'replace',
    'effect' => 'fade',
  );
  $form['hospital']['#attributes'] = array('style' => 'width: 238px;');
  $form['hospital']['#suffix'] = '<a href="#" class="gcomp-search-autoc"></a>';
    
  // Hidden filed to store the department
  $form['own_dept_wards'] = array(
    '#type' => 'hidden',
    '#value' => empty($store_dept_wards) ? '' : serialize($store_dept_wards),
  );
  
  // Store current user's hospital id
  // will be used in submit function to determine if the user is super user
  $form['user_hospital'] = array(
    '#type' => 'hidden',
    '#default_value' => $params['user_profile']->asp_hospital_id,
  );
  
  // To make sure after calling ajax still calling to this url
  $form['#action'] = '/graph/comparison-graph';
}

/**
 * This function is called by aspcm_comparison_graph_departments_form_submit.
 * 
 * By adding some new elements in form_state, such as my departments,
 * meanwhile, my wards is defined also to save the wards which contains the selected departments.
 * 
 * The main purpose is to pass the above described values to build a graph with the selected departments in my hospital.
 * 
 * @param $form
 * @param $form_state
 * @param $params, pass additional parameters using this array
 */
function aspcm_comparison_my_departments_form_submit($form, &$form_state, $params = array()) {
  if (!isset($form_state['values']['user_hospital'])) {
    return;
  }
  
  // Define an array to hold ward node ids
  $form_state['values']['own_wards'] = array();
  
  // The wards nids belongs to the selected departments
  $dept_wards = @unserialize($form_state['values']['own_dept_wards']);
  // Selected departments nids
  $dept_ids = $dept_wards ? array_keys($dept_wards) : array();
  
  if ($form_state['values']['major_value'] == 'compliance_rate') {
    $form_state['values']['own_department'] = $form_state['values']['own_clinical_dept'];
    $temp_dept_ids = drupal_map_assoc($form_state['values']['own_clinical_dept']);
    // Get departments' names
    $temp_dept_names = array_intersect_key($form['own_clinical_dept']['#options'], $temp_dept_ids);
    
    // The string of selected departments of current user's own hospital's
    // and will be used as legend displaying above the graph 
    $form_state['values']['own_department_name'] = implode('、', $temp_dept_names);
    
    foreach ($form_state['values']['own_clinical_dept'] as $val) {
      if (in_array($val, $dept_ids)) {
        $form_state['values']['own_wards'] = array_merge($form_state['values']['own_wards'], $dept_wards[$val]);
      }
    }
  }
  else {
    $form_state['values']['own_department'] = $form_state['values']['own_government_dept'];
    $temp_dept_ids = drupal_map_assoc($form_state['values']['own_government_dept']);
    $temp_dept_names = array_intersect_key($form['own_government_dept']['#options'], $temp_dept_ids);
    
    // The string of selected departments of current user's own hospital's
    // and will be used as legend displaying above the graph
    $form_state['values']['own_department_name'] = implode('、', $temp_dept_names);
    
    foreach ($form_state['values']['own_government_dept'] as $val) {
      if (in_array($val, $dept_ids)) {
        $form_state['values']['own_wards'] = array_merge($form_state['values']['own_wards'], $dept_wards[$val]);
      }
    }
  }
}

/**
 * Combine the legend with my departments selected 
 * 
 * @param $legend
 * @param $params
 */
function aspcm_comparison_my_departments_legend($legend, $params) {
  if (isset($params['own_department_name']) && !empty($params['own_department_name'])) {
    $legend .= t('My departments：') . $params['own_department_name'] ."\n";
  }
  return $legend;
}

/**
 * Rebuild the form when my/departments/ajax menu called
 * 
 * So, when user selected hospital name, to rebuild the form to show the departments belongs to the selected hospital
 */
function aspcm_comparison_my_departments_form_rebuild() {
  if (isset($_POST['hospital'])) {
    $form_state = array('storage' => NULL, 'submitted' => FALSE);
    $form_build_id = $_POST['form_build_id'];
    
    // Retrieve the form from the cache.
    $form = form_get_cache($form_build_id, $form_state);
    
    // Preparing for processing the form.
    //$args = $form['#parameters'];
    //$form_id = array_shift($args);
    // Since some of the submit handlers are run, redirects need to be disabled.
    $form_state['no_redirect'] = TRUE;
    // When a form is rebuilt after Ajax processing, its #build_id and #action
    // should not change.
    // @see drupal_rebuild_form()
    $form_state['rebuild_info']['copy']['#build_id'] = TRUE;
    $form_state['rebuild_info']['copy']['#action'] = TRUE;
    $form_id = $form['#form_id'];
    $form_state['post'] = $form['#post'] = $_POST;
    $form_state['input'] = $_POST;
    
    $form['#programmed'] = $form['#redirect'] = FALSE;
    $form_state['rebuild'] = TRUE;
    
    $temp_hospital = aspcm_get_hospital_ids($_POST['hospital']);
    //$form_state['values']['hospital_id'] = count($temp_hospital) ? array_shift($temp_hospital) : NULL;
    
    // Set the hospital value, will be used to retrieve the clinical departments
    // See: aspcm_comparison_my_departments_form_elements()
    $form_state['values']['hospital'] = $_POST['hospital'];
    $form_state['values']['major_value'] = $_POST['major_value'];

    // To avoid function not found for rebuiding aspcm_comparison_graph_form
    if (!function_exists($form_id)) {
      module_load_include('inc', 'aspcm', '/forms/aspcm_comparison_graph');
    }
    
    //$form = drupal_retrieve_form($form_id, $form_state);
    //form_set_cache($form_build_id, $form, $form_state);
    //$form = form_builder($form_id, $form, $form_state);
    
    // This function calls the submit handlers,
    // which put whatever was worthy of keeping into $form_state.
    //drupal_process_form($form_id, $form, $form_state);
    
    // Call drupal_rebuild_form which destroys $_POST, this will call build the form again
    // Form generator function is called and creates the form again
    // The new form gets cached and processed again, but because $_POST is destroyed, the submit handlers will not be called again.
    $form = drupal_rebuild_form($form_id, $form_state, $form);
    
    $gov_depts = drupal_render($form['own_government_dept']);
    $clin_depts = drupal_render($form['own_clinical_dept']);
    $dept_wards = drupal_render($form['own_dept_wards']);
    
    drupal_json_output(drupal_json_output(
                  array('status' => TRUE, 
                        'data' => array('own_government_dept' => $gov_depts, 
                                        'own_clinical_dept' => $clin_depts
                                  )
                  )
               )
    );
  }
}

/**
 * Define menu callback
 * @param array $items
 */
function aspcm_comparison_my_departments_menu(&$items) {
  $items['my/departments/ajax'] = array(
    'page callback' => 'aspcm_comparison_my_departments_form_rebuild',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
}
