<?php
/**
 * INC000006324270, Show compliance rate value in monthly data input page
 * 
 */

/**
 * sub function of hook_menu().
 * @see aspcm.module::aspcm_menu()
 * Add a new callback menu to retrieve compliance rate value 
 *  
 * @param $items
 */
function aspcm_ward_monthly_data_input_menu(&$items) {
  $items['get/compliance-rate'] = array(
    'page callback' => 'aspcm_ward_monthly_get_compliance_rate',
    'access callback'  => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
}

/**
 * Form element to display compliance rate value on the page
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function aspcm_ward_monthly_data_input_form_form_elements(&$form, $form_state) {
  $text = t('N/A');
  
  $form['compliance_rate'] = array(
    '#markup' => '<label>'. t('Compliance Rate_1') .'： </label><span id="compliance-rate" style="width: 115px;background:none;">'. $text .'</span>',
  );
}

/**
 * 
 * Ajax menu callback function, to calculate compliance rate of current form submission 
 * 
 * Echo the value 
 */
function aspcm_ward_monthly_get_compliance_rate() {
  echo aspcm_calc_compliance_rate($_POST);
}

/**
 * Calculate the compliance rate with the posted values
 * 
 * @param $data
 */
function aspcm_calc_compliance_rate($data) {
  // Define an array to hold products values
  $products = array();
  
  // Define an array to hold severity_levels values
  $severity_levels = array();
  // Define an array to hold number of days that severity_level has value filled 
  $severity_levels_count = array();
  
  // number of days for the month
  $count_days_of_month = $data['count_days'];
  // user choosed calculation model, actual or estimated
  $calc_model = isset($data['severity_level']) ? $data['severity_level'] : $data['severity_level_hide'];
  
  // Define an array to hold the other values
  $params = array('wid' => $data['ward_hide'], 'hid' => $data['hospital_hide'], 'alcoholunit' => $data['alcoholunit'], 'soapunit' => $data['soapunit']);
  
  foreach ($data as $key => $value) {
    // While user fill the actual number of patients value of that month
    if ($calc_model == 'actual' && strpos($key, 'actual_') === 0) {
      // Get the nid of the field
      // e.g.: actual_2045, then id will be 2045
      $id = ltrim($key, 'actual_');
      if (is_numeric($id)) {
        $severity_levels[$id] = $value;
      }
    }
    // When calculate estimated value, the formular will be 
    // number_of_patients_a_week_of_the_severity_level * number_of_days_in_this_month / number_of_days_value_filled
    // 
    // For example: In Dec
    //           Mon, Tue, Wed, Thu, Fri, Sat, Sun
    // Level 1    10   10   10             
    //
    // So the value of above will be: (10+10+10) * 30 / 3 = 300
    elseif ($calc_model == 'estimated' && strpos($key, 'estimated_') === 0) {
      // When no value for that day, should  not consider this value
      if (empty($value) && $value !== 0) {
        continue;
      }
      
      // Get the nid of the field
      // e.g.: estimated_2045_1, this represents the severity_level nid is 2045 of day 1 
      $id = ltrim($key, 'estimated_');
      list($id, $day) = explode('_', $id);
      
      if (!is_numeric($day) || ((int) $day < 1 || (int) $day > 7)) {
        continue;
      }
      
      // Summerize the number of patients of the week for each severity levels
      // And save the number of days filled value for patients in an array for each levels
      if (!isset($severity_levels[$id])) {
        $severity_levels[$id] = $value;
        $severity_levels_count[$id] = 1;
      }
      else {
        $severity_levels[$id] += $value;
        $severity_levels_count[$id]++;
      }
    }
    elseif (strpos($key, 'product_') === 0) {
      // Get the nid of the field
      // e.g.: product_1003, the product node id will be 1003
      $id = ltrim($key, 'product_');
      if (is_numeric($id)) {
        $products[$id] = $value;
      }
    }
  }
  
  // Unset the array
  unset($data);
  
  // Define a variable to hold the washing hands frequency of all the products used in this hospital 
  $product_total_used_frequency = 0;
  if (count($products)) {
    $product_total_used_frequency = aspcm_calc_products_total_used_frequency($products, $params);
  }
  
  // Define a variable to hold the ideal washing hands frequency based on the patients there
  $severity_level_total_ideal_frequency = 0;
  if (count($severity_levels)) {
    
    // When estimated model choosed
    // Should use following formular to calculate this month's patients number for each severity level
    // number_of_patients_a_week_of_the_severity_level * number_of_days_in_this_month / number_of_days_value_filled
    if ($calc_model == 'estimated') {
      foreach ($severity_levels as $id => $val) {
        $severity_levels[$id] = aspcm_calc_estimating_patients($val, $severity_levels_count[$id], $count_days_of_month);
      }
    }
    
    $severity_level_total_ideal_frequency = aspcm_calc_ideal_frequency($severity_levels);
  }
  
  // Unset the arrays
  unset($products);
  unset($severity_levels);
  unset($severity_levels_count);
  
  // calculate percentage value
  // calculated washing hands number / ideal wahsing hands number 
  $retval = $severity_level_total_ideal_frequency == 0 ? 0 : $product_total_used_frequency * 100 / $severity_level_total_ideal_frequency;
  $retval = round($retval, 1);
  
  return $retval == 0 ? 0 : "{$retval}%";
}

/**
 * Calculate all used products' wishing times (frequency)
 * 
 * @param $products
 */
function aspcm_calc_products_total_used_frequency($products, $params = array()) {
  /* modify by linda 2012/03/14 */
  if (!empty($params['hid'])) {
    $custom_perpush_arr = array();
    $custom_perpush_rs = db_query('SELECT * FROM {field_data_field_aspcm_ref_products} WHERE entity_id=:hid AND field_aspcm_ref_products_nid IN (:pid)',
                                   array(':hid' => $params['hid'], ':pid' => array_keys($products)));

	foreach ($custom_perpush_rs as $item) {
	  if (!empty($item->field_aspcm_product_per_push)) {
	    $custom_perpush_arr[$item->field_aspcm_ref_products_nid] = $item->field_aspcm_product_per_push;
	  }
	  
	}
  }
    
  $result = db_query("SELECT n.nid, avpp.field_aspcm_volume_per_push_value AS pushvalue,
                      apv.field_aspcm_product_volume_value AS volume, 
                      apcd.field_aspcm_product_category_des_value AS category FROM node AS n
                      LEFT JOIN field_data_field_aspcm_volume_per_push AS avpp ON n.nid = avpp.entity_id
                      LEFT JOIN field_data_field_aspcm_product_volume AS apv ON n.nid = apv.entity_id
                      LEFT JOIN field_data_field_aspcm_ref_product_category AS arpc ON n.nid = arpc.entity_id
                      INNER JOIN field_data_field_aspcm_product_category_des AS apcd ON apcd.entity_id = arpc.field_aspcm_ref_product_category_nid
                      WHERE n.type = 'aspcm_product' AND n.nid IN (:nid)", array(':nid' => array_keys($products)));
  /* modify by linda 2012/03/14 end */
  
  $product_used_frequency = array();
  
  foreach ($result as $item) {
    if (isset($custom_perpush_arr[$item->nid]) && !empty($custom_perpush_arr[$item->nid])) {
	  $item->pushvalue = $custom_perpush_arr[$item->nid];
	}
    $product_used_frequency[] = aspcm_calc_product_total_used_frequency($products[$item->nid], $item, $params);
	
  }
  
  return array_sum($product_used_frequency);  
}

/**
 * Calculate the product total used frequency. 
 * One product total used volume divided by one push volume.
 * 
 * Use third parameter when pushvalue is depended on each hospital
 * 
 * @param $product
 * @param $params
 */
function aspcm_calc_product_total_used_frequency($prod_used_volume, $info, $params = array()) {
  $result = 0;
  
  /* modify by linda 2012/03/15 */ 
  // If the ward measures use bottle instead of using volume
  // convert the value into volume
  if ($info->category == ALCOHOL_JP) {
    if ($params['alcoholunit'] == 'bottle') {
	  $prod_used_volume *= $info->volume;
	}
  } 
  else if ($info->category == SOAP_JP) {
    if ($params['soapunit'] == 'bottle') {
	  $prod_used_volume *= $info->volume;
	}
  }
  /* modify by linda 2012/03/15 end */
  
  if (!empty($params) && isset($params['custom_pushvalue'])) {
    $result = $prod_used_volume / $params['custom_pushvalue'];
  }
  elseif ($info && !empty($info->pushvalue)) {
    $result = $prod_used_volume / $info->pushvalue;
  }
  
  return $result;
}

/**
 * Calculate the ideal used products washing time (frequency)
 * 
 * @param $severity_level
 */
function aspcm_calc_ideal_frequency($severity_level) {
  $result = db_query("SELECT n.nid, ahhn.field_aspcm_hand_hy_number_value AS frequency FROM node AS n
                      LEFT JOIN field_data_field_aspcm_hand_hy_number AS ahhn ON n.nid = ahhn.entity_id
                      WHERE n.type = 'aspcm_department' AND nid IN (:nid)", 
                      array(':nid' => array_keys($severity_level)));
  foreach ($result as $item) {
    $ideal_frequency[] = aspcm_calc_severitylevel_ideal_frequency($severity_level[$item->nid], $item);
  }
  
  return array_sum($ideal_frequency);
}

/**
 * Each patient of certain severity level has its standard washing hands frequency,
 * by multiple the number of patients, will know the ideal washing frequency.
 * 
 * @param $patient_num
 * @param $info
 */
function aspcm_calc_severitylevel_ideal_frequency($patient_num, $info) {
  return $info->frequency * $patient_num;
}

/**
 * Calculate one month patients number, according to one week's data register.
 * 
 * @param $weekly_data, a week's patients filled in the estimation form
 * @param $defined_days, how many days have filled the patients in the estimation form
 * @param $month_days, how many days this month has
 */
function aspcm_calc_estimating_patients($weekly_data, $defined_days, $month_days) {
  if (empty($defined_days)) {
    return 0;
  }

  return $weekly_data * $month_days / $defined_days;
}