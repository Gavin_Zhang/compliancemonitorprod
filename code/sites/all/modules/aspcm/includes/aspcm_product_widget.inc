<?php
/**
 * INC000006324309
 * @author Bala
 * 
 * Product Finder Widget
 */
module_load_include('inc', 'aspcm', 'includes/aspcm_product.class');

function aspcm_product_widget_menu(&$items) {
  // Define a menu for ajax call, searching products with provided parameters
  $items['product/search/ajax'] = array(
    'page callback' => 'aspcm_product_widget_find_products',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  
  // Define a menu for ajax call, searching companies names
  /*$items['company/search/autocomplete'] = array(
    'page callback' => 'aspcm_product_widget_find_companies_auto',    
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );*/
  
  // Define a menu for ajax call, searching products names
  $items['product/search/autocomplete'] = array(
    'page callback' => 'aspcm_product_widget_find_products_auto',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  
  // Define a menu for ajax call, to save the selected products and its customized perpush volume
  $items['product/save/ajax'] = array(
    'page callback' => 'aspcm_product_widget_save_products',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  
  /*$items['product/selected/ajax'] = array(
    'page callback' => 'aspcm_selected_products',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );*/
}

function aspcm_product_widget_form_alter(&$form, &$form_state) {  
  $form['#validate'][] = 'aspcm_hospital_node_form_custom_validate';
  $form['product_finder'] = array(
    '#type' => 'fieldset',
    '#attributes' => array('id' => 'product-finder-widget'),
    '#weight' => 12,
  );
  
  $form['product_finder']['srch_cname'] = array(
    '#type' => 'select',
    '#options' => aspcm_product_widget_find_all_companies(),
    '#title' => t('Select for Company name'),
  );
  
  $form['product_finder']['srch_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#ajax' => array(
      'event' => '',
      'callback' => 'aspcm_product_widget_find_products',
      'wrapper' => 'edit-srch-result',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['product_finder']['srch_pname'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#title' => t('Product Name'),
    '#autocomplete_path' => 'product/search/autocomplete',
  );  
  
  $form['product_finder']['product_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  
  $form['product_finder']['description'] = array(
    '#markup' => '<p class="info-mess product-search-info">' . t("選択のチェックボックスにチェックを入れて「一時保存」ボタンを押すと製品を登録することができます。
	<br />すべての製品の登録が完了したら「登録完了」ボタンを押してください。 <br />").'<span class="custom-perpush-color">※</span>'.t("「製品選択チェックボックスのチェックをはずし「登録完了」ボタンを押すと、その製品の過去に入力を<br />したデータ（払い出し本数/使用量）は削除されますのでご注意ください。」") . '</p>',
  );
  
  $product = new Product();
  $products = $product->get_products_by_hid($form['nid']['#value']);

  $form['selected_products_markup'] = array(
    '#weight' => 13.5,
    '#markup' => '<div class="form-item"><label>使用製品情報: <span class="form-required" title="' .t('This field is required'). '">*</span></label></div>',	
  );
  
  $form['selected_products'] = array(
    '#weight' => 14,
    '#markup' => $form_state['custom_validate_fail'] || !empty($_SESSION['messages']['error']) ? aspcm_rebuild_products_table($form_state) : aspcm_render_products_table(
      $products,  //TODO: use array_keys($custom_perpush_data)
      array(
        'header' => array(t('Select'), t('Company Name'), t('Alcohol/Soap'), t('Product'), t('Default PerPush Volume'), t('Custom PerPush Volume')), 
        'prefix' => 'real_', 
        'custom_perpush' => TRUE,
      )
    ),    
  );   
  
  $form['add_products_btn'] = array(
    '#markup' => '<div id="add-products-wrapper">'.
      '<p style="font-size:84.6%">' .'<span class="custom-perpush-color">※</span>'.t("製品選択チェックボックスのチェックをはずし保存ボタンを押すと、その製品の過去に入力をしたデータ（払い出し本数/使用量）<br />は削除されますのでご注意ください。") . '</p>'.
      '<br /><a href="#selprod" class="lnk-add" id="add-products-button">'. t('Add products') .'</a></div>',
    '#weight' => 15,
  );

  $form['products_hidden'] = array(
    '#type' => 'hidden',
    '#default_value' => aspcm_get_product_to_hidden($products), // clear the values
  );
      
}

function aspcm_get_product_to_hidden($products) {
  $result = '';  
  if (!empty($products)) {
    foreach ($products as $key => $value) {
      $result .= $key. ':' .$value['custom_perpush']. ';';
    }
  }
  if (!empty($result)) {
    $result = substr($result, 0, strlen($result)-1);
  }
  return $result;
}

/**
 * Searching products and return with a html table.
 * 
 */
function aspcm_product_widget_find_products() {
  $product = new Product();
  
  $ptype = NULL;
  if (!empty($_POST['srch_pgroup']) && $_POST['srch_pgroup'] != 'none') {
    $ptype = check_plain(strtolower($_POST['srch_pgroup'])) == 'alcohol' ? ALCOHOL_JP : SOAP_JP;
  }
  
  $options = array(
    'cname' => check_plain($_POST['srch_cname']),
    'pgroup' => $ptype,
    'pname' => check_plain($_POST['srch_pname']),
    'isjnj' => check_plain($_POST['srch_is_jnj'])
  );
  //var_dump($options);
  $products = $product->get_products(array(), $options);
  
  $products_hidden = $_POST['selected_products'];
  
  if (!empty($products_hidden)) {
    $products_arr = explode(';', $products_hidden);
    foreach ($products_arr as $value) {
      $product_arr = explode(':', $value);
      if (in_array($product_arr[0], array_keys($products))) {
        $products[$product_arr[0]]['custom_perpush'] = $product_arr[1];
        $products[$product_arr[0]]['checked'] = TRUE;
      }
    }
  }
  
  //var_dump($products);
  $options = array(
    'prefix' => 'find_', 
    'header' => array(t('Select'), t('Company Name'), t('Alcohol/Soap'), t('Product'), t('Default PerPush Volume'), t('Custom PerPush Volume'). '<span class="custom-perpush-color">※</span>'),
  );
  
  $custom_info = '<p class="info-mess custom-perpush-info"><span class="custom-perpush-color">※</span>' 
                 . t('手指衛生回数1回あたりの手指衛生剤使用量を、1プッシュの吐出量から貴院での1回あたり手指衛生剤使用量に変更することができます。') . '</p>';
  
  if (!empty($products)) {
    echo $custom_info. '<div id="search-result-content">' .aspcm_render_products_table($products, $options). '</div>';
  }
  else {
    echo '<div id="no-search-result">' .t('No search result.'). '</div>';
  }
}

/**
 * Save the products selected and its customized perpush volume,
 * then render an html table with the selected products and their perpush volume.
 * 
 * Will use cache table to save the selected products and their perpush volume values temporary,
 * after using, the cached data will be removed from cache table.
 */
function aspcm_product_widget_save_products() {
  $results = array();

  $products_hidden = $_POST['selected_products'];
  if (!empty($products_hidden)) {
    $products_arr = explode(';', $products_hidden);
    $product_ids = array();
    foreach ($products_arr as $value) {
      $product_arr = explode(':', $value);
      array_push($product_ids, $product_arr[0]);
    }
    $product = new Product();
    $results = $product->get_products($product_ids);
    
    foreach ($products_arr as $value) {
      $product_arr = explode(':', $value);
      $results[$product_arr[0]]['custom_perpush'] = $product_arr[1];      
    }
  }
  echo aspcm_render_products_table($results,
  array(
        'header' => array(t('Select'), t('Company Name'), t('Alcohol/Soap'), t('Product'), t('Default PerPush Volume'), t('Custom PerPush Volume')), 
        'prefix' => 'real_', 
        'custom_perpush' => TRUE,    
      ));
}

/**
 * Return json string of companies names
 * 
 * @param $name
 */
function aspcm_product_widget_find_companies_auto() {
  $jnj = $_POST['srch_is_jnj'] == 'no' ? FALSE : TRUE;
  $product = new Product();
  $companies = $product->get_product_companies($jnj);
  echo $companies;
}

function aspcm_product_widget_find_all_companies() {
  $product = new Product();
  $companies = $product->get_product_companies();
  return $companies;
}

function aspcm_product_widget_find_products_auto($name) {
  $product = new Product();
  $results = $product->get_products(NULL, array('pname' => $name));
  $output = array();
  foreach ($results as $item) {
    $output[$item['pname']] = $item['pname'];    
  }
  drupal_json_output($output);
}

function aspcm_selected_products() {
  $prod_ids = explode('#', $_GET['selected_products_store']);
  $prod_ids = array_map('check_plain', $prod_ids);
  
  $product = new Product();
  $products = $product->get_products($prod_ids);
  
  echo aspcm_render_products_table(
    $products,
    array(
      'header' => array(t('Select'), t('Company Name'), t('Alcohol/Soap'), t('Product'), t('Default PerPush Volume'), t('Custom PerPush Volume'). '<span class="custom-perpush-color">※</span>'),
      'prefix' => 'real_', 
      'custom_perpush' => TRUE,
    )
  );
}

function aspcm_rebuild_products_table(&$form_state) {
  if (!empty($form_state['values']['field_aspcm_selected_products'][LANGUAGE_NONE][0]['value'])) {
    $product_ids = array();
    $products = array();
    $dict = $form_state['values']['field_aspcm_selected_products'][LANGUAGE_NONE][0]['value'];
    $products_arr = explode(';', $dict);
    foreach ($products_arr as $key => $value) {
      if (!empty($value)) {
        $product = explode(':', $value);
        $product_ids[$product[0]] = $product[1];
      }
    }
    $product_obj = new Product();
    $products = $product_obj->get_products(array_keys($product_ids));
    foreach ($products as $key => $value) {
      if (in_array($key, array_keys($product_ids))) {
        $products[$key]['custom_perpush'] = $product_ids[$key];
        if ($form_state['error_ids'][$key]) {
          $products[$key]['error'] = TRUE;
        }
      }
    }
    return aspcm_render_products_table(
      $products,
      array(
      'header' => array(t('Select'), t('Company Name'), t('Alcohol/Soap'), t('Product'), t('Default PerPush Volume'), t('Custom PerPush Volume')),
      'prefix' => 'real_', 
      'custom_perpush' => TRUE,
      )
    );
  }
  else {
    return NULL;
  }
}

function aspcm_render_products_table($products, $options = array()) {
  if (empty($products)) {
    return NULL;
  }
  
  $rows = array();
  foreach ($products as $rs) {
    $ck_select = "<input class=\"selprod-checkbox-group\" type=\"checkbox\" name=\"{$options['prefix']}{$rs['pid']}\" id=\"{$options['prefix']}{$rs['pid']}\" value=\"{$rs['pid']}\"";
    $ck_select .= ($options['custom_perpush'] || $rs['checked']) ? "checked=\"checked\"" : '';
    $ck_select .=" />";
    
    $lbl_prod_name = "<label for=\"{$options['prefix']}{$rs['pid']}\">{$rs['pname']}</label>";
    
    if ($rs['error']) {
      $txt_custom_value = "<input type=\"text\" class=\"form-text error custom-perpush-text\" name=\"custom_perpush_{$rs['pid']}\" id=\"custom-perpush-{$rs['pid']}\" value=\"{$rs['custom_perpush']}\" /> ml";
    }
    else {
      $txt_custom_value = "<input type=\"text\" class=\"form-text custom-perpush-text\" name=\"custom_perpush_{$rs['pid']}\" id=\"custom-perpush-{$rs['pid']}\" value=\"{$rs['custom_perpush']}\" /> ml";
    }
    
    $rows[$rs['pid']] = array(array('data' => $ck_select, 'class' => 'align-center'), $rs['cname'], $rs['category_name'], $lbl_prod_name, $rs['perpush'] ." ml", $txt_custom_value);
  }
  
  $class = $options['custom_perpush'] ? 'selected-products-table' : 'product-finder-table';
  
  foreach ($options['header'] as $hvalue) {
    $header[] = array('data' => $hvalue);
  }
  foreach ($rows as $rvalue) {
    $row_arr[] = array('data' => $rvalue);
  }
  
  $variable = array(
    'header' => $header,
    'rows' => $row_arr,
    'attributes' => array('class' => $class),
  );
  return theme_table($variable);
}

/**
 * Implementation of hook_validate().
 * To organize the custom perpush volume values with related products, before saving into the field_aspcm_selected_products.
 * 
 * @param $form
 * @param $form_state
 */
function aspcm_hospital_node_form_custom_validate(&$form, &$form_state) {
  // #post data keys, get from posted arrays
  $keys = array_keys($form_state['input']);  
  // all the keys which contains values of custom perpush values
  $custom_perpush = array_filter($keys, '_custom_perpush_pid');
  // all the keys which contains the selected product ids
  $selected = array_filter($keys, '_p_pid'); 
  if (empty($selected)) {
    form_set_error('', t('使用製品情報 field is required.'));
  }
    
  $results = array();
  $products = array();
  $dict = array();
  foreach ($custom_perpush as $p) {
    // get the product id
    $pid = ltrim($p, 'custom_perpush_');
    
    // only get selected products' custom perpush value
    if (in_array('real_'. $pid, $selected)) {
      // combine as PID:VALUE    
      if (isset($form_state['input'][$p]) && trim($form_state['input'][$p]) != '') {
        if(is_numeric($form_state['input'][$p])) {
          $value = (float)$form_state['input'][$p];
          if ($value > 9.99 || $value < 0.01) {
            $form_state['custom_validate_fail'] = TRUE;
            $form_state['error_ids'][$pid] = TRUE;
          }
        }
        else {
          $form_state['custom_validate_fail'] = TRUE;
          $form_state['error_ids'][$pid] = TRUE;
        }
      }
      $results[] = "{$pid}:{$form_state['input'][$p]}"; // custom perpush value      
      $products[] = array('nid' => $pid);
    }
  }  
  //$form_state['custom_validate'] = TRUE;
  //form_set_error('', 'test');
  //$form_state['rebuild'] = TRUE;
  //var_dump($results);
  //die();
  // save the value to field_aspcm_selected_products, when saving a node the value will be saved into db
  $form_state['values']['field_aspcm_selected_products'][LANGUAGE_NONE][0]['value'] = implode(';', $results);
  if ($form_state['custom_validate_fail'] || !empty($_SESSION['messages']['error'])) {
    //if validate fail set form value before rebuild 6-7
    $form['selected_products']['#markup'] = aspcm_rebuild_products_table($form_state);  
    $form_state['rebuild'] = TRUE;
  }
  if ($form_state['custom_validate_fail']) {
    form_set_error('', t('the value may be no smaller than 0.01 or no larger than 9.99.'));
  }

  unset($form_state['values']['field_aspcm_ref_products']);

  $form_state['values']['field_aspcm_ref_products'][LANGUAGE_NONE] = $products;
  //$form_state['values']['dict'] = $dict;
}

/**
 * callback of determing the field is custom perpush
 * @param $var
 */
function _custom_perpush_pid($var) {
  return stristr($var, 'custom_perpush_'); 
}

/**
 * callback of determing the field is selected product
 * @param $var
 */
function _p_pid($var) {
  return stristr($var, 'real_');
}

function _ltrim_custom_perpush_find_pid($var) {
  return ltrim($var, 'custom_perpush_find_');
}