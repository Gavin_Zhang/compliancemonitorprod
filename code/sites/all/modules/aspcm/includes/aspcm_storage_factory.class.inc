<?php
module_load_include('inc', 'aspcm', 'includes/aspcm_storage_mysql.class');
module_load_include('inc', 'aspcm', 'includes/aspcm_storage_survey.class');

class StorageFactory {
  const DRUPALDB_SAVE_LOCATION = 'drupal';
  const WS_SAVE_LOCATION = 'emktdashboard';
  
  static function getStorage($storage_name) {
    switch ($storage_name) {
    
      case self::WS_SAVE_LOCATION:
        return new SurveyWS(ASPCM_PRINT_INFO);
      
      case self::DRUPALDB_SAVE_LOCATION:
      default:
        return new ASPCMMysql(ASPCM_PRINT_INFO);
    }
  }
  
}