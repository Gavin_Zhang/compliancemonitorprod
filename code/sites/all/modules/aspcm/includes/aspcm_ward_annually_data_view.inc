<?php 
// $Id$
/**
 * @file
 * Responsible for logic functions related to the web service.
 */

module_load_include('inc', 'aspcm', 'includes/aspcm_ward_monthly_data_input');

/**
 *
 * the function is get severity by name the name from ward 
 * @param $name ,department name
 * return array()
 */
function get_severity_by_ward_name($name) {
//  $query = "SELECT node.nid,node.title FROM (
//    SELECT node.nid nid,dept.field_severity_level_nid level_id,dept.field_aspcm_clinical_department_value dep_value
//    FROM node node INNER JOIN content_type_aspcm_department dept ON node.nid = dept.nid 
//    WHERE field_aspcm_clinical_department_value ='%s' ) department 
//    INNER JOIN node ON department.level_id = node.nid ORDER BY title DESC";
//  $result = db_query($query, $name);
//  while ($row = db_fetch_object($result)) {  
//    if ($row) {
///* 'nid' => $row->nid,
//   'title' => $row->title,
// */
//      $severity_nid[] = $row->nid;
//    }
//  }
//  return $severity_nid;
}


/**
 * Implementation of function get_product_by_year(), this function get all the product by the $hid, $wid
 * Enter description here ...
 * @param $hid
 * @param $wid
 */
function get_product_by_year($hid, $wid, $year) {
  //get all the products with category.
  $products = get_products_by_hid($hid, $wid);
  //group the products by category
  $category = array();
  $result = array();
  foreach ($products as $row) {
    // group product by its category
    if (!in_array($row['category'], $category)) {
      array_push($category, $row['category']);
      $result[$row['category']][] = $row;
    }
    else {
      array_push($result[$row['category']], $row);
    }
  }
  //create the products table by category with loop
   
  return products;
}

/**
 * Function that load data from the web service by year.
 * @param $form_state
 * @return
 * array[month] =>array(
 * 	array=>form_state,
 * )
 */
function aspcm_input_form_load_by_year($hid, $wid, $year) {
  $result = array();
  $start_month = get_start_month($wid);
  $begin = date( "Y/m", strtotime($year ."-01-01 +". ($start_month - 1) ." months"));
  $end = date( "Y/m", strtotime(($year + 1) ."-01-01 +". ($start_month - 2) ." months"));

  $innerbegin = $begin;
  while ($innerbegin <= $end) {
    $date = explode('/', $innerbegin);
    $query = "SELECT title, nid FROM {node} WHERE type = 'aspcm_input_form' AND title like :title";
    $str = $hid .':'. $wid .':'. $date[0];
    $result_db = db_query($query, array(':title' => "$str%"));
    foreach ($result_db as $row) {
      $data = explode(':', $row->title);
      if ($data[2] >= $begin && $data[2] <= $end) {
        $nids[] = $row->nid;
      }
    }
    $innerbegin = date("Y/m", strtotime($date[0] .'-01-01 +1 years')); 
  }
  
  $safe_storage = get_storage_instance('aspcm_input_form');
  if ($safe_storage) {
    $datas = $safe_storage->load_data_ids($nids);
    if (is_array($datas)) {
      foreach ($datas as $key => $value) {
        unset($datas[$key]);
        $data['values'] = is_array($value['body']) ? unserialize($value['body'][0]['value']) : unserialize($value['body']);
        $date = $data['values']['date'];
        $result_array[$date] = $data;
      }
    }
  }
  
  $innerbegin = $begin;
  while ($innerbegin <= $end) {
    $date = explode('/', $innerbegin);
    $index = $date[1] + 0;
    if (isset($result_array[$innerbegin])) {
      $result[$index]['values'] = $result_array[$innerbegin]['values'];
    }
    else {
      $result[$index]['values'] = array('hospital' => $hid, 'ward' =>  $wid, 'date' => $innerbegin, );
    }
    $innerbegin = date("Y/m", strtotime($date[0] ."-". $date[1] ."-01 + 1 months"));
  }

  return $result;
 /* 
  $result = array();
  $form_state['values']['hospital'] = $hid;
  $form_state['values']['ward'] = $wid;
  //Load data from year/startmonth
  for ($i = $start_month - 1; $i < 12; $i++) {
      $form_state['values']['date'] = date( "Y/m", strtotime($year ."-01-01 +". $i ." months"));
     $result[$i+1] = aspcm_input_form_load($form_state);    
  }
  //Load data from nextyear
  for ($i = $start_month - 2; $i > -1; $i--) {
      $form_state['values']['date'] = date( "Y/m", strtotime(($year+1) ."-01-01 +". $i ." months"));
     $result[$i+1] = aspcm_input_form_load($form_state);    
  }
  
   var_dump($result);
  return $result;*/
}
