<?php
// $Id$
/**
 * @file 
 * aspcm_storage_generic.class.inc
 * A generic class that provides some generic methods that will be used for the specific implementation of the storages
 */

module_load_include('inc', 'aspcm', 'includes/aspcm_storage_interface');
 
class StorageGeneric implements IASPCMStorage {

  
  protected $site_id;
  protected $type_id;
  protected $info_id;
  protected $question_control_id;
  protected $question_control_answer_id;
  protected $question_data_id;
  protected $ids_set;
  protected $print_info;
  
  // available encoding types
  const ECD_BASE64   = 'base64';
  const ECD_JSON     = 'json';
  const ECD_PHP      = 'php';
  const ECD_PLAINTXT = 'plaintext';
  
  function StorageGeneric ($print_info = FALSE) {
    $this->print_info = $print_info;
  }

  // ######################### Interface implementation #########################
  
  function set_ids($site_id, $type_id, $question_control_id, $question_control_answer_id, $question_data_id, $info_id = NULL) {
    $this->site_id = $site_id; // site id in emkt dashboard
    $this->type_id = $type_id; // survey id
    $this->info_id = $info_id; // node id, that will be treated as user id for survey module
    $this->question_control_id = $question_control_id; // this is the question that usually is a radio button
    $this->question_control_answer_id = $question_control_answer_id; // this is the answer for the question control that usually is a radio button
    $this->question_data_id = $question_data_id; // this is the question that will be used to store the data into Survey WS
    
    // flag to let know this method was executed
    $this->ids_set = TRUE;
  }
  
  // the methods below should be overwritten by this class childs
  function save_data($data) {}
  function load_data($info_id) {}
  function load_data_ids($info_ids) {}
  
  // ######################### End of Interface implementation #########################
  
  /**
   * Encode a string based on a parameter that specifies the format the string will have
   * 
   * The resulting string will be divided into two parts: <store_format>|<encoded_string>
   * The <store_format> part can be one of these:
   * - json, this will execute only the json_decode() function in the <encoded_string>
   * - base64, in this case, this function consider that the string was first encoded
   *   using serialize() PHP function, then encoded with base64_encode function
   * - php, this will execute only the unserialize() function on the <encoded_string>
   * - plaintext
   * 
   * Actually, if the <store_format> is not one of the first 3 items, the <encoded_string> will
   *  considered as a plain text string, and it will be returned as is.
   * 
   * @param string $store_format
   * @return string
   */  
  function encode_data($data, $store_format) {
    if (is_array($data)) {
      switch ($store_format) {
        case self::ECD_BASE64:
          $data = base64_encode(serialize($data));
          break;
        case self::ECD_JSON:
          $data = json_encode($data);
          break;
        case self::ECD_PHP: // php format is the same as default
        default:
          $data = serialize($data);
          $store_format = self::ECD_PHP;
      }
    } 
    else {
      $data = (string) $data;
      $store_format = self::ECD_PLAINTXT;
    }
    
    return "$store_format|$data";
  }
  
  /**
   * Decode a string based on a specific parameter inside the string.
   * 
   * This is the format of the expected string:
   * <pre>
   * "json|{"a":1,"b":2,"c":3,"d":4,"e":5}"
   * </pre>
   * 
   * If the string doesn't follow this format, an Exception will be throwed.
   * 
   * The string is dived into two parts: <encoding_format>|<encoded_string>
   * The <encoding_format> part can be one of these:
   * - json, this will execute only the json_decode() function in the <encoded_string>
   * - base64, in this case, this function consider that the string was first encoded
   *   using serialize() PHP function, then encoded with base64_encode function
   * - php, this will execute only the unserialize() function on the <encoded_string>
   * - plaintext
   * 
   * Actually, if the <encoding_format> is not one of the first 3 items, the <encoded_string> will
   *  considered as a plain text string, and it will be returned as is.
   * 
   * @param string $data
   * @return array
   */
  function decode_data($data) {
    $data = explode('|', $data);
    
    if (!is_array($data)) {
      $msg = "The informed data is not in the expected format <encoding_format>|<encoded_string>";
      throw new Exception($msg);
    }

    switch ($data[0]) {
      case self::ECD_BASE64:
        if (!$data = unserialize(base64_decode($data[1]))) {
          $msg = __METHOD__ . ": ERROR: Failed to unserialize the data for ID #{$row->id} using {$data[0]}\n";
          print_info($msg);
        }
        break;

      case self::ECD_JSON:
        if (!$data = json_decode($data[1], TRUE)) {
          $msg = __METHOD__ . ": ERROR: Failed to unserialize the data for ID #{$row->id} using {$data[0]}\n";
          print_info($msg);
        }
        break;

      case self::ECD_PHP:
        if (!$data = unserialize($data[1])) {
          $msg = __METHOD__ . ": ERROR: Failed to unserialize the data for ID #{$row->id} using {$data[0]}\n";
          print_info($msg);
        }
      default:
        // if the format is plaintext, nothing needs to be done
        $data = $data[1];
    }
    
    return $data;
  }
  
  function check_set_ids ($method) {
    if (!$this->ids_set) {
      $msg = __METHOD__ . ": ERROR: set_ids() must be called before {$method}()";
      $this->debug_log($msg);
      return FALSE;
    }
    return TRUE;
  }
  
  function debug_log ($msg) {
    if ($this->print_info) {
      error_log($msg);
    }
  }
  
}