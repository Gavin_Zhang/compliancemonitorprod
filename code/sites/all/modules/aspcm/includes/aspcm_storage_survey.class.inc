<?php
module_load_include('inc', 'aspcm', 'includes/aspcm_storage_generic.class');
module_load_include('inc', 'aspcm', 'includes/aspcm_storage_survey_client.class');

class SurveyWS extends StorageGeneric {
  
  function SurveyWS($print_info) {
    $this->print_info = $print_info;
  }

  function save_data($data) {

    // check if all necessary data was given
    if (!$this->check_set_ids(__FUNCTION__)) {
      return FALSE;
    }
    
    // prepare the data for the survey fields
    $dashboard_values = array (
      0 => array (
        'question' => $this->question_control_id,
        'answer' => $this->question_control_answer_id,
      ),

      1 => array (
        'question' => $this->question_data_id, //Data
        'answer' => $data,
      ),
    );
    
    $survey_client = $this->get_survey_object();
    
    // Configure the object and send the data
    $survey_client->set_user_id((int)$this->info_id);
    $survey_client->set_answer_data($dashboard_values);
    $res = $survey_client->save_answer();
    
    return TRUE;
  }
  
  function load_data($info_id) {
  
    // check if all necessary data was given
    if (!$this->check_set_ids(__FUNCTION__)) {
      return FALSE;
    }
  
    $survey_client = $this->get_survey_object();
    $survey_client->set_user_id((int) $info_id);
    $res = $survey_client->retrieve_answer();
    
    // Survey always send the data in JSON format
    $res = json_decode($res);
     // print_r($res);
      if(!empty($res) && is_array($res)){
          $info_id = $this->extract_data($res[0]);
      }
    return $info_id;
  }
  
  function load_data_ids($info_ids) {
    print_info("load multiple ids");
    // check if all necessary data was given
    if (!$this->check_set_ids(__FUNCTION__)) {
      return FALSE;
    }
    
    // check the parameter is correct
    if (!is_array($info_ids)) {
      return FALSE;
      print_info("argument must be an array");
    }
    
    $save_method = variable_get('aspcm_data_save_method', ASPCM_PROD_SAVE_BATCH);
    
    if ($save_method == ASPCM_PROD_SAVE_ONEBYONE) {
      print_info("retrieving information one by one");
      return $this->load_data_ids_onebyone($info_ids);
    }
    print_info("retrieving information in batch");
  
    $survey_client = $this->get_survey_object();
    $survey_client->set_user_ids(implode(",", $info_ids));
    $res = $survey_client->retrieve_answer_list();
    
    // use the value of the array as it's key, so we can return the same amount of ids requested
    $info_ids = array_flip($info_ids);
    
    if (!empty($res)) {
      // Survey always send the data in JSON format
      $results = json_decode($res);
      
      // extract the data from each result from the webservice call
      foreach ($results as $res) {
        $info_ids[$res->userId] = $this->extract_data($res);
      }
    }
    else{// else judge add by Benny for ACMSUPP-35 20120412
    	return FALSE;
    }

    return $info_ids;
  }
  
  function extract_data($res) {
    // check which field is the the one with the data
	if (!empty($res->evaluationResult[0]->evaluationQuestionResult)) {
	  foreach ($res->evaluationResult[0]->evaluationQuestionResult as $answer) {
        if ($answer->questionId == $this->question_data_id) {
          return $this->decode_data($answer->responseText);
          break;
        }
      }
	}

    return '';
  }
  
  function get_survey_object() {
    // class that will send/retrieve data to/from Survey
    return new SurveyClient($this->type_id, $this->site_id);
  }
  
  function load_data_ids_onebyone($info_ids) {
  
    // get the content based on the ids
    $content = array(); // this is the default value if nothing is found
    if (!empty($info_ids)) {
	  foreach ($info_ids as $id) {
        // cast the id to be an integer
        $id = (int) $id;
        print_info("retrieving information for ID #$id");
        //db_query("SELECT * FROM comments"); // to avoid 'mysql has gone away' problem, send a dummy query to mysql
      
        // try to get the data for this id ...
        if (!$data = $this->load_data($id)) {
        
          // insert an empty array, so the user will know this value is empty
          $content[$id] = array();
        } else {
          // ... could get the data, save it into the array that going to be returned
          $content[$id] = $data;
        }
      }
	}

    // return the content found
    return $content;
  
  }
}