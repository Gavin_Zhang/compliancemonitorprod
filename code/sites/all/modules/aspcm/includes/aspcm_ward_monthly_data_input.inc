<?php

// $Id$

/**
 * @file
 * 
 * Operations of aspcm ward monthly data input form
 * 
 */


/**
 * Save the input form as a node, use node_save api.
 * In order to save the form data into MSSQL, the general save api is called, which receives data as node
 * @link ../includes/aspcm_safe_storage.class.inc
 * @param $form_state
 */
function aspcm_input_form_save(&$form_state = NULL) {
  //save hospital,ward cookie 

  //$node = aspcm_input_form_build_node($form_state);
  // Since the title of the node is unqiue for the input form, 
  // the title is combined with hospital_id, ward_id and that month
  // so it's important to check the data is existing or not based on the title

  $node_title = aspcm_input_form_build_node_title($form_state['values']['hospital'], $form_state['values']['ward'], $form_state['values']['date']);
  
  //$node = node_load(array('title' => $node_title, 'type' => 'aspcm_input_form'));
  $query = db_query("SELECT n.*, r.*,fb.* FROM node n 
          INNER JOIN node_revision r ON r.vid = n.vid 
          LEFT JOIN field_data_body AS fb ON n.nid = fb.entity_id
          WHERE n.title = :title AND n.type = 'aspcm_input_form'",
          array(':title' => $node_title));
  
  $node = $query->fetchObject();
  // Update an existing node
  if ($node) {
    // change the body content
	
    /* modify by linda 2012/03/15 
     * transform all product value to the unit of ml
     */	
    tranform_monthly_input_data_to_ml($form_state);	
    /* modify by linda 2012/03/15 end */

    $node->language = LANGUAGE_NONE;
    $node->body[$node->language][0]['value']   = serialize($form_state['values']);
    $node->body[$node->language][0]['summary']  = NULL;
    $node->body[$node->language][0]['format']  = 'full_html';
  }
  // Create a new node
  else {
    // a new node object
    $node = aspcm_input_form_build_node($form_state);
  }

  //��������ˡ���Ϊûת��
  // Save the content with node_save to make sure aspcm_nodeapi is invoked
  node_save($node);
  $form_state['values']['node_nid'] = $node->nid;
}

/**
 * Populate a node object for the input form data
 * All the fields in the input form will be saved in $node->body.
 * @param unknown_type $form_state
 */
function aspcm_input_form_build_node(&$form_state) {
  global $user;
      
  // Unset any form button level handlers 
  if (isset($form_state['clicked_button'])) {
    unset($form_state['clicked_button']);
  }
  
  // Unset the operation handlers
  unset($form_state['values']['Previous and Save']);
  unset($form_state['values']['Save']);
  unset($form_state['values']['Next and Save']);
  unset($form_state['values']['Search']);
  unset($form_state['values']['search_click']);
  unset($form_state['values']['save and redirect']);
  unset($form_state['values']['form_build_id']);
  unset($form_state['values']['form_token']);
  unset($form_state['values']['form_id']);
  unset($form_state['redirect']);
  unset($form_state['storage']);
  unset($form_state['submitted']);
  
  // Create a node object
  $node = new stdClass();
  
  // Generate a node title for the input form
  $node->title = aspcm_input_form_build_node_title($form_state['values']['hospital'], $form_state['values']['ward'], $form_state['values']['date']);
  
  //transform all product value to the unit of ml
  tranform_monthly_input_data_to_ml($form_state);
  
  // Populate a body for the node
  // Use the fields in the input form to build 
  $node->language = LANGUAGE_NONE;
  $node->body[$node->language][0]['value'] = serialize($form_state['values']);
  $node->body[$node->language][0]['summary']  = NULL;
  $node->body[$node->language][0]['format']  = 'full_html';
  $node->type = 'aspcm_input_form';
  
  $node->nid = '';
  
  // Populate the "authored by" field.
  $node->uid = $user->uid;

  $node->created = !empty($node->date) ? strtotime($node->date) : time();
  $node->validated = TRUE;
  
  return $node;
} 

/**
 * Function that load data from the web service.
 * @param $form_state
 */
function aspcm_input_form_load($form_state) {
  //save search key to temp
  $hid = $form_state['values']['hospital'];
  $wid = $form_state['values']['ward'];
  $date = $form_state['values']['date'];
  $node_title = aspcm_input_form_build_node_title($form_state['values']['hospital'], $form_state['values']['ward'], $form_state['values']['date']);

  //unset the $form_state when the data not exitst.
  unset($form_state);
  //set the search key
  $form_state['values']['hospital'] = $hid;
  $form_state['values']['ward'] =  $wid;
  $form_state['values']['date'] = $date;
  
  //$node = node_load(array('title' => $node_title, 'type' => 'aspcm_input_form'));
  $result = db_query("SELECT nid FROM {node} WHERE title = :node_title AND type = 'aspcm_input_form'",
           array(':node_title' => $node_title));
  if ($node = $result->fetchObject()) {
    $node = node_load($node->nid);
  }
  if ($node) {
    // return the form data when found
    $content = unserialize($node->body['und'][0]['value']);
    if ($content) {
      return array('values' => $content);
    }
    else {
      $msg = t("Unable to unserialize the string %string. \nstring length %length");
      $args = array('%string' => $node->body['und'][0]['value'], '%length' => strlen($content));
      watchdog(ASPCM_WATCHDOG_TYPE, $msg, $args, WATCHDOG_ERROR);
    }
  }
  else {
    //jaguar add ward alcohol and soap.it will use in the bottle to ml
    $ward_product_unit = get_ward_product_unit($wid);
    $form_state['values']['alcoholunit'] = $ward_product_unit['alcohol'];
    $form_state['values']['soapunit'] = $ward_product_unit['soap'];
    //end
  }
  // return form_state when the form is new
  return $form_state;
}

/**
 * 
 * Enter description here ...
 * @param $hospital
 * @param $ward
 * @param $date
 * 
 * @return $key
 */
function aspcm_input_form_build_node_title($hospital, $ward, $date) {
  // The title will be the [nid of hosiptial]:[nid of ward]:[the date of the input form]
  return $hospital .':'. $ward .':'. $date;
}

/**
 * Implementation of get_ward_compliance_rate().
 * @param $wardid
 * @return rate;
 */
function aspcm_input_form_get_compliance_rate($ward_id) {
  //global $itrinno_site_id, $aspcm_queue;
  //$safe_storage = new SafeStorage('aspcm_ward', $itrinno_site_id);
  //$arr = $safe_storage->load_data($ward_id);//wardid
  $arr = get_storage_array('aspcm_ward', $ward_id);
  
  $rate = $arr['field_aspcm_compliance_rate'][0]['value']; 
  return $rate;
}

/**
 * Get severity name by ward id
 * @param 
 * @param 
 */
function aspcm_get_severity_name($ward_id) {
  //global $itrinno_site_id, $aspcm_queue;
  // $safe_storage = new SafeStorage('aspcm_ward', $itrinno_site_id);
  //$arr = $safe_storage->load_data($ward_id);//wardid
  
  $arr = get_storage_array('aspcm_ward', $ward_id);
  $levelname = $arr['field_aspcm_criterial_by_level'][0]['value'];// level name
  
  return  $severity_nid;
}


/**
 * Author:Linda 2012/03/20
 * Implement: Transform the product input data value to ml with ward product setted unit
 * Parameter: $form_state
 */
function tranform_monthly_input_data_to_ml(&$form_state) {
  $product_category = get_product_category();
  if (count($form_state['values']) > 0) {
    //ward product setted unit
    $alcohol_unit = $form_state['values']['alcoholunit'];
    $soap_unit = $form_state['values']['soapunit'];

    foreach ($form_state['values'] as $tmp_key => $tmp_data) {
      if (preg_match('/product_/', $tmp_key)) {
        $product_id = trim($tmp_key, 'product_');
        $result = db_query("
                    SELECT arpc.field_aspcm_ref_product_category_nid AS category, 
                           apv.field_aspcm_product_volume_value AS volume FROM node AS n
                    LEFT JOIN field_data_field_aspcm_ref_product_category AS arpc ON n.nid = arpc.entity_id
                    LEFT JOIN field_data_field_aspcm_product_volume AS apv ON n.nid = apv.entity_id
                    WHERE n.type = 'aspcm_product' AND n.nid = :nid", 
                    array(':nid' => $product_id)
                  );
        $product_info = $result->fetchObject();

        if ($product_info != false) {
          $category_name = isset($product_category[$product_info->category]) ? $product_category[$product_info->category] : '';
          $cond1 = $category_name == ALCOHOL_JP && $alcohol_unit == 'bottle';
          $cond2 = $category_name == SOAP_JP && $soap_unit == 'bottle';

          if ($cond1 || $cond2) {
                $form_state['values'][$tmp_key] = $tmp_data * $product_info->volume;
          } 
          else {
          $form_state['values'][$tmp_key] = $tmp_data;
          }			
        }	           
      }	
    }
  }
  $form_state['values']['newdata'] = 1;
}

