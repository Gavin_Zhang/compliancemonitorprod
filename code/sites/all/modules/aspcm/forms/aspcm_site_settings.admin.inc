<?php
// $Id$
/**
 * @file: ASPCM to manage the protected fields which the values should be saved in MSSQL
 */

/**
 * Implementation of aspcm_protected_fields_form().
 * A form to manage the content type fields:
 * @param $form_state
 */
function aspcm_site_settings() {
  $form = array();
  
  $form['protected_fields_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Protected Fields'),
    '#weight' => -10,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
   
  // CONTENT TYPE fields
  $form['protected_fields_fieldset']['aspcm_protected_fields'] = array(
    '#type' => 'textarea',
    '#title' => t('Content types fileds'),
    '#default_value' => variable_get('aspcm_protected_fields', ''),
    '#description' => t('Format: CONTENT TYPE NAME|SURVEY ID|QUESTION CONTROL ID|QUESTION CONTROL ANSWER ID|QUESTION DATA ID|fields of this CONTENT TYPE separated with a comma (,). Put another content type field in aonther line.'),
  );
  
  // Data save options
  $form['protected_fields_fieldset']['aspcm_data_save_options'] = array(
    '#type' => 'radios',
    '#title' => t('Current Environment'),
    '#description' => t('Select the environment you are using now. Once Development is selected, 
        data will be saved in drupal\'s safe_table, otherwise, will be save in MSSQL.'),
    '#default_value' => variable_get('aspcm_data_save_options', ASPCM_DEV_SAVE_LOCATION),
    '#options' => array(ASPCM_DEV_SAVE_LOCATION => t('Development'), ASPCM_PROD_SAVE_LOCATION => t('Stage/Production')),
  );

  // Save method
  $form['protected_fields_fieldset']['aspcm_data_save_method'] = array(
    '#type' => 'radios',
    '#title' => t('Save Method'),
    '#description' => t('Select how the data is going to be saved in Stage/Production environment.
      If using batch, the system will use the new method in Evalution service called \'usersRetake\', otherwise, \'retake\' will be used, which is very slow'),
    '#default_value' => variable_get('aspcm_data_save_method', ASPCM_PROD_SAVE_BATCH),
    '#options' => array(ASPCM_PROD_SAVE_BATCH => t('Batch'), ASPCM_PROD_SAVE_ONEBYONE => t('One by One')),
  );

  // WS location
  $form['protected_fields_fieldset']['aspcm_data_ws_location'] = array(
    '#type' => 'textfield',
    '#title' => t('Webservice location'),
    '#description' => t('If the field has some value, this will be used to access the Evaluation
      webservice that will store the data ousite Drupal. If the field is empty, the module will use
      the information configured within framework module. Ex.: http://HOST/fwk_Evaluation_API_v2.0/usersRetake'),
    '#default_value' => variable_get('aspcm_data_ws_location', ''),
    '#size' => 77,
  );
  
  // Enable or Disable the comparsion cache.
  $form['protected_fields_fieldset']['aspcm_comparsion_cache'] = array(
    '#type' => 'radios',
    '#title' => t('Disable or Enable hospital comparsion cache'),
    '#description' => t('Select if enable the cache when calculating the hospital comparsion.'),
    '#default_value' => variable_get('aspcm_comparsion_cache', 'on'),
    '#options' => array('off'=>t('Disable'), 'on'=>t('Enable')),
  );
  // Set the hospital range.
  $form['protected_fields_fieldset']['aspcm_comparsion_range'] = array(
    '#type' => 'textfield',
    '#title' => t('Hospital Comparsion Range'),
    '#description' => t('Provide the range, e.g.: 10, sepcify the range of hospital while calculating the hospital comparsion.'),
    '#default_value' => variable_get('aspcm_comparsion_range', 10),
    '#max_length' => 3,
    '#size' => 3,
  );
  
  // Show messages in error_log
  $form['protected_fields_fieldset']['aspcm_data_message_errorlog'] = array(
    '#type' => 'radios',
    '#title' => t('Debug information in error_log'),
    '#description' => t('Select if some debug information will be printed in the apache error_log. This option should be turned on only in the development environment.'),
    '#default_value' => variable_get('aspcm_data_message_errorlog', ASPCM_DONT_PRINT_INFO),
    '#options' => array(ASPCM_PRINT_INFO => t('Print information in the log'), ASPCM_DONT_PRINT_INFO => t('Don\'t print information in the log')),
  );
  //set import csv codeing format
  $form['protected_fields_fieldset']['aspcm_code_format'] = array(
    '#type' => 'textfield',
    '#title' => t('CSV code format'),
    '#description' => t('Set export csv format:UTF-8,Shift-JIS,Windows-31J'),
    '#default_value' => variable_get('aspcm_code_format', 'UTF-8'),
  );
  
  // Node types
  $form['theme_node_types_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme option for these content types'),
    '#weight' => -5,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $node_types = node_type_get_types();
  
  // create an array with $types['node_type'] = Type Name
  foreach ($node_types as $name => $attr) {
    $types[$name] = $attr->name;
  }
  
  $form['theme_node_types_fieldset']['aspcm_site_settings_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#options' => $types,
    '#default_value' => variable_get('aspcm_site_settings_node_types', array()),
  );
  
  $form['theme_node_types_fieldset']['markup'] = array(
    '#value' => t('Choose asp_cm as default theme for content types when editing, adding, deleting pages.'),
  );
  
  $form['miscellaneous_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Miscellaneous settings'),
    '#weight' => -2,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['miscellaneous_fieldset']['aspcm_site_launch_year'] = array(
    '#type' => 'textfield',
    '#title' => t('Site launch year'),
    '#description' => t('Provide the date with format YYYY, e.g.: 2011, sepcify the launch year of this site.'),
    '#default_value' => variable_get('aspcm_site_launch_year', date('Y')),
    '#max_length' => 4,
    '#size' => 4,
  );
  
  return system_settings_form($form);
}