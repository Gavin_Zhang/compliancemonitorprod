<?php
// $Id$
/**
 * @file
 */

define('ASPCM_DEFAULT_SEND_EMAIL', 'your_user@example.com');
define('ASPCM_DEFAULT_FORM_EMAIL', 'noreply@example.com');

function email_address_settings() {
  $form['mail_addresses_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email'),
    '#weight' => -5,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['mail_addresses_fieldset']['email_address'] = array(
    '#type' => 'textarea',
    '#title' => t("contact-us E-mail address"),//連絡先のメールアドレス
    '#default_value' => variable_get("email_address", ASPCM_DEFAULT_SEND_EMAIL),
  ); 
  
  $form['mail_addresses_fieldset']['email_from_address'] = array(
    '#type' => 'textarea',
    '#title' => t("sender's E-mail address"),//連絡先のメールアドレス
    '#default_value' => variable_get("email_from_address", ASPCM_DEFAULT_FORM_EMAIL),
  ); 
  
  $form['mail_templates_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email templates'),
    '#weight' => -1,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['mail_templates_fieldset']['contact_us'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact us'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mail_templates_fieldset']['contact_us']['aspcm_wico_mail_notice'] = array(
    '#type' => 'textarea',
    '#title' => t('WICO Contact Us Notice'),
    '#default_value' => variable_get('aspcm_wico_mail_notice', _contact_us_mail_notice()),
  );
   
  $form['mail_templates_fieldset']['contact_us']['aspcm_contact_us_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('aspcm_contact_us_email_subject', _mail_subject_tpl()),
  );
 
  $form['mail_templates_fieldset']['contact_us']['aspcm_contact_us_email_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#description' => t('Place the email template in this field.'),
    '#default_value' => variable_get('aspcm_contact_us_email_body', _mail_html_tpl()),
  );
  
  $form['mail_templates_fieldset']['contact_us']['aspcm_contact_us_preview'] = array(
    '#type' => 'textarea',
    '#title' => t('Preview'),
    '#description' => t('Place the template for contact us confirmation page.'),
    '#default_value' => variable_get('aspcm_contact_us_preview', _preview_html_tpl()),
  );
  
  $form['mail_templates_fieldset']['contact_us']['aspcm_contact_us_email_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Mail Description'),
    '#default_value' => variable_get('aspcm_contact_us_email_description', _mail_desc_tpl()),
  );
  
  return system_settings_form($form);
}

function _placeholders() {
  return '%title represents the type of the question,  
    %choose represents the chooice user selected,  
    %question_type represents the type of the question, 
    %last_name represents last name, 
    %first_name represents first name, 
    %last_name_phonetic represents last name of english name, 
    %first_name_phonetic represents first name of english name, 
    %employer_name represents employer name, 
    %department_name represents the department name, 
    %positions represents the position, 
    %zip_code represents zipcode, 
    %address represents address, 
    %phone_1 represents phone number, 
    %email_1 represents email address, 
    %message represents the question content';
}

/* Initial mail template html code */
function _mail_html_tpl() {
  return '<table style="border: 1px solid rgb(204, 204, 204); margin-top: 20px; font-size: 13px; width: 100%;">
   <tr><td>
  <table style="font-size: 13px; ">
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px; width: 250px;">WEBサイト版コンプライアンスモニターを使用していますか？：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">%choose</td>
    </tr>
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px;">お問い合わせ内容：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">%question_type</td>
    </tr>
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px;">お名（漢字）：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">%first_name %last_name</td>
    </tr>
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px;">お名（カタカナ）：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">%first_name_phonetic %last_name_phonetic</td>
    </tr>
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px;">勤務先名称：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">%employer_name</td>
    </tr>
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px;">部署名：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">%department_name</td>
    </tr>
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px;">職種：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">%positions</td>
    </tr>
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px;">勤務先住所：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">郵便番号 %zip_code，%address</td>
    </tr>
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px;">ご連絡先電話番号（半角）：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">%phone_1</td>
    </tr>
    <tr>
      <td style="background-color: #EEE; font-weight: bold; padding: 6px 15px;">メールアドレス（半角）：</td>
      <td style="padding: 5px; word-wrap: break-word; word-break: normal;">%email_1</td>
    </tr>
  </table>
  </td></tr>
    <tr><td style="padding: 0 15px 5px; word-wrap: break-word; word-break: normal;">%message</td></tr>
</table>';
}

function _preview_html_tpl() {
  return '<div class="content-manual">
  <div class="field">
      <div class="field-label">WEBサイト版コンプライアンスモニターを使用していますか？：</div>
      <div class="field-items">%choose</div>
  </div >
  <div class="field">
      <div class="field-label">お問い合わせ内容：</div>
      <div class="field-items">%question_type</div>
    </div>
   <div class="field">
      <div class="field-label">お名（漢字）：</div>
      <div class="field-items">%first_name %last_name</div>
    </div>
   <div class="field">
      <div class="field-label">お名（カタカナ）：</div>
      <div class="field-items">%first_name_phonetic %last_name_phonetic</div>
    </div>
   <div class="field">
      <div class="field-label">勤務先名称：</div>
      <div class="field-items">%employer_name</div>
    </div>
   <div class="field">
      <div class="field-label">部署名：</div>
      <div class="field-items"> %department_name</div>
    </div>
   <div class="field">
      <div class="field-label">職種：</div>
      <div class="field-items">%positions</div>
    </div>
   <div class="field">
      <div class="field-label">勤務先住所：</div>
      <div class="field-items">郵便番号 %zip_code，%address</div>
    </div>
   <div class="field">
      <div class="field-label">ご連絡先電話番号（半角）：</div>
      <div class="field-items">%phone_1</div>
    </div>
   <div class="field">
      <div class="field-label">メールアドレス（半角）：</div>
      <div class="field-items">%email_1</div>
    </div>
    <div class="field">%message</div>
</div>';
}

function _mail_desc_tpl() {
  return '<p style="font-size: 13px;">** 下記メールは一部、社外のお客様の個人情報を含んでおりますので取り扱いにはご注意ください **
*----------------------------------------------------------------------------*
コンプライアンスモニター管理者各位
お疲れ様です。
下記の問い合わせがCMウェブサイトから寄せられましたので対応をお願いいたします。
*----------------------------------------------------------------------------*</p>';
}

function _mail_subject_tpl() {
  return 'コンプライアンスモニターお問い合わせ【%title】';
}

function _contact_us_mail_notice() {
  return '<div class="contact-descript">
<h3>ご注意</h3>
<p>「送信」ボタンをクリックして登録情報をご提供いただくことで、これらの情報に対して本サイトのプライバシーポリシーが適用されることに同意いただいたものとさせていただきます。</p>
<p class="tar"><a href="/legalnotice/privacy-policy" class="ext-link-text">インターネットプライバシーポリシー</a><a href="/legalnotice/privacy-policy" class="ext-link"></a></p>
<p>お寄せいただいた個人情報は弊社からの回答、また同意いただいた情報提供およびそれに付随する業務の目的以外には使用いたしません。</p>
<p>個人情報のご提供は任意ですが、ご提供いただけない場合、弊社からの回答および関連する情報の提供ができなくなりますので、予めご了承下さい。</p>
<p class="tar"><a href="/legalnotice/protection-policy" class="ext-link-text">個人情報保護に関する事項</a><a href="/legalnotice/protection-policy" class="ext-link"></a></p>
<p>回答は、場合により、電話、文書等で行わせていただくことがございます。また、お問い合わせの内容により、回答まで多少お時間をいただく場合もございますのでご了承ください。</p>
<p>このサイトは、有害事象報告のためのものではありません。有害事象については、弊社担当営業にご相談ください。</p>
</div>';
}