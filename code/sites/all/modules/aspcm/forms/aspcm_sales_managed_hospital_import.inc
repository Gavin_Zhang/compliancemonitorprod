<?php
// $Id$

/**
 * @file: aspcm_sales_managed_hospital_import.inc
 */

define ('ASP_HOSPITALS_MANAGEMENT', 'asp_hospitals_management');

/**
 * Implementation of aspcm_sales_managed_hospitals_import().
 * @param  $form_state
 */
module_load_include('inc', 'aspcm', 'includes/aspcm_csv');
function aspcm_sales_managed_hospitals_import($form, &$form_state) {
  $form = array();
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  $form['import'] = array(
    '#type' => 'file',
  );

  $form['upload'] = array(
    '#type' => 'submit',
    '#value' => t('インポート'),
    '#attributes' => array('class' => array('lnk-empty-butt')),
  );

  $form['#action'] = '/masters-mgmt/sales-hospital-import';
  $form['#theme'] = 'theme_aspcm_sales_managed_hospitals_import_form';
  return $form;
}

/**
 *
 * Implementation of aspcm_sales_managed_hospitals_import_validate().
 * @param array $form
 * @param array $form_state
 */
function aspcm_sales_managed_hospitals_import_validate($form, &$form_state) {
  //Upload a file.
  $validators = array('file_validate_extensions' => array('csv'));
  $destination = "public://";
  //$validators['file_validate_extensions'] = array('csv');
  $files = file_save_upload('import', $validators, $destination, FILE_EXISTS_REPLACE);
  $form_state['values']['import'] = $files; // Put the file information to $form_state.
}
/**
 *
 * Implementation of aspcm_sales_managed_hospitals_import_submit().
 * @param array $form
 * @param array $form_state
 */
function aspcm_sales_managed_hospitals_import_submit($form, &$form_state) {
    //CSV file processing.
    $csv_file = fopen($form_state['values']['import']->uri, 'r');

    if ($csv_file) { //Check the file type.
      $filename = $form_state['values']['import']->filename;
      if (substr($filename, strlen($filename)-4, 4) != '.csv') {
          drupal_set_message(t('ファイル形式がCSVであることを確認してください。'), 'error');
          return NULL;
      }

      $error_msg; $i = 0;
      while ($data = fgetcsv($csv_file)) {

        // Ignore the title line
        if ($i == 0) {
          $i++;
          continue;
        }

        $item_list[] = array_map('trim', $data);
      }

      $result = csv_file_data_confirm($item_list,$form_state,$error_msg);

      if ($result) {
        drupal_set_message(t('CSVデータがデータベースにアップロードされました。'));
      }
      else {
        drupal_set_message(t('データをアップロードすることができませんでした。CSVファイルを確認し、正しいフォーマットであるかどうか確認してください。'), 'error');
      }

      fclose($csv_file);
      file_delete($form_state['values']['import']);
    }
    else {
      drupal_set_message(t('CSVファイルを選択してください。もしくはCSVファイルパスが正しいことを確認してください。'), 'error');
    }

    // Flush the caches to update the values on the page
    drupal_flush_all_caches();
}

/**
 * Implementation of csv_file_data_confirm().
 * 
 * @param array $item_list
 * @param array $form_state
 * @param array $error_msg
 * Here we compare the CSV file data with current site data, and cover it with CSV file data if all matched.
 */
function csv_file_data_confirm($item_list, $form_state, &$error_msg) {
  // $item_list will be an array either two columns (hospital code, sale code) or 
  // three columns (hospital code, sale code, user Japanese name) 
  
  // Determine how many hospitals will be updated a time at most
  $update_process_items = 20;
  
  $data = csv_file_data_preparation($item_list);
  
  foreach ($data as $itms) {
    $tmp_nids_chunks = array_unique($itms['hids']);
    unset($itms['hids']);
    
    $tmp_nids_chunks = array_chunk($tmp_nids_chunks, $update_process_items);
    
    foreach ($tmp_nids_chunks as $hids) {
      update_content_type_aspcm_hospital($hids, $itms['params']);
    }
    
    unset($tmp_nids_chunks);
  }
  
  return TRUE;
}

/**
 * Prepare data before updating into database
 * @param $item_list
 */
function csv_file_data_preparation($item_list) {
  // Define an array to hold the values, such as sale's code, name, hospital node ids
  $data = array();
  
  // Get all hospitals
  $hospitals = get_hospitals();
  
  // Get all user names, to make sure the users in csv file are already in Drupal
  $users_name_uid = get_all_user_name_and_uid();
  
  if (empty($users_name_uid)) {
    return $data;
  }
  
  // Make user names to be the keys of the elements
  // To fasten the user name searching by calling isset($arr[$name]) 
  $unames = drupal_map_assoc($users_name_uid['name']);
  // Unset the array to release memory
  unset($users_name_uid);
  
  // Looping the items in the csv file
  for ($k = 0; $k < count($item_list); $k++) {
    
    // Variables to hold hospital code and node_id found
    $h_code = $h_nid = NULL;    
    // If sale's Japanese name is also provided in the csv third column
    // $uname_jp will be the variable to hold the value
    $uname_jp = $sale_code = NULL;
    
    // If the user is in Drupal already
    if (isset($unames[$item_list[$k][1]])) {
      for ($j = 0; $j < count($hospitals); $j++) {
        $h_code = $hospitals[$j]['code'][0]['value'];

        // If hospital code is in Drupal already
        if ($item_list[$k][0] == $h_code){
          $h_nid = $hospitals[$j]['nid'];
          $sale_code = $item_list[$k][1];
          $uname_jp = isset($item_list[$k][2]) ? $item_list[$k][2] : NULL;
          break;
        }
      }
      
      // Both the hospital id and hospital sale's code
      if (!empty($h_nid) && !empty($sale_code)) {
        
        if (!isset($data[$sale_code])) {
          $params = array('sale_code' => $sale_code, 'type' => 'csv');
          if (!empty($uname_jp)) {
            $params['uname_jp'] = mb_convert_encoding($uname_jp, "UTF-8", "Shift-JIS");
          }
          $data[$sale_code]['params'] = $params;
        }
                
        $data[$sale_code]['hids'][] = $h_nid;
      }
    }
  }
  
  unset($unames);
  unset($hospitals);
  unset($item_list);

  return $data;
}
