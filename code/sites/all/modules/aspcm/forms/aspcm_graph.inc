<?php
// $Id$
/**
 * @file graph.inc
 *
 * A bdle of functions of graph form
 */

module_load_include('inc', 'aspcm', '/includes/aspcm_ward_annually_data_view');
module_load_include('inc', 'aspcm', 'includes/aspcm_graph_default_configuration');

/**
 * Implementation of aspcm_graph_form().
 * This is a callback function for the path '/graph'.
 * @see aspcm_menu()
 * @return Array
 */
function aspcm_graph_form($form, &$form_state) {
  global $default_settings, $user;

  $form = get_cookie_from_hispitalandward($form_state);
  //Fix the form id to avoid layout issue.
  $form['#id'] = 'aspcm-graph-form';
  //Set $id from cookie and put the presetA by default
  if (isset($_COOKIE['ASP_PresetID'])) {
    $id = $_COOKIE['ASP_PresetID'];
    $form_state['active'] = $_COOKIE['ASP_PresetID'];
  }
  else {
    $id = 1;
    $form_state['active'] = 1;
  }
  //Get the current user's settings
  $row = aspcm_get_personal_setting($user->uid, $id);
// add by yueshuo
$form['presetRadios'] = array(
    '#type' => 'radios',
    '#attributes' =>array('style' => array('float:left'),'class'=>array('presetRadios_ie')),
      '#options' => array(
        t('遵守状況グラフ'),
        t('感染率推移グラフ'),
        t('病棟別グラフ'),
        t('自己設定グラフ'),
    ),
    '#default_value' =>0,
    '#suffix' => "<div class='clear'></div>"
//    '#weight' => 1,
  );
    //ajax check radio user setting url
    $form['ajax_check_user_setting_url'] = array(
        '#type' => 'hidden',
        '#value' => url('alj_ajax_check_single_graph_personal_setting'),
      );
  //Put the three preset ;die;button in the graph page//（modified by yueshuo）
//  $form['presetA'] = array(
//    '#type' => 'submit',
//    '#value' => '遵守状況グラフ',
//    '#attributes' => ($form_state['active'] == 1) ? array('class' => array('active')) : array('class' => array()),
//    '#weight' => 1,
//    '#prefix' => ($form_state['active'] == 1) ? "<li class='tab-active'>" : "<li>",
//    '#suffix' => "</li>"
//  );
//  $form['presetB'] = array(
//    '#type' => 'submit',
//    '#value' => '感染率推移グラフ',
//    '#attributes' => ($form_state['active'] == 2)? array('class' => array('active')):array('class' => array()),
//    '#weight' => 2,
//    '#prefix' => ($form_state['active'] == 2) ? "<li class='tab-active'>" : "<li>",
//    '#suffix' => "</li>"
//  );
//  $form['presetC'] = array(
//    '#type' => 'submit',
//    '#value' => '病棟別グラフ',
//    '#attributes' => ($form_state['active'] == 3)? array('class' => array('active')):array('class' => array()),
//    '#weight' => 3,
//    '#prefix' => ($form_state['active'] == 3) ? "<li class='tab-active'>" : "<li>",
//    '#suffix' => "</li>"
//  );
   
  //Put four generate buttons in the graph page
   $form['generate_main'] = array(
    '#type' => 'submit', 
    '#value' => t('Generate Graph'), 
    '#attributes' => array('class' => array('lnk-empty-butt')),
    '#weight' => 4,
  );
  $form['generate_major'] = array(
    '#type' => 'submit', 
    '#value' => t('Generate Graph A'), 
    '#weight' => 5,
  );
  $form['generate_minor'] = array(
    '#type' => 'submit', 
    '#value' => t('Generate Graph B'), 
    '#weight' => 6,
  );
  $form['generate_horizontal'] = array(
    '#type' => 'submit', 
    '#value' => t('Generate Graph C'), 
    '#weight' => 7,
  );
 
  $form['horizontal_axis'] = array(
    '#type' => 'radios',
    '#options' => array(
      'period' => t('期間'),
      'ward' => t('病棟')
      ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['horizontal_axis'][$id] : $row->horizontal_axis) : $form_state['values']['horizontal_axis'],
  );
  $form['horizontal_start'] = array(
    '#type' => 'textfield',
    '#attributes' => array('class' => array('small')),
    '#maxlength' => 7,
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['horizontal_start'][$id] : $row->horizontal_start) : $form_state['values']['horizontal_start'],
	'#field_suffix' => 'から',
    '#field_prefix' => '例：2011/04',
  );
  $form['horizontal_end'] = array(
    '#type' => 'textfield',
    '#attributes' => array('class' => array('small')),
    '#maxlength' => 7,
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['horizontal_end'][$id] : $row->horizontal_end) : $form_state['values']['horizontal_end'],
	'#field_suffix' => 'まで',
    '#field_prefix' => '例：2011/09',
  );

  $form['unit_of_period_displayed'] = array(
    '#type' => 'select',
    '#options' => array(
      '1' => t('１ヶ月'),
      '2' => t('２ヶ月'),
      '3' => t('３ヶ月'),
      '4' => t('４ヶ月'),
      '6' => t('６ヶ月'),
      '12' => t('１２ヶ月')
    ),
    
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['unit_of_period_displayed'][$id] : $row->unit_of_period_displayed) : $form_state['values']['unit_of_period_displayed'],
  );
  //Put the major axis fields in the graph page

  $form['major_value'] = array(
    '#type' => 'radios',
    //'#title' => 'Major Value',
    '#options' => array(
      'hand_washing' => t('手指衛生回数'),
      'antiseptic_used' => t('手指衛生剤使用量'),
      'compliance_rate' => t('遵守率'),
      'infection_rate' => t('感染率'),
      'none' => t('なし'),
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['major_value'][$id] : $row->major_value) : $form_state['values']['major_value'],
  );

  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['major_wash_number'] = array(
    '#type' => 'select',
    '#options' => array(
      'all_patient' => t('アルコール手指消毒剤＋石けん　1患者日あたり'),
      'all_patient_1000' => t('アルコール手指消毒剤＋石けん　1スタッフあたり'),
      'alcohol_patient' => t('アルコール手指消毒剤　1患者日あたり'),
      'alcohol_staff' => t('アルコール手指消毒剤　1スタッフあたり'),
      'soap_patient' => t('石けん　1患者日あたり'),
      'soap_staff' => t('石けん　1スタッフあたり'),
    
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['major_wash_number'][$id] : $row->major_wash_number) : $form_state['values']['major_wash_number'],
  );
  
  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['major_antisepic_volume'] = array(
    '#type' => 'select',
    '#options' => array(
      'all_patient' => t('アルコール手指消毒剤＋石けん　1患者日あたり'),
      'all_patient_1000' => t('アルコール手指消毒剤＋石けん　1000患者日あたり'),
      'alcohol_patient' => t('アルコール手指消毒剤　1患者日あたり'),
      'alcohol_patient_1000' => t('アルコール手指消毒剤　1000患者日あたり'),
      'sopa_patient' => t('石けん　1患者日あたり'),
      'sopa_patient_1000' => t('石けん　1000患者日あたり'),
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['major_antisepic_volume'][$id] : $row->major_antisepic_volume) : $form_state['values']['major_antisepic_volume'],
    
  );  
    
  $form['major_maximum_value'] = array(
    '#type' => 'radios',
    '#options' => array(
      'automatic' => t('自動（自動で主軸の最大値を決定する）'),
      'fixed' => t('固定（自分で任意の最大値を入力する）'),
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['major_maximum_value'][$id] : $row->major_maximum_value) : $form_state['values']['major_maximum_value'],
  );
  
  $form['major_fixed_value'] = array(
    '#type' => 'textfield',
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['major_fixed_value'][$id] : $row->major_fixed_value) : $form_state['values']['major_fixed_value'],
    '#attributes' => array('class' => array('small')),
    '#maxlength' => 8,
  );
  
  //Put the minor axis fields in the graph page
  $form['minor_value'] = array(
    '#type' => 'radios',
    '#options' => array(
      'hand_washing' => t('手指衛生回数'),
      'antiseptic_used' => t('手指衛生剤使用量'),
      'compliance_rate' => t('遵守率'),
      'infection_rate' => t('感染率'),
      'none' => t('なし'),
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['minor_value'][$id] : $row->minor_value) : $form_state['values']['minor_value'],
  );
  
  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['minor_wash_number'] = array(
    '#type' => 'select',
    '#options' => array(
      'all_patient' => t('アルコール手指消毒剤＋石けん　1患者日あたり'),
      'all_patient_1000' => t('アルコール手指消毒剤＋石けん　1スタッフあたり'),
      'alcohol_patient' => t('アルコール手指消毒剤　1患者日あたり'),
      'alcohol_staff' => t('アルコール手指消毒剤　1スタッフあたり'),
      'soap_patient' => t('石けん　1患者日あたり'),
      'soap_staff' => t('石けん　1スタッフあたり'),
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['minor_wash_number'][$id] : $row->minor_wash_number) : $form_state['values']['minor_wash_number'],
    
  );
  
  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['minor_antisepic_volume'] = array(
    '#type' => 'select',
    '#options' => array(
      'all_patient' => t('アルコール手指消毒剤＋石けん　1患者日あたり'),
      'all_patient_1000' => t('アルコール手指消毒剤＋石けん　1000患者日あたり'),
      'alcohol_patient' => t('アルコール手指消毒剤　1患者日あたり'),
      'alcohol_patient_1000' => t('アルコール手指消毒剤　1000患者日あたり'),
      'sopa_patient' => t('石けん　1患者日あたり'),
      'sopa_patient_1000' => t('石けん　1000患者日あたり'),
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['minor_antisepic_volume'][$id] : $row->minor_antisepic_volume) : $form_state['values']['minor_antisepic_volume'],
    
  );  
  
  $form['minor_maximum_value'] = array(
    '#type' => 'radios',
    '#options' => array(
      'automatic' => t('自動（自動で副軸の最大値を決定する）'),
      'fixed' => t('固定（自分で任意の最大値を入力する）'),
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['minor_maximum_value'][$id] : $row->minor_maximum_value) : $form_state['values']['minor_maximum_value'],
  );
  
  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['major_single_bar'] = array(
    '#type' => 'radios',
    '#options' => array(
      'multiple' => t('アルコール手指消毒剤と石けんを別々に表示'),      
      'single' => t('アルコール手指消毒剤と石けんを一つに統合'),
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['major_single_bar'][$id] : $row->major_single_bar) : $form_state['values']['major_single_bar'],
  );
  
  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['minor_single_line']= array(
    '#type' => 'radios',
    '#options' => array(
      'multiple' => t('アルコール手指消毒剤と石けんを別々に表示'),
      'single' => t('アルコール手指消毒剤と石けんを一つに統合'),
    ),
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['minor_single_line'][$id] : $row->minor_single_line) : $form_state['values']['minor_single_line'],
  );
  
  $form['minor_fixed_value'] = array(
    '#type' => 'textfield',
    '#default_value' => !isset($form_state['values']) ? (empty($row) ? $default_settings['minor_fixed_value'][$id] : $row->minor_fixed_value) : $form_state['values']['minor_fixed_value'],
    '#attributes' => array('class' => array('small')),
    '#maxlength' => 8,
  );
  
  $form['custom_error'] = array(
    '#type' => 'hidden',
    '#value' => !isset($form_state['custom_error']['#value']) ? FALSE : $form_state['custom_error']['#value'],
  );
  
  $form['save'] = array(
    '#type' => 'submit', 
    '#value' => t('Save preset'), 
  );
  
  $form['reset'] = array(
    '#type' => 'submit', 
    '#value' => t('Reset preset'), 
     
  );
  
  $form['image'] = array(
    '#markup' => get_image_src($form_state),
  );
 
  $form['download'] = array(
    '#type' => 'submit', 
    '#value' => t('Download image'),
    '#access' => $form_state['values']['download_button']['visible']==false?false:true,    
  );

  $form['graph_path'] = array(
    '#type' => 'hidden',
	'#attributes' => array('id' => 'edit-graph-path'),
    '#value' => !isset($form_state['values']['graph_path']) ? '' : $form_state['values']['graph_path'],
  );
  
  $infection_rate_values = !empty($form['ward']['#options']) ? get_diseases_by_wids($form['ward']['#options']) : array();
  
  $form['minor_infection_rate'] = array(
    '#type' => 'select',
    '#options' => $infection_rate_values,
    '#default_value' => !isset($form_state['values']) ? '' : $form_state['values']['minor_infection_rate'],
  );
  
  $form['major_infection_rate'] = array(
    '#type' => 'select',
    '#options' => $infection_rate_values,
    '#default_value' => !isset($form_state['values']) ? '' : $form_state['values']['major_infection_rate'],
  );
  
  //var_dump(mktime(0, 0, 0, 1, 12, 2011));
  $form['#action'] = '/graph/single-graph';
  $form['#theme'] = 'theme_aspcm_graph_form';
  //6-7
 // $form['#token'] = FALSE;
  return $form;
}

function get_cookie_from_hispitalandward($form_state) {
  $c_hid = $_COOKIE['ASP_HospitalID_Graph'];
  $c_list_id = $_COOKIE['ASP_WardID_Graph'];
  // first click the graph page
  global $user;
  $user_obj = get_user_profile($user->id);
 
  if (!empty($user_obj->asp_hospital_id)&&empty($user_obj->asp_ward_id)&&empty($_COOKIE['ASP_HospitalID_Graph'])) {
    $h_id = $user_obj->asp_hospital_id;
    $c_hid = $h_id;
  }
  else if (!empty($user_obj->asp_hospital_id)&&!empty($user_obj->asp_ward_id)&&empty($_COOKIE['ASP_HospitalID_Graph'])) { 
    $h_id = $user_obj->asp_hospital_id;
    $w_id1 = $user_obj->asp_ward_id .'#';
    $c_hid = $h_id;
    $c_list_id = $w_id1;
  }
  
  $w_id = array();
  if (isset($c_hid)) {
    $c_hname = get_hospital_name($c_hid);
  }
  else {
    $c_hname = '';
  }
  if (isset($c_list_id)) {
    $w_id = explode("#", $c_list_id);
  }
  if (isset($form_state['values']['ward'])) {
    $wardlist = $form_state['values']['ward'];
    foreach ((array)$wardlist as $key => $value) {
      $form_state_wid .= $key .'#';
    }
  }
  $hname = isset($form_state['values']['hospital']) ? $form_state['values']['hospital'] : $c_hname;
  $w_id = isset($form_state['values']['ward']) ? $form_state_wid : $w_id;
  //---------$form start
  $form = array();
  $form = _graph_hospital_ward_form_elemements($hname, $w_id);
  return $form;
}

function _graph_hospital_ward_form_elemements($hosp_name = NULL, $w_id = NULL) {
  global $user;
  
  $form = array();
  $attr = array('class' => array('form-select required'));
  
  $user_obj = get_user_profile($user->id);
  if (!empty($user_obj->asp_hospital_id)) {
    $attr['readonly'] = 'readonly';
  }
  $search_auto = 'search-autoc';
  if ($user_obj->dishos) {$search_auto = '';}
  
  $form['hospital'] = array(
    '#type' => 'textfield',
    '#default_value' => $hosp_name,
    '#attributes' => $attr,
    '#autocomplete_path' => 'ajax/hospital/autocomplete',
    '#ajax' => array(
      'event' => '', // don't put any values, because this event trigger is customized
      //'path' => 'ward/jsgraph',
      'callback' => 'aspcm_ward_jsgraph',
      'wrapper' => 'edit-ward-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#suffix' => '<a href="#" class="'. $search_auto .'"></a>',
    '#disabled' => $user_obj->dishos,
  );
  //save the value when the 'hospital' is disabled
  $form['hospital_hide'] = array(
    '#type' => 'hidden',
    '#default_value' => $hosp_name ,
  );
  $wards = array();
  if (!empty($hosp_name)) {
    $wards = aspcm_get_ward_name($hosp_name);
  }
  foreach ($wards as $key => $value) {
    $wardid[] = $key;
  }
  if (isset($w_id) && !empty($w_id)) {
    foreach ((array)$w_id as $key1 => $value1) {
      if ($value1 != '') {
      $ww_id[] = $value1;
      }
    }
  }
  $flag = TRUE;
  if (isset($ww_id) && !empty($ww_id) && isset($wardid) && !empty($wardid)) {
    foreach ($ww_id as $key => $value) {
      if (!in_array($value, $wardid)) {
      $flag = FALSE;
      break;
      }
    }
  }
   //when first enter,selected all;
  if (empty($ww_id)) {
    $flag = FALSE;
  }
  if (count(explode('#', $ww_id[0]))>1) {
    $ww_id = explode('#', $ww_id[0]);
    $ww_id = (array)$ww_id;
  }
  if (!empty($user_obj->asp_hospital_id)&&!empty($user_obj->asp_ward_id)) {
    if (empty($ww_id[0])) {
      $ww_id[0] = $user_obj->asp_ward_id;
    }
    $getward = get_ward_by_wid($ww_id[0]);
    $a = array();
    foreach ($getward as $key => $value) {
       $a[$key]['id'] = $key;
       $a[$key]['val'] = $value;
    }
    $form['ward3']=array(
      '#type' => 'hidden',
      '#id' => 'edit-ward3',
      '#value' => array(json_encode($a))
      );
    $form['ward'] = array(
      '#type' => 'select',
	  '#id' => 'edit-ward',
      '#options' => $getward,
      '#attributes' => $attr,
      '#multiple' => TRUE,
      '#disabled' => empty($getward) ? TRUE : FALSE,
      //'#required' => empty($wards) ? FALSE : TRUE,
      //'#default_value' => array($ww_id[0])
    );
    $form['ward2'] = array(
      '#type' => 'select',
    '#id' => 'edit-ward2',
      //'#options' => $getward,
      '#attributes' => $attr,
      '#multiple' => TRUE,
      '#disabled' => empty($getward) ? FALSE : FALSE,
      //'#required' => empty($wards) ? FALSE : TRUE,
      // '#default_value' => array($ww_id[0]),
      '#weight' => 0.001,
    );
  }
  else {
    $aa = array();
    foreach ($wards as $key => $value) {
       $aa[$key]['id'] = $key;
       $aa[$key]['val'] = $value;
    }
    $form['ward3']=array(
      '#type' => 'hidden',
      '#id' => 'edit-ward3',
      '#value' => array(json_encode($aa))
      );
    $form['ward'] = array(
      '#type' => 'select',
	  '#id' => 'edit-ward',
      '#options' => $wards,
      '#attributes' => array('class' => 'form-select required'),
      '#multiple' => TRUE,
      '#disabled' => empty($wards) ? TRUE : FALSE,
      //'#default_value' => $flag ? $ww_id : $wardid
    );
    $form['ward2'] = array(
      '#type' => 'select',
    '#id' => 'edit-ward2',
      //'#options' => $wards,
      '#attributes' => array('class' => 'form-select required'),
      '#multiple' => TRUE,
      '#disabled' => empty($wards) ? FALSE : FALSE,
      // '#default_value' => $flag ? $ww_id : $wardid,
      '#weight' => 0.001,
    );
  }
  $form['#theme'] = 'theme_aspcm_graph_form';
  return $form;
}


/**
 * Submit event for graph form
 * 
 * @param $form
 * @param $form_state
 */
function aspcm_graph_form_submit($form, &$form_state) {
  
  module_load_include('inc', 'aspcm', '/includes/aspcm_graph_process');
  global $user;
  //set cookie
  $hname = $form_state['values']['hospital'];
  $hid = get_hospital_id_by_hospitalname($hname);
  
  // $form_state['values']['ward'] = $form_state['values']['ward2'];
  $wardlist = $form_state['values']['ward2'];//240 => string '240'
  
  //Set $id accroding to the cookie and put the presetA by default
  if (isset($_COOKIE['ASP_PresetID'])) {
    $id = $_COOKIE['ASP_PresetID'];
    $form_state['active'] = $_COOKIE['ASP_PresetID'];
  }
  else {
    $id = 1;
    $form_state['active'] = 1;
  }

  foreach ($wardlist as $key => $value) {
    $wid .= $key .'#';
  }
  
  setcookie("ASP_HospitalID_Graph", $hid, 0, '/');
  setcookie("ASP_WardID_Graph", $wid, 0, '/');
  //get cookie when click preset
  $form = get_cookie_from_hispitalandward($form_state);

  //get the user operation
  $op = $form_state['clicked_button']['#id'];

  $form_state['rebuild'] = TRUE;
  
  $form_state['values']['graph_name'] = time();
  
  //set title for the graph (modified by yueshuo)
  if (!isset($form_state['values']['presetRadios'])){//default valule
      $form_state['values']['title'] = '遵守状況グラフ';
  }else {
      $ac   =        $form_state['values']['presetRadios'];
      $ac   ==  0 && $form_state['values']['title'] = '遵守状況グラフ';
      $ac   ==  1 && $form_state['values']['title'] = '感染率推移グラフ';
      $ac   ==  2 && $form_state['values']['title'] = '病棟別グラフ';
      $ac   ==  3 && $form_state['values']['title'] = '自己設定グラフ';
      unset($ac);
  }
  
  //set title for the graph
//  if ($id == 1) {
//    $form_state['values']['title'] = '遵守状況グラフ';
//  }
//  elseif ($id == 2) {
//    $form_state['values']['title'] = '感染率推移グラフ';
//  }
//  elseif ($id == 3) {
//    $form_state['values']['title'] = '病棟別グラフ';
//  }

  switch ($op) {
    case 'edit-preseta': 
      aspcm_preset_submit($id, $form_state);
      break;
    
    case 'edit-presetb':
      aspcm_preset_submit($id, $form_state);
      break;
    
    case 'edit-presetc':
      aspcm_preset_submit($id, $form_state);
      break;
    
    case 'edit-save':
      aspcm_save_personal_setting($form_state, $user->uid);
      aspcm_build_graph($form_state['values']);
      break;
      
    case 'edit-reset':
      aspcm_reset_personal_setting($form_state, $user->uid);
      aspcm_preset_submit($id, $form_state);
      break;
      
    case 'edit-download':
      aspcm_graph_download($form_state['input']['graph_path']);
      break;  
    default:  
      //rebuild the $form_state
      $form_state['rebuild'] = TRUE;
      //generate the graph
      aspcm_build_graph($form_state['values']);
      break;
  }
}

/**
 * Preset tab clicked
 * 
 * First, we will check if current user has his/her own settings for generating chart, 
 * if there isn't, the default values will be applied to generate the chart. 
 * 
 * @param $id, the id of the tab, Preset tabs will be marked as 1, 2, 3
 * @param $form_state
 */
function aspcm_preset_submit($id, &$form_state) {
  global $default_settings, $user;
  
  // Save the graph data to the $form_state
  $form_state['active'] = $id;

  // Get the personal settings form aspcm_personal_table according to the user_id and $id(preset_num)
  $saved = aspcm_get_personal_setting($user->uid, $id);

  $form_state['values']['major_value'] = empty($saved) ? $default_settings['major_value'][$id] : $saved->major_value;
  $form_state['values']['major_maximum_value'] = empty($saved) ? $default_settings['major_maximum_value'][$id] : $saved->major_maximum_value;
  $form_state['values']['major_fixed_value'] = empty($saved) ? $default_settings['major_fixed_value'][$id] : $saved->major_fixed_value;
  $form_state['values']['major_wash_number'] = empty($saved) ? $default_settings['major_wash_number'][$id] : $saved->major_wash_number;
  $form_state['values']['major_antisepic_volume'] = empty($saved) ? $default_settings['major_antisepic_volume'][$id] : $saved->major_antisepic_volume;      

  $form_state['values']['minor_value'] = empty($saved) ? $default_settings['minor_value'][$id] : $saved->minor_value;      
  $form_state['values']['minor_maximum_value'] = empty($saved) ? $default_settings['minor_maximum_value'][$id] : $saved->minor_maximum_value;
  $form_state['values']['minor_fixed_value'] = empty($saved) ? $default_settings['minor_fixed_value'][$id] : $saved->minor_fixed_value;
  $form_state['values']['minor_wash_number'] = empty($saved) ? $default_settings['minor_wash_number'][$id] : $saved->minor_wash_number;
  $form_state['values']['minor_antisepic_volume'] = empty($saved) ? $default_settings['minor_antisepic_volume'][$id] : $saved->minor_antisepic_volume;
     
  $form_state['values']['horizontal_axis'] = empty($saved) ? $default_settings['horizontal_axis'][$id] : $saved->horizontal_axis;
  $form_state['values']['horizontal_start'] = empty($saved) || empty($saved->horizontal_start)? $default_settings['horizontal_start'][$id] : $saved->horizontal_start;
  $form_state['values']['horizontal_end'] = empty($saved) || empty($saved->horizontal_end) ? $default_settings['horizontal_end'][$id] : $saved->horizontal_end;
  $form_state['values']['unit_of_period_displayed'] = empty($saved) ? $default_settings['unit_of_period_displayed'][$id] : $saved->unit_of_period_displayed;
  $form_state['values']['major_single_bar'] = empty($saved) ? $default_settings['major_single_bar'][$id] : $saved->major_single_bar;
  $form_state['values']['minor_single_line'] = empty($saved) ? $default_settings['minor_single_line'][$id] : $saved->minor_single_line;

  //6-7 reset $form_state['input'] value before rebuild form 
  $form_state['input']['major_value'] = $form_state['values']['major_value'];
  $form_state['input']['major_maximum_value'] = $form_state['values']['major_maximum_value'];
  $form_state['input']['major_fixed_value'] = $form_state['values']['major_fixed_value'];
  $form_state['input']['major_wash_number'] = $form_state['values']['major_wash_number'];
  $form_state['input']['major_antisepic_volume'] = $form_state['values']['major_antisepic_volume'];
  $form_state['input']['minor_value'] = $form_state['values']['minor_value'];
  $form_state['input']['minor_maximum_value'] = $form_state['values']['minor_maximum_value'];
  $form_state['input']['minor_fixed_value'] = $form_state['values']['minor_fixed_value'];
  $form_state['input']['minor_wash_number'] = $form_state['values']['minor_wash_number'];
  $form_state['input']['minor_antisepic_volume'] = $form_state['values']['minor_antisepic_volume'];  
  $form_state['input']['horizontal_axis'] = $form_state['values']['horizontal_axis'];
  $form_state['input']['horizontal_start'] = $form_state['values']['horizontal_start'];
  $form_state['input']['horizontal_end'] = $form_state['values']['horizontal_end'];  
  $form_state['input']['unit_of_period_displayed'] = $form_state['values']['unit_of_period_displayed'];
  $form_state['input']['major_single_bar'] = $form_state['values']['major_single_bar'];  
  $form_state['input']['minor_single_line'] = $form_state['values']['minor_single_line'];  
  
  //Generate the graph
  aspcm_build_graph($form_state['values']);
 
}

/**
 * Implementation of aspcm_graph_form_validate($form, &$form_state).
 * Validate the input form fields values
 * @param $form
 * @param $form_state
 * @return Array
 */
function aspcm_graph_form_validate($form, &$form_state) {
  // If it comes from ajax call, passby it.
  if (isset($form_state['input']['ajax'])) return;
  //var_dump($form_state);
  $user_obj = get_user_profile();
  if (empty($form_state['input']['hospital']) && $user_obj->dishos) {
    $form_state['input']['hospital'] = $form_state['input']['hospital_hide'];
    $form_state['values']['hospital'] = $form_state['input']['hospital_hide'];
  }
  
  if (!isset($form_state['input']['major_single_bar'])) {
    $form_state['values']['major_single_bar'] = 'single';
  }
  
  if (!isset($form_state['input']['minor_single_line'])) {
    $form_state['values']['minor_single_line'] = 'single';
  }
  
  $err_count = 0;
  if (empty($form_state['values']['hospital'])) {
    form_set_error('hospital', t('病院名 項目は必須です。'));
    $err_count++;
  }
  //Validate the date format
  $date_error = graph_date_field_validate($form_state['values']);
  if ($date_error) {
    $err_count++;
  }
  
  //Put two fields which needs to validate into $fixed.
  $fixed[] = $form_state['values']['major_fixed_value'];
  $fixed[] = $form_state['values']['minor_fixed_value'];
  //Validate the two '固定（自分で値を入力）' fields.
  foreach ($fixed as $key => $value) {
    if ($key == 0) {
      $name = 'major_fixed_value';
      $name2 = 'major_maximum_value';
      $name3 = '主軸';
	  $name4 = 'major_value';
    } 
    else {
      $name = 'minor_fixed_value';
      $name2 = 'minor_maximum_value';
      $name3 = '副軸';
	  $name4 = 'minor_value';
    }
    //Only when $form_state['values'][$name2]=='fixed',we need to validate the textfields.
    if ($form_state['values'][$name4] != 'none' && $form_state['values'][$name2] == 'fixed') {
      if (!empty($value)) {
        $flag = preg_match("/^[0-9]*$/", $value);
        if ($flag != 1) {
          form_set_error($name, $name3 .' 固定（自分で値を入力）:1 より大きい値を入力してください。 ');
          $err_count++;
        }
      }
      else {
        form_set_error($name, $name3 .' 固定（自分で値を入力）:1 より大きい値を入力してください。 ');
        $err_count++;
      }
    }
  }

  if ($err_count > 0) {
    $form_state['custom_error']['#value'] = TRUE;
    
    if($form['presetA']['#attributes']['class'] == 'active'){
      setcookie('ASP_PresetID', '1', 0, '/');
    }
    if($form['presetB']['#attributes']['class'] == 'active'){
      setcookie('ASP_PresetID', '2', 0, '/');
    }
    if($form['presetC']['#attributes']['class'] == 'active'){
      setcookie('ASP_PresetID', '3', 0, '/');
    }
    //$form_state['rebuild'] = TRUE;
  }
  else {
    $form_state['custom_error']['#value'] = FALSE;
  }
}
/**
 * Get the wards based on the hospital user selected
 * 
 * We need to rebuild the form after the wards built, 
 * ..this is needed to pass form validation after form selects values changed
 */
function aspcm_ward_jsgraph() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];

  // Retrieve the form from the cache.
  $form = form_get_cache($form_build_id, $form_state);
  
  // Since the $form_build_id comes from a POST, we have to validate if the form was found or not.
  if (empty($form)) {
    $msg = t('Could not find the form with $form_id from the cache table.');
    $args = array('form_id' => $form_build_id);
    watchdog(ASPCM_WATCHDOG_TYPE, $msg, $args, WATCHDOG_ERROR);
    exit;
  }
  
  // Since some of the submit handlers are run, redirects need to be disabled.
  $form_state['no_redirect'] = TRUE;
 // When a form is rebuilt after Ajax processing, its #build_id and #action
  // should not change.
  // @see drupal_rebuild_form()
  $form_state['rebuild_info']['copy']['#build_id'] = TRUE;
  $form_state['rebuild_info']['copy']['#action'] = TRUE; 
  // Preparing for processing the form.
  //$args = $form['#parameters'];
  //$form_id = array_shift($args); 
  $form_id = $form['#form_id'];
  $form_state['post'] = $form['#post'] = $_POST;
  $form_state['input'] = $_POST;
  $form_state['submitted'] = FALSE;
  $form_state['method'] = 'post';
  $form['#programmed'] = $form['#redirect'] = FALSE;
  $form_state['rebuild'] = true;
  
  // This function calls the submit handlers,
  // which put whatever was worthy of keeping into $form_state.
  drupal_process_form($form_id, $form, $form_state);
  
  // Call drupal_rebuild_form which destroys $_POST, this will call build the form again
  // Form generator function is called and creates the form again
  // The new form gets cached and processed again, but because $_POST is destroyed, the submit handlers will not be called again.
  //$form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  $form = drupal_rebuild_form($form_id, $form_state, $form);

  // AHAH callback picks a piece of the form and renders it.
  $output_ward = drupal_render($form['ward']);
  $major_diseases = drupal_render($form['major_infection_rate']);
  $minor_diseases = drupal_render($form['minor_infection_rate']);
  drupal_json_output(drupal_json_output(array('status' => TRUE, 'data' => array('ward' => $output_ward , 'major_diseases' => $major_diseases, 'minor_diseases' => $minor_diseases)))); 
  exit();
}