<?php

/**
 * Modify the drupal mail system to send HTML emails.
 */
class AspcmmailMailSystem implements MailSystemInterface {
  /**
   * Concatenate and wrap the e-mail body for plain-text mails.
   *
   * @param $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return
   *   The formatted $message.
   */
  public function format(array $message) {
    $message['body'] = implode("\n\n", $message['body']);
    return $message;
  }
    /**
   * Send an e-mail message, using Drupal variables and default settings.
   *
   * @see http://php.net/manual/function.mail.php
   * @see drupal_mail()
   *
   * @param $message
   *   A message array, as described in hook_mail_alter().
   * @return
   *   TRUE if the mail was successfully accepted, otherwise FALSE.
   */
  public function mail(array $message) {
    // If 'Return-Path' isn't already set in php.ini, we pass it separately
    // as an additional parameter instead of in the header.
    // However, if PHP's 'safe_mode' is on, this is not allowed.
    if (isset($message['headers']['Return-Path']) && !ini_get('safe_mode')) {
      $return_path_set = strpos(ini_get('sendmail_path'), ' -f');
      if (!$return_path_set) {
        $message['Return-Path'] = $message['headers']['Return-Path'];
        unset($message['headers']['Return-Path']);
      }
    }
    $mimeheaders = array();
    foreach ($message['headers'] as $name => $value) {
      $mimeheaders[] = $name . ': ' . mime_header_encode($value);
    }
    $line_endings = variable_get('mail_line_endings', MAIL_LINE_ENDINGS);
    // Prepare mail commands.
    $mail_subject = mime_header_encode($message['subject']);
    // Note: e-mail uses CRLF for line-endings. PHP's API requires LF
    // on Unix and CRLF on Windows. Drupal automatically guesses the
    // line-ending format appropriate for your system. If you need to
    // override this, adjust $conf['mail_line_endings'] in settings.php.
    $mail_body = preg_replace('@\r?\n@', $line_endings, $message['body']);
    // For headers, PHP's API suggests that we use CRLF normally,
    // but some MTAs incorrectly replace LF with CRLF. See #234403.
    $mail_headers = join("\n", $mimeheaders);

    // We suppress warnings and notices from mail() because of issues on some
    // hosts. The return value of this method will still indicate whether mail
    // was sent successfully.
    if (!isset($_SERVER['WINDIR']) && strpos($_SERVER['SERVER_SOFTWARE'], 'Win32') === FALSE) {
      if (isset($message['Return-Path']) && !ini_get('safe_mode')) {
        // On most non-Windows systems, the "-f" option to the sendmail command
        // is used to set the Return-Path. There is no space between -f and
        // the value of the return path.
        $mail_result = @mail(
          $message['to'],
          $mail_subject,
          $mail_body,
          $mail_headers,
          '-f' . $message['Return-Path']
        );
      }
      else {
        // The optional $additional_parameters argument to mail() is not
        // allowed if safe_mode is enabled. Passing any value throws a PHP
        // warning and makes mail() return FALSE.
        $mail_result = @mail(
          $message['to'],
          $mail_subject,
          $mail_body,
          $mail_headers
        );
      }
     }
     else {
      // On Windows, PHP will use the value of sendmail_from for the
      // Return-Path header.
      $old_from = ini_get('sendmail_from');
      ini_set('sendmail_from', $message['Return-Path']);
      $mail_result = @mail(
         $message['to'],
         $mail_subject,
         $mail_body,
         $mail_headers
       );
      ini_set('sendmail_from', $old_from);
     }
     return $mail_result;
  }
  
}


define('ASPCM_DEFAULT_SEND_EMAIL', 'your_user@aspcm.co.jp');
define('ASPCM_DEFAULT_FORM_EMAIL', 'noreply@aspcm.co.jp');
// $Id$
/**
 * @file: aspcm_email_form.inc
 * render aspcm_email_form form, confirmation page and sending email 
 * for more detail comments please refer to aspcm_email_form.inc
 */

/**
 * Create a page to contain the form and link
 */ 
function drupal_get_form_page() {
  $output = drupal_get_form('aspcm_email_form');
  return $output;
}

/**
 * build a form called by drupal_get_form_page()
 */
function aspcm_email_form($form, $form_state) {  

  $form['mail_wico_notice'] = array(
    '#markup' => variable_get('aspcm_wico_mail_notice', ''),
  );
  
  //to judgement to show form page or confirm page
  if (isset($form_state['storage']['aspcm_confirm_form'])) {
    return aspcm_confirm_form($form, $form_state);
  }

  $saved_values = array();//save the field values if there are errors
  
  if (isset($form_state['storage']['page_one_values'])) {
    $saved_values = $form_state['storage']['page_one_values'];
  }

  $form['#attributes']=array('class'=>'node-form');
  

  //for check box
  $form['choose'] = array(
    '#type' => 'radios',
    '#options' => array(
      'yes' => t('はい'),
      'no' => t('いいえ')
    ),
    '#default_value' => $saved_values['choose'],
  );
  
  //for drop-down box
  $_SESSION['question_type_yes_class'] = 'hidden';
  $_SESSION['question_type_no_class'] = 'hidden';
  
  if(isset($saved_values['choose'])) {
    if($saved_values['choose'] == 'yes') {
      $_SESSION['question_type_yes_class']='';
    }
    elseif($saved_values['choose'] == 'no') {
      $_SESSION['question_type_no_class'] = '';
    } 
  }
  
  $form['question_type_yes'] = array(
    '#type' => 'select',
    '#default_value' => '本ツールの使用方法について',
    '#options'=>drupal_map_assoc(array('本ツールの使用方法について','システムトラブル／不具合について','製品について知りたい','製品のサンプルを使いたい','ご意見・ご要望','その他')),
    '#default_value' => $saved_values['question_type_yes'],
  );
  
  $form['question_type_no'] = array(
    '#type' => 'select',
    '#default_value' => '本ツールを使用してみたい',
    '#options'=>drupal_map_assoc(array('本ツールを使用してみたい','本ツールの説明を受けたい','製品について知りたい','製品のサンプルを使いたい','その他')),
    '#default_value' => $saved_values['question_type_no'],
  );
  
  $form['first_name'] = array(
    '#type' => 'textfield',
    '#size' => 18,
    '#maxlength' => 20,  
    '#default_value' => $saved_values['first_name'],
  );

  $form['last_name'] = array(
    '#type' => 'textfield',
    '#size' => 18,
    '#maxlength' => 20,  
    '#default_value' => $saved_values['last_name'],
  );
 
  $form['first_name_phonetic'] = array(
    '#type' => 'textfield',
    '#size' => 18,
    '#maxlength' => 20,  
    '#default_value' => $saved_values['first_name_phonetic'],
  );
  
  $form['last_name_phonetic'] = array(
    '#type' => 'textfield',
    '#size' => 18,
    '#maxlength' => 20,  
    '#default_value' => $saved_values['last_name_phonetic'],
  );
  
  $form['employer_name'] = array(
    '#type' => 'textfield',
    '#size' => 36,
    '#maxlength' => 100,  
    '#default_value' => $saved_values['employer_name'],
  );
  
  $form['department_name'] = array(
    '#type' => 'textfield',
    '#size' => 36,
    '#maxlength' => 50,  
    '#default_value' => $saved_values['department_name'],
  );
  
  $form['positions'] = array(
    '#type' => 'select',
    '#default_value' => $saved_values['positions'], 
    '#options' => array(
      '看護師（ICN）' => '看護師（ICN）',
      '看護師' => '看護師',
      '医師（ICD）' => '医師（ICD）',
      '医師' => '医師',
      '薬剤師（ICPh）' => '薬剤師（ICPh）',
      '薬剤師' => '薬剤師',
      '臨床検査技師（ICMT)' => '臨床検査技師（ICMT)',
      '臨床検査技師' => '臨床検査技師',
      'その他' => 'その他',
    ),
    //'#required' => TRUE,
  );
  $form['zip_first'] = array(
    '#type' => 'textfield',
    '#size' => 6,
    '#maxlength' => 3,
    '#default_value' => $saved_values['zip_first'],
  );
  
  $form['zip_second'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#maxlength' => 4,
    '#default_value' => $saved_values['zip_second'],
  );
  
  $form['address'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#maxlength' => 200,  
    '#default_value' => $saved_values['address'],
  );  
  
  $form['phone_1'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#maxlength' => 5,
    '#default_value' => $saved_values['phone_1'],
  );
  
  $form['phone_2'] = array(
    '#type' => 'textfield',
    '#size' => 8, 
    '#maxlength' => 4,
    '#default_value' => $saved_values['phone_2'],
  );
  
  $form['phone_3'] = array(
    '#type' => 'textfield',
    '#maxlength' => 4,
    '#size' => 8,
    '#default_value' => $saved_values['phone_3'],
  );
  
  $form['email_1'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#default_value' => $saved_values['email_1'],
  );

  $form['email_2'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#default_value' => $saved_values['email_2'],
  );
  
  $form['message'] = array(
    '#type' => 'textarea',
    '#default_value' => $saved_values['message'],
  );
  
  $form['form_submit'] = array(
    '#type' => 'submit',
    '#value' => t('入力内容を確認する'),
    '#attributes' => array('title' => "入力内容を確認する",),
  );
  
  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => '入力内容の削除',
    '#attributes' => array('title' => "入力内容の削除",),
    '#validate' => array('email_form_clear'),
  );
  
  $form['#theme'] = 'theme_aspcm_email_form';
  
  return $form;
}
//--------------------------------------confirm page-------------------------------------------

  //if isset $form_state['storage']['aspcm_confirm_form'], will goto this form
function aspcm_confirm_form($form, &$form_state) {
  //markup heard information for confirm page  
  $form['#attributes'] = array('class' => 'aspcm-email-form-confirm');

  //use drupal api check_plain to filter fields  
  foreach($form_state['values'] as $key => $value) {
    $form_values["$key"] = check_plain($value);
  }
	
  $subject = NULL;
  $pre_content = '<div class="content">'. _aspcm_mail_content($form_state['values'], $subject) .'</div>';
  
  // The content, which user just filled from contact us form
  $form['markup_field'] = array(
    '#markup' => $pre_content,
  );
    
  $form['goback'] = array(
    '#type' => 'submit',
    '#value' => '入力フォームに戻る',
    '#validate' => array('email_form_goback'),
    '#attributes' => array('title' => "入力フォームに戻る", ), 
  );
	
  $form['send'] = array(
    '#type' => 'submit',
    '#value' => '送信する',
    '#attributes' => array('title' => "送信する", ), 
  );
  
  return $form;
}

//--------------------------------------validation page-------------------------------------------

  //implement hook_validate: when user click 'submit' button it will recall this function 
function aspcm_email_form_validate($form, &$form_state) {

  if (!isset($form_state['storage']['aspcm_confirm_form'])) {

    $form_state['storage']['page_one_values'] = $form_state['values']; 

    foreach($form_state['values'] as $key => $value) {
      $form_values["$key"] = check_plain($value);
    }

    foreach($form_values as $field => $value) {
      if($field != 'hidPwdLevel'){
        if(empty($form_values["$field"])) {
          form_set_error("$field", t("$field field is required."));
        }
      }
    }
    if(!preg_match('/^[0-9]{3}$/', $form_values["zip_first"])) {
      form_set_error("zip_first", t("郵便番号_1 should be 3 numbers"));
    }

    if(!preg_match('/^[0-9]{4}$/', $form_values["zip_second"])) {
      form_set_error("zip_second",t("郵便番号_2 should be 4 numbers"));
    }

    if(!preg_match('/^[0-9]+$/', $form_values["phone_1"])) {
      form_set_error("phone_1", "ご連絡先電話番号_1".':'.t("Please enter the number."));
    }

    if(!preg_match('/^[0-9]+$/', $form_values["phone_2"])) {
      form_set_error("phone_2", "ご連絡先電話番号_2".':'.t("Please enter the number."));
    }

    if(!preg_match('/^[0-9]+$/', $form_values["phone_3"])) {
      form_set_error("phone_3", "ご連絡先電話番号_3".':'.t("Please enter the number."));
    }

    if(!valid_email_address($form_values['email_1'])) {
      form_set_error("email_1", t("Please enter a valid email format."));
      form_set_error("email_2", ' ');
    }

    if ($form_values['email_2']!=$form_values['email_1']) {
      form_set_error("email_2",t("E-mail does not match."));
    }

    $phone_length = strlen($form_state['values']['phone_1'])
                  + strlen($form_state['values']['phone_2'])
                  + strlen($form_state['values']['phone_3']);

    if($phone_length != '11' && $phone_length != '10') {
      form_set_error( 'phone_1', t('The phone should be 10 or 11 numbers.'));
      form_set_error( 'phone_2', ' ');
      form_set_error( 'phone_3', ' ');
    }
  }
}

function aspcm_email_form_mail($key, &$message, $params) {
  $headers = array(
    'MIME-Version' => '1.0',
    'Content-Type' => 'text/html; charset=UTF-8; format=flowed;',
    'Content-Transfer-Encoding' => '8Bit',
    'X-Mailer' => 'DrupalASP'
  );

  foreach ($headers as $key => $value) {
    $message['headers'][$key] = $value;
  }

  $message['subject'] = $params['subject'];
  $message['body'][] = $params['body'];
}

/**
 * Implementation of aspcm_email_form_submit()
 * 
 * Send email to users
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function aspcm_email_form_submit($form, &$form_state) {
   // Handle page 1 submissions
  if ($form_state['clicked_button']['#id'] == 'edit-form-submit') {
    $form_state['storage']['aspcm_confirm_form'] = TRUE; // We set this to determine
                                               // which elements to display
                                               // when the page reloads.                                        
    // Values below in the $form_state['storage'] array are saved 
    // to carry forward to subsequent pages in the form.
    $form_state['storage']['page_one_values'] = $form_state['values'];
    $form_state['rebuild'] = TRUE;
  }
  // Handle page 2 submissions
  else {
    // Define email subject variable
    // This variable will be assigned by using subject template from configuration
    $subject = '';
    
    // Get the the description of the email from configuration 
    $desc = '<div>'. variable_get('aspcm_contact_us_email_description', '') .'</div>';
    
    // Add the mail description to the top of the mail
    $body = nl2br($desc) . _aspcm_mail_content($form_state['storage']['page_one_values'], $subject);

    //todo change this email address  
    $email_address = variable_get('email_address', ASPCM_DEFAULT_SEND_EMAIL);
    $valid_email = explode(';', $email_address);
    
    $from = variable_get('email_from_address', ASPCM_DEFAULT_FORM_EMAIL);
    
    //todo change subject 
    $params = array(
      'body' => $body,
      'subject' => $subject,
    );
    
    foreach($valid_email as $value) {
      $value = trim($value);
      $message = drupal_mail('aspcm_email_form', 'form', $value, language_default(), $params, $from, TRUE);
      if (!$message['result']) {
        drupal_set_message(t('There is an error sending your email'), 'error');
        watchdog(ASPCM_WATCHDOG_TYPE, __FUNCTION__ . 'email_form' . t('Can\'t sending email to ') . $value, array(), WATCHDOG_ERROR);
      }
    }
    drupal_goto('email-thanks');
  }
}

function _aspcm_mail_content($items, &$subject = '') {
  $output = '';
 
  $ques_type = $items['choose'] == 'yes' ? check_plain($items["question_type_yes"]) : check_plain($items["question_type_no"]);
  $choice = $items['choose'] == 'yes' ? 'はい' : 'いいえ';
  $title = $ques_type;
  
  if ($subject === NULL) {
    $body_tpl = variable_get('aspcm_contact_us_preview', '');
  }
  else {
    $subject_tpl = variable_get('aspcm_contact_us_email_subject', '');
    $subject = strtr($subject_tpl, array('%title' => $title));
    
    $body_tpl = variable_get('aspcm_contact_us_email_body', '');
  }
 
  $regx_replace = array(
    '%title' => $title, 
    '%choose' => $choice, 
    '%question_type' => $ques_type,
    '%last_name' => check_plain($items['last_name']),
    '%first_name' => check_plain($items['first_name']),
    '%last_name_phonetic' => check_plain($items['last_name_phonetic']),
    '%first_name_phonetic' => check_plain($items['first_name_phonetic']),
    '%employer_name' => check_plain($items['employer_name']),
    '%department_name' => check_plain($items['department_name']),
    '%positions' => check_plain($items['positions']),
    '%zip_code' => check_plain($items['zip_first']. '-'. $items["zip_second"]),
    '%address' => check_plain($items['address']),
    '%phone_1' => check_plain($items['phone_1'] .'-'. $items['phone_2'] .'-'. $items['phone_3']),
    '%email_1' => check_plain($items['email_1']),
    '%message' => nl2br(check_plain($items['message'])),
  );
  
  $output = strtr($body_tpl, $regx_replace);
  
  return $output;
}

//functions of clear and goback 
function email_form_clear($form, &$form_state) {
  unset($form_state['input']);  // ensures fields are blank after reset
                                  // button is clicked
  unset($form_state['storage']); // ensures the reset button removes the
                                  // new_name part
  $form_state['rebuild'] = TRUE;
}

/*
 * when user click goback button in confirm page, will call this funcion
 */
function email_form_goback($form, &$form_state) {

  unset($form_state['storage']['aspcm_confirm_form']);

  $form_state['rebuild'] = TRUE; 
}

