<?php 
// $Id$
/**
 * @file
 * Generate the graph for hospital comparison
 */

module_load_include('inc', 'aspcm', '/includes/aspcm_comparison_graph_process');
module_load_include('inc', 'aspcm', '/includes/aspcm_ward_annually_data_view');

define ('DEFAULT_YEAR_STARTING_MONTH', '04');
define ('DEFAULT_YEAR_ENDING_MONTH', '03');

/**
 * 
 * call this function build form
 * @param unknown_type $form_state
 * @return $form
 */
function aspcm_comparison_graph_form($form, &$form_state) {
  // For some purpose, the month of the year does not always start from Jan.
  // Get the system year and month
  $year_now = date('Y');
  $month_now = date('m');
  
  // If current month is larger than the default starting month
  // we can be sure that the data belongs to current year 
  if ($month_now >= (int)DEFAULT_YEAR_STARTING_MONTH) {
    $year = $year_now;
  }
  // Otherwise, the data belongs to last year
  else {
    $year = $year_now - 1;
  }
  
  // INC000006324168
  // Author: bala
  $user_profile = get_user_profile();
  $gov_depts = get_government_dept();
  $clin_depts = get_clinical_dept();
  // End INC000006324168
  
  $form['hospital'] = array(
    //'#title' => 'hospital name',
    '#type' => 'textfield',
    '#default_value' => !isset($form_state['values']['hospital']) ? '' : $form_state['values']['hospital'],
    '#autocomplete_path' => 'ajax/hospital/autocomplete',
    '#access' => empty($user_profile->asp_hospital_id) ? TRUE : FALSE,
  );
  
  $form['horizontal_start'] = array(
    '#type' => 'textfield',
    '#attributes' => array('class' => array('small')),
    '#maxlength' => 7,
    //'#required' => TRUE,
    '#field_suffix' => 'から',
    '#field_prefix' => '例：2011/04',
    '#default_value' => !isset($form_state['values']['horizontal_start']) ? $year .'/'. DEFAULT_YEAR_STARTING_MONTH : $form_state['values']['horizontal_start'],
  );
  
  $form['horizontal_end'] = array(
    '#type' => 'textfield',
    '#attributes' => array('class' => array('small')),
    '#maxlength' => 7,
    //'#required' => TRUE,
    '#field_suffix' => 'まで',
    '#field_prefix' => '例：2011/09',
    '#default_value' => !isset($form_state['values']['horizontal_end']) ? ($year + 1) .'/'. DEFAULT_YEAR_ENDING_MONTH : $form_state['values']['horizontal_end'],
  );
  
  $form['unit_of_period_displayed'] = array(
    '#type' => 'select',    
    '#options' => array(
      '1' => t('１ヶ月'),
      '2' => t('２ヶ月'),
      '3' => t('３ヶ月'),
      '4' => t('４ヶ月'),
      '6' => t('６ヶ月'),
      '12' => t('１２ヶ月')
    ),
    '#default_value' => !isset($form_state['values']['unit_of_period_displayed']) ? '' : $form_state['values']['unit_of_period_displayed'],
  );
  
  $form['major_value'] = array(
    '#type' => 'select',
    '#options' => array(
      'hand_washing' => t('手指衛生回数'),
      'antiseptic_used' => t('手指衛生剤使用量'),
      'compliance_rate' => t('遵守率'),      
    ),
    '#default_value' => !isset($form_state['values']['major_value']) ? '' : $form_state['values']['major_value'],
  );
  
  $form['hospital_beds'] = array(
    '#type' => 'checkboxes',
    '#title' => t('病床数'),
    '#options' => array(
      'more_than_500' => t('大規模病院（500床以上）'),
      '200_to_500' => t('中規模病院（200床以上～500床未満）'),
      'less_than_200' => t('小規模病院（200床未満）'),      
    ),
    //'#suffix' => '<div class="clear"></div>',
    '#default_value' => !isset($form_state['values']['hospital_beds']) ? array() : array_values($form_state['values']['hospital_beds']),
  );
  
  $form['clinical_dept'] = array(
    '#type' => 'checkboxes',
    '#title' => t('診療科'),
    '#options' => $clin_depts,
    '#default_value' => !isset($form_state['values']['clinical_dept']) ? array() : array_values($form_state['values']['clinical_dept']),
  );
  
  $form['government_dept'] = array(
    '#type' => 'checkboxes',
    '#title' => t('診療科'),
    '#options' => $gov_depts,    
    '#default_value' => !isset($form_state['values']['government_dept']) ? array() : array_values($form_state['values']['government_dept']),
  );
  
  // INC000006324168
  // Author: bala
  // See: includes/aspcm_comparison_graph.pack.inc
  if (function_exists('aspcm_comparison_my_departments_form_elements')) {
    aspcm_comparison_my_departments_form_elements(
      $form, 
      $form_state, 
      array('user_profile' => $user_profile, 'gov_depts' => $gov_depts, 'clin_depts' => $clin_depts)
    );
  }
  // End INC000006324168
  
  $form['area'] = array(
    '#type' => 'checkboxes',
    '#title' => t('地域別'),
    '#options' => get_area(),  
    '#default_value' => !isset($form_state['values']['area']) ? array() : array_values($form_state['values']['area']),  
  );
  
  $form['select_all_above_conditions'] = array(
    '#type' => 'checkboxes',
    '#options' => array(
    'select_all' => t('上記で選択した条件を合算し表示する'),
    ),
    '#default_value' => !isset($form_state['values']['select_all_above_conditions']) ? array() : array_values($form_state['values']['select_all_above_conditions']),
  );
  
  $form['generate_main'] = array(
    '#type' => 'submit', 
    '#value' => t('グラフを表示'), 
    //'#attributes' => array('class' => 'lnk-empty-butt'),
    '#weight' => 4,
  );
  
  $form['generate_major'] = array(
    '#type' => 'submit', 
    '#value' => t('グラフを表示'), 
    '#weight' => 5,
  );
  
  $form['generate_minor'] = array(
    '#type' => 'submit', 
    '#value' => t('グラフを表示'), 
    '#weight' => 6,
  );
  
  $form['generate_horizontal'] = array(
    '#type' => 'submit', 
    '#value' => t('グラフを表示'), 
    '#weight' => 7,
  );
  
  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['major_wash_number'] = array(
    '#type' => 'select',
    '#options' => array(
      'all_patient' => t('アルコール手指消毒剤＋石けん　1患者日あたり'),
      'all_patient_1000' => t('アルコール手指消毒剤＋石けん　1スタッフあたり'),
      'alcohol_patient' => t('アルコール手指消毒剤　1患者日あたり'),
      'alcohol_staff' => t('アルコール手指消毒剤　1スタッフあたり'),
      'soap_patient' => t('石けん　1患者日あたり'),
      'soap_staff' => t('石けん　1スタッフあたり'),
    ),
    '#default_value' => !isset($form_state['values']['major_wash_number']) ? '' : $form_state['values']['major_wash_number'],
  );
  
  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['major_antisepic_volume'] = array(
    '#type' => 'select',
    '#options' => array(
      'all_patient' => t('アルコール手指消毒剤＋石けん　1患者日あたり'),
      'all_patient_1000' => t('アルコール手指消毒剤＋石けん　1000患者日あたり'),
      'alcohol_patient' => t('アルコール手指消毒剤　1患者日あたり'),
      'alcohol_patient_1000' => t('アルコール手指消毒剤　1000患者日あたり'),
      'sopa_patient' => t('石けん　1患者日あたり'),
      'sopa_patient_1000' => t('石けん　1000患者日あたり'),
    ),
    '#default_value' => !isset($form_state['values']['major_antisepic_volume']) ? '' : $form_state['values']['major_antisepic_volume'],
  );
  
  /*$form['major_compliance_rate'] = array(
    '#type' => 'markup',
    '#value' => t('遵守率'),
  );*/
  
  $form['major_maximum_value'] = array(
    '#type' => 'radios',    
    '#options' => array(
      'automatic' => t('自動（自動で主軸の最大値を決定する）'),
      'fixed' => t('固定（自分で任意の最大値を入力する）'),
    ),
    '#default_value' => !isset($form_state['values']['major_maximum_value']) ? 'automatic' : $form_state['values']['major_maximum_value'],
  );
  
  $form['major_fixed_value'] = array(
    '#type' => 'textfield',    
    '#attributes' => array('class' => array('small')),
    '#default_value' => !isset($form_state['values']['major_fixed_value']) ? '' : $form_state['values']['major_fixed_value'],
    '#maxlength' => 8,
  );
  
  //modified by yueshuo start 他设施
//  $form['minor_value'] = array(
//    '#type' => 'radios',    
//    '#options' => array(
//      'hand_washing' => t('手指衛生回数'),
//      'antiseptic_used' => t('手指衛生剤使用量'),
//      'compliance_rate' => t('遵守率'),      
//      'none' => t('なし'),
//  ),
//    '#default_value' => !isset($form_state['values']['minor_value']) ? 'none' : $form_state['values']['minor_value'],
//  );
  $form['minor_value'] = array(
    '#type' => 'select',    
    '#options' => array(
      'hand_washing' => t('手指衛生回数'),
      'antiseptic_used' => t('手指衛生剤使用量'),
      'compliance_rate' => t('遵守率'),      
      'none' => t('なし'),
  ),
    '#default_value' => !isset($form_state['values']['minor_value']) ? 'none' : $form_state['values']['minor_value'],
  );
  //end
  
  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['minor_wash_number'] = array(
    '#type' => 'select',
    '#options' => array(
      'all_patient' => t('アルコール手指消毒剤＋石けん　1患者日あたり'),
      'all_patient_1000' => t('アルコール手指消毒剤＋石けん　1スタッフあたり'),
      'alcohol_patient' => t('アルコール手指消毒剤　1患者日あたり'),
      'alcohol_staff' => t('アルコール手指消毒剤　1スタッフあたり'),
      'soap_patient' => t('石けん　1患者日あたり'),
      'soap_staff' => t('石けん　1スタッフあたり'),
    ),
    '#default_value' => !isset($form_state['values']['minor_wash_number']) ? '' : $form_state['values']['minor_wash_number'], 
  );
  
  //Change the 'アルコール' to 'アルコール手指消毒剤' by Benny for ACMSUPP-46 20121226 
  $form['minor_antisepic_volume'] = array(
    '#type' => 'select',
    '#options' => array(
      'all_patient' => t('アルコール手指消毒剤＋石けん　1患者日あたり'),
      'all_patient_1000' => t('アルコール手指消毒剤＋石けん　1000患者日あたり'),
      'alcohol_patient' => t('アルコール手指消毒剤　1患者日あたり'),
      'alcohol_patient_1000' => t('アルコール手指消毒剤　1000患者日あたり'),
      'sopa_patient' => t('石けん　1患者日あたり'),
      'sopa_patient_1000' => t('石けん　1000患者日あたり'),
    ),
    '#default_value' => !isset($form_state['values']['minor_antisepic_volume']) ? '' : $form_state['values']['minor_antisepic_volume'],
  );  
  
  $form['minor_maximum_value'] = array(
    '#type' => 'radios',    
    '#options' => array(
      'automatic' => t('自動（自動で副軸の最大値を決定する）'),
      'fixed' => t('固定（自分で任意の最大値を入力する）'),
    ),
    '#default_value' => !isset($form_state['values']['minor_maximum_value']) ? 'automatic' : $form_state['values']['minor_maximum_value'],
  );
  
  $form['minor_fixed_value'] = array(
    '#type' => 'textfield',
    '#default_value' => !isset($form_state['values']['minor_fixed_value']) ? '' : $form_state['values']['minor_fixed_value'],
    '#attributes' => array('class' => array('small')),
    '#maxlength' => 8,
  ); 
  
  $form['image'] = array(
    '#markup' => comparison_get_image_src($form_state),
  );
  
  $form['download'] = array(
    '#type' => 'submit', 
    '#value' => t('Download image'),
    '#access' => $form_state['values']['graph']['visible']==FALSE?FALSE:TRUE,    
  );
  
  /*$form['clinical_dept_hidden'] = array(
    '#type' => 'hidden',
    '#value' => get_clinical_dept_hidden_value(),
  );*/
  
  $form['graph_path'] = array(
    '#type' => 'hidden',
    '#attributes' => array('id' => 'edit-graph-path'),
    '#value' => !isset($form_state['values']['graph_path']) ? '' : $form_state['values']['graph_path'],
  );
  $form['#theme'] = 'theme_aspcm_comparison_graph_form';
  
  return $form;
}

/**
 * 
 * get hospital id list by clinical department.
 */
function get_clinical_dept() {  
  $result = array();
  $query_a = db_select('node', 'n')
          ->fields('n', array('title', 'nid'));
  $query_a->join('field_data_field_aspcm_clinical_dep_des', 'dd', 'n.nid = dd.entity_id');
  $query_a->join('field_data_field_aspcm_clinical_order', 'co', 'n.nid = co.entity_id');
  $res = $query_a->condition('n.type', 'aspcm_clinical_department', '=')
          ->orderBy('co.field_aspcm_clinical_order_value', 'ASC')
          ->execute();
 
  foreach ($res as $row) {
    $result[$row->nid] = check_plain($row->title);
  }
  
  //Begin:Adding for removing the empty clinical_department while calculating the compliance rate.
  $query_b = "SELECT DISTINCT(fn.field_aspcm_department_name_nid) AS nid FROM node AS n
              JOIN (field_data_field_aspcm_department_name AS fn) ON n.nid = fn.entity_id
              WHERE TYPE = :type";
  $res = db_query($query_b, array(':type' => 'aspcm_department'));
  foreach ($res as $row) {
    $temp[] = $row->nid;
  }
  
  foreach ($result as $key => $value) {
    if (!in_array($key,$temp)) {
  	  unset($result[$key]);
  	}
  }
  //End
  
  return $result;
}

/**
 * 
 * get hospital id list by government department.
 */
function get_government_dept() {  
  $result = array();
  $query = "SELECT n.title, n.nid FROM node AS n
            JOIN (field_data_field_aspcm_gov_order AS fo) ON n.nid = fo.entity_id
            WHERE TYPE = :type 
            ORDER BY fo.field_aspcm_gov_order_value ASC";
  
  $res = db_query($query, array(':type' => 'aspcm_government_department'));
  
  foreach ($res as $row) {
    $result[$row->nid] = check_plain($row->title);
  }
    
  return $result;
}

/**
 * 
 * hidden clinical dept when select compliance rate
 */
function get_clinical_dept_hidden_value() { 
  $query = "SELECT * FROM node AS n
            JOIN (field_data_field_aspcm_clinical_dep_des AS des) ON n.nid = des.entity_id
            JOIN (field_data_field_aspcm_clinical_order AS od) ON n.nid = od.entity_id
            WHERE TYPE = :type
            ORDER BY od.field_aspcm_clinical_order_value ASC";
  
  $res = db_query($query, array(':type' => 'aspcm_clinical_department'));
  foreach ($res as $row) {
    //$result .= $row->nid .'|'; 
    $clinical_show[$row->nid] = $row->nid;
  }
  $clinical_all = get_clinical_dept();
  if (!empty($clinical_all)) {
    foreach ($clinical_all as $key=>$value) {
      if (!empty($clinical_show)) {
        foreach ($clinical_show as $show) {
          if ($key == $show) {
            unset($clinical_all[$key]);
          }
        }
      }
    }
  }
  if (!empty($clinical_all)) {
    $hidden_nid = array_keys($clinical_all);
    $result = implode('|', $hidden_nid);
  }  
  return $result;
}

/**
 * 
 * get hospital id list by area.
 */
function get_area() {
  $result = array();
  $query = "SELECT n.title, n.nid FROM node n
            JOIN (field_data_field_aspcm_area_des AS des) ON n.nid = des.entity_id
            JOIN (field_data_field_aspcm_area_order AS od) ON n.nid = od.entity_id
            WHERE n.type = :type
            ORDER BY od.field_aspcm_area_order_value ASC";
  $res = db_query($query, array(':type' => 'aspcm_area'));
  foreach ($res as $row) {
    $result[$row->nid] = check_plain($row->title);
  }  
  return $result;
}

/**
 * 
 * submit event for input form
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function aspcm_comparison_graph_form_submit($form, &$form_state) {
/*@author_lxl_start comparison-graph 他施設条件　項目は必須です。*/  
  if(!isset($_SESSION)){
     session_start(); 
  }
  $_SESSION['comparison_graph_radio']=$form_state['input']['comparison_graph_radio'];
  if($form_state['input']['comparison_graph_radio'] == 'radio_yes'){ 
    $flag = '';
    $hospital_beds = $form_state['values']['hospital_beds'];
    $government_dept = $form_state['values']['government_dept'];
    $area = $form_state['values']['area'];
    foreach ($hospital_beds as $key => $value) {
      if($value != '0'){
        $flag = 'flag1';break;
      }
    }
    if($flag == ''){
      foreach ($government_dept as $key => $value) {
        if($value != '0'){
          $flag = 'flag2';break;
        }
      }
    }
    if($flag == ''){
      foreach ($area as $key => $value) {
        if($value != '0'){
          $flag = 'flag3';break;
        }
      }
    }
    if($flag == ''){
      drupal_set_message(t('他施設条件　項目は必須です。'), 'error');
        return;
    }
  }
/*@author_lxl_end  comparison-graph 他施設条件　項目は必須です。*/
  // Get the user operation          
  if ($form_state['clicked_button']['#id'] == 'edit-download') {
    aspcm_graph_download($form_state['input']['graph_path']);
  }
  
  $form_state['values']['graph_name'] = 'comparison-graph-'. time();
  
  if ($form_state['values']['major_value'] == 'compliance_rate') {
    $form_state['values']['department'] = $form_state['values']['clinical_dept'];
  }
  else {
    $form_state['values']['department'] = $form_state['values']['government_dept'];
  }
  
  // INC000006324168
  // Author: Bala
  
  // See: includes/aspcm_comparison_graph.pack.inc
  if (function_exists('aspcm_comparison_my_departments_form_submit')) {
    aspcm_comparison_my_departments_form_submit($form, $form_state);
  }
  
  /* If the criterias nothing selected, should consider as selected all the options */
  
  $temp_values = $form_state['values'];
  
  // Filter the empty / unselected items to get the selected ones
  $beds_filtered = array_filter($form_state['values']['hospital_beds']);
  $depts_filtered = array_filter($form_state['values']['department']);
  $area_filtered = array_filter($form_state['values']['area']);
  
  if (empty($beds_filtered)) {
    $beds_keys = array_keys($form_state['values']['hospital_beds']);
  }
  
  if (empty($depts_filtered)) {
    $dept_keys = array_keys($form_state['values']['department']);
  }
  
  if (empty($area_filtered)) {
    $area_keys = array_keys($form_state['values']['area']);
  }
  
  // Set to all fields selected
  if (isset($beds_keys) && isset($dept_keys) && isset($area_keys)) {
    $temp_values['hospital_beds'] = array_combine($beds_keys, $beds_keys);
    $temp_values['department'] = array_combine($dept_keys, $dept_keys);
    $temp_values['area'] = array_combine($area_keys, $area_keys);
    
    $temp_values['select_all_above_conditions']['select_all'] = 'select_all';
  }

  aspcm_build_comparison_graph($temp_values, $form_state['values']['graph']['visible']);
  $form_state['rebuild'] = TRUE;
  // End INC000006324168
  
  //$params = $form_state['values'];
  //$result = $form_state['values'];
}

/**
 * 
 * call validate when form submit
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function aspcm_comparison_graph_form_validate($form, &$form_state) {
  $err_count = 0;    
  $date_error = graph_date_field_validate($form_state['values']);
  if ($date_error) {
    $err_count++;
  }
  if (!empty($form_state['values']['hospital'])) {
    $hos_nid = get_hospital_id_by_hospitalname($form_state['values']['hospital']);
    if ($hos_nid) {
      $form_state['values']['my_hospital'] = $hos_nid;
    }
    else {
      form_set_error('hospital', t('hospital not exist.'));
      $err_count++;
    }
  }
  else {
    $profile = get_user_profile();
    if (!empty($profile->asp_hospital_id)) {
      $form_state['values']['my_hospital'] = $profile->asp_hospital_id;
    }
  }
  /*if (empty($form_state['values']['horizontal_start'])) {
    fields_error_message($err_count, 'horizontal_start');
  }
  if (empty($form_state['values']['horizontal_end'])) {
    fields_error_message($err_count, 'horizontal_end');
  }*/
  //Put two fields which needs to validate into $fixed.
  $fixed[] = $form_state['values']['major_fixed_value'];
  $fixed[] = $form_state['values']['minor_fixed_value'];
  //Validate the two '固定（自分で値を入力）' fields.
  foreach ($fixed as $key => $value) {
    if ($key == 0) {
      $name = 'major_fixed_value';
      $name2 = 'major_maximum_value';
      $name3 = '主軸';
    }
    else {
      $name = 'minor_fixed_value';
      $name2 = 'minor_maximum_value';
      $name3 = '副軸';
    }
    //Only when $form_state['values'][$name2]=='fixed',we need to validate the textfields.
    if ($form_state['values']['minor_value'] != 'none' && $form_state['values'][$name2] == 'fixed') {
      if (!empty($value)) {
        $flag = preg_match("/^[0-9]*$/", $value);
        if ($flag != 1) {   
          form_set_error($name, $name3 . t(' 固定（自分で値を入力）:1 より大きい値を入力してください。 '));
          $err_count++;
        }
      }
      else {
        form_set_error($name, $name3 . t(' 固定（自分で値を入力）:1 より大きい値を入力してください。 '));
        $err_count++;
      }
    }
  }  
  if ($err_count > 0) {
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * 
 * Get the image src
 * @param unknown_type $form_state
 */
function comparison_get_image_src($form_state) {
  global $user;
  $src = base_path().file_directory_path().'/graph/'. $user->uid .'/'.$form_state['values']['graph_name'].'.png'; 
  //$src = "/sites/all/modules/aspcm/images/graph.png";  
  if (!$form_state['values']['graph']['visible']) $src = "/sites/all/modules/aspcm/images/graph-blank.png";
  return "<img src='" .$src. "' width='770' alt='グラフ' />";
}
