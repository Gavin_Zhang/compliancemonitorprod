<?php


/**
 * @file
 * Drush commands for framework backup.
 */
 
/**
 * Implementation of hook_drush_command().
 */
function fwk_backup_drush_command() {
  $items['fwkbkp-backup'] = array(
    'callback' => 'fwk_backup_drush_backup',
    'description' => dt('Backup the site\'s database with Framework Backup.'),
    'aliases' => array('fb'),
    'examples' => array(
      'drush fwkbkp-backup' => 'Backup the default databse to the manual backup directory using the default settings.', 
      'drush fwkbkp-backup db scheduled mysettings' => 'Backup the database to the scheduled directory using a settings profile called "mysettings"', 
      'drush fwkbkp-backup files' => 'Backup the files directory to the manual directory using the default settings. The Framework Backup Files module is required for files backups.', 
    ),
    'arguments' => array(
      'source'        => "Optional. The id of the source (usually a database) to backup. Use 'drush fwkbkp-sources' to get a list of sources. Defaults to 'db'",
      'destination'   => "Optional. The id of destination to send the backup file to. Use 'drush fwkbkp-destinations' to get a list of destinations. Defaults to 'manual'",
      'profile'       => "Optional. The id of a settings profile to use. Use 'drush fwkbkp-profiles' to get a list of available profiles. Defaults to 'default'",
    ),
  );
  $items['fwkbkp-restore'] = array(
    'callback' => 'fwk_backup_drush_restore',
    'description' => dt('Restore the site\'s database with Framework Backup.'),
    'arguments' => array(
      'source'        => "Required. The id of the source (usually a database) to restore the backup to. Use 'drush fwkbkp-sources' to get a list of sources. Defaults to 'db'",
      'destination'   => "Required. The id of destination to send the backup file to. Use 'drush fwkbkp-destinations' to get a list of destinations. Defaults to 'manual'",
      'backup id'     => "Required. The id of a backup file restore. Use 'drush fwkbkp-backups' to get a list of available backup files.",
    ),
    'options' => array(
      'yes' => 'Skip confirmation',
    ),
  );
  $items['fwkbkp-destinations'] = array(
    'callback' => 'fwk_backup_drush_destinations',
    'description' => dt('Get a list of available destinations.'),
  );

  $items['fwkbkp-sources'] = array(
    'callback' => 'fwk_backup_drush_sources',
    'description' => dt('Get a list of available sources.'),
  );
  $items['fwkbkp-profiles'] = array(
    'callback' => 'fwk_backup_drush_profiles',
    'description' => dt('Get a list of available settings profiles.'),
  );
  $items['fwkbkp-backups'] = array(
    'callback' => 'fwk_backup_drush_destination_files',
    'description' => dt('Get a list of previously created backup files.'),
    'arguments' => array(
      'destination'   => "Required. The id of destination to list backups from. Use 'drush fwkbkp-destinations' to get a list of destinations.",
    ),
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function fwk_backup_drush_help($section) {
  switch ($section) {
    case 'drush:fwkbkp-backup':
      return dt("Backup the site's database using default settings.");
    case 'drush:fwkbkp-restore':
      return dt('Restore the site\'s database with Framework Backup.');
    case 'drush:fwkbkp-destinations':
      return dt('Get a list of available destinations.');
    case 'drush:fwkbkp-profiles':
      return dt('Get a list of available settings profiles.');
    case 'drush:fwkbkp-backups':
      return dt('Get a list of previously created backup files.');
  }
}

/**
 * Backup the default database.
 */
function fwk_backup_drush_backup($source_id = 'db', $destination_id = 'manual', $profile_id = 'default') {
  fwk_backup_include('profiles', 'destinations');

  // Set the message mode to logging.
  _fwk_backup_message_callback('_fwk_backup_message_drush');

  if (!fwk_backup_get_destination($source_id)) {
    _fwk_backup_message("Could not find the source '@source' . Try using 'drush fwkbkp-sources' to get a list of available sources or use 'db' to backup the Drupal database.", array('@source' => $source_id), 'error');
    return;
  }
  if (!fwk_backup_get_destination($destination_id)) {
    _fwk_backup_message("Could not find the destination '@destination' . Try using 'drush fwkbkp-destinations' to get a list of available destinations.", array('@destination' => $destination_id), 'error');
    return;
  }
  $settings = fwk_backup_get_profile($profile_id);
  if (!$settings) {
    _fwk_backup_message("Could not find the profile '@profile' . Try using 'drush fwkbkp-profiles' to get a list of available profiles.", array('@profile' => $profile_id), 'error');
    return;
  }

  _fwk_backup_message('Starting backup...');
  $settings->destination_id = $destination_id;
  $settings->source_id = $source_id;
  fwk_backup_perform_backup($settings);
}

/**
 * Restore to the default database.
 */
function fwk_backup_drush_restore($source_id = '', $destination_id = '', $file_id = '') {
  drush_print(dt('Restoring will delete some or all of your data and cannot be undone. ALWAYS TEST YOUR BACKUPS ON A NON-PRODUCTION SERVER!'));
  if (!drush_confirm(dt('Are you sure you want to restore the database?'))) {
    return drush_user_abort();
  }

  fwk_backup_include('profiles', 'destinations');

  // Set the message mode to drush output.
  _fwk_backup_message_callback('_fwk_backup_message_drush');

  if (!fwk_backup_get_destination($source_id)) {
    _fwk_backup_message("Could not find the source '@source' . Try using 'drush fwkbkp-sources' to get a list of available sources or use 'db' to backup the Drupal database.", array('@source' => $source_id), 'error');
    return;
  }
  if (!$destination = fwk_backup_get_destination($destination_id)) {
    _fwk_backup_message("Could not find the destination '@destination' . Try using 'drush fwkbkp-destinations' to get a list of available destinations.", array('@destination' => $destination_id), 'error');
    return;
  }
  if (!$file_id || !$file = fwk_backup_destination_get_file($destination_id, $file_id)) {
    _fwk_backup_message("Could not find the file '@file' . Try using 'drush fwkbkp-backups @destination' to get a list of available backup files in this destination destinations.", array('@destination' => $destination_id, '@file' => $file_id), 'error');
    return;
  }

  _fwk_backup_message('Starting restore...');
  $settings = array('source_id' => $source_id);
  fwk_backup_perform_restore($destination_id, $file_id);
}

/**
 * Get a list of available destinations.
 */
function fwk_backup_drush_destinations() {
  return _fwk_backup_drush_destinations('all');
}

/**
 * Get a list of available sources.
 */
function fwk_backup_drush_sources() {
  return _fwk_backup_drush_destinations('source');
}


/**
 * Get a list of available destinations with the given op.
 */
function _fwk_backup_drush_destinations($op = NULL) {
  fwk_backup_include('destinations');
  $rows = array(array(dt('ID'), dt('Name'), dt('Operations')));
  foreach (fwk_backup_get_destinations($op) as $destination) {
    $rows[] = array(
      $destination->get_id(),
      $destination->get_name(),
      implode(', ', $destination->ops()),
    );
  }
  drush_print_table($rows, TRUE, array(32, 32));
}

/**
 * Get a list of available profiles.
 */
function fwk_backup_drush_profiles() {
  fwk_backup_include('profiles');
  $rows = array(array(dt('ID'), dt('Name')));
  foreach (fwk_backup_get_profiles() as $profile) {
    $rows[] = array(
      $profile->get_id(),
      $profile->get_name(),
    );
  }
  drush_print_table($rows, TRUE, array(32, 32));
}

/**
 * Get a list of files in a given destination
 */
function fwk_backup_drush_destination_files($destination_id = NULL) {
  fwk_backup_include('destinations');

  // Set the message mode to drush output.
  _fwk_backup_message_callback('_fwk_backup_message_drush');

  if (!$destination_id) {
    _fwk_backup_message("You must specify an existing destination. Try using 'drush fwkbkp-destinations' to get a list of available destinations.", array('@destination' => $destination_id), 'error');
    return;
  }
  if (!$destination = fwk_backup_get_destination($destination_id)) {
    _fwk_backup_message("Could not find the destination '@destination' . Try using 'drush fwkbkp-destinations' to get a list of available destinations.", array('@destination' => $destination_id), 'error');
    return;
  }

  $out = array(array(
    dt('Filename'),
    dt('Date'),
    dt('Age'),
    dt('Size'),
  ));

  $files      = $destination->list_files();
  $i          = 0;
  foreach ((array)$files as $file) {
    // Show only files that can be restored from.
    if ($file->is_recognized_type()) {
      $info = $file->info();
      $out[] = array(
        check_plain($info['filename']),
        format_date($info['filetime'], 'small'),
        format_interval(time() - $info['filetime'], 1),
        format_size($info['filesize']),
      );
    }
  }
  if (count($out) > 1) {
    drush_print_table($out, TRUE);
  }
  else {
    drush_print(dt('There are no backup files to display.'));
  }
}

/**
 * Send a message to the drush log.
 */
function _fwk_backup_message_drush($message, $replace, $type) {
  // Use drush_log to display to the user.
  drush_log(strip_tags(dt($message, $replace)), str_replace('status', 'notice', $type));
  // Watchdog log the message as well for admins.
  _fwk_backup_message_log($message, $replace, $type);
}
