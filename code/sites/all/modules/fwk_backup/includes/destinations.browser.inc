<?php


/**
 * @file
 * Functions to handle the browser upload/download backup destination.
 */

/**
 * A destination type for browser upload/download.
 *
 * @ingroup fwk_backup_destinations
 */
class fwk_backup_destination_browser extends fwk_backup_destination {
  /**
   * Get a row of data to be used in a list of items of this type.
   */  
  function get_list_row() {
    // Return none as this type should not be displayed.
    return array();
  }
}

/**
 * A destination type for browser upload.
 *
 * @ingroup fwk_backup_destinations
 */
class fwk_backup_destination_browser_upload extends fwk_backup_destination_browser {
  var $supported_ops = array('restore');
  function __construct() {
    $params = array();
    $params['name'] = "Upload";
    $params['destination_id'] = 'upload';
    parent::__construct($params);
  }

  /**
   * File load destination callback.
   */
  function load_file($file_id) {
    fwk_backup_include('files');
    if ($file = file_save_upload('fwk_backup_restore_upload')) {
      $out = new fwk_backup_file(array('filepath' => $file->uri));
      fwk_backup_temp_files_add($file->uri);
      return $out;
    }
    return NULL;
  }
}

/**
 * A destination type for browser download.
 *
 * @ingroup fwk_backup_destinations
 */
class fwk_backup_destination_browser_download extends fwk_backup_destination_browser {
  var $supported_ops = array('manual backup');
  function __construct() {
    $params = array();
    $params['name'] = "Download";
    $params['destination_id'] = 'download';
    parent::__construct($params);
  }

  /**
   * File save destination callback.
   */
  function save_file($file, $settings) {
    fwk_backup_include('files');
    $file->transfer();
  }
}

