The Framework Backup module has been based on the latest stable version of the
Backup and Migrate module as of the date of its development (7.x-2.4).

Most of the functionality originally offered is present, with exception to the
ability to restore previous backups to the site database, that is, the Drupal
site database cannot be overwritten by using Framework Backup.

REQUIREMENTS
------------

Framework Backup has no module dependency. It can be installed together with
Backup and Migrate, although it does not require it.

BEFORE DOING ANY DATABASE BACKUPS, PLEASE MAKE SURE THAT THE PRIVATE FILES
DIRECTORY IS PROPERLY SET. The configuration page can be accessed through the
menu in Configuration > Media > File System. That should point to a directory
with write permissions.

SETTINGS AND USAGE
------------------

Settings and functionalities of the Framework Backup module can be reached through
the menu in Configuration > System > Framework Backup. They're divided in 4 tabs:
Backup, Destinations, Profiles and Schedules.

Framework Backup provides default settings that should be enough for most cases.
It has two default backup endpoints ("Manual Backups Directory" and "Download")
that can be used either to store the database backups in a site directory or to
download them right away, when the backup is done. Database backups sent to the
Manual Backups Directory can be later downloaded by going to the Destinations
tab and clicking on "list files" and then "download".

- Destinations

Different sources and endpoints (called "Destinations") can be set in the
"Destinations" tab. There are some different destinations types, such as
directory and database. Settings for each new destination depend on their type,
but in general are used to identify the resource (e.g. a database destination
type will have settings such as db host name, db user name etc.)

In general, all destinations can be listed as a source or an endpoint in backup
operations, except for database type destinations, that won't be listed as
endpoints to avoid overwriting the site's default database.

You may add new destinations or override current destinations settings to modify
where you database backups should be stored.

- Profiles

Profiles can be created or overriden as well through the Profiles tab. Profiles
are groups of settings that determine how the backups will be made. For instance,
a profile defines what tables should be excluded from a backup.

When using the "Advanced Backup" feature in the Backup tab a profile can be loaded
before creating the backup so that its settings are used. That same profile can
be altered and saved in that operation if "Save settings" is checked. Other than
that, profiles can be created or altered in the Profiles tab.

- Schedules

Periodic database backups can be scheduled in the Schedules tab. To schedule a
backup, the user must choose a destination and a profile to be used. Schedules are
always periodic, that is, CANNOT be set to run in prespecified dates and times.