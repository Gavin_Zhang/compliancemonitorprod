<?php

// $Id$
define('CACHE_DISABLED', 0);
define('CACHE_NORMAL', 1);

define('FRAMEWORK_1_5_CONFIG_URL', 'admin/config/framework-1-5');

/**
 * Implements hook_boot().
 */
function framework_1_5_boot() {
  // register json_decode and json_encode as functions if they're not available
  if (!function_exists('json_decode')) {
    function json_decode($content, $assoc = FALSE) {
      if ($assoc) {
        $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
      }
      else {
        $json = new Services_JSON;
      }
      return $json->decode($content);
    }
  }

  if (!function_exists('json_encode')) {
    function json_encode($content) {
      $json = new Services_JSON;
      return $json->encode($content);
    }
  }
}

/**
 * Implements hook_init().
 */
function framework_1_5_init() {
  _framework_1_5_global_inc();
  
  module_load_include('php', 'framework_1_5', 'classes/_Constants');

  global $itrinno_site_id;
  $itrinno_site_id = ContextService::getSiteId();

  drupal_add_js(drupal_get_path('module', 'framework_1_5') . '/js/framework_1_5.js');
}

/**
 * Implements hook_menu().
 */
function framework_1_5_menu() {
  $items = array();

  $items[FRAMEWORK_1_5_CONFIG_URL] = array(
    'title' => 'Framework Settings',
    'description' => 'Framework general settings',
    'access arguments' => array('configure framework_1_5 settings'),
    'page callback' => 'system_admin_menu_block_page',
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $items[FRAMEWORK_1_5_CONFIG_URL . '/settings'] = array(
    'title' => 'Emarketing Framework Configuration',
    'description' => 'Framework configuration options.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('framework_1_5_generalsettings_form'),
    'access arguments' => array('configure framework_1_5 settings'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => 0
  );

  $items[FRAMEWORK_1_5_CONFIG_URL . '/changesuperadmin'] = array(
    'title' => 'Change Super Admin',
    'description' => 'Allows you to change the super admin username.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_framework_1_5_changesuperadmin_form'),
    'access arguments' => array('change super admin user name'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => 2
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function framework_1_5_permission() {
  return array(
    'configure framework_1_5 settings' => array(
      'title' => t('Configure framework settings'),
    ),
    'change super admin user name' => array(
      'title' => t('Change super admin user name'),
    )
  );
}

/**
 * Implements hook_form().
 */
function framework_1_5_generalsettings_form($form, &$form_state) {
  $form = array();

  $form['general_settings'] = array(
    '#title' => t('General Settings'),
    '#type' => 'fieldset',
  );

  $form['general_settings']['frameworkemkt_1_5_siteid'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#default_value' => variable_get('frameworkemkt_1_5_siteid', ''),
    '#size' => 10,
    '#maxlength' => 15,
    '#description' => t('Site identification.'),
  );

  $form['general_settings']['frameworkemkt_1_5_authkey'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorization key'),
    '#default_value' => variable_get('frameworkemkt_1_5_authkey', ''),
    '#size' => 25,
    '#maxlength' => 50,
    '#description' => t('Site authorization key.'),
  );

  $form['general_settings']['frameworkemkt_1_5_supporturl'] = array(
    '#type' => 'textfield',
    '#title' => t('Support URL'),
    '#default_value' => variable_get('frameworkemkt_1_5_supporturl', ''),
    '#size' => 25,
    '#description' => t('Absolute URL for the "Support" link.'),
  );

  $gmap_signup_page_link = l(t('in this page'), 'http://code.google.com/apis/maps/signup.html', array('attributes' => array('title' => 'GMaps API Signup Page', 'target' => '_blank')));
  $form['general_settings']['frameworkemkt_1_5_google_api'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Map API key'),
    '#default_value' => variable_get('frameworkemkt_1_5_google_api', ''),
    '#size' => 25,
    '#description' => t('Key to be used with Google Maps API. That can be generated !gmap_signup_page_link.', array('!gmap_signup_page_link' => $gmap_signup_page_link)),
  );

  $form['overrides'] = array(
    '#title' => t('Override Global Settings'),
    '#type' => 'fieldset',
  );

  // get some default values from global.inc if available
  $global_values = array(
    'framework_1_5_service_registry_url' => GlobalSettings::getServiceRegistryUrl(),
    'framework_1_5_cas_service_domain' => GlobalSettings::getCASDomain(),
    'framework_1_5_cas_service_port' => GlobalSettings::getCASPort(),
    'framework_1_5_cas_service_uri' => GlobalSettings::getCASUri(),
  );

  $default_values = array(
    'framework_1_5_service_registry_url' => defined('GLOBALS_REGISTRY_URL') ? GLOBALS_REGISTRY_URL : '',
    'framework_1_5_cas_service_domain' => defined('GLOBALS_CAS_SERVER_URL') ? GLOBALS_CAS_SERVER_URL : '',
    'framework_1_5_cas_service_port' => defined('GLOBALS_CAS_PORT') ? GLOBALS_CAS_PORT : 443,
    'framework_1_5_cas_service_uri' => defined('GLOBALS_CAS_URI') ? GLOBALS_CAS_URI : 'casv20',
  );

  // Field to override the default service url
  $form['overrides']['framework_1_5_service_registry_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Override the Service Registry URL'),
    '#default_value' => $global_values['framework_1_5_service_registry_url'],
    '#size' => 100,
    '#maxlength' => 255,
    '#description' => t('Provide the service registry url.<br />Current default value [<strong>@service_registry_url_default_value</strong>]', array('@service_registry_url_default_value' => $default_values['framework_1_5_service_registry_url'])),
  );

  // Field to override the default cas url
  $form['overrides']['framework_1_5_cas_service_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Override the CAS Service URL'),
    '#default_value' => $global_values['framework_1_5_cas_service_domain'],
    '#size' => 50,
    '#maxlength' => 50,
    '#description' => t('CAS server hostname.<br />Current default value [<strong>@cas_server_url</strong>]', array('@cas_server_url' => $default_values['framework_1_5_cas_service_domain'])),
  );

  // Field to override the default cas port
  $form['overrides']['framework_1_5_cas_service_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Override the CAS port'),
    '#default_value' => $global_values['framework_1_5_cas_service_port'],
    '#size' => 50,
    '#maxlength' => 50,
    '#description' => t('CAS server port.<br />Current default value [<strong>@cas_port</strong>]', array('@cas_port' => $default_values['framework_1_5_cas_service_port'])),
  );

  // Field to override the default cas uri
  $form['overrides']['framework_1_5_cas_service_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Override the CAS URI'),
    '#default_value' => $global_values['framework_1_5_cas_service_uri'],
    '#size' => 30,
    '#maxlength' => 30,
    '#description' => t('CAS server URI. <br/>Current default value [<strong>@cas_uri</strong>]', array('@cas_uri' => $default_values['framework_1_5_cas_service_uri'])),
  );

  $form['overrides']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset to defaults'),
    '#submit' => array('_framework_1_5_reset_global_settings'),
  );

  return system_settings_form($form);
}

/**
 * Reset framework overridden configurations.
 */
function _framework_1_5_reset_global_settings() {
  variable_del('framework_1_5_service_registry_url');
  variable_del('framework_1_5_cas_service_domain');
  variable_del('framework_1_5_cas_service_port');
  variable_del('framework_1_5_cas_service_uri');

  drupal_set_message(t('The configuration options have been reset to their default values.'));
}

/**
 * Loads the global.inc includes file. Works for both ITS (.framework in the
 * Drupal root) or Acquia (.framework in the module directory) environments.
 * @author erickj@ciandt.com
 * @since 26-apr-2013
 */
function _framework_1_5_global_inc() {
  // let's search for .framework in Drupal's root parent directory
  // (that's where .framework should be in ITS servers)
  $global_filepath = dirname(DRUPAL_ROOT) . '/.framework/website/global.inc';
  if (file_exists($global_filepath)) {
    require_once $global_filepath;
  }
  else {
    // if that can't be found we can fallback to framework inside the
    // module's directory
    // note that this directory name is currently not prepended by "."
    $global_filepath = drupal_get_path('module', 'framework_1_5') . '/framework/website/global.inc';
    if (file_exists($global_filepath)) {
      require_once $global_filepath;
    }
    // if we can't find any, show and log the error and terminate Drupal
    else {
      $message = t('Global configuration file @filename does not exist. Please contact the administrator.', array('@filename' => $global_filepath));
      echo 'ITrInno Framework: ' . $message;
      watchdog('ITrInno Framework', $message, array(), WATCHDOG_ERROR);
      drupal_exit();
    }
  }
}

/**
 * Implements hook_theme().
 */
function framework_1_5_theme($existing, $type, $theme, $path) {
  return array(
    'source_target_list' => array(
      'render element' => 'element',
    ),
    'source_list' => array(
      'template' => 'source-list',
      'render element' => 'element',
    ),
  );
}

/**
 *  Form for changing Super Admin
 */
function _framework_1_5_changesuperadmin_form() {
  // Create fieldset for the body:
  $form['field_set'] = array(
    '#type' => 'fieldset',
    '#title' => t('Super Admin settings'),
  );
  // Text box for Registry Url
  $form['field_set']['super_admin_user_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Super Admin User Name'),
    '#required' => TRUE,
    '#weight' => 1,
    '#description' => t('Form to change the super user admin name'),
  );
  // Submit button
  $form['field_set']['username_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 4,
    '#attributes' => array('onclick' => 'return confirmation_msg()'),
  );
  return $form;
}

/**
 *  Submit function for changing super admin
 *  @param $form
 *  @param &$form_state
 */
function _framework_1_5_changesuperadmin_form_submit($form, &$form_state) {
  // get the name from prev page entered by the user
  $super_admin_name = $form_state['values']['super_admin_user_name'];

  // In user table
  // Select users whose name matches with name provided by the user
  $select_user_fetch = db_select('users', 'u')
      ->fields('u', array('uid'))
      ->condition('name', $super_admin_name)
      ->execute()
      ->fetchObject();

  // Get the uid of the user
  if ($select_user_fetch->uid == 1) {
    // User is already a Super admin. Do nothing.
    drupal_set_message(t('%proposed_name is already a super admin', array('%proposed_name' => $super_admin_name)));
  }
  else {
    if ($select_user_fetch->uid != '') {
      // Already an user exists with same name. So delete the existing record
      db_delete('users')
          ->condition('uid', $select_user_fetch->uid)
          ->execute();
    }

    // Update the name of uid-1's record
    db_update('users')
        ->fields(array('name' => $super_admin_name))
        ->condition('uid', 1)
        ->execute();

    drupal_set_message(t('Super admin name has been updated.'));
  }

  // In authmap table
  // Select users whose name matches with name provided by the user
  $auth_select_user_fetch = db_select('authmap', 'a')
      ->fields('a', array('uid'))
      ->condition('authname', $super_admin_name)
      ->execute()
      ->fetchObject();

  if ($auth_select_user_fetch->uid == 1) {
    // User is already a Super admin and present in authmap. Do nothing.
  }
  else {
    if ($auth_select_user_fetch->uid != '') {
      // Already an user exists with same name. So delete the existing record
      db_delete('authmap')
          ->condition('uid', $auth_select_user_fetch->uid)
          ->execute();

      // Update the name of uid-1's record
      db_update('authmap')
          ->fields(array('authname' => $super_admin_name))
          ->condition('uid', 1)
          ->execute();
    }
    else {
      // Check whether uid 1 exists
      $auth_uid_exists_fetch = db_select('authmap', 'a')
          ->addExpression('COUNT(uid)', 'uid_count')
          ->condition('uid', 1)
          ->execute()
          ->fetchObject();

      if ($auth_uid_exists_fetch->uid_count > 0) {
        // Already an user exists with uid = 1. So delete the existing record
        db_delete('authmap')
            ->condition('uid', 1)
            ->execute();
      }

      // Insert a new record with new user name with id as 1
      db_insert('authmap')
          ->fields(array('uid' => 1, 'authname' => $super_admin_name, 'module' => 'cas'))
          ->execute();
    }
  }
}

/**
 * Implements hook_element_info().
 */
function framework_1_5_element_info() {
  $type['source_target_list'] = array(
    '#input' => TRUE,
    '#default_value' => array(),
    '#element_validate' => array('source_target_list_validate'),
    '#process' => array('source_target_list_expand'),
  );

  return $type;
}

/**
 * Source-target list element process function.
 */
function source_target_list_expand($element, $form_state, $complete_form) {
  if (empty($element['#value'])) {
    $element_value = $element['#post'][$element['#name'] . '_list'];
    if (is_array($element_value)) {
      foreach ($element_value as $key => $value) {
        $element['#value'][$value] = $value;
      }
    }
    else {
      $element['#value'] = $element_value;
    }
  }
  return $element;
}

/**
 * Source-target list element validate function.
 */
function source_target_list_validate($element, &$form_state) {
  if ($element['#required'] === TRUE &&
      (empty($element['#post'][$element['#parents'][0] . '_list']))) {

    form_error($element, t('@element_title field is required.', array('@element_title' => $element['#title'])));
  }
  if (!empty($element['#post'][$element['#parents'][0] . '_list'])) {
    $form_state['values'][$element['#parents'][0]] = $element['#post'][$element['#parents'][0] . '_list'];
    $element['#value'] = $element['#post'][$element['#parents'][0] . '_list'];
  }
}

/**
 * Implementation of theme for the source-target list element.
 */
function theme_source_target_list($variables) {
  $element = $variables['element'];
  $class = array('form-text');

  drupal_add_js(drupal_get_path('module', 'framework_1_5') . '/js/source_target_list.js');

  _form_set_class($element, $class);
  $output = theme('source_list', array('element' => $element));

  return theme('form_element', $element, $output);
}

/**
 * Get service registry cached data.
 */
function get_service_registry_cache($cid = NULL) {
  global $user;

  if (!$cid) {
    $cid = _service_registry_get_cache_id();
  }

  $cached_data = cache_get($cid, 'cache_service_registry');

  // @TODO: have to handle all CACHE parameters supported by Drupal. Only
  // CACHE_TEMPORARY is handled right now
  if (is_object($cached_data)) {
    if (($cached_data->expire > REQUEST_TIME && $cached_data->expire != CACHE_TEMPORARY) || ($cached_data->expire == CACHE_TEMPORARY && $cached_data->created < $user->cache)) {
      return $cached_data;
    }
  }
  return FALSE;
}

/**
 * Set service registry cached data (stored in the cache_service_registry table).
 * @param mixed $data
 *   The service registry data object for caching.
 * @param string $cid
 *   Cache ID.
 */
function set_service_registry_cache($data, $cid = NULL) {
  if (!$cid) {
    $cid = _service_registry_get_cache_id();
  }

  if (variable_get('service_registry_cache', CACHE_NORMAL) == CACHE_NORMAL) {
    cache_set($cid, $data, 'cache_service_registry', REQUEST_TIME + variable_get('service_registry_cache_lifetime', 300));
  }
}

function set_service_registry_framework_cache($service_name, $service_identity, $data, $cid = NULL) {
  if (!$cid) {
    $cid = _service_registry_framework_get_cache_id($service_name, $service_identity);
  }

  if (variable_get('service_registry_cache', CACHE_NORMAL) == CACHE_NORMAL) {
    cache_set($cid, $data, 'cache_service_registry', REQUEST_TIME + variable_get('service_registry_cache_lifetime', 300));
  }
}

/**
 * Assemble the cache_id to use for the service registry page.
 *
 * The cache_id string reflects the viewing context for the current service
 * registry page, obtained by concatenating the relevant context information
 *
 * Theme and language contexts are automatically differenciated.
 *
 * @return
 *   The string used as cache_id for the service regsitry.
 *
 * 	Author Sunil Dias. A
 */
function _service_registry_get_cache_id($ServiceData = NULL) {
  if ($ServiceData) {
    $cid = 'url:services:service-cid:' . $ServiceData->GetMethod() . ':' . $ServiceData->GetServiceName() . '/' . $ServiceData->GetServiceIdentity();
  }
  else {
    if ($_GET['page']) {
      $page = filter_xss_admin($_GET['page']);
    }
    else {
      $page = 1;
    }
    $cid = $page . 'page:service_registry:page_cid:' . filter_xss_admin($_GET['q']);
  }
  return $cid;
}

function _service_registry_framework_get_cache_id($service_name, $service_identity) {
  return 'url:services:service-cid:GET:' . $service_name . '/' . $service_identity;
}

/**
 * Implements hook_flush_caches().
 */
function framework_1_5_flush_caches() {
  return array('cache_service_registry');
}

/**
 * Implements hook_form_alter().
 * 
 * Alter the system performace settigns form to add the service registry cache
 * form items.
 */
function framework_1_5_form_system_performance_settings_alter(&$form, &$form_state) {
  $form['service_registry_cache'] = array(
    '#type' => 'fieldset',
    '#weight' => 0,
    '#title' => t('Service Registry cache'),
    '#description' => t('Enabling the Service Registry cache can offer a performance increase for all e-Marketing Dashboard users by preventing the service registry from being reconstructed on each page load using the request to service.'),
  );

  $form['service_registry_cache']['service_registry_cache'] = array(
    '#type' => 'radios',
    '#title' => t('Service Registry cache mode'),
    '#default_value' => variable_get('service_registry_cache', CACHE_NORMAL),
    '#options' => array(
      CACHE_DISABLED => t('Disabled'),
      CACHE_NORMAL => t('Enabled (recommended)'),
    ),
    '#description' => t('The Service Registry caching is enabled by default for 5 minutes.'),
  );

  $period = drupal_map_assoc(array(0, 60, 180, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200, 86400), 'format_interval');
  $period[0] = '<' . t('none') . '>';
  $form['service_registry_cache']['service_registry_cache_lifetime'] = array(
    '#type' => 'select',
    '#title' => t('Minimum cache lifetime'),
    '#default_value' => variable_get('service_registry_cache_lifetime', 300),
    '#options' => $period,
    '#description' => t('On high-traffic sites, it may be necessary to enforce a minimum cache lifetime. The minimum cache lifetime is the minimum amount of time that will elapse before the cache is emptied and recreated, and is applied to service registry caches.'),
  );

  $form['service_registry_cache']['clear_service_registry_cache'] = array(
    '#type' => 'submit',
    '#value' => t('Clear service registry cached data'),
    '#submit' => array('_framework_1_5_system_service_registry_clear_cache_submit'),
  );

  $form['page_cache']['cache_lifetime']['#description'] = t('On high-traffic sites, it may be necessary to enforce a minimum cache lifetime. The minimum cache lifetime is the minimum amount of time that will elapse before the cache is emptied and recreated, and is applied to page, service registry and block caches. A larger minimum cache lifetime offers better performance, but users will not see new content for a longer period of time.');

  return $form;
}

/**
 * Submit callback; clear service registry caches.
 */
function _framework_1_5_system_service_registry_clear_cache_submit(&$form_state, $form) {
  cache_clear_all('*', 'cache_service_registry', TRUE);
  drupal_set_message(t('Service Registry cache cleared.'));
}

function _createUserInfo($user) {
  $uid = $user->uid;
  if ($uid) {
    $name = $user->name;
    $mail = $user->mail;
  }
  else {
    $name = '';
    $mail = '';
  }
  return new UserInfo($uid, $name, $mail);
}