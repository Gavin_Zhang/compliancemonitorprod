<?php

final class ITrInnoAppContextMgr {

  public static function getCurrent() {
    return $GLOBALS[CONTEXT_KEY];
  }

  /**
   * Context to be set. It allows to set only once as of now to avoid overwrite
   * Should be set something better.
   * @param ITrInnoAppContext $ctx
   */
  public static function setCurrent(ITrInnoAppContext $ctx) {
    if (!isset($GLOBALS[CONTEXT_KEY])) {
      $GLOBALS[CONTEXT_KEY] = $ctx;
    }
  }

  public static function getCurrentSiteId() {
    $ctx = ITrInnoAppContextMgr::getCurrent();

    return $ctx->getCurrentSiteInfo()->getSiteId();
  }

  public static function getRootSiteId() {
    $ctx = ITrInnoAppContextMgr::getCurrent();

    return $ctx->getRootSiteInfo()->getSiteId();
  }
}

?>