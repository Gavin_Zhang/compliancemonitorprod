<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ITrInnoAppContext
 *
 * @author Praveen
 */

final class ITrInnoAppContext {

  public $IsUserLoggedIn = true;

  public $UserInfo;
  public $SiteInfoList = array(); //Array of SiteInfo
  public $RootSiteInfo;
  public $CurrentSiteInfo;

  public function __construct(
		  UserInfo $userInfo
		, $siteInfoList
        , $currentSiteId
        , SiteInfo $rootSiteInfo
        , $isLoggedIn) {

    $this->UserInfo = $userInfo;
    $this->SiteInfoList = $siteInfoList;
    $this->RootSiteInfo = $rootSiteInfo;
    $this->CurrentSiteInfo = $this->findCurrentSiteInfo($currentSiteId);
    //$this->siteInfoList = $this->findUserSites($userInfo->getUserId());
    $this->IsUserLoggedIn = $isLoggedIn;

  }


  /****************** Getter & Setter methods ********************/

  /**
   * Get current logged user info
   * @return UserInfo
   */
  public function getUserInfo() {
    return $this->UserInfo;
  }

  /**
   *
   * @return Array of SiteInfo
   */
  public function getSiteInfoList() {
    return $this->SiteInfoList;
  }

  /**
   * Returns E-Marketing dashboard site info.
   * @return SiteInfo
   */
  public function getRootSiteInfo() {
    return $this->RootSiteInfo;
  }

  /**
   *
   */
  public function getCurrentSiteInfo() {
    return $this->CurrentSiteInfo;
  }

  public function getIsUserLoggedIn() {
    return $this->IsUserLoggedIn;
  }

  /****************** End Getter & Setter methods ********************/




  /****************** Private Methods ********************/

  private function findCurrentSiteInfo($siteId) {

    if (!isset($this->SiteInfoList)) {
      return null;
    }

    $currentSiteInfo = null;

    //Loop through the current list of the siteInfoList and set the variable

    return $currentSiteInfo;
  }

  /****************** End Private Methods ******************/

}

?>