<?php

/**
 * Service helper class to get the ContextInfo
 *
 * @author Jag
 */
final class ContextService {

  static public function getSites($siteIdList) {
    $siteInfoList = null;

    $serviceInfo = ServiceRegistryMgr::getServiceInfo(FRAMEWORK_SERVICE_NAME, FRAMEWORK_ACTION_LOAD_SERVICE_META);
    if (is_object($serviceInfo)) {
      $implode_SiteId = implode(",", $siteIdList);
      $add_SiteId = $serviceInfo->getServiceUrl() . "?siteId=" . $implode_SiteId;
      $serviceInfo->setServiceUrl($add_SiteId);

      //	echo $add_SiteId;exit;				
      $serviceInfo->setSiteAuthorizationKey(self::getAdminSiteAuthorizationKey());

      // Create RESTAPI instance
      $restApi = GetRestApiInstance();
      $response = $restApi->executeRequest($serviceInfo);

      $siteInfoList = JSONSerializer::decode($response);
      if (!empty($siteInfoList)) {
        $siteInfoList = JSONSerializer::copyArrayObject($siteInfoList->SiteInfoList, 'SiteInfo');
      }
    }
    return $siteInfoList;
  }

  static public function getUserSites($userId) {

    // Local table holds the mapping of user to list of sites
    // Jagadeesh
    $listOfSites = array();
    //$result = db_query("SELECT siteid FROM {user_site} WHERE uid = %d", $userId);
    $result = db_select('user_site', 'us')
        ->fields('us', array('siteid'))
        ->condition('uid', $userId)
        ->execute();

    foreach ($result as $node) {
      $listOfSites[] = $node->siteid;
    }

    return $listOfSites;
  }

  static public function getCurrentSiteId() {
    return (isset($_COOKIE[CURRENT_CONTENT_SITE_ID]) ? filter_xss_admin($_COOKIE[CURRENT_CONTENT_SITE_ID]) : NULL);
  }

  static public function getAdminSiteId() {
    return variable_get('frameworkemkt_1_5_siteid', '');
  }

  static public function getAdminSiteAuthorizationKey() {
    return variable_get('frameworkemkt_1_5_authkey', '');
  }

  static public function popAdminSite($siteId, &$siteList) {
    $foundSite = null;

    if (!isset($siteList)) {
      return null;
    }
    
    // Loop through the array
    $index = 0;
    foreach ($siteList as $subarray) {
      if ($subarray->SiteId == $siteId) {
        $foundSite = $subarray;
        unset($siteList->$index);
        break;
      }
      $index = $index + 1;
    }

    return $foundSite;
  }

  static public function getCurrentSiteAuthKey() {
    return ContextService::getSiteAuthorizationKey();
  }

  static public function getSiteAuthorizationKey() {
    return variable_get('frameworkemkt_1_5_authkey', '');
  }

  static public function getSiteId() {
    return variable_get('frameworkemkt_1_5_siteid', '');
  }

}
?>