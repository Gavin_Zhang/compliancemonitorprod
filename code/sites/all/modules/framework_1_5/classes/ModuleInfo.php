<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModuleInfo
 *
 * @author Praveen
 */
final class ModuleInfo {
  //put your code here
  // IMPORTANT: DO NOT USE THESE PUBLIC VARIABLE USE get and set instead while USING
  public $ModuleId;
  public $ModuleName;

  /*public function __construct($moduleId, $moduleName){
   $this->ModuleId = $moduleId;
   $this->ModuleName = $moduleName;
   }*/

  public function getModuleId() {
    return $this->ModuleId;
  }

  public function getModuleName() {
    return $this->ModuleName;
  }

  public function setModuleId($value) {
    $this->ModuleId = $value;
  }

  public function setModuleName($value) {
    $this->ModuleName = $value;
  }

}
?>