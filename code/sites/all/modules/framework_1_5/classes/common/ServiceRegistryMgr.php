<?php

final class ServiceRegistryMgr {

  private function __construct() {
    // 
  }

  public static function clearRegistryCache($service_name, $service_identity) {
    $cid = _service_registry_framework_get_cache_id($service_name, $service_identity);
    cache_clear_all($cid, 'cache_service_registry');
  }

  public static function clearAllRegistryCache() {
    cache_clear_all('*', 'cache_service_registry', TRUE);
  }

  public static function getServiceInfo($service_name, $service_action) {
    $site_id = ContextService::getAdminSiteId();

    // Create RESTAPI instance
    $restapi = GetRestApiInstance();

    $framework_registry_url = FRAMEWORK_REGISTRY_URL;
    switch ($service_name) {
      case 'DAM':
      case 'ProductLocator':
      case 'ProductLocator2':
        $framework_registry_url = str_replace(arrray('siteManagement', '/load', '[SITEID]'), array($service_name, '/' . $service_action, $site_id), $framework_registry_url);
        break;
      case 'WebProxy':
        $framework_registry_url = str_replace('[SITEID]', $site_id, GlobalSettings::getServiceRegistryUrl());
        $framework_registry_url = "{$framework_registry_url}{$site_id}/{$service_name}/{$service_action}";
        break;
      default:
        $framework_registry_url = str_replace('[SITEID]', $site_id, $framework_registry_url);
    }

    // This serviceInfo needs to be created by ourselves as to get the serviceInfo of other objects we need to know the service registry. We cannot lookup.
    $service_data = new ServiceInfo();
    $service_data->setSiteId($site_id);
    $service_data->setServiceName($service_name);
    $service_data->setServiceUrl($framework_registry_url);
    $service_data->setVerb(HTTP_GET); //create HTTP_GET, HTTP_POST, HTTP_PUT, HTTP_DELETE
    $service_data->setSiteAuthorizationKey(ContextService::getAdminSiteAuthorizationKey());
    $service_data->setAction($service_action);
    
    $response = $restapi->executeRequest($service_data);

    // and copy it to the service info object
    $service_info = new ServiceInfo();
    $service_info = JSONSerializer::decodeAs($response, $service_info);
    $service_info->setSiteAuthorizationKey(ContextService::getAdminSiteAuthorizationKey());

    if (!empty($service_info->ServiceUrl)) {
      return $service_info;
    }
    else {
      watchdog(
          'ITrInno Service Registry Manager',
          'ServiceUrl property is not set in ServiceInfo object.',
          array(),
          WATCHDOG_ERROR
          );

      return NULL;
    }
  }

}

?>