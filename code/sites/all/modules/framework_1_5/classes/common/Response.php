<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

final class Response {
  private $errCode;
  private $errMsg;
  private $responseBody;
  private $httpStatusCode;
  private $hasError = 'false';

  public function __construct(
	$responseBody
	, $httpStatusCode = NULL
	, $errCode = NULL
	, $errMsg = NULL
	, $hasError = FALSE) {

    $this->responseBody = $responseBody;
    $this->httpStatusCode = $httpStatusCode;
    $this->errMsg = $errMsg;
    $this->errCode = $errCode;
    $this->hasError = $hasError;

  }

  public function getErrCode() {
    return $this->errCode;
  }

  public function getErrMsg() {
    return $this->errMsg;
  }

  public function getResponseBody() {
    return $this->responseBody;
  }

  public function getHasError() {
    return $this->hasError;
  }
}

?>