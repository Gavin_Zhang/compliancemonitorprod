<?php

final class UserInfo {

  // IMPORTANT: DO NOT USE THESE PUBLIC VARIABLE USE get and set instead while USING
  public $UserId;
  public $UserName;
  public $UserEmail;

  public function __construct($userId, $userName, $userEmail) {
    $this->UserId = $userId;
    $this->UserName = $userName;
    $this->UserEmail = $userEmail;
  }

  public function getUserId() {
    return $this->UserId;
  }

  public function getUserName() {
    return $this->UserName;
  }

  public function getUserEmail() {
    return $this->UserEmail;
  }

}

?>