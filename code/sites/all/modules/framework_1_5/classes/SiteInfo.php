<?php

final class SiteInfo {
  //name, description, siteauthorization, siteurl, contact email, status
  // IMPORTANT: DO NOT USE THESE PUBLIC VARIABLE USE get and set instead while USING
  public $SiteId;
  public $SiteName;
  public $SiteUrl;
  public $SiteAuthorizationKey;

  public $ModuleInfoList = array(); //Array of module

  /*public function __construct(
   $siteId
   , $siteName
   , $siteUrl
   , $siteAuthorizationKey
   , $moduleList){

   $this->SiteId = $siteId;
   $this->SiteName = $siteName;
   $this->SiteUrl = $siteUrl;
   $this->SiteAuthorizationKey = $siteAuthorizationKey;

   $this->ModuleInfoList = $moduleList;

   }*/

  public function getSiteId() {
    return $this->SiteId;
  }

  public function setSiteId($value) {
    $this->SiteId = $value;
  }

  public function getSiteName() {
    return $this->SiteName;
  }

  public function setSiteName($value) {
    $this->SiteName = $value;
  }

  public function getSiteUrl() {
    $this->SiteUrl;
  }

  public function setSiteUrl($value) {
    $this->SiteUrl = $value;
  }

  public function getSiteAuthorizationKey() {
    return $this->SiteAuthorizationKey;
  }

  public function setSiteAuthorizationKey($value) {
    $this->SiteAuthorizationKey = $value;
  }

  public function getModuleInfoList() {
    return $this->ModuleInfoList;
  }

  public function setModuleInfoList($listValue) {
    $this->ModuleInfoList = $listValue;
  }
}

?>