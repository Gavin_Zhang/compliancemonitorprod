<?php

final class SecurityContextMgr {

  public static function getCurrent() {
    return $GLOBALS["ITrInnoSecurityCtx"];
  }

  public static function setCurrent(ITrInnoAppContext $ctx) {
    if (!isset($GLOBALS["ITrInnoSecurityCtx"])) {
      $GLOBALS["ITrInnoSecurityCtx"] = $ctx;
    }
  }

}
?>