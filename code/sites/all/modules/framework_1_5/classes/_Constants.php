<?php

/**
 * Description of _Constants
 *
 * @author Praveen
 */

define('CURRENT_CONTENT_SITE_ID', 'fwk_itrinno_site_id');
define('ADMIN_SITE_ID', 'fwk_itrinno_admin_site_id');

define('FRAMEWORK_MODULE_NAME', 'framework_1_5');
define('FRAMEWORK_ACTION_CREATE', 'create');
define('FRAMEWORK_SERVICE_NAME', 'context');
define('FRAMEWORK_ACTION_LOAD_SERVICE_META', 'GetServiceInfo'); // Service action name for ServiceInfo look up

// Constanct for the HTTP verbs
define('HTTP_GET', 'GET');
define('HTTP_POST', 'POST');
define('HTTP_PUT', 'PUT');
define('HTTP_DELETE', 'DELETE');

define('CONTEXT_KEY', "ITrInnoAdminCtx");

// Get Registry url from global.inc file
$registry_url = GlobalSettings::getServiceRegistryUrl();
$service_url = $registry_url . '[SITEID]/siteManagement/load';
define('FRAMEWORK_REGISTRY_URL', $service_url);
?>