<?php

/**
 * @file
 * Administratives features for the ITrInno Form Filter module.
 */

/**
 * Implements hook_form().
 *
 * Administrative form for ITrInno Form Filter.
 */
function itrinno_form_filter_configuration($form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'itrinno_form_filter') . '/itrinno_form_filter.admin.js');

  $form = array();

  $form['form_protection'] = array(
    '#type' => 'fieldset',
    '#title' => 'Form protection settings',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['form_protection']['itrinno_form_filter_field_protection_patterns'] = array(
    '#type' => 'textarea',
    '#size' => 80,
    '#title' => t('Form protection patterns'),
    '#description' => t('ITrInno Form Filter list of patterns for checking form submitted values.'),
    '#default_value' => variable_get('itrinno_form_filter_field_protection_patterns', 'X3CSCRIPT,%27 OR ,%22 OR ,FUNCTION%28%29,FUNCTION(),X3CIFRAME,%3CIFRAME,%3CSCRIPT,<SCRIPT,<IFRAME,<?PHP,<%,<?,EXEC SP_ (or EXEC XP_),AND USER_NAME(),; DESC,ASCII,UNI/*,\' OR ,\' AND ," OR  ," AND ,%3C?,%3C%3F,%253C%3F,%3C?PHP,%253CSCRIPT,%2527 OR ,%2522 OR,%27 AND ,%22 AND,%2527 AND ,%2522 AND,WAITFOR DELAY,WAITFOR%20DELAY,BENCHMARK,USER(),VERSION(), WAITFOR+DELAY, OR 1=1,+OR+1%3D1,OR 2,OR+2," AND ,%22+AND+,AND 1,AND+1,AND 2,AND+2,DROP TABLE,DROP+TABLE,SELECT *,SELECT+%2A,UNION ,UNION+,random(,random%28,BENCHMARK,FUNCTION (,FUNCTION+%28,ONCLICK,ONDBLCLICK,onmousedown,onmousemove,onmouseover,onmouseout,onmouseup,onkeydown,onkeypress,onkeyup,onabort,onerror,onload,onresize,onscroll,onunload,onblur,onchange,onfocus,onreset,onselect,onsubmit,onevent'),
    '#required' => TRUE,
    '#wysiwyg' => FALSE,
  );

  $form['form_protection']['itrinno_form_filter_form_error_message'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('Unsafe form value error message'),
    '#description' => t('The message shown to the user when a form validator detects a hacking attempt.'),
    '#default_value' => variable_get('itrinno_form_filter_form_error_message', 'An unsafe submitted value has been detected. Please review the input values for this form.'),
    '#required' => FALSE,
  );

  $form['form_protection']['itrinno_form_filter_display_link_on_forms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display include/exclude link'),
    '#description' => t('If checked a "Include/Exclude" link will be displayed in every form.')
      . '<br /><em>'
      . t('This link will only be visible for users with the "toggle form filtering" permission. If that user wants to disable/enable filtering for any form then enable this, browse to the corresponding form and perform the appropriate action through the available link. Checking/Unchecking this will only show/hide the link in forms and makes no changes to the module\'s functionalty.')
      . '</em>',
    '#default_value' => variable_get('itrinno_form_filter_display_link_on_forms', TRUE),
  );

  $excluded_form_ids = array_filter(explode(',', variable_get('itrinno_form_filter_excluded_form_ids', '')));
  $excluded_form_ids = (empty($excluded_form_ids)) ? array(t('No forms have been excluded from the checklist yet.')) : array_map('trim', $excluded_form_ids);

  $form['form_protection']['itrinno_form_filter_excluded_form_ids'] = array(
    '#type' => 'item',
    '#title' => t('Excluded form IDs'),
    '#description' => t('List of form ids that will bypass the checklist while being submitted. To exclude/include any form from/to this list please browse to the form and click the link provided below it.'),
    '#markup' => theme('item_list', array('items' => $excluded_form_ids)),
  );

  $form['inc_data_protection'] = array(
    '#type' => 'fieldset',
    '#title' => 'Incoming data protection settings',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['inc_data_protection']['itrinno_form_filter_inc_data_protection_patterns'] = array(
    '#type' => 'textarea',
    '#size' => 80,
    '#title' => t('Incoming data protection patterns'),
    '#description' => t('ITrInno Form Filter default list of patterns for checking incoming data.'),
    '#default_value' => variable_get('itrinno_form_filter_inc_data_protection_patterns', 'X3CSCRIPT#\'#%3C#%3E#%2527#>#<#%3CSCRIPT%3E#%22#%2C#%27#%253C#%253E#%2522#X3CSTYLE#QSS#X3C#X3E#"#JAVASCRIPT#&LT#&GT#%26GT%3B#%26LT%3B#FUNCTION%28%29#FUNCTION()#X3CIFRAME#%3CIFRAME#WAITFOR DELAY#WAITFOR%20DELAY#BENCHMARK#USER()#VERSION()# OR 1=1#+OR+1%3D1#OR 2#OR+2#" AND #%22+AND+#AND 1#AND+1#AND 2#AND+2#DROP TABLE#DROP+TABLE#SELECT *#SELECT+%2A#UNION #UNION+#random(#random%28#BENCHMARK#FUNCTION (#FUNCTION+%28#ONCLICK#ONDBLCLICK#onmousedown#onmousemove#onmouseover#onmouseout#onmouseup#onkeydown#onkeypress#onkeyup#onabort#onerror#onload#onresize#onscroll#onunload#onblur#onchange#onfocus#onreset#onselect#onsubmit#onevent'),
    '#required' => TRUE,
    '#wysiwyg' => FALSE,
  );

  $form['inc_data_protection']['itrinno_form_filter_inc_data_error_message'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('Unsafe incoming data error message'),
    '#description' => t('The message shown to the user when some unsafe data is sent.'),
    '#default_value' => t(variable_get('itrinno_form_filter_inc_data_error_message', 'An unsafe value has been detected in the request.')),
    '#required' => FALSE,
  );

  $form['include_exclude_links'] = array(
    '#type' => 'fieldset',
    '#title' => 'Form Verfication Include/Exclude Links',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['include_exclude_links']['itrinno_form_filter_excluding_form_message'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('"Exclude" link text'),
    '#description' => t('The "exclude from checklist" link caption. Defaults to "Disable form filtering".'),
    '#default_value' => variable_get('itrinno_form_filter_excluding_form_message', t('Disable form filtering')),
    '#required' => TRUE,
  );

  $form['include_exclude_links']['itrinno_form_filter_including_form_message'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#title' => t('"Include" link text'),
    '#description' => t('The "include in checklist" link caption. Defaults to "Enable form filtering".'),
    '#default_value' => variable_get('itrinno_form_filter_including_form_message', t('Enable form filtering')),
    '#required' => TRUE,
  );

  $form['x_frame_options_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'X-Frame-Options header settings',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['x_frame_options_settings']['itrinno_form_filter_enable_iframe_header'] = array(
    "#type" => "checkbox",
    "#title" => t("Use X-Frame-Options header with HTTP response"),
    "#description" => t('If checked X-Frame-Options header will be added to HTTP response to prevent CSRF attacks. <br /><strong>WARNING: Using this header may affect loading contents in iframes.</strong>'),
    "#default_value" => variable_get('itrinno_form_filter_enable_iframe_header', 0),
  );

  $form['x_frame_options_settings']['itrinno_form_filter_iframe_header_options'] = array(
    "#type" => "select",
    "#title" => t("Options"),
    "#description" => theme('item_list', array(t('DENY: Disallow contents to be loaded in iframes'), t('SAMEORIGIN: Allow contents in iframes if the URL has the same domain as that of parent URL'), t('ALLOW-FROM: Allow the specified URLs to be loaded in iframes'))),
    "#options" => array('DENY' => 'DENY', 'SAMEORIGIN' => 'SAMEORIGIN', 'ALLOW-FROM' => 'ALLOW-FROM'),
    "#default_value" => variable_get('itrinno_form_filter_iframe_header_options', ''),
    "#prefix" => '<div class="form-element-wrapper" style="display: ' . (variable_get('itrinno_form_filter_enable_iframe_header', 0) ? 'inline' : 'none') . ';">',
    "#suffix" => '</div>',
  );

  $form['x_frame_options_settings']['itrinno_form_filter_iframe_header_url'] = array(
    "#type" => "textfield",
    "#title" => t("URL"),
    "#description" => t('URLs to be used with ALLOW-FROM option.'),
    "#default_value" => variable_get('itrinno_form_filter_iframe_header_options', ''),
    "#prefix" => '<div class="form-element-wrapper" style="display: ' . (variable_get('itrinno_form_filter_iframe_header_url', '') == 'ALLOW-FROM' ? 'inline' : 'none') . ';">',
    "#suffix" => "</div>",
  );

  $form['data_sanitization_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Data Sanitization options',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['data_sanitization_options']['itrinno_form_filter_data_sanitization_function'] = array(
    "#type" => "select",
    '#required' => TRUE,
    "#title" => t("Select how the data to be sanitized"),
    "#description" => t('Function that will be used for filtering input data.'),
    "#options" => array('FILTER_XSS' => 'FILTER_XSS', 'CHECK_PLAIN' => 'CHECK_PLAIN'),
    "#default_value" => variable_get('itrinno_form_filter_data_sanitization_function', 'FILTER_XSS'),
  );

  $form['data_sanitization_options']['itrinno_form_filter_data_process_option'] = array(
    "#type" => "select",
    '#required' => TRUE,
    "#title" => t('Data processing option'),
    "#description" => theme('item_list', array(t('REDIRECT: user will be redirected to the error page'), t('ERROR: display an error message without redirecting the user'), t('CONTINUE: data will be filtered and then submitted'))),
    "#options" => array('ERROR' => 'ERROR', 'REDIRECT' => 'REDIRECT', 'CONTINUE' => 'CONTINUE'),
    "#default_value" => variable_get('itrinno_form_filter_data_process_option', ''),
  );

  $form['data_sanitization_options']['itrinno_form_filter_redirect_to_page'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#title' => t('Redirect-to page'),
    '#description' => t('Page to which the user will be redirected in case a hacking attempt is detected in the request. Defaults to @frontpage.', array('@frontpage' => '<front>')),
    '#default_value' => variable_get('itrinno_form_filter_redirect_to_page', '<front>'),
    '#required' => TRUE,
    '#wysiwyg' => FALSE,
  );

  $form['itrinno_form_filter_exclude_path'] = array(
    '#type' => 'textarea',
    '#size' => 100,
    '#title' => t('Exclude paths'),
    '#description' => t('Add pages to be excluded from ITrInno Form Filter checks.'),
    '#default_value' => variable_get('itrinno_form_filter_exclude_path', implode(PHP_EOL, array('admin*', 'node/add*', 'node/*/edit*'))),
    '#required' => FALSE,
    "#wysiwyg" => FALSE,
  );

  $form['itrinno_form_filter_enable_debug_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug info'),
    '#description' => t("Determines if debug info about filtered data should be logged in Drupal's watchdog.")
      . '<br /><strong>'
      . t("WARNING: user submitted sensitive data may be stored in Drupal's watchdog when this feature is enabled. <strong>NOT RECOMMEDED</strong> on production environments.")
      . '</strong>',
    '#default_value' => variable_get('itrinno_form_filter_enable_debug_info', FALSE),
  );

  return system_settings_form($form);
}

/**
 * Provides a form to set the list of excluded form IDs manually.
 * @author erickj@ciandt.com
 * @since 01-mar-2013
 */
function itrinno_form_filter_excluded_forms_settings($form, &$form_state) {
  $form = array();

  $form['itrinno_form_filter_excluded_form_ids'] = array(
    '#type' => 'textarea',
    '#title' => t('Excluded Form IDs'),
    '#description' => t('A comma-separated list of form IDs that should bypass form submitted values verification.')
      . '<br /><strong>'
      . t('WARNING: including form id\'s in this list REMOVES ALL PROTECTION against cross-site scripting and SQL injection attacks offered by this module for form submitted values.')
      . '</strong>',
    '#default_value' => variable_get('itrinno_form_filter_excluded_form_ids', ''),
  );

  return system_settings_form($form);
}

/**
 * Confirmation form for the Enable/Disable form filtering feature.
 * @author erickj@ciandt.com
 * @since 27-feb-2013
 */
function itrinno_form_filter_include_exclude_form($form, &$form_state, $action, $formid) {
  // keep the parameters as form values
  $form = array(
    'formid' => array(
      '#type' => 'value',
      '#value' => filter_xss_admin($formid),
    ),
    'action' => array(
      '#type' => 'value',
      '#value' => filter_xss_admin($action),
    ),
  );

  $yes_caption = ($action == 'include') ? t('Enable') : t('Disable');
  $no_caption = t('Cancel');

  $confirmation_message = t('Are you sure you want to !action %form_id form in ITrInno Form Filter checklist?', array('!action' => $action, '%form_id' => $formid));
  $description = ($action == 'include') ? t('If enabled, this form will be automatically protected against attacks.') : t('If disabled, this form will NOT be automatically protected against attacks.');

  return confirm_form($form, $confirmation_message, 'admin/config/itrinno_form_filter', $description, $yes_caption, $no_caption);
}

/**
 * Sets the form to be include/excluded from ITrInno Form Filter validation.
 * @author erickj@ciandt.com
 * @since 27-feb-2013
 */
function itrinno_form_filter_include_exclude_form_submit($form, &$form_state) {
  $formid = $form['formid']['#value'];
  $action = $form['action']['#value'];

  $excluded_form_ids = _itrinno_form_filter_get_excluded_form_ids();

  switch ($action) {
    case 'include':
      if (in_array($formid, $excluded_form_ids)) {
        $key = array_search($formid, $excluded_form_ids);
        unset($excluded_form_ids[$key]);
      }
      break;
    case 'exclude':
      if (!in_array($formid, $excluded_form_ids)) {
        $excluded_form_ids[] = $formid;
      }
      break;
  }

  variable_set('itrinno_form_filter_excluded_form_ids', implode(',', $excluded_form_ids));
  drupal_set_message(t('The changes were saved successfully.'));
}