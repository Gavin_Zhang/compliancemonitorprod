<?php

/**
 * @file
 * Helper functions for ITrInno Form Filter module.
 */

/**
 * Filters values to be displayed in a form in order to prevent XSS attacks.
 * @author erickj@ciandt.com
 * @since 21-feb-2013
 *
 * @param array $element
 *   A FAPI element.
 * @param array $callbacks
 *   A list of functions that should be run against the values to filter them.
 *   'filter_xss_admin' is the default value. Those functions should accept the value
 *   to be filtered as their first parameter.
 * @param array $callbacks_parameters
 *   An associative array keyed by callback function name with function parameters.
 *   Example (calling filter_xss() only allowing '&lt;p&gt;' and '&lt;strong&gt;' tags):
 *   <code>
 *     _itrinno_form_filter_filter_form_contents($form, array('filter_xss'), array('filter_xss' => array(array('p', 'strong'))));
 *   </code>
 */
function _itrinno_form_filter_filter_form_contents(&$form_element, $callbacks = array('filter_xss_admin'), $callbacks_parameters = array()) {
  $types_to_filter = array('fieldset', 'markup', 'checkboxes', 'radios', 'select', 'textfield', 'textarea', 'item');

  // we should have callbacks to filter content
  // if we don't, bail out
  if (!(is_array($callbacks)) || (empty($callbacks))) {
    return;
  }
  // $element should be a FAPI item, so has to be an array
  if (!is_array($form_element)) {
    return;
  }

  $elements = _itrinno_form_filter_get_form_elements($form_element, $types_to_filter);
  foreach ($elements as &$element) {
    foreach ($callbacks as $callback) {
      if (!function_exists($callback)) {
        continue;
      }
      $callback_parameters = (isset($callbacks_parameters[$callback])) ? $callbacks_parameters[$callback] : array();
      // get the key names that should be filtered for this form element
      $value_idxs = _itrinno_form_filter_get_filterable_properties_names($element);
      foreach ($value_idxs as $value_idx) {
        if (!isset($element[$value_idx])) {
          continue;
        }
        $original_value = $element[$value_idx];
        $filtered_value = $element[$value_idx] = _itrinno_form_filter_filter_nested_values($element[$value_idx], $callback, $callback_parameters);

        if ($original_value != $filtered_value) {
          $log_message = 'Form element of type @element_type property @property value has been changed';
          $message_params = array('@element_type' => $element['#type'], '@property' => $value_idx);
          _itrinno_form_filter_log_debug_info($log_message, $message_params, $original_value, $filtered_value);
        }
      }
    }
  }
}

/**
 * Filters values that have been submitted through a form in order to prevent XSS attacks.
 * @author erickj@ciandt.com
 * @since 21-feb-2013
 *
 * @param array $form_element
 *   A FAPI element.
 * @param array $callbacks
 *   A list of functions that should be run against the values to filter them.
 *   'filter_xss' is the default callback. Those functions should accept the value
 *   to be filtered as their first parameter.
 * @param array $callbacks_parameters
 *   An associative array keyed by callback function name with function parameters.
 *   Example (calling filter_xss() only allowing '&lt;p&gt;' and '&lt;strong&gt;' tags):
 *   <code>
 *     _itrinno_form_filter_filter_form_contents($form, $form_state, array('filter_xss'), array('filter_xss' => array(array('p', 'strong'))));
 *   </code>
 * @return array
 *   A list of form elements names that have been filtered.
 */
function _itrinno_form_filter_filter_form_state_values($form_element, &$form_state, $callbacks = array('filter_xss'), $callbacks_parameters = array()) {
  $changed_elements = array();

  // we should have callbacks to filter values
  // if we don't, bail out
  if (!(is_array($callbacks)) || (empty($callbacks))) {
    return $changed_elements;
  }
  // $element should be a FAPI item, so has to be an array
  if (!is_array($form_element)) {
    return $changed_elements;
  }

  // get the references for all form elements that should be checked against
  // our validators
  $elements = _itrinno_form_filter_get_form_elements($form_element);
  foreach ($elements as $element) {
    foreach ($callbacks as $callback) {
      // skip invalid callback functions
      if (!function_exists($callback)) {
        continue;
      }
      $callback_parameters = (isset($callbacks_parameters[$callback])) ? $callbacks_parameters[$callback] : array();

      // get the key name of the item that should be filtered
      list($element_name, $element_value) = _itrinno_form_filter_get_element_name_and_value($element, $form_state);
      if ($element_value == NULL) {
        continue;
      }

      // get the filtered value and set if in $form_state
      // also list form elements that have been changed
      // this can be used to set form errors outside the scope of this function
      $original_value = $element_value;
      $filtered_value = _itrinno_form_filter_filter_nested_values($original_value, $callback, $callback_parameters);
      if ($original_value != $filtered_value) {
        $changed_elements[] = $element_name;

        $log_message = 'Form submitted value for @element_name has been changed';
        $message_params = array('@element_name' => $element_name);
        _itrinno_form_filter_log_debug_info($log_message, $message_params, $original_value, $filtered_value);
      }

      form_set_value($element, $filtered_value, $form_state);
    }
  }

  return $changed_elements;
}

/**
 * Recursively inspects form API elements and returns references to items that
 * match the specified form API types.
 * @author erickj@ciandt.com
 * @since 28-feb-2013
 *
 * @param array $element
 *   Form API element to inspect.
 * @param array $types_to_get
 *   List of suitable form API types that should be returned. By default they
 *   are 'textfield', 'textarea', 'hidden', 'item', 'checkboxes', 'radios'.
 * @param array $elements
 *   Elements array for a certain form. Should only be used in recursive
 *   internal calls.
 * @return array
 */
function _itrinno_form_filter_get_form_elements(&$element, $types_to_get = array('textfield', 'textarea', 'hidden', 'item', 'checkboxes', 'radios'), &$elements = array()) {
  if (!is_array($element)) {
    return $elements;
  }

  $children_elements = element_children($element);

  foreach ($children_elements as $key) {
    $child_element = &$element[$key];

    $children_keys = element_children($child_element);
    $has_children_elements = (count($children_keys) > 0);

    if ($has_children_elements) {
      _itrinno_form_filter_get_form_elements($child_element, $types_to_get, $elements);
    }

    if (is_array($child_element) && isset($child_element['#type']) && in_array($child_element['#type'], $types_to_get)) {
      $elements[] = &$child_element;
    }
  }

  return $elements;
}

/**
 * Filter form elements values recursively in a way that nested arrays will
 * get fully filtered.
 * @author erickj@ciandt.com
 * @since 21-feb-2013
 *
 * @param string|array $value
 *   The value to be filtered.
 * @param string $callback
 *   A callback function to do the actual filtering. This function must accept
 *   the valued to be filtered as its first parameter.
 * @param array $extra_callback_parameters
 *   An associative array keyed by callback function name with function parameters.
 * @return string|array
 *   The filtered form element values.
 *
 * @see _itrinno_form_filter_filter_value().
 */
function _itrinno_form_filter_filter_nested_values($value, $callback, $extra_callback_parameters = array()) {
  if (is_array($value)) {
    foreach ($value as &$sub_value) {
      $sub_value = _itrinno_form_filter_filter_nested_values($sub_value, $callback, $extra_callback_parameters);
    }
    return $value;
  }
  else {
    return _itrinno_form_filter_filter_value($value, $callback, $extra_callback_parameters);
  }
}

/**
 * Filters a single form element value.
 * @author erickj@ciandt.com
 * @since 21-feb-2013
 *
 * @param string $value
 *   The value to be filtered.
 * @param string $callback
 *   A callback function to do the actual filtering. This function must accept
 *   the valued to be filtered as its first parameter.
 * @param array $extra_callback_parameters
 *   An array with parameters to be passed onto the filtering function.
 * @return string
 *   The filtered value as returned by the filtering callback function.
 */
function _itrinno_form_filter_filter_value($value, $callback, $extra_callback_parameters = array()) {
  // set the form item current value as the first callback parameter
  $callback_parameters = array($value);
  if (!(is_null($extra_callback_parameters)) && (is_array($extra_callback_parameters))) {
    $callback_parameters = array_merge($callback_parameters, $extra_callback_parameters);
  }

  // return the new form item value (filtered by callback)
  return call_user_func_array($callback, $callback_parameters);
}

/**
 * Returns a list of properties that should be filtered given a FAPI element.
 * @author erickj@ciandt.com
 * @since 21-Feb-2013
 *
 * @param array $element A FAPI element.
 * @return mixed
 *   A list of FAPI properties to be filtered. Returns FALSE if element is invalid.
 */
function _itrinno_form_filter_get_filterable_properties_names($element) {
  if (!isset($element['#type'])) {
    return FALSE;
  }

  $filterable_properties = array('#title', '#description');

  switch ($element['#type']) {
    case 'markup':
    case 'item':
      $filterable_properties[] = '#markup';
      break;
    case 'checkboxes':
    case 'radios':
      $filterable_properties[] = '#title';
      $filterable_properties[] = '#options';
      break;
    case 'textfield':
    case 'textarea':
      // 
      break;
    default:
      $filterable_properties[] = '#default_value';
  }

  return $filterable_properties;
}

/**
 * Filters form submitted values by stripping out patterns considered dangerous.

 * @param mixed $value The value to be filtered.
 * @return mixed The filtered value.
 */
function _itrinno_form_filter_prevent_hacking($value, $pattern_list_name) {
  $field_protection_patterns = array_filter(explode(',', drupal_strtolower(variable_get($pattern_list_name, ''))));

  foreach ($field_protection_patterns as $pattern) {
    $value = str_ireplace($pattern, '', $value);
  }

  return $value;
}

/**
 * Get an element value by its parents.
 * 
 * @param type $search_array
 *   Array in which to search for element submitted values. Tipically a
 *   $form_state['values'] array corresponding to that element.
 * @param type $parents
 *   List of parent elements (usually taken from the element #parents' property.
 * @param type $current
 *   Current #parents array index being inspected.
 * @return mixed
 *   The element's submitted value if found or NULL if not found.
 */
function _itrinno_form_filter_get_value_by_parents($search_array, $parents, $current = 0) {
  $current_index = $parents[$current];
  // if parents indicate a non-existent item, return NULL
  if (!isset($search_array[$current_index])) {
    return NULL;
  }

  if (is_array($search_array[$current_index])) {
    // keep searching if there is a next index to inspect
    if (isset($parents[$current + 1])) {
      return _itrinno_form_filter_get_value_by_parents($search_array[$current_index], $parents, $current + 1);
    }
    // if there's no further index to inspect, return what we got so far
    return $search_array[$current_index];
  }

  // return the element's value when found
  return $search_array[$current_index];
}

/**
 * Get element name and submitted value either by the default identifier property
 * or its parents.
 * 
 * @param array $element
 *   A FAPI element to inspect.
 * @param array $form_state
 *   $form_state with submitted element values.
 * @return array
 *   An array with (element name, element value).
 */
function _itrinno_form_filter_get_element_name_and_value($element, $form_state) {
  $parents = $element['#parents'];
  $element_name = implode('][', $parents);
  $element_value = _itrinno_form_filter_get_value_by_parents($form_state['values'], $parents);
  return array($element_name, $element_value);
}

/**
 * Filter input data ($_GET, $_POST and $_REQUEST) depending on configured options.
 */
function _itrinno_form_filter_sanitize_input_data() {
  if (($function = _itrinno_form_filter_get_filtering_function()) !== NULL) {
    _itrinno_form_filter_process_input_data($function);
  }
}

/**
 * Retrieve the proper error message depending on type of data submitted and
 * message configurations.
 * 
 * @param string $type 'url' for generic incoming data filtering, 'form' for
 * form submitted values filtering.
 */
function _itrinno_form_filter_data_error_message($type) {
  // don't show error messages when process option is to continue with filtered
  // values only
  if (variable_get('itrinno_form_filter_data_process_option', '') == 'CONTINUE') {
    return NULL;
  }

  switch ($type) {
    case 'url':
      return variable_get('itrinno_form_filter_inc_data_error_message', t('An unsafe value has been detected in the request.'));
    case 'form':
      return variable_get('itrinno_form_filter_form_error_message', t('An unsafe submitted value has been detected. Please review the input values for this form.'));
  }
}