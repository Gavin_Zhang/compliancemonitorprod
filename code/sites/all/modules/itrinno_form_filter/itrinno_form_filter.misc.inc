<?php

/**
 * @file
 * ITrInno Form Filter miscelaneous helper functions.
 */

/**
 * Gets the current page path alias.
 * @author erickj@ciandt.com
 * @since 21-Feb-2013
 *
 * @return string Current page path alias (or system path if no alias present).
 */
function _itrinno_form_filter_get_current_page() {
  return (isset($_GET['q']) ? drupal_get_path_alias($_GET['q']) : '');
}

/**
 * Tell if the page should be excluded from input data filtering.
 * @author erickj@ciandt.com
 * @since 21-Feb-2013
 * 
 * @param string $path Current page path (may be system path or alias).
 * @return boolean TRUE if path matches an excluded path, FALSE otherwise.
 */
function _itrinno_form_filter_is_excluded_page($path = NULL) {
  // set $path as the current path if none has been passed
  $path = (is_null($path)) ? _itrinno_form_filter_get_current_page() : drupal_get_path_alias($path);

  $excluded_paths = variable_get('itrinno_form_filter_exclude_path', '');

  if (!empty($excluded_paths)) {
    if (drupal_match_path($path, $excluded_paths)) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Returns the filtered and trimmed list of excluded form IDs.
 * @author erickj@ciandt.com
 * @since 01-mar-2013
 *
 * @return array A list of form IDs that should bypass form filter verification.
 */
function _itrinno_form_filter_get_excluded_form_ids() {
  // get the comma-separated list of values, turn it into an array, remove empty
  // values and trim the remaining ones
  return array_map('trim', array_filter(explode(',', variable_get('itrinno_form_filter_excluded_form_ids', ''))));
}

/**
 * Returns a themed list of changes applied by content filters.
 * @author erickj@ciandt.com
 * @since 02-mar-2013
 *
 * @param mixed $original_value Value before filtering.
 * @param mixed $filtered_value Value after filtering.
 * @return string A themed list of changes (as and ouptut of theme('item_list')).
 */
function _itrinno_form_filter_themed_changes_list($original_value, $filtered_value) {
  return theme(
    'item_list',
    array('items' => array(
      t('Original:') . ' ' . htmlspecialchars(print_r($original_value, TRUE)),
      t('Filtered:') . ' ' . htmlspecialchars(print_r($filtered_value, TRUE))
  )));
}

/**
 * Log debug info when values are deemed as unsafe and are filtered.
 * 
 * @param string $message
 *   The untranslated message to be logged. May contain placeholders as accepted
 *   by format_string().
 * @param array $message_params
 *   An associative array of message placeholders and values to be used.
 * @param type $original_value
 *   Original item value.
 * @param type $filtered_value
 *   Post-filtering value.
 * 
 * @see watchdog()
 * @see t()
 */
function _itrinno_form_filter_log_debug_info($message, $message_params = array(), $original_value = NULL, $filtered_value = NULL) {
  if (!variable_get('itrinno_form_filter_enable_debug_info', FALSE)) {
    return;
  }

  if (!is_null($original_value) && !is_null($filtered_value)) {
    $message_params += array(
      '!themed_changes_list' => _itrinno_form_filter_themed_changes_list($original_value, $filtered_value),
    );
  }

  watchdog('ITrInno Form Filter', $message . '!themed_changes_list', $message_params, WATCHDOG_DEBUG);
}

/**
 * Determine data sanitization function name.
 * 
 * @return string|null
 *   Data sanitization function name. Defaults to 'filter_xss'.
 */
function _itrinno_form_filter_get_filtering_function() {
  switch (variable_get('itrinno_form_filter_data_sanitization_function', 'FILTER_XSS')) {
    case 'FILTER_XSS':
      return 'filter_xss';
    case 'CHECK_PLAIN':
      return 'check_plain';
  }

  return 'filter_xss';
}

/**
 * Take appropriate action depending on configuration when an unsafe value is
 * filtered.
 * 
 * @param string $type 'url' for generic input data filtering and 'form' for
 * form filtering.
 */
function _itrinno_form_filter_treat_errors($type) {
  $message = _itrinno_form_filter_data_error_message($type);

  switch (variable_get('itrinno_form_filter_data_process_option', '')) {
    case 'ERROR':
      drupal_set_message($message, 'error', FALSE);
      break;
    case 'REDIRECT':
      drupal_set_message($message, 'error');

      drupal_goto(variable_get('itrinno_form_filter_redirect_to_page', '<front>'));
      break;
  }
}