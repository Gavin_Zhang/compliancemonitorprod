/**
 * @file
 * Show or hide X-Frame-Options related settings fields.
 */

(function($) {
  $(document).ready(function() {
    var iframe_header_options_select = $('select[name="itrinno_form_filter_iframe_header_options"]');
    iframe_header_options_select.data('previousValue', iframe_header_options_select.val());
    
    function toggleInputValue(input, showValue) {
      if (!showValue) {
        // store the input current value and then reset it
        input.data('valueStorage', input.val());
        input.val('');
      }
      else {
        // restore the input previous value in case any has been stored so far
        var valueStorage = input.data('valueStorage');
        if (typeof(valueStorage) != 'undefined') {
          input.val(valueStorage);
        }
      }
    }

    // X-Frame-Options mode select box
    iframe_header_options_select.change(function() {
      var relatedElement = $('input[name="itrinno_form_filter_iframe_header_url"]');
      var wrapper = relatedElement.parents('.form-element-wrapper');
      if ($(this).val() == 'ALLOW-FROM' && $(this).parents('.form-element-wrapper').is(':visible')) {
        wrapper.show();
        // restore previously informed URL value if ALLOW-FROM is selected
        toggleInputValue(relatedElement, true);
      }
      else {
        wrapper.hide();
        // clear URL value if ALLOW-FROM is not selected
        // let's just make this change if we're coming from the ALLOW-FROM option
        // (i.e. change from DENY to SAMEORIGIN or the opposite won't override
        // the field value)
        if ($(this).data('previousValue') == 'ALLOW-FROM') {
          toggleInputValue(relatedElement, false);
        }
      }
      $(this).data('previousValue', $(this).val());
    });  

    // Show/Hide X-Frame-Options header option fields
    $('input[name="itrinno_form_filter_enable_iframe_header"]').change(function() {
      var relatedElement = $('select[name="itrinno_form_filter_iframe_header_options"]');
      var wrapper = relatedElement.parents('.form-element-wrapper');
      if ($(this).is(':checked')) {
        wrapper.show();
      }
      else {
        wrapper.hide(); 
      }

      relatedElement.trigger('change');
    });

    // trigger the change even once to set the inital state
    $('input[name="itrinno_form_filter_enable_iframe_header"]').trigger('change');
  });
})(jQuery)