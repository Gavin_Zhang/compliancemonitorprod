
<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->separator: an optional separator that may appear before a field.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<?php
$title = '';// public 'class' => string 'title' 
$url = '';  // public 'class' => string 'field-aspcm-link-url-value' 
$imageurl =''; //  public 'class' => string 'field-aspcm-upload-fid' 
$description = '';//public 'class' => string 'field-aspcm-description-value' 
?>
<?php
  foreach ($fields as $id => $field) {
    if ($field->class == 'title') {
      $title = strip_tags($field->content) ;
    }
    elseif ($field->class == 'field-aspcm-link-url') {
      $url = strip_tags($field->content);
    }
    elseif ($field->class == 'field-aspcm-upload') {
      $imageurl = strip_tags($field->content);
    }
    elseif ($field->class == 'field-aspcm-description') {
      $description = strip_tags($field->content);
    }
  }
?>
 <a class="" href="<?php print $url;?>" target="_blank" title="<?php print $title;?>">  
<img width="160" height="65" alt="<?php print $title;?>" title="<?php print $title;?>" src="<?php print $imageurl;?>"/></a><br />
<a class="header-link" href="<?php print $url;?>" target="_blank" title="<?php print $title;?>"><?php print $title;?></a> 
<p><?php print $description;?></p>

<!--
<?php foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

  <<?php print $field->inline_html;?> class="views-field-<?php print $field->class; ?>">
    <?php if ($field->label): ?>
      <label class="views-label-<?php print $field->class; ?>">
        <?php print $field->label; ?>:
      </label>
    <?php endif; ?>
      <?php
      // $field->element_type is either SPAN or DIV depending upon whether or not
      // the field is a 'block' element type or 'inline' element type.
      ?>
      <<?php print $field->element_type; ?> class="field-content"><?php print $field->content; ?></<?php print $field->element_type; ?>>
  </<?php print $field->inline_html;?>>
<?php endforeach; ?>

-->

