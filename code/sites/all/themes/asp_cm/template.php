<?php
// Get styles (add css stylesheets here to avoid IE 30 stylesheets limit)
function get_asp_cm_style() {
  $style = theme_get_setting('style');
  return $style;
}
drupal_add_css(drupal_get_path('theme','asp_cm').'/css/style-zero.css');
drupal_add_css(drupal_get_path('theme','asp_cm') . '/css/' . get_asp_cm_style() . '.css');

//drupal_add_css(drupal_get_path('theme','asp_cm').'/_custom/custom-style.css');

$roundcorners = theme_get_setting('roundcorners');
  if ($roundcorners == '1'){ 
  drupal_add_css(drupal_get_path('theme','asp_cm').'/css/round.css');
}


/**
 * Modify theme variables
 */
function asp_cm_preprocess(&$vars) {
  global $user;                                           // Get the current user
  //$vars['is_admin'] = in_array('ADMIN', $user->roles);    // Check for Admin, logged in
  $vars['logged_in'] = ($user->uid > 0) ? TRUE : FALSE;
  
  // Reduce the redirect of front page to improve the performance of default / home page
  // front_page variable will be added to logo link
  $vars['front_page'] = $user->uid == 0 ? '/default' : '/home';
}
/*
 * theme_preprocess_html
 */
function asp_cm_preprocess_html(&$vars) {
  $left = isset($vars['page']['left']) ? TRUE : FALSE;
  $right = isset($vars['page']['right']) ? TRUE : FALSE;
  $vars['classes_array'] = array();
  $vars['classes_array'][] = 'layout-'. ($left ? 'left-main' : 'main') 
  . ($right ? '-right' : '');                                                           // Page sidebars are active (Jello Mold)
  $vars['classes_array'][] = $vars['logged_in'] ? 'logged-in' : 'not-logged-in';
  $vars['classes_array'][] = $vars['is_front'] ? 'front' : 'not-front';
  if ($node = menu_get_object()) {
    $vars['classes_array'][] = 'full-node';                                                         // Page is one full node
    $vars['classes_array'][] = drupal_html_class('node-type-' . $node->type);                       // Page has node-type-x, e.g., node-type-page

    // Add any taxonomy terms for node pages
    if (isset($node->taxonomy)) {
      foreach ($node->taxonomy as $taxonomy_id => $term_info) {
        $vars['classes_array'][] = 'tag-'. $taxonomy_id;                                            // Page has terms (tag-x)
      }
    }
  }
  // Add unique classes for each page and website section
  if (!$vars['is_front']) {
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $vars['classes_array'][] = id_safe('section-' . $section);
    $vars['classes_array'][] = id_safe('page-' . $path);
    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        if ($section == 'node') {
          array_pop($vars['classes_array']); // Remove 'section-node'
        }
        $vars['classes_array'][] = 'section-node-add'; // Add 'section-node-add'
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        if ($section == 'node') {
          array_pop($vars['classes_array']); // Remove 'section-node'
        }
        $vars['classes_array'][] = 'section-node-' . arg(2); // Add 'section-node-edit' or 'section-node-delete'
      }
    }
  }
  else {
    $vars['classes_array'][] = 'page-default';
  }
  // Build array of additional body classes and retrieve custom theme settings
  $layoutwidth = theme_get_setting('layout-width');
  if ($layoutwidth == '0'){ 
    $vars['classes_array'][] = 'layout-jello';
  }
  if ($layoutwidth == '1'){ 
    $vars['classes_array'][] = 'layout-fluid';
  }
  if ($layoutwidth == '2'){ 
    $vars['classes_array'][] = 'layout-fixed';
  }
  $sidebarslayout = theme_get_setting('sidebarslayout');
  if ($sidebarslayout == '0'){ 
    $vars['classes_array'][] = ($left ? 'l-m' : 'm') . ($right ? '-r' : '') . '-var';
  }
  if ($sidebarslayout == '1'){ 
    $vars['classes_array'][] = ($left ? 'l-m' : 'm') . ($right ? '-r' : '') . '-fix';
  }
  if ($sidebarslayout == '2'){ 
    $vars['classes_array'][] = ($left ? 'l-m' : 'm') . ($right ? '-r' : '') . '-var1';
  }
  if ($sidebarslayout == '3'){ 
    $vars['classes_array'][] = ($left ? 'l-m' : 'm') . ($right ? '-r' : '') . '-fix1';
  }
  if ($sidebarslayout == '4'){ 
    $vars['classes_array'][] = ($left ? 'l-m' : 'm') . ($right ? '-r' : '') . '-eq';
  }
  $blockicons = theme_get_setting('blockicons');
  if ($blockicons == '2'){ 
    $vars['classes_array'][] = 'bicons32';
  }
  if ($blockicons == '1'){ 
    $vars['classes_array'][] = 'bicons48';
  }
// Add Panels classes and lang
  $vars['classes_array'][] = (module_exists('panels_page') && (panels_page_get_current())) ? 'panels' : '';  // Page is Panels page
  $vars['classes_array'][] = ($vars['language']->language) ? 'lg-'. $vars['language']->language : '';        // Page has lang-x
  $siteid = theme_get_setting('siteid');
  $vars['classes_array'][] = $siteid;
  $vars['classes_array'] = array_filter($vars['classes_array']);                                             // Remove empty elements

  // Set IE6 & IE7 & all IE stylesheets
  $theme_path = base_path() . path_to_theme();
  $vars['ie6_style'] = '<link type="text/css" rel="stylesheet" media="all" href="' . $theme_path . '/css/ie6.css" />' . "\n";
  $vars['ie7_style'] = '<link type="text/css" rel="stylesheet" media="all" href="' . $theme_path . '/css/ie7.css" />' . "\n";
  $vars['ie8_style'] = '<link type="text/css" rel="stylesheet" media="all" href="' . $theme_path . '/css/ie8.css" />' . "\n";
  $vars['ie_style'] = '<link type="text/css" rel="stylesheet" media="all" href="' . $theme_path . '/css/ie.css" />' . "\n";
  
}
/*
 * theme_process_html
 */
function asp_cm_process_html(&$vars) {

  // Use grouped import technique for more than 30 un-aggregated stylesheets (css limit fix for IE)
  $css = drupal_add_css();
  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
    if (theme_get_setting('fix_css_limit') && !variable_get('preprocess_css', FALSE) && asp_cm_css_count($css) > 26) {
      $styles = '';
      $suffix = "\n".'</style>'."\n";
      foreach ($css as $media => $types) {
        $prefix = '<style type="text/css" media="'. $media .'">'."\n";
        $imports = array();
        foreach ($types as $files) {
          foreach ($files as $file => $preprocess) {
            $imports[] = '@import "'. base_path() . $file .'";';
            if (count($imports) == 30) {
              $styles .= $prefix . implode("\n", $imports) . $suffix;
              $imports = array();
            }
          }
        }
        $styles .= (count($imports) > 0) ? ($prefix . implode("\n", $imports) . $suffix) : '';
      }
      $vars['styles'] = $styles;
    }
    else {
      $vars['styles'] = drupal_get_css(); // Use normal link technique
    }
  }    
  // Disabling system css.
  $vars['css'] = drupal_add_css('sites/default/files/uploads/cssstyle/custom.css');
  unset($vars['css']['modules/node/node.css']);
  //unset($vars['css']['modules/system/system.base.css']);
  //unset($vars['css']['modules/system/system.menus.css']);
  unset($vars['css']['modules/user/user.css']);
  unset($vars['css']['sites/all/modules/ckeditor/css/ckeditor.css']);
  unset($vars['css']['sites/all/modules/survey/css/survey.css']);//add by benny for ACMSUPP-72
  $vars['styles'] = drupal_get_css($vars['css']);  

  $scripts = drupal_add_js();
  unset($scripts['misc/jquery.js']);
  unset($scripts['sites/all/themes/asp_cm/js/curvycorners.js']);
  unset($scripts['sites/all/modules/survey/js/survey_admin.js']);//add by benny for ACMSUPP-72
  
  $vars['scripts'] = drupal_get_js('header', $scripts);
}

/*
 * theme_preprocess_page
 */
function asp_cm_preprocess_page(&$vars) {

  global $user;   //add by Benny for ACMSUPP-13 20111130
  //get hospital or ward page title.
  aspcm_set_title_custom($vars);
  //Assign $var['show_loading_icon'] value.
  $vars['show_loading_icon'] = _show_loding_icon();

  $path = drupal_get_path_alias($_GET['q']);
  //fix tab under user create page;
  
  if($path == 'admin/people/create') {
    unset($vars['help']);
    unset($vars['tabs']);
  }
  
  if(preg_match('/user\/\d*\/edit$/', $path)) {
   unset($vars['help']);   
   
   //add by Benny for ACMSUPP-13 20111130 begin
   $user_data = unserialize($user->data);
   if (!isset($user_data['verification']) && $user->uid != 0) {
		drupal_add_js('jQuery(document).ready(function () {
		$("#user-verification").show();
		title=$("#user-verification").parent().parent().children()[0].innerHTML;
		showpopup4("#user-verification", title, 530, 500);
		})','inline');
   }
   //add by Benny for ACMSUPP-13 20111130 end
   
  }
  if (arg(0) == 'node' && arg(2) == 'edit' && is_numeric(arg(1))) {
    drupal_set_title($vars['node']->title);
  }
  if (arg(0) == 'node' && arg(2) == 'delete' && is_numeric(arg(1))) {
    unset($vars['tabs']);
  }
  
  global $language;
// Remove the duplicate meta content-type tag, a bug in Drupal 6
// $vars['head'] = preg_replace('/<meta http-equiv=\"Content-Type\"[^>]*>/', '', $vars['head']);
// Remove sidebars if disabled
// https://www.drupal.org/node/423992
//  if (!$vars['show_blocks']) {
//    $vars['left'] = '';
//    $vars['right'] = '';
//  }

// Generate menu tree from source of primary links
  if (module_exists('i18nmenu')) {
    $vars['primary_links_tree']=i18nmenu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
    }
    else {
      $vars['primary_links_tree'] = drupal_render(menu_tree(variable_get('menu_main_links_source', 'main-menu')));
    }

// TNT THEME SETTINGS SECTION
// Display mission statement on all pages
  if (theme_get_setting('mission_statement_pages') == 'all') {
    $vars['mission'] = theme_get_setting('mission', false);  
  }

// Hide breadcrumb on all pages
  if (theme_get_setting('breadcrumb_display') == 0) {
    $vars['breadcrumb'] = '';  
  }

  $vars['site_name'] = t(variable_get('site_name', ''));

// Set site title, slogan, mission, page title & separator (unless using Page Title module)
  if (!module_exists('page_title')) {
    $title = $vars['site_name'];
    $slogan = t(variable_get('site_slogan', ''));
    $mission = t(variable_get('site_mission', ''));
    $page_title = t(drupal_get_title());
    $title_separator = theme_get_setting('configurable_separator');
    if (drupal_is_front_page()) {                                                // Front page title settings
      switch (theme_get_setting('front_page_title_display')) {
        case 'title_slogan':
          $vars['head_title'] = drupal_set_title($title . $title_separator . $slogan, PASS_THROUGH);
          break;
        case 'slogan_title':
          $vars['head_title'] = drupal_set_title($slogan . $title_separator . $title, PASS_THROUGH);
          break;
        case 'title_mission':
          $vars['head_title'] = drupal_set_title($title . $title_separator . $mission, PASS_THROUGH);
          break;
        case 'custom':
          if (theme_get_setting('page_title_display_custom') !== '') {
            $vars['head_title'] = drupal_set_title(t(theme_get_setting('page_title_display_custom')), PASS_THROUGH);
          }
      }
    }
    else {                                                                       // Non-front page title settings
      switch (theme_get_setting('other_page_title_display')) {
        case 'ptitle_slogan':
          $vars['head_title'] = drupal_set_title($page_title . $title_separator . $slogan, PASS_THROUGH);
          break;
        case 'ptitle_stitle':
          $vars['head_title'] = drupal_set_title($page_title . $title_separator . $title, PASS_THROUGH);
          break;
        case 'ptitle_smission':
          $vars['head_title'] = drupal_set_title($page_title . $title_separator . $mission, PASS_THROUGH);
          break;
        case 'ptitle_custom':
          if (theme_get_setting('other_page_title_display_custom') !== '') {
            $vars['head_title'] = drupal_set_title($page_title . $title_separator . t(theme_get_setting('other_page_title_display_custom')), PASS_THROUGH);
          }
          break;
        case 'custom':
          if (theme_get_setting('other_page_title_display_custom') !== '') {
            $vars['head_title'] = drupal_set_title(t(theme_get_setting('other_page_title_display_custom')), PASS_THROUGH);
          }
      }
    }
    $vars['head_title'] = strip_tags($vars['head_title']);                       // Remove any potential html tags
  }

// Set meta keywords and description (unless using Meta tags module)
  if (!module_exists('nodewords')) {
    if (theme_get_setting('meta_keywords') !== '') {
      $keywords = '<meta name="keywords" content="'. theme_get_setting('meta_keywords') .'" />';
      $vars['head'] .= $keywords ."\n";
    } 
    if (theme_get_setting('meta_description') !== '') {
      $keywords = '<meta name="description" content="'. theme_get_setting('meta_description') .'" />';
      $vars['head'] .= $keywords ."\n";
    } 
  }

  // Use the template based on current path 
  $path = drupal_get_path_alias($_GET['q']);
    
  if (preg_match('/^legalnotice\/.*?/', $path)) {
    $vars['theme_hook_suggestions'][] = 'page__legalnotice';
  }
  if ((preg_match('/^contact-us/', $path))||(preg_match('/^email-thanks/', $path))) {
    $vars['theme_hook_suggestions'][] = 'page__legalnotice';
  }
  

}


function asp_cm_preprocess_block(&$vars) {
  
  // Add regions with themed blocks (e.g., left, right) to $themed_regions array and retrieve custom theme settings
  $themedblocks = theme_get_setting('themedblocks');
  
  if ($themedblocks == '0'){ 
    $themed_regions = array('left','right');
  }
  
  if ($themedblocks == '1'){ 
    $themed_regions = array('left','right','user1','user2','user3','user4','user5','user6','user7','user8');
  }
  
  if (is_array($themed_regions)) {
    $vars['themed_block'] = (in_array($vars['block']->region, $themed_regions)) ? TRUE : FALSE;
  }
  else {
    $vars['themed_block'] = FALSE;
  }
  
}

/**
 * Generate the copyright year information 
 */
function _get_copyright_year() {
  $launch_year = variable_get('aspcm_site_launch_year', date('Y'));
  $output = $launch_year;
  
  $cur_year = date('Y');
  
  if (intval($launch_year) < intval($cur_year)) {
    $output .= ' - '. intval($cur_year);
  }
  
  return $output;
}

function asp_cm_preprocess_node(&$vars) {
// Build array of handy node classes
  $node_classes = array();
  $node_classes[] = $vars['zebra'];                                              // Node is odd or even
  $node_classes[] = (!$vars['node']->status) ? 'node-unpublished' : '';          // Node is unpublished
  $node_classes[] = ($vars['sticky']) ? 'sticky' : '';                           // Node is sticky
  $node_classes[] = (isset($vars['node']->teaser)) ? 'teaser' : 'full-node';     // Node is teaser or full-node
  $node_classes[] = 'node-type-'. $vars['node']->type;                           // Node is type-x, e.g., node-type-page

// Add any taxonomy terms for node teasers
  if ($vars['teaser'] && isset($vars['taxonomy'])) {
    foreach ($vars['taxonomy'] as $taxonomy_id_string => $term_info) {
      $taxonomy_id = array_pop(explode('_', $taxonomy_id_string));
      $node_classes[] = 'tag-'. $taxonomy_id;                                    // Node teaser has terms (tag-x)
//      $taxonomy_name = id_safe($term_info['title']);
//      if ($taxonomy_name) {
//        $node_classes[] = 'tag-'. $taxonomy_name;                              // Node teaser has terms (tag-name)
//      }
    }
  }

  $node_classes = array_filter($node_classes);                                  // Remove empty elements
  $vars['node_classes'] = implode(' ', $node_classes);                          // Implode class list with spaces

// Add node regions
  $vars['node_middle'] = block_get_blocks_by_region('node_middle');
  $vars['node_bottom'] = block_get_blocks_by_region('node_bottom');

// Render Ubercart fields into separate variables for node-product.tpl.php
if (module_exists('uc_product') && uc_product_is_product($vars) && $vars['template_files'][0] == 'node-product') {
  $node = node_build_content(node_load($vars['nid']));
  $vars['uc_image'] = drupal_render($node->content['image']);
  $vars['uc_body'] = drupal_render($node->content['body']);
  $vars['uc_display_price'] = drupal_render($node->content['display_price']);
  $vars['uc_add_to_cart'] = drupal_render($node->content['add_to_cart']);
  $vars['uc_weight'] = drupal_render($node->content['weight']);
  $vars['uc_dimensions'] = drupal_render($node->content['dimensions']);
  $vars['uc_model'] = drupal_render($node->content['model']);
  $vars['uc_list_price'] = drupal_render($node->content['list_price']);
  $vars['uc_sell_price'] = drupal_render($node->content['sell_price']);
  $vars['uc_cost'] = drupal_render($node->content['cost']);
  $vars['uc_additional'] = drupal_render($node->content);
}


// Node Theme Settings

// Date & author
  if (!module_exists('submitted_by')) {
    $date = t('') . format_date($vars['node']->created, 'medium');               // Format date as small, medium, or large
    $author = theme('username', (array) $vars['node']);
   
    $author_only_separator = t('');
    $author_date_separator = t(' &#151; ');
    $submitted_by_content_type = (theme_get_setting('submitted_by_enable_content_type') == 1) ? $vars['node']->type : 'default';
    $date_setting = (theme_get_setting('submitted_by_date_'. $submitted_by_content_type) == 1);
    $author_setting = (theme_get_setting('submitted_by_author_'. $submitted_by_content_type) == 1);
    $author_separator = ($date_setting) ? $author_date_separator : $author_only_separator;
    $date_author = ($date_setting) ? $date : '';
    $date_author .= ($author_setting) ? $author_separator . $author : '';
    $vars['submitted'] = $date_author;
  }

// Taxonomy
  $taxonomy_content_type = (theme_get_setting('taxonomy_enable_content_type') == 1) ? $vars['node']->type : 'default';
  $taxonomy_display = theme_get_setting('taxonomy_display_'. $taxonomy_content_type);
  $taxonomy_format = theme_get_setting('taxonomy_format_'. $taxonomy_content_type);
  if ((module_exists('taxonomy')) && ($taxonomy_display == 'all' || ($taxonomy_display == 'only' && $vars['page']))) {
    $vocabularies = taxonomy_get_vocabularies($vars['node']->type);
    $output = '';
    $term_delimiter = ' | ';
    foreach ($vocabularies as $vocabulary) {
      if (theme_get_setting('taxonomy_vocab_hide_'. $taxonomy_content_type .'_'. $vocabulary->vid) != 1) {
        $terms = taxonomy_node_get_terms_by_vocabulary($vars['node'], $vocabulary->vid);
        if ($terms) {
          $term_items = '';
          foreach ($terms as $term) {                        // Build vocabulary term items
            $term_link = l($term->name, taxonomy_term_path($term), array('attributes' => array('rel' => 'tag', 'title' => strip_tags($term->description))));
            $term_items .= '<li class="vocab-term">'. $term_link . $term_delimiter .'</li>';
          }
          if ($taxonomy_format == 'vocab') {                 // Add vocabulary labels if separate
            $output .= '<li class="vocab vocab-'. $vocabulary->vid .'"><span class="vocab-name">'. $vocabulary->name .':</span> <ul class="vocab-list">';
          //$output .= '<li class="vocab vocab-'. $vocabulary->vid .'"> <ul class="vocab-list">';
            $output .= substr_replace($term_items, '</li>', -(strlen($term_delimiter) + 5)) .'</ul></li>';
          }
          else {
            $output .= $term_items;
          }
        }
      }
    }
    if ($output != '') {
      $output = ($taxonomy_format == 'list') ? substr_replace($output, '</li>', -(strlen($term_delimiter) + 5)) : $output;
      $output = '<ul class="taxonomy">'. $output .'</ul>';
    }
    $vars['terms'] = $output;
  }
  else {
    $vars['terms'] = '';
  }
}


function asp_cm_preprocess_comment(&$vars) {
  global $user;
  // Build array of handy comment classes
  $comment_classes = array();
  static $comment_odd = TRUE;                                                                             // Comment is odd or even
  $comment_classes[] = $comment_odd ? 'odd' : 'even';
  $comment_odd = !$comment_odd;
  $comment_classes[] = ($vars['comment']->status == COMMENT_NOT_PUBLISHED) ? 'comment-unpublished' : '';  // Comment is unpublished
  $comment_classes[] = ($vars['comment']->new) ? 'comment-new' : '';                                      // Comment is new
  $comment_classes[] = ($vars['comment']->uid == 0) ? 'comment-by-anon' : '';                             // Comment is by anonymous user
  $comment_classes[] = ($user->uid && $vars['comment']->uid == $user->uid) ? 'comment-mine' : '';         // Comment is by current user
  $node = node_load($vars['comment']->nid);                                                               // Comment is by node author
  $vars['author_comment'] = ($vars['comment']->uid == $node->uid) ? TRUE : FALSE;
  $comment_classes[] = ($vars['author_comment']) ? 'comment-by-author' : '';
  $comment_classes = array_filter($comment_classes);                                                      // Remove empty elements
  $vars['comment_classes'] = implode(' ', $comment_classes);                                              // Create class list separated by spaces
  // Date & author
  $submitted_by = t('') .'<span class="comment-name">'.  theme('username', $vars['comment']) .'</span>';
  $submitted_by .= t(' - ') .'<span class="comment-date">'.  format_date($vars['comment']->timestamp, 'small') .'</span>';  // Format date as small, medium, or large
  $vars['submitted'] = $submitted_by;
}


/**
 * Set defaults for comments display
 * (Requires comment-wrapper.tpl.php file in theme directory)
 */
function asp_cm_preprocess_comment_wrapper(&$vars) {
  $vars['display_mode']  = COMMENT_MODE_FLAT_EXPANDED;
  $vars['display_order'] = COMMENT_ORDER_OLDEST_FIRST;
  $vars['comment_controls_state'] = COMMENT_CONTROLS_HIDDEN;
}


/**
 * Adds a class for the style of view  
 * (e.g., node, teaser, list, table, etc.)
 * (Requires views-view.tpl.php file in theme directory)
 */
function asp_cm_preprocess_views_view(&$vars) {
  $vars['css_name'] = $vars['css_name'] .' view-style-'. drupal_clean_css_identifier(strtolower($vars['view']->type), $filter = array('_' => '-'));
}


/**
 * Modify search results based on theme settings
 */
function asp_cm_preprocess_search_result(&$variables) {
  static $search_zebra = 'even';
  $search_zebra = ($search_zebra == 'even') ? 'odd' : 'even';
  $variables['search_zebra'] = $search_zebra;
  
  $result = $variables['result'];
  $variables['url'] = check_url($result['link']);
  $variables['title'] = check_plain($result['title']);

  // Check for existence. User search does not include snippets.
  $variables['snippet'] = '';
  if (isset($result['snippet']) && theme_get_setting('search_snippet')) {
    $variables['snippet'] = $result['snippet'];
  }
  
  $info = array();
  if (!empty($result['type']) && theme_get_setting('search_info_type')) {
    $info['type'] = check_plain($result['type']);
  }
  if (!empty($result['user']) && theme_get_setting('search_info_user')) {
    $info['user'] = $result['user'];
  }
  if (!empty($result['date']) && theme_get_setting('search_info_date')) {
    $info['date'] = format_date($result['date'], 'small');
  }
  if (isset($result['extra']) && is_array($result['extra'])) {
    // $info = array_merge($info, $result['extra']);  Drupal bug?  [extra] array not keyed with 'comment' & 'upload'
    if (!empty($result['extra'][0]) && theme_get_setting('search_info_comment')) {
      $info['comment'] = $result['extra'][0];
    }
    if (!empty($result['extra'][1]) && theme_get_setting('search_info_upload')) {
      $info['upload'] = $result['extra'][1];
    }
  }

  // Provide separated and grouped meta information.
  $variables['info_split'] = $info;
  $variables['info'] = implode(' - ', $info);

  // Provide alternate search result template.
  $variables['template_files'][] = 'search-result-'. $variables['type'];
}


/**
 * Hide or show username '(not verified)' text
 */
function asp_cm_username($object) {
  if ((!$object['uid']) && $object['name']) {
    $output = (!empty($object['homepage'])) ? l($object['name'], $object['homepage'], array('attributes' => array('rel' => 'nofollow'))) : check_plain($object['name']);
    $output .= (theme_get_setting('user_notverified_display') == 1) ? ' ('. t('not verified') .')' : '';

  }
  else {
    if(user_access('access user profiles') && $object['uid'] && $object['name']) {
      $output = theme_username(
        array(
          'name' => $object['name'], 
          'link_path' => 'user/' . $object['uid'], 
          'link_options' => array('attributes' => array('title' => t('View user profile.'))),
          'extra' => ''
          )
      );
    }
    else {
      $output = check_plain($object['name']);;
    }
  }

  return $output;
}


/**
 * User links
 */
function asp_cm_login(){
$loginlinks = theme_get_setting('loginlinks');
  if ($loginlinks == '1'){ 
    global $user;
    $login = url(drupal_get_path_alias('user'));
    $register = url(drupal_get_path_alias('user/register'));
    $usr_path = 'user/'.$user->uid;
    $myAccount = drupal_get_path_alias($usr_path);
    $logOut = drupal_get_path_alias('user/logout');
    if(!$user->uid) {
      $output = "<a href=\"$login\">" . t('Log in') . '</a>' . " | <a href=\"$register\">" . t('Register') . "</a>";
    }
    if($user->uid) {
      /*$output = theme('item_list', array(
      l(t('My account |'), $myAccount, array('title' => t('My account'))),
      l(t(' Log out'), $logOut)));*/
	  $output =  'Hi, Username' . "  | " . "<a href=\"$logOut\">" . t('Log out') . '</a>';
    }
    return $output;
  }
}


/**
 * Set default form file input size 
 */
function asp_cm_file($variables) {
  $variables['element']['#size'] = ($variables['element']['#size'] > 40) ? 40 : $variables['element']['#size'];
  return theme_file($variables);
}


/**
 * Count the total number of CSS files in $vars['css']
 */
function asp_cm_css_count($array) {
  $count = 0;
  foreach ($array as $item) {
    $count = (is_array($item)) ? $count + asp_cm_css_count($item) : $count + 1;
  }
  return $count;
}


/**
 * Creates a link with prefix and suffix text
 *
 * @param $prefix
 *   The text to prefix the link.
 * @param $suffix
 *   The text to suffix the link.
 * @param $text
 *   The text to be enclosed with the anchor tag.
 * @param $path
 *   The Drupal path being linked to, such as "admin/content/node". Can be an external
 *   or internal URL.
 *     - If you provide the full URL, it will be considered an
 *   external URL.
 *     - If you provide only the path (e.g. "admin/content/node"), it is considered an
 *   internal link. In this case, it must be a system URL as the url() function
 *   will generate the alias.
 * @param $options
 *   An associative array that contains the following other arrays and values
 *     @param $attributes
 *       An associative array of HTML attributes to apply to the anchor tag.
 *     @param $query
 *       A query string to append to the link.
 *     @param $fragment
 *       A fragment identifier (named anchor) to append to the link.
 *     @param $absolute
 *       Whether to force the output to be an absolute link (beginning with http:).
 *       Useful for links that will be displayed outside the site, such as in an RSS
 *       feed.
 *     @param $html
 *       Whether the title is HTML or not (plain text)
 * @return
 *   an HTML string containing a link to the given path.
 */
function asp_cm_themesettings_link($prefix, $suffix, $text, $path, $options) {
  return $prefix . (($text) ? l($text, $path, $options) : '') . $suffix;
}


/**
 * Breadcrumb override
 */
function asp_cm_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $breadcrumb[0] = l(t('ホーム'), NULL);
  //get current URL
  $current_url = drupal_get_path_alias($_GET['q']);
  $current_url_arr = explode("/", $current_url);
  $url_num = count($current_url_arr);
  if ($url_num >= 2) {
    $current_url_arg = $current_url_arr[$url_num-2];
  }
  $num = count($breadcrumb);
  if(count($breadcrumb) > 1) {
    for ($i = 1; $i<$num; $i++) {
      array_pop($breadcrumb);
    }
  }

  if (!empty($breadcrumb)) {
    /*@author_lxl_start lxl_breadcrumb*/
    if($current_url=='ward-monthly-data-input'){
      $breadcrumb[] = '<a href="/ward-data/ward-annually-data-view" title="データ入力">データ入力</a>';
    }
    if($current_url=='node/add/aspcm-hospital'){
      $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報管理">病院情報管理</a>';
      $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報を管理する">病院情報を管理する</a>';
    }
    if($current_url=='node/add/aspcm-ward'){
      $breadcrumb[] = '<a href="/hospital-mgmt/ward" title="病院情報管理">病院情報管理</a>';
      $breadcrumb[] = '<a href="/hospital-mgmt/ward" title="病棟情報を管理する">病棟情報を管理する</a>';
    }
    if($current_url=='user/create'){
      $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報管理">病院情報管理</a>';
      $breadcrumb[] = '<a href="/hospital-mgmt/asp-user-management" title="ユーザー情報を管理する">ユーザー情報を管理する</a>';
    }
    if (preg_match('/^user.*.edit.*?/', $current_url)) {
      $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報管理">病院情報管理</a>';
      $breadcrumb[] = '<a href="/hospital-mgmt/asp-user-management" title="ユーザー情報を管理する">ユーザー情報を管理する</a>';
    }
    if($current_url=='node/add/aspcm-severity'){
      $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
      $breadcrumb[] = '<a href="/masters-mgmt/severity" title="重症度レベル">重症度レベル</a>';
    }
    if($current_url=='node/add/aspcm-product'){
      $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
      $breadcrumb[] = '<a href="/masters-mgmt/product" title="製品リスト">製品リスト</a>';
    }
    if($current_url=='node/add/aspcm-clinical-department'){
      $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
      $breadcrumb[] = '<a href="/masters-mgmt/severity-name" title="重症度名リスト">重症度名リスト</a>';
    }
    if($current_url=='node/add/aspcm-department'){
      $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
      $breadcrumb[] = '<a href="/masters-mgmt/department" title="重症度別手指衛生リスト">重症度別手指衛生リスト</a>';
    }
    if($current_url=='node/add/aspcm-government-department'){
      $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
      $breadcrumb[] = '<a href="/masters-mgmt/government" title="診療科リスト">診療科リスト</a>';
    }
    if($current_url=='node/add/aspcm-disease'){
      $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
      $breadcrumb[] = '<a href="/masters-mgmt/disease" title="病原体リスト">病原体リスト</a>';
    }
    /*@author_lxl_end lxl_breadcrumb*/
    //fix breadcrumb under "/hospital-mgmt/asp-user-management"
    if (preg_match('/^users.*?/', $current_url)) {
      $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報管理">病院情報管理</a>';
      $breadcrumb[] = '<a href="/hospital-mgmt/asp-user-management" title="ユーザー情報を管理する">ユーザー情報を管理する</a>';
    }
    //fix breadcrumb under "/ward-data" 
    else if (preg_match('/^ward-data.*?/', $current_url)) {
      $breadcrumb[] = '<a href="/ward-data/ward-annually-data-view" title="データ入力">データ入力</a>';
    }
    //fix breadcrumb under "/graph" 
    else if (preg_match('/^graph.*?/', $current_url)) {
      $breadcrumb[] = '<a href="/graph/single-graph" title="グラフ">グラフ</a>';
    }
    //fix breadcrumb under "/severity_level" 
    else if (preg_match('/^severity_level.*?/', $current_url) || preg_match('/^severity_level_normal.*?/', $current_url)) {
      $breadcrumb[] = '<a href="/severity_level_normal" title="重症度レベル判定">重症度レベル判定</a>';
    }
    //fix breadcrumb under "/news" 
    else if (preg_match('/^news.*?/', $current_url)) {
      for ($i = 1; $i<$url_num; $i++ ) {
        if ($i == 1){
          $breadcrumb[] = '<a href="/news" title="新着情報">新着情報</a>';
        }
      }
    }
    //fix breadcrumb under "/help" 
    else if (preg_match('/^help\/(admin\/|super\/|sales\/|others\/).*?/', $current_url)) {
      for ($i = 1; $i<$url_num; $i++ ) {
        if ($i == 1){
          $breadcrumb[] = '<a href="/help/top" title="へルプ">へルプ</a>';
        }
      }
    }
   
    //fix breadcrumb under "/hospital-mgmt\/hospital" and "/hospital-mgmt\/ward"
    else if (preg_match('/^hospital-mgmt\.*?/', $current_url)) {
      for ($i = 1; $i<$url_num; $i++ ) {
        if ($i == 1){
          $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報管理">病院情報管理</a>';
        }
        else if ($i == 2){
          if ($current_url_arg == 'hospital') {
            $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報を管理する">病院情報を管理する</a>';
          }
          else if ($url_num > 3);
          else {
            $breadcrumb[] = '<a href="/hospital-mgmt/ward" title="病棟情報を管理する">病棟情報を管理する</a>';
          }
        }
      }
    }
    //fix breadcrumb under "/masters-mgmt"
    else if (preg_match('/^masters-mgmt.*?/', $current_url)) {
      for ($i = 1; $i<$url_num; $i++ ) {
        if ($i == 1){
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
        }
        else if ($i == 2){
          if ($current_url_arg == 'product') {
            $breadcrumb[] = '<a href="/masters-mgmt/product" title="製品リスト">製品リスト</a>';
          }
          else if($current_url_arg == 'severity'){
            $breadcrumb[] = '<a href="/masters-mgmt/severity" title="重症度レベル">重症度レベル</a>';
          }
          else if($current_url_arg == 'disease'){
            $breadcrumb[] = '<a href="/masters-mgmt/disease" title="病原体リスト">病原体リスト</a>';
          }
          else if($current_url_arg == 'government'){
            $breadcrumb[] = '<a href="/masters-mgmt/government" title="診療科リスト">診療科リスト</a>';
          }
          else if($current_url_arg == 'department'){
            $breadcrumb[] = '<a href="/masters-mgmt/department" title="重症度別手指衛生リスト">重症度別手指衛生リスト</a>';
          }
          else if($current_url_arg == 'severity-name'){
            $breadcrumb[] = '<a href="/masters-mgmt/severity-name" title="重症度名リスト">重症度名リスト</a>';
          }
        }
      }
    }
    /*@author  lxl_start*/
    else if (preg_match('/^node.*?/', $current_url)) {
      $n = node_load(arg(1));
      if(!empty($n) && is_object($n)){
        $n_type = $n->type;
        if($n_type == 'aspcm_hospital'){
          $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報管理">病院情報管理</a>';
          $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報を管理する">病院情報を管理する</a>';
        }else if($n_type == 'aspcm_ward'){
          $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報管理">病院情報管理</a>';
          $breadcrumb[] = '<a href="/hospital-mgmt/ward" title="病棟情報を管理する">病棟情報を管理する</a>';
        }else if($n_type == 'aspcm_product'){
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="製品リスト">製品リスト</a>';
        }else if($n_type == 'aspcm_severity'){
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
          $breadcrumb[] = '<a href="/masters-mgmt/severity" title="重症度レベル">重症度レベル</a>';
        }else if($n_type == 'aspcm_clinical_department'){
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
          $breadcrumb[] = '<a href="/masters-mgmt/severity-name" title="重症度名リスト">重症度名リスト</a>';
        }else if($n_type == 'aspcm_department'){
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
          $breadcrumb[] = '<a href="/masters-mgmt/department" title="重症度別手指衛生リスト">重症度別手指衛生リスト</a>';
        }else if($n_type == 'aspcm_government_department'){
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
          $breadcrumb[] = '<a href="/masters-mgmt/government" title="診療科リスト">診療科リスト</a>';
        }else if($n_type == 'aspcm_disease'){
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
          $breadcrumb[] = '<a href="/masters-mgmt/disease" title="病原体リスト">病原体リスト</a>';
        }
//lxl_start
        else if($n_type == 'aspcm_area'){
          $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報管理">病院情報管理</a>';
          $breadcrumb[] = '<a href="/hospital-mgmt/hospital" title="病院情報を管理する">病院情報を管理する</a>';
        }else if($n_type == 'aspcm_product_category'){
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="マスターデータ">マスターデータ</a>';
          $breadcrumb[] = '<a href="/masters-mgmt/product" title="製品リスト">製品リスト</a>';
        }
//lxl_end        
      }
      
    }

    /*@author  lxl_end*/
    $breadcrumb[] = drupal_get_title();
    if (preg_match('/^home.*?/', $current_url) || preg_match('/^default.*?/', $current_url)) {
      return ;
    }
    return '<div class="breadcrumb">'. implode(' &raquo; ', $breadcrumb) .'</div>';
  }
}


// Quick fix for the validation error: 'ID "edit-submit" already defined'
/*$elementCountForHack = 0;

function asp_cm_submit($variables) {
  global $elementCountForHack;
  return str_replace('edit-submit', 'edit-submit-' . ++$elementCountForHack, theme('button', $variables['element']));
}*/

/**
 * Returns HTML for a button form element.
 */
function asp_cm_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}


/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'id'.
 * - Replaces any character except A-Z, numbers, and underscores with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 *   The string
 * @return
 *   The converted string
 */
function id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id' . $string;
  }
  return $string;
}


// retrieve additional custom theme settings
$preload = theme_get_setting('cssPreload'); // print the js file if css image preload enabled
  if ($preload == '1'){
    drupal_add_js(drupal_get_path('theme','asp_cm').'/js/preloadCssImages.jQuery_v5.js'); // load the javascript
    drupal_add_js('jQuery(document).ready(function(){
    jQuery.preloadCssImages();
  });
  ','inline');
}

$dropdownjs = theme_get_setting('menutype'); // if we choose dropdown
if($dropdownjs == '1'){ 
  drupal_add_js(drupal_get_path('theme','asp_cm').'/js/jquery.hoverIntent.minified.js'); // load the javascript
	//drupal_add_js(drupal_get_path('theme','asp_cm').'/js/dropdown.js'); // load the javascript	
}
 //drupal_add_js(drupal_get_path('theme','asp_cm').'/js/jquery.fixpng.js'); // load the javascript

function menupos() {
  $navpos = theme_get_setting('navpos'); // Primary links position 
    if ($navpos == '0'){ 
      return 'navleft';
  }
    if ($navpos == '1'){ 
      return 'navcenter';
  }
    if ($navpos == '2'){ 
      return 'navright';
  }
}

/**
 * theme_form_element($variables)
 */
function asp_cm_form_element($variables) {
  $element = &$variables['element'];
  //show '*' after some labels
  if($element['#name'] == 'asp_hospital_name_visiable' || $element['#name'] == 'asp_hospital_name_visiable_noajax' || $element['#name'] == 'asp_ward_name_visiable') {
    $element['#required'] = TRUE;
  }
  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  if(isset($element['#no_ajax'])) {
    $attributes['class'] = array('form-item hospital-name-only');
  }
  else {
    $attributes['class'] = array('form-item');
  }
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  // Add element #id for ward.
  if ($element['#name'] == 'ward' && strpos($element['#id'], 'edit-ward') !== FALSE) {
    $attributes['id'] = 'edit-ward-wrapper';
  }
  elseif ($element['#name'] == 'severity_level' && $element['#id'] == 'severity_level') {
    $attributes['id'] = 'severity_level-wrapper';
  }
  elseif ($element['#name'] == 'files[import]' && $element['#id'] == 'edit-import') {
    $attributes['id'] = 'edit-import-wrapper';
  }
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description']) && $element['#name']!='field_aspcm_ref_general_disease[und]') {
    $output .= ' <div class="description">'. $element['#description'] ."</div>\n";
  }
  elseif($element['#name']=='field_aspcm_ref_general_disease[und]') {
  $output .= ' <div class="description table-header-desease">'. $element['#description'] ."</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}
/**
 * theme_form_element_label
 */
function asp_cm_form_element_label($variables) {
  $add_class = '';
  $element = $variables['element'];
  
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }
  
  if($_GET['q'] == 'admin/people/create' || $_GET['q'] == 'user/create') {
      
    switch ($element['#title']) {
        case 'ALJ Admin':
        $add_class = ' class-admin';
        break;
        case 'ALJ Sales':
        $add_class = ' class-sales';
        break;
        case '病棟担当者':
        $add_class = ' class-ward';
        break;
        case '管理担当者':
        $add_class = ' class-hospital';
        break;
   }
   
  }
  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option'. $add_class;
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}
/*
 * thme_radio
 */
function asp_cm_radio($variables) {
  
  $element = $variables['element'];
  $element['#attributes']['type'] = 'radio';
  element_set_attributes($element, array('id', 'name','#return_value' => 'value'));

  if (isset($element['#return_value']) && $element['#value'] !== FALSE && $element['#value'] == $element['#return_value']) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-radio'));

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

function _asp_cm_user_profile($content) {
  $output = $content;
  $invalidate = array(
    t('You are not authorized to access this page.')
  );
  
  if (arg(0) == 'user' && !in_array($content, $invalidate)) {
    
    if (arg(2) != '' && in_array(arg(2), array('edit', 'cancel', 'imce'))) {
      return $output;
    }
    
    if (is_numeric(arg(1))) {
      $user_data = user_load(arg(1));
      if ($user_data) {
        $output = '<div class="content"> <div class="content-manual"> <div class="field"><div class="field-label">ユーザーID：</div><div class="field-items">' . $user_data->name . '</div></div>';
        $output .= '<div class="field"><div class="field-label">メールアドレス：</div><div class="field-items">' . $user_data->mail . '</div></div>';
        $output .= '<div class="field"><div class="field-label">お名前：</div><div class="field-items">' . $user_data->asp_user_japanese_name . '</div></div>';
        $output .= '<div class="field"><div class="field-label">フリガナ：</div><div class="field-items">' . $user_data->asp_designation . '</div></div>';
        $output .= '<div class="field"><div class="field-label">所属：</div><div class="field-items">' . $user_data->asp_division . '</div></div>';
        /*@author_lxl_start add_user_creater*/
        $output .= '<div class="field"><div class="field-label">有効：</div><div class="field-items">' . ($user_data->status==1?'有効':'無効') . '</div></div>';
        $output .= '<div class="field"><div class="field-label">作成者：</div><div class="field-items">' . get_user_creater(arg(1)) . '</div></div>';
        /*@author_lxl_end*/
        if (in_array(ASPCM_SUPER_USER, $user_data->roles)){
          $hospital = get_hospital_name($user_data->asp_hospital_id);
          $output .= '<div class="field"><div class="field-label">病院名：</div><div class="field-items">' . $hospital . '</div></div>';
        }
        elseif (in_array(ASPCM_REGULAR_USER, $user_data->roles)){
          $wardname = get_ward_by_wid($user_data->asp_ward_id);
          $hospital = get_hospital_name($user_data->asp_hospital_id);
          $output .= '<div class="field"><div class="field-label">病院名：</div><div class="field-items">' . $hospital . '</div></div>';
          $output .= '<div class="field"><div class="field-label">病棟名：</div><div class="field-items">' . $wardname[$user_data->asp_ward_id] . '</div></div>';
        }
        
        $output .= '<div class="clear"></div>  </div></div>';
        //add go back link
        global $user;
        if(in_array(ASPCM_ADMIN, $user->roles) || in_array(ASPCM_SUPER_USER, $user->roles)) {
          $output .= '<div class="block-goback"><a class="link-back" href="/hospital-mgmt/asp-user-management">ユーザー一覧画面に戻る</a></div>';
        }
      }
    }
  }
  
  return $output;
}
/**
 * theme_menu_link
 * @param array $variables
 * @return type
 */
function asp_cm_menu_link(array $variables) {
  $element = $variables['element'];

  //Add class='loading-link' to the hospital,ward and user management menu link
  if ($element['#href'] == 'hospital-mgmt/hospital') {
    $element['#attributes']['class'][] = 'loading-link';
  }
  elseif ($element['#href'] == 'hospital-mgmt/ward') {
    $element['#attributes']['class'][] = 'loading-link';
  }
  elseif ($element['#href'] == 'hospital-mgmt/asp-user-management') {
    $element['#attributes']['class'][] = 'loading-link';
  }

  $current_url_arr = explode("/", $_GET['q']);
  
  //If user go to master view,edit or delete page,highlight the top menu.
  if(arg(0) == 'node') {
    //Get current operational node.
    $nid = $current_url_arr[1];
    $node = node_load($nid, NULL, TRUE);
    //If current node is aspcm_hospital or aspcm_ward etc, add a class to the menu link.
    if (in_array($node->type, array('aspcm_hospital', 'aspcm_ward'))) {
      if (preg_match("/hospital-mgmt\/top/i", $element['#href'])) {
        $element['#attributes']['class'][] = 'active-trail';
      }
    }
    if (in_array($node->type,array('aspcm_product', 'aspcm_severity', 'aspcm_clinical_department', 
                'aspcm_department', 'aspcm_government_department', 'aspcm_disease',))) {
      if (preg_match("/masters-mgmt\/top/i", $element['#href'])) {
        $element['#attributes']['class'][] = 'active-trail';
      }
    }
  }
  //If user go to user profile view ,edit or delete page,highlight the top menu.
  elseif (arg(0) == 'user') {
    if (preg_match("/hospital-mgmt\/top/i", $element['#href'])) {
      $element['#attributes']['class'][] = 'active-trail';
    }
  }
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


/**
 * See if the page need the loading pacture.
 *
 */
function _show_loding_icon() {
  $current_url_arr = explode("/", $_GET['q']);
  if (in_array($_GET["q"], array(
    'ward-monthly-data-input', 
    'ward-data/ward-annually-data-view', 
    'graph/single-graph', 
    'graph/comparison-graph',
    'hospital-mgmt/hospital', 
    'hospital-mgmt/ward'
    ))) {
    return TRUE;
  }
  elseif($current_url_arr[0] == 'hospital-mgmt' && ($current_url_arr[1] == 'hospital' || $current_url_arr[1] == 'ward')) return TRUE;
  else return FALSE;
}

/*
 * Implement of theme_select()
 * Shift the _none option
 */
function asp_cm_select($variables) {
  $element = $variables['element'];
  if ($element['#required'] == TRUE && isset($element['#options']['_none'])) {
    unset($element['#options']['_none']);
  }
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-select'));

  return '<select' . drupal_attributes($element['#attributes']) . '>' . form_select_options($element) . '</select>';
}

/*
set hospital and ward title when validate failed.
*/
function aspcm_set_title_custom(&$vars) {
  if (isset($vars['node']->type) && empty($vars['node']->title) && ($vars['node']->type == 'aspcm_ward' || $vars['node']->type == 'aspcm_hospital')) {
    $nid = $vars['node']->nid;
    $node_cache_key = "aspcm-node-view-". $nid;
    //do we need to get data from cache first?  aspcm-node-view-19111
    //$cache = cache_get($node_cache_key); $node_cache = $cache->data;
    if ($cache = cache_get($node_cache_key)) {
      $data = $cache->data;
       $vars['node']->title = $data->title;
    }
    else {
      $safe_storage = get_storage_instance($vars['node']->type);
      $data = $safe_storage->load_data($nid);
      $vars['node']->title = $data['title'];
    }
  }
}

/**
 * template_preprocess_maintenance_page
 */
function asp_cm_preprocess_maintenance_page(&$vars) {
  // Add favicon
  if (theme_get_setting('toggle_favicon')) {
    $favicon = theme_get_setting('favicon');
    $type = theme_get_setting('favicon_mimetype');
    drupal_add_html_head_link(array('rel' => 'shortcut icon', 'href' => drupal_strip_dangerous_protocols($favicon), 'type' => $type));
  }

  global $theme;
  // Retrieve the theme data to list all available regions.
  $theme_data = list_themes();
  $regions = $theme_data[$theme]->info['regions'];
  // Get all region content set with drupal_add_region_content().
  foreach (array_keys($regions) as $region) {
    // Assign region to a region variable.
    $region_content = drupal_get_region_content($region);
    isset($vars[$region]) ? $vars[$region] .= $region_content : $vars[$region] = $region_content;
  }

  // Setup layout variable.
  $vars['layout'] = 'none';
  if (!empty($vars['sidebar_first'])) {
    $vars['layout'] = 'first';
  }
  if (!empty($vars['sidebar_second'])) {
    $vars['layout'] = ($vars['layout'] == 'first') ? 'both' : 'second';
  }

  // Construct page title
  if (drupal_get_title()) {
    $head_title = array(
      'title' => strip_tags(drupal_get_title()),
      'name' => variable_get('site_name', 'Drupal'),
    );
  }
  else {
    $head_title = array('name' => variable_get('site_name', 'Drupal'));
    if (variable_get('site_slogan', '')) {
      $head_title['slogan'] = variable_get('site_slogan', '');
    }
  }

  // set the default language if necessary
  $language = isset($GLOBALS['language']) ? $GLOBALS['language'] : language_default();

  $vars['head_title_array'] = $head_title;
  $vars['head_title'] = implode(' | ', $head_title);
  $vars['base_path'] = base_path();
  $vars['front_page'] = url();
  $vars['breadcrumb'] = '';
  $vars['feed_icons'] = '';
  $vars['help'] = '';
  $vars['language'] = $language;
  $vars['language']->dir = $language->direction ? 'rtl' : 'ltr';
  $vars['logo'] = theme_get_setting('logo');
  $vars['messages'] = $vars['show_messages'] ? theme('status_messages') : '';
  $vars['main_menu'] = array();
  $vars['secondary_menu'] = array();
  $vars['site_name'] = (theme_get_setting('toggle_name') ? variable_get('site_name', 'Drupal') : '');
  $vars['site_slogan'] = (theme_get_setting('toggle_slogan') ? variable_get('site_slogan', '') : '');
  $vars['tabs'] = '';
  $vars['title'] = drupal_get_title();
  $site_footer = variable_get('footer_message_msg', 
    array('value' => 'This is default site footer content.'));
  $format = isset($site_footer['format']) ? $site_footer['format'] : NULL;
  $vars['footer_message'] = check_markup($site_footer['value'], $format);;
  $vars['classes_array'] = array();
  // Compile a list of classes that are going to be applied to the body element.
  $vars['classes_array'][] = 'in-maintenance';
  // Build array of additional body classes and retrieve custom theme settings
  $layoutwidth = theme_get_setting('layout-width');
  if ($layoutwidth == '0'){ 
    $vars['classes_array'][] = 'layout-jello';
  }
  if ($layoutwidth == '1'){ 
    $vars['classes_array'][] = 'layout-fluid';
  }
  if ($layoutwidth == '2'){ 
    $vars['classes_array'][] = 'layout-fixed';
  }
  if (isset($vars['db_is_active']) && !$vars['db_is_active']) {
    $vars['classes_array'][] = 'db-offline';
  }
  if ($vars['layout'] == 'both') {
    $vars['classes_array'][] = 'two-sidebars';
  }
  elseif ($vars['layout'] == 'none') {
    $vars['classes_array'][] = 'no-sidebars';
  }
  else {
    $vars['classes_array'][] = 'one-sidebar sidebar-' . $vars['layout'];
  }

  // Dead databases will show error messages so supplying this template will
  // allow themers to override the page and the content completely.
  if (isset($vars['db_is_active']) && !$vars['db_is_active']) {
    $vars['theme_hook_suggestion'] = 'maintenance_page__offline';
  }
}