<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<table class="<?php print $classes; ?>">
  <?php if (!empty($title)) : ?>
    <caption><?php print $title; ?></caption>
  <?php endif; ?>
  <thead>
    <tr>
      <?php foreach ($header as $field => $label): ?>
        <th class="views-field views-field-<?php print $fields[$field]; ?>">
          <?php print $label; ?>
        </th>
      <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($rows as $count => $row): ?>
      <tr class="<?php print ($count % 2 == 0) ? 'even' : 'odd';?>">
        <?php foreach ($row as $field => $content): ?>

          <td class="views-field views-field-<?php 
          if($fields[$field] == 'name-1')	{
                  print 'view-node';
          }
          else {
                  print $fields[$field];
          }?>">
          <?php if($fields[$field] == 'uid'):?> 	  
          <?php $email = user_load($content)->profile_local_email;
                 print $email; ?>
          <?php endif;?>
          <?php if($fields[$field] == 'uid-1'):?>
            <?php
                $hospital_id = user_load($content)->asp_hospital_id;
                $hospital_name = get_hospital_name($hospital_id);
                print $hospital_name;
            ?>
          <?php endif;?>
          <?php if($fields[$field] == 'name-1') : ?>
          <?php 
            $path = drupal_get_path_alias($_GET['q']);
            $content_str = preg_match('/href="(.*?)"/', $content, $content_str_arr); 
          ?>
          <?php $content = '<a href="' . $content_str_arr[1] .'?destination='. $path .'" title="View user profile.">表示</a>'; ?>
          <?php endif;?>
          <?php if($fields[$field] == 'rid'): ?>
            <?php $content = str_replace(array('Regular User', 'Super User'), array('病棟担当者', '管理担当者'), $content); ?>
          <?php endif;?>
          <?php if($fields[$field] != 'uid' && $fields[$field] != 'uid-1') : ?>
            <?php print $content; ?>
          <?php endif; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
