<!-- node --> 
<div id="node-<?php print $node->nid; ?>" class="node <?php print $node_classes; ?>">
  <?php if ($submitted): ?>
    <?php print $picture ?>
  <?php endif;?>
  
  <?php if ($page == 0): ?>
  <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>

  <?php if ($terms): ?>
  <div class="terms">
    <?php print $terms; ?>
  </div>
  <?php endif;?>

  <?php if ($node_middle && !$teaser): ?>
  <div id="node-middle">
    <?php print $node_middle; ?>
  </div>
  <?php endif; ?>

  <div class="content">
    <div class="content-manual">
      
      <?php  
        hide($content['comments']);
        hide($content['links']);
        if (isset($content['body'])) {
          print $content['body'][0]['#markup'];
          hide($content['body']);
        }
        print render($content); 
      ?>
      <div class="clear"></div>
    </div>
  </div>
  <?php if($node->type =='aspcm_news') {?> <div align="right"> <?php print "作成日付:".date("Y-m-d H:i", $created); ?></div> <?php }?>
  <?php if (!empty($content['links'])): ?>
  <div class="links">
    <?php print render($content['links']); ?>
  </div>
  <?php endif; ?>

  <?php if ($node_bottom && !$teaser): ?>
  <div id="node-bottom">
    <?php print $node_bottom; ?>
  </div>
  <?php endif; ?>
</div>
<!-- /node-<?php print $node->nid; ?> --> 

<?php 
  /**
   *  Custom code define for retuen master page
   */
 $type = $node->type;
 $flag = 0;
 switch ($type) {
  case 'aspcm_clinical_department':
    $url = 'masters-mgmt/severity-name';
    break;
  case 'aspcm_product':
    $url = 'masters-mgmt/product';
    break;
  case 'aspcm_severity':
    $url = 'masters-mgmt/severity';
    break;
  case 'aspcm_disease':
    $url = 'masters-mgmt/disease';
    break;
  case 'aspcm_government_department':
    $url = 'masters-mgmt/government';
    break;
  case 'aspcm_department':
    $url = 'masters-mgmt/department';
    break;
  case 'aspcm_hospital':
    $url = 'hospital-mgmt/hospital';
    break;
  case 'aspcm_ward':
    $url = 'hospital-mgmt/ward';
    break;
  case 'severity_treatment':
   $url = 'severity_admin/severity_mt';
   break;
  case 'severity_condition':
    $url = 'severity_admin/severity_cond';
    break;
  case 'severity_nurse_treatment':
    $url = 'severity_admin/nurse_mt';
    break;
  case 'severity_nurse_condition':
    $url = 'severity_admin/nurse_cond';
    break;
  case 'aspcm_news':
    $url = $_SERVER['HTTP_REFERER'];
    $current = $_SERVER["REQUEST_URI"];
    $urlarray = explode('/',$url);
    if(count($urlarray) == 5) {
      $urlstring ='/'.$urlarray[3].'/'.$urlarray[4];
    }
    if(count($urlarray) == 6) {
    $editdele = $urlarray[5];
    $add = $urlarray[4];
    if($editdele == 'edit' || $add == 'add' || $editdele == 'delete') {
      $url = base_path() . 'news';
    } 
    }
    if($current == $urlstring) {
       $url = base_path() . 'news';
    }
    $flag = 1;
    break;
  default:
    break;
 }
 //when click preview $node->op =='Preview'
if(isset($url) && empty($node->op)){
  if($flag == 1) {
    print '<div><a class="lnk-empty-butt" href ="' . $url . '" title="戻る">戻る</a></div>';
  }
  else {
    $url = base_path() . $url;
    print '<div><a class="lnk-empty-butt" href ="' . $url . '" title="戻る">戻る</a></div>';
  }
}
?>



