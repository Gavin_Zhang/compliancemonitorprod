<?php
/**
 * @file views-view.tpl.php
 * Main view template
 *
 * Variables available:
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 * - $admin_links: A rendered list of administrative links
 * - $admin_links_raw: A list of administrative links suitable for theme('links')
 *
 * @ingroup views_templates
 */
?>
<div class="view view-<?php print $css_name; ?> view-id-<?php print $name; ?> view-display-id-<?php print $display_id; ?>">
<?php if ($name == 'aspcm_ward'): ?>
<div  class="info-mess">
  <?php print t('新しく病棟情報を登録するには、新規追加ボタンをクリックしてください。
				病棟情報を編集する場合には、編集ボタンをクリックしてください。'); ?>
</div>
<?php endif; ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>
  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>
  <?php if ($attachment_before): ?>
    <div class="attachment-before">
      <?php print $attachment_before;?>
    </div>
  <?php endif; ?>
  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>
  <?php
  /**
   *  Custom code define for add button
   */
  $hide_news_add = 'FALSE';
  $user_obj = get_user_profile();
  $flag = '';
  $url = '';
  $user_flag = '';
  
  if (empty($user_obj->asp_hospital_id)) { //jnj
    $hide_hos_add = 'FALSE';
    $hide_ward_add = 'FALSE';
    if(!user_access('create aspcm_news content')) { //jnjsales
      $hide_news_add = 'TRUE';
    }
  }
  elseif (empty($user_obj->asp_ward_id)) {//super
    $hide_hos_add = 'TRUE';
    $hide_ward_add = 'FALSE';
    $hide_news_add = 'TRUE';
  }
  else { //reguar
    $hide_hos_add = 'TRUE';
    $hide_ward_add = 'TRUE';
    $hide_news_add = 'TRUE';
  }
  switch ($name)
  {
    case 'aspcm_government_department':
      $url = "aspcm-government-department";
      $flag = '1';
      break;
    case 'aspcm_department':
      $url = "aspcm-department";
      $flag = '1';
      break;
    case 'aspcm_hospital':
      $url = "aspcm-hospital";
      $flag = $hide_hos_add;
      break;
    case 'aspcm_ward':
      $url = "aspcm-ward";
      $flag = $hide_ward_add;
      break;
    case 'aspcm_product':
      $url = "aspcm-product";
      $flag = '1'; 
      break;
    case 'aspcm_severity':
      $url = "aspcm-severity";
      $flag = '1'; 
      break;
    case 'aspcm_disease':
      $url = "aspcm-disease";
      $flag = '1';
      break;
    case 'aspcm_severity_name':
      $url = "aspcm-clinical-department";
      $flag = '1';
      break;
    case 'aspcm_product_category':
      $url = "aspcm-product-category";
      $flag = '1';
      break;
    case 'severity_cond_config_view':
      $url = "severity-condition";
      $flag = '1';
      break;
    case 'severity_mt_config_view':
      $url = "severity-treatment";
      $flag = '1';
      break;
    case 'nursing_mt_config_view':
      $url = "severity-nurse-treatment";
      $flag = '1';
      break;
    case 'nursing_cond_config_view':
      $url = "severity-nurse-condition";
      $flag = '1';
      break;
    case 'aspcm_user_management':
      $url_for_user = 'admin/user/user/create';
      $flag = '1';
      $user_flag = 1;
      break;
    case 'aspcm_all_news':
      $url = "aspcm-news";
      $flag = $hide_news_add;
      break;
    case 'aspcm_important_news':
      $urlnews = 'news';
      $flag = '2';
      break;
  }
  if($user_flag == 1){
    $url = base_path() . "user/create";
  }
  else {
    $url = base_path() . "node/add/" . $url;
  } 
  ?>
  <?php if($flag == '1') {?> 
  <div class="new-item-link"><a class="lnk-add" href="<?php print $url; ?>">新規追加</a>
  <!-- export all users -->
  <?php if(current_path() == 'hospital-mgmt/asp-user-management' && $hide_hos_add == 'FALSE'){ ?>
  <input type="button" value="エクスポート" class="form-submit" onclick="window.location.href='users_export_csv'">
  <?php } ?>

  </div> 
  <?php } elseif($flag == '2') {?>

  <div class="more-links"><a href="<?php print $urlnews;?>">続きを見る</a></div>
  <?php } elseif($flag == 'TRUE') {?>

  <?php } elseif($flag == 'FALSE') {?>

   <div class="new-item-link"><a class="lnk-add" href="<?php print $url; ?>">新規追加</a></div>
  <?php }?>
  
  <div class="clear"></div>
</div> 
