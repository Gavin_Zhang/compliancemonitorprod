﻿/* Japanese initialisation for the jQuery UI date picker plugin. */
/* Written by Kentaro SATO (kentaro@ranvis.com). */
jQuery(function($){
	$.datepicker.regional['ja'] = {
		closeText: '閉じる',
		prevText: '&#x3c;前',
		nextText: '次&#x3e;',
		currentText: '今日',
		monthNames: ['1月','2月','3月','4月','5月','6月',
		'7月','8月','9月','10月','11月','12月'],
		monthNamesShort: ['1月','2月','3月','4月','5月','6月',
		'7月','8月','9月','10月','11月','12月'],
		dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
		dayNamesShort: ['日','月','火','水','木','金','土'],
		dayNamesMin: ['日','月','火','水','木','金','土'],
		weekHeader: '週',
		//dateFormat: 'yy/mm',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '年'};
	$.datepicker.setDefaults($.datepicker.regional['ja']);
});
var unavailableDates = ["5-2011","6-2011","8-2011"];

function unavailable(date) {
  dmy = (date.getMonth()+1) + "-" + date.getFullYear();
  if ($.inArray(dmy, unavailableDates) == -1) {
    return [true, ""];
  } else {
    return [false,"","Unavailable"];
  }
}
            function Calendar() {

		       $("#form-input #edit-date-wrapper input").datepicker({ 
					showOn: "button",
					buttonImage: "sites/all/themes/asp_cm/images/icons/icon_calendar.png",
					buttonImageOnly: true,
					buttonText: "日付を選択する",
					changeMonth: true,
					dateFormat: 'yy/mm'
				}); 
			}
			function Popup_Calendar() {
				/*$("#edit-horizontal-start, #edit-horizontal-end").datepicker({ 			
					showOn: "button",
					buttonImage: "sites/all/themes/asp_cm/images/icons/icon_calendar.png",
					buttonImageOnly: true,
					buttonText: "日付を選択する",
					changeMonth: true,
					//beforeShowDay: unavailable,
				});*/
				var dates = $("#edit-horizontal-start, #edit-horizontal-end").datepicker({
					showOn: "button",
					buttonImage: "sites/all/themes/asp_cm/images/icons/icon_calendar.png",
					buttonImageOnly: true,
					buttonText: "日付を選択する",
					changeMonth: true,
					dateFormat: 'yy/mm',
					onSelect: function( selectedDate ) {
						en = $("#edit-horizontal-end").val();
						st = $("#edit-horizontal-start").val();
						instance = $( this ).data( "datepicker" );
						//alert(instance.settings.dateFormat);
						//alert(selectedDate);
						/*var option = this.id == "edit-horizontal-start" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );*/
						var option = this.id == "edit-horizontal-start" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								'yy/mm/dd',
								selectedDate+'/15', instance.settings );		
						dates.not( this ).datepicker( "option", option, date );
						if( this.id == "edit-horizontal-start") { $("#edit-horizontal-end").val(en);}
						if( this.id == "edit-horizontal-end") { $("#edit-horizontal-start").val(st);}
					}
				});
		        $.datepicker.regional['ja'];
				$.datepicker.formatDate('yy/mm');
	        };
	        
	        $(document).ready(function(){ 
				//Calendar();
				//$.datepicker.regional['ja'];
				//$.datepicker.formatDate('yy/mm');
				//Popup_Calendar();
	         })