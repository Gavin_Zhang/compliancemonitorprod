(function($){

var cookieHospital = 'ASP_HospitalID';
var cookieWard = 'ASP_WardID';
var cookiePreset = 'ASP_PresetID';
var vals = [];
	vals['popup-major'] = [];
	vals['popup-minor'] = [];
	vals['popup-horiz'] = [];
var pids = [];
	pids['popup-major'] = [];
	pids['popup-minor'] = [];
	pids['popup-horiz'] = [];
var pnames = ['popup-major','popup-minor','popup-horiz'];
var cookieDelete = 'delete_flag';	
var flagcr = false;
var flag2 = true; // for Drupal keyup autocomplete function
var ws_page;
var ws_hosp_id;
var countl = 0;

var arr_checked = [];
var arr_unchecked = [];
var arr_savetemp = [];


//0 Point jQuery Drop Down Menu 
jQuery('#navlinks li.expanded > a').addClass("expandfirst");
jQuery('#navlinks li.expanded ul li.expanded > a').addClass('expand');
jQuery('#navlinks li.expanded > ul').addClass('firstsublayer');
jQuery('#navlinks li.expanded ul li a').removeClass('active');
jQuery('#navlinks li.expanded ul li.expanded > ul').removeClass('firstsublayer').addClass('sublayer'); 

// @author_lxl_start
// graph/single-graph div#edit-ward-wrapper
if($("form#aspcm-graph-form").length>0){
    var arr0 = $( "select#edit-ward" ).val();
    if(arr0 != null){
        $.each(arr0, function(i,val){
            var $a_obj = $("#edit-ward").find("option[value="+val+"]");
            var $a_obj2 = $("#edit-ward2").find("option[value="+val+"]");
            if($a_obj2.val()==undefined){
                $("#edit-ward2").append("<option value='"+val+"'>"+$a_obj.text()+"</option>"); 
                $a_obj.remove();
            }   
          }); 
    }
	$("#select-all-wards").click(function(){	
        var arr = $( "select#edit-ward" ).val();
		if(arr != null){
			$.each(arr, function(i,val){
				var $a_obj = $("#edit-ward").find("option[value="+val+"]");
				var $a_obj2 = $("#edit-ward2").find("option[value="+val+"]");
				if($a_obj2.val()==undefined){
					$("#edit-ward2").append("<option value='"+val+"'>"+$a_obj.text()+"</option>"); 
				    $a_obj.remove();
                }	
			  }); 
		}
		return false;
	});
    //reset  
	$("#reset-all-wards").click(function(){
        if(($("#edit-hospital").attr('disabled')) == 'disabled'){
            var arr3 = $( "input[name=ward3]" ).val();
            var jsonObj = eval('(' + arr3 + ')');
            $("#edit-ward option").remove();
            $("#edit-ward2 option").remove();
            $.each(jsonObj, function(data) {
                $("#edit-ward").append("<option value='"+this.id+"'>"+this.val+"</option>"); 
            });
        }else{
            $(".search-autoc").trigger('click');
        } 
        return false;
	});
    $("#edit-generate-main,#edit-save-confirm").click(function(){
        $("#edit-ward2").find("option").attr('selected','selected');
        $("#edit-ward").find("option").attr('selected',false);
        var arr2 = $( "select#edit-ward2" ).val();
        if(arr2 != null){
            $("#edit-ward").append("<option value='1'></option><option value='2'></option><option value='3'></option>");
            $.each(arr2, function(i,val){
                var $a_obj = $("#edit-ward").find("option[value="+val+"]");
                var $a_obj2 = $("#edit-ward2").find("option[value="+val+"]");
                if($a_obj.val()==undefined){
                    $("#edit-ward").append("<option value='"+val+"' style='display:none;' selected='selected'>"+$a_obj2.text()+"</option>"); 
                }   
              }); 
        }       
    });
}
// @author_lxl_end

//add by yueshuo  自设施
if (jQuery('body').hasClass('not-front section-graph page-graph-single-graph')) {
    //主轴
    jQuery('#edit-major-value-compliance-rate,#edit-major-value-infection-rate').attr('disabled','disabled');//遵守率
    jQuery('#edit-major-value-none').attr('disabled',true);// なし
    
    // 副轴
    jQuery('#edit-minor-value-hand-washing,#edit-minor-value-antiseptic-used,#edit-minor-value-none,#edit-minor-value-infection-rate,#edit-minor-value-compliance-rate').attr('disabled','disabled');//手指衛生回数
    //POST 
    var preset_radios_post_flag =   $('input[name="presetRadios"]');
//    for (var i=0;i<4;i++) {
//        if (i == 0){
//            if (preset_radios_post_flag[i].checked) {
//                $('#edit-minor-value-compliance-rate').attr({checked:true,disabled:false});//副轴
//                $('#edit-major-value-hand-washing').attr({checked:true});//主轴
//            }
//        } else if (i == 1) {
//            if (preset_radios_post_flag[i].checked) {
//                $('#edit-minor-value-infection-rate').attr({checked:true,disabled:false});//副轴
//                $('#edit-major-value-hand-washing').attr({checked:true});//主轴
//            }
//        } else if (i == 2) {
//            if (preset_radios_post_flag[i].checked) {
//                $('#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate').attr({disabled:false});//副轴
//                $('#edit-major-value-hand-washing').attr({checked:true});//主轴
//            }
//        } else if (i == 3) {
//            if (preset_radios_post_flag[i].checked) {
//                $('#edit-minor-value-hand-washing,#edit-minor-value-antiseptic-used,#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate,#edit-minor-value-none,#edit-major-value-compliance-rate,#edit-major-value-infection-rate,#edit-major-value-none').attr({disabled:false})
//            }
//        }           
////        i == 0 ? preset_radios_post_flag[i].checked ? $('#edit-minor-value-compliance-rate').attr({checked:true,disabled:false}) : '' : '';
////        i == 1 ? preset_radios_post_flag[i].checked ? $('#edit-minor-value-infection-rate').attr({checked:true,disabled:false}) : '' : '';
////        i == 2 ? preset_radios_post_flag[i].checked ? $('#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate').attr({disabled:false}) : '' : '';
////        i == 3 ? preset_radios_post_flag[i].checked ? $('#edit-minor-value-hand-washing,#edit-minor-value-antiseptic-used,#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate,#edit-minor-value-none,#edit-major-value-compliance-rate,#edit-major-value-infection-rate,#edit-major-value-none').attr({disabled:false}) : '' : '';
//    }
    var preset_radios_post_flag =   $('input[name="presetRadios"]');
    for (var i=0;i<4;i++) {
        if (i == 0){
            if (preset_radios_post_flag[i].checked) {
                $('#edit-minor-value-compliance-rate').attr({checked:true,disabled:false});//副轴
                $('#edit-major-value-antiseptic-used').attr('checked') == 'checked' ? '' : $('#edit-major-value-hand-washing').attr({checked:true});//主轴
                $('#edit-horizontal-axis-ward,#edit-horizontal-axis-period').attr('disabled',true);
                $('#edit-horizontal-axis-period').attr({disabled:false,checked:true});
            }
            
        } else if (i == 1) {
            if (preset_radios_post_flag[i].checked) {
                $('#edit-minor-value-infection-rate').attr({checked:true,disabled:false});//副轴
//                $('#edit-major-value-hand-washing').attr({checked:true});//主轴
                $('#edit-major-value-antiseptic-used').attr('checked') == 'checked' ? '' : $('#edit-major-value-hand-washing').attr({checked:true});//主轴
                $('#edit-horizontal-axis-ward,#edit-horizontal-axis-period').attr('disabled',true);
                $('#edit-horizontal-axis-period').attr({disabled:false,checked:true});
            }
            
        } else if (i == 2) {
            if (preset_radios_post_flag[i].checked) {
                $('#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate').attr({disabled:false});//副轴
//                $('#edit-major-value-hand-washing').attr({checked:true});//主轴
                $('#edit-major-value-antiseptic-used').attr('checked') == 'checked' ? '' : $('#edit-major-value-hand-washing').attr({checked:true});//主轴
                $('#edit-horizontal-axis-ward,#edit-horizontal-axis-period').attr('disabled',true);
                $('#edit-horizontal-axis-ward').attr({disabled:false,checked:true});
            }
            
        } else if (i == 3) {
            if (preset_radios_post_flag[i].checked) {
                $('#edit-minor-value-hand-washing,#edit-minor-value-antiseptic-used,#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate,#edit-minor-value-none,#edit-major-value-compliance-rate,#edit-major-value-infection-rate,#edit-major-value-none').attr({disabled:false})
            }
        }           
//        i == 0 ? preset_radios_post_flag[i].checked ? $('#edit-minor-value-compliance-rate').attr({checked:true,disabled:false}) : '' : '';
//        i == 1 ? preset_radios_post_flag[i].checked ? $('#edit-minor-value-infection-rate').attr({checked:true,disabled:false}) : '' : '';
//        i == 2 ? preset_radios_post_flag[i].checked ? $('#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate').attr({disabled:false}) : '' : '';
//        i == 3 ? preset_radios_post_flag[i].checked ? $('#edit-minor-value-hand-washing,#edit-minor-value-antiseptic-used,#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate,#edit-minor-value-none,#edit-major-value-compliance-rate,#edit-major-value-infection-rate,#edit-major-value-none').attr({disabled:false}) : '' : '';
    }
    
    /**
     * @type cache Array
     * @description The query to the user settings to save to the array, from left to right in order to correspond to the 0-3 with the Radio representation (the default for the number type), query every Radio settings are an object to save to
     * corresponding to the cache array and cache the data, judge data types are (type number) digital (request database), character (no data set by the user), types of objects (setting information of a user). Determine the type of the array
     * for processing, if the number (number) is the query database, if you have the user set the data returned to the data object, there is no return to the NULL to change the character type
     * @author yueshuo  20160523
     */
    var cache   =   [0,1,2,3];
    
    /**
     * handle single-graph User  Radio save settings
     * @author yueshuo  20160523 
     */
    function alj_single_graph_user_setting(radio_value){
        
        $('#edit-horizontal-start').val(cache[radio_value].horizontal_start);//start time 
        $('#edit-horizontal-end').val(cache[radio_value].horizontal_end);//end time 
        
        // 主轴
        switch (cache[radio_value].major_value){
            case 'hand_washing':
                $('#edit-major-value-hand-washing').attr('checked',true);
                $("#edit-major-wash-number").find('option').each(function () {
                    $(this).attr('selected',false);
                });
                $('#edit-major-wash-number').find('option[value="'+cache[radio_value].major_wash_number+'"]').prop('selected',true);
                break;
            case 'antiseptic_used':
                $('#edit-major-value-antiseptic-used').prop('checked','checked');
                $("#edit-major-antisepic-volume").find('option').each(function () {
                    $(this).attr('selected',false);
                });
                $('#edit-major-antisepic-volume').find('option[value="'+cache[radio_value].major_antisepic_volume+'"]').prop('selected',true);
                
                break;
            case 'compliance_rate':
                $('#edit-major-value-compliance-rate').attr('checked','checked');
                break;
            case 'infection_rate':
                $('#edit-major-value-infection-rate').attr('checked','checked');
                break;
            case 'none':
                $('#edit-major-value-none').attr('checked','checked');
                break;
        }
        //手指衛生剤表示形式を選びます：
        if (cache[radio_value].major_single_bar == 'multiple') {// アルコール手指消毒剤と石けんを別々に表示
            $('#edit-major-single-bar-multiple').prop('checked',true);
        }else if (cache[radio_value].major_single_bar == 'single') {  //アルコール手指消毒剤と石けんを一つに統合
            $('#edit-major-single-bar-single').prop('checked',true);
        }
        //グラフの主軸に表示する最大値を選びます：
        if (cache[radio_value].major_maximum_value == 'automatic') {    //自动
            $('#edit-major-maximum-value-automatic').prop('checked',true);
        }else if (cache[radio_value].major_maximum_value == 'fixed'){   //固定
            $('#edit-major-maximum-value-fixed').prop('checked',true);  
        }
        $('#edit-major-fixed-value').val(cache[radio_value].major_fixed_value);// 固定value
        
        
        // 副轴
        switch (cache[radio_value].minor_value){
            case 'hand_washing':
                $('#edit-minor-value-hand-washing').attr('checked',true);
                $('#edit-minor-wash-number').find('option').each(function () {
                    $(this).attr('selected',false);
                });
                $('#edit-minor-wash-number').find('option[value="'+cache[radio_value].major_antisepic_volume+'"]').prop('selected',true);
                break;
            case 'antiseptic_used':
                $('#edit-minor-value-antiseptic-used').prop('checked','checked');
                $('#edit-minor-antisepic-volume').find('option').each(function () {
                    $(this).attr('selected',false);
                });
                $('#edit-minor-antisepic-volume').find('option[value="'+cache[radio_value].major_antisepic_volume+'"]').prop('selected',true);
                break;
            case 'compliance_rate':
                $('#edit-minor-value-compliance-rate').attr('checked','checked');
                break;
            case 'infection_rate':
                $('#edit-minor-value-infection-rate').attr('checked','checked');
                break;
            case 'none':
                $('#edit-minor-value-none').attr('checked','checked');
                break;
        }
        //手指衛生剤表示形式を選びます：
        if (cache[radio_value].minor_single_line == 'multiple') {   //アルコール手指消毒剤と石けんを別々に表示
            $('#edit-minor-single-line-multiple').prop('checked',true);
        } else if (cache[radio_value].minor_single_line == 'single') {   //アルコール手指消毒剤と石けんを別々に表示
            $('#edit-minor-single-line-single').prop('checked',true);   //アルコール手指消毒剤と石けんを一つに統合
        }
        //グラフの副軸に表示する最大値を選びます：
        if (cache[radio_value].minor_maximum_value  ==  'automatic') {  //自动
            $('#edit-minor-maximum-value-automatic').prop('checked',true);
        } else if (cache[radio_value].minor_maximum_value  ==  'fixed') {   //固定
            $('#edit-minor-maximum-value-fixed').prop('checked',true);
        }
        $('#edit-minor-fixed-value').val(cache[radio_value].minor_fixed_value); //固定value
        
        
        //横轴
        if (cache[radio_value].horizontal_axis == 'period') {   //期間
            $('#edit-horizontal-axis-period').prop('checked',true);
        } else if (cache[radio_value].horizontal_axis == 'ward') {  //病棟
            $('#edit-horizontal-axis-ward').prop('checked',true);
        }
        
        $('input[name="horizontal_axis"]').attr('disabled',true);
        switch ( radio_value ) {
            case 0:
            case 1:
                $('#edit-horizontal-axis-period').attr({disabled:false,checked:true});
                break;
            case 2:
                $('#edit-horizontal-axis-ward').attr({disabled:false,checked:true});
                break;
        }
        $('#edit-unit-of-period-displayed').find('option').each(function () {
            $(this).attr('selected',false);
        });
        $('#edit-unit-of-period-displayed').find('option[value="'+cache[radio_value].unit_of_period_displayed+'"]').prop('selected',true);
    }
    
    $("input[name='presetRadios']").bind('change', function(){
        //主轴
        jQuery('#edit-major-value-compliance-rate,#edit-major-value-infection-rate').attr('disabled','disabled');//遵守率
        jQuery('#edit-major-value-none').attr('disabled',true);// なし

        // 副轴
        jQuery('#edit-minor-value-hand-washing,#edit-minor-value-none,#edit-minor-value-antiseptic-used,#edit-minor-value-infection-rate').attr('disabled','disabled');//手指衛生回数
        var url   = $('input[name="ajax_check_user_setting_url"]').val();
        var radio_value = $(this).val();
        var type        =   Object.prototype.toString.call(cache[radio_value]);
        if (typeof cache[radio_value] == 'number'){
            $.ajax({
                url: url,
                async: false,
                type:'POST',
                data:{activity_id:$(this).val()},
                success: function (data){
                    var result;
                    if (data)
                        result  =   JSON.parse(data);
                    else 
                        result =   String(radio_value);
                    cache[radio_value]  =   result;
                }
            });
        }
        
        if ($(this).val() == 0) {//遵守状況グラフ
            $('#edit-minor-value-compliance-rate,#edit-minor-value-none,#edit-minor-value-infection-rate,#edit-horizontal-axis-ward').attr('checked',false);
            
            $('#edit-minor-value-compliance-rate').attr('disabled',false);//遵守率 
            $('#edit-minor-value-infection-rate').attr('disabled','disabled');
        
            //read ajax data setting
            if (typeof cache[radio_value] == 'object'){
                alj_single_graph_user_setting(radio_value);
                return;
            }
            
            $('#edit-minor-value-compliance-rate,#edit-horizontal-axis-period').attr('checked','checked');//副轴 遵守状況グラフ
            $('#edit-horizontal-axis-ward,#edit-horizontal-axis-period').attr('disabled',true);
            $('#edit-horizontal-axis-period').attr('disabled',false);
        }else if ($(this).val() == 1) { //感染率推移グラフ
            $('#edit-minor-value-infection-rate,#edit-minor-value-compliance-rate,#edit-minor-value-none,#edit-horizontal-axis-ward').attr('checked',false);
            
            $('#edit-minor-value-infection-rate').attr('disabled',false);
            $('#edit-minor-value-compliance-rate').attr('disabled','disabled');
            
            
            if (typeof cache[radio_value] == 'object'){
                alj_single_graph_user_setting(radio_value);
                return;
            }
            
            $('#edit-major-value-hand-washing,#edit-minor-value-infection-rate,#edit-horizontal-axis-period').attr('checked','checked');
            $('#edit-horizontal-axis-ward,#edit-horizontal-axis-period').attr('disabled',true);
            $('#edit-horizontal-axis-period').attr('disabled',false);
            
        }else if ($(this).val() == 2) {//病棟別グラフ
            $('#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate').attr('disabled',false);
            
            $('#edit-minor-value-infection-rate,#edit-horizontal-axis-period').attr('checked',false);
            
            if (typeof cache[radio_value] == 'object'){
                alj_single_graph_user_setting(radio_value);
                return;
            }
            
            $('#edit-major-value-hand-washing,#edit-horizontal-axis-ward,#edit-minor-value-compliance-rate').attr('checked','checked');// 横轴 期间
            $('#edit-horizontal-axis-ward').attr('disabled',false);
            $('#edit-horizontal-axis-period').attr('disabled',true);
        }else if ($(this).val() == 3) {//自己設定グラフ
            //主轴
            $('#edit-major-value-compliance-rate,#edit-major-value-none,#edit-major-value-infection-rate').attr('disabled',false);//遵守率
            
            //副轴
            $('#edit-minor-value-compliance-rate,#edit-minor-value-hand-washing,#edit-minor-value-antiseptic-used,#edit-minor-value-infection-rate,#edit-minor-value-none').attr('disabled',false);
            
            if (typeof cache[radio_value] == 'object'){
                alj_single_graph_user_setting(radio_value);
                return;
            }
            
            $('#edit-minor-value-none,#edit-minor-value-hand-washing,#edit-minor-value-antiseptic-used,#edit-minor-value-compliance-rate,#edit-minor-value-infection-rate').attr('checked',false);
            $('#edit-minor-value-hand-washing').attr('checked','checked');
            $('#edit-horizontal-axis-ward,#edit-horizontal-axis-period').attr('disabled',false);
        }
    });
}

    
//他设施 graph/comparison-graph
//if (jQuery('body').hasClass('section-graph page-graph-comparison-graph')) {
//   $('#edit-minor-value-hand-washing,#edit-minor-value-antiseptic-used,#edit-minor-value-compliance-rate,#edit-minor-value-none').attr({disabled:true,checked:false});
//   $('#edit-minor-value-hand-washing').attr('checked',true);
//   var minor_selected_flag = $('#minor_selected_flag').val();
//   minor_selected_flag  =   parseInt(minor_selected_flag);
//   
//   minor_selected_flag == 2 ? $('#edit-minor-value-antiseptic-used').attr({checked:true}) : '';
//   minor_selected_flag == 3 ? $('#edit-minor-value-compliance-rate').attr({checked:true}) : '';
//   minor_selected_flag == 4 ? $('#edit-minor-value-none').attr({checked:true}) : '';
//   
//   $('#minor_selected_flag').bind('change',function (){
//        $('#edit-minor-value-hand-washing,#edit-minor-value-antiseptic-used,#edit-minor-value-compliance-rate,#edit-minor-value-none').attr({disabled:true,checked:false});
//        var minor_selected_flag  =   $('#minor_selected_flag').val();
//        if (minor_selected_flag == 1){
//            $('#edit-minor-value-hand-washing').attr('checked',true);
//        }else if (minor_selected_flag == 2){
//            $('#edit-minor-value-antiseptic-used').attr('checked',true);
//        }else if (minor_selected_flag == 3){
//            $('#edit-minor-value-compliance-rate').attr('checked',true);
//        }else if (minor_selected_flag == 4){
//            $('#edit-minor-value-none').attr('checked',true);
//        }
//   });
//}
//
//他设施modified 201604
if (jQuery('body').hasClass('section-graph page-graph-comparison-graph')) {
    //After the user submits,save the current options settings
    var edit_minor_value_flag   =   $('#edit-minor-value').val();
    $('#edit-minor-antisepic-volume').attr({disabled:true});
    $('#edit-minor-wash-number').attr({disabled:true});
    switch (edit_minor_value_flag) {
        case 'hand_washing': 
            $('#minor_value_flag_0').attr({checked:true});
            $('#edit-minor-wash-number').removeAttr('disabled');
            break;
        case 'antiseptic_used': 
            $('#minor_value_flag_1').attr({checked:true});
            $('#edit-minor-antisepic-volume').removeAttr('disabled');
            break;
        case 'compliance_rate': 
            $('#minor_value_flag_2').attr({checked:true});
            break;
        case 'none': 
            $('#minor_value_flag_3').attr({checked:true});
            break;
    }

//When the options change
$('#edit-minor-value').bind('change',function () {
    var that    =   $(this);
    for(var i=0;i<4;i++){
        $('#minor_value_flag_'+i).attr({checked:false});
    }
    $('#edit-minor-antisepic-volume').attr({disabled:true});
    $('#edit-minor-wash-number').attr({disabled:true});
    switch (that.val()) {
        case 'hand_washing': 
            $('#minor_value_flag_0').attr({checked:true});
            $('#edit-minor-wash-number').removeAttr('disabled');
            break;
        case 'antiseptic_used': 
            $('#minor_value_flag_1').attr({checked:true});
            $('#edit-minor-antisepic-volume').removeAttr('disabled');
            break;
        case 'compliance_rate': 
            $('#minor_value_flag_2').attr({checked:true});
            break;
        case 'none': 
            $('#minor_value_flag_3').attr({checked:true});
            break;
    }
    return;
})
}
// end
 
// show & hide functions
function show(){
	jQuery(this).children('.firstsublayer, .sublayer').show(); 
}
function hide(){
	jQuery(this).children('.firstsublayer, .sublayer').hide();	
}
// set some options	for the hover effect
var config = {    
     sensitivity: 5, // number = sensitivity threshold (must be 1 or higher)    
     interval: 100,  // number = milliseconds for onMouseOver polling interval    
     over: show,     // function = onMouseOver callback (REQUIRED)    
     timeout: 200,   // number = milliseconds delay before onMouseOut (200)  
     out: hide       // function = onMouseOut callback (REQUIRED)    
};
jQuery('#navlinks li.expanded').hoverIntent(config);

//show_loading();
			
//Function for IEs only
if($.browser.msie){
	
	var settings5 = { 
			  tl: { radius: 5 }, 
			  tr: { radius: 5 }, 
			  bl: { radius: 5 }, 
			  br: { radius: 5 }, 
			  antiAlias: true, 
			  autoPad: true, 
			  validTags: ["div"] 
			}
	var settings_top5 = { 
			  tl: { radius: 5 }, 
			  tr: { radius: 5 }, 
			  bl: { radius: 0 }, 
			  br: { radius: 0 }, 
			  antiAlias: true, 
			  autoPad: true
		}; 
	var settings3 = { 
			  tl: { radius: 3 }, 
			  tr: { radius: 3 }, 
			  bl: { radius: 0 }, 
			  br: { radius: 0 }, 
			  antiAlias: true, 
			  autoPad: true
		}	  
		
		$("#navlinks > ul.menu> li").corner(settings_top5);
		$("body").not(".page-default").not(".page-home").not("#legalnotice").find("#main h1.title").corner(settings_top5); //#main h1.title, #sidebar-right h2.title
		$("#banner-block, body:not(.section-legalnotice):not(.section-help) #main .content-manual, #sidebar-right .themed-block").corner(settings5);
		$("#legalnotice-content h2").corner(settings3);
		$("#preset-panel li").corner(settings_top5);
		
	//Hover issue for IE6
		if($.browser.version=="6.0"){
			//buttons
			makehover(".form-submit", "form-submit-hover");	
			$("#navlinks ul.menu li li").bgiframe();
		}
	
	//master list tables
		//layout for IE6&IE7 only
		if($.browser.version<="7.0"){
			$("#main table.views-table td:empty").html("&nbsp;");
		}
}

	$('#navlinks .expandfirst').bind('click',function(){
		$('#navlinks .firstsublayer').hide()
		$(this).next().show();
		return false;
	});

	
	$("#aspcm-graph-form #edit-download, #aspcm-comparison-graph-form #edit-download").bind('click',function(){
		$("#edit-graph-path").val($("#img-graph img").attr('src'));
	});
  // @author_lxl_start
  // Clinical-Goverment Hospitals (logic for Compare Graph Page - radio)
  	var checked = $('#comparison_graph_radio_no').attr('checked');
    if(checked == 'checked'){
        $("#comparison_graph_grey").addClass('comparison_graph_grey');
    }
    $("#comparison_graph_radio_no,#comparison_graph_radio_yes").bind('click', function(){
	    comparison_graph_radio($(this).val());
	  });
  // @author_lxl_end  
	// Clinical-Goverment Hospitals (logic for Compare Graph Page - Selectbox)
  $("#edit-major-value").bind('change', function(){
    compare_graph_dept($(this).val());
  });
  compare_graph_dept($("#edit-major-value").val()); 

	// Disable delete button in Masters
	if($.cookie(cookieDelete)== 'false') {
		$("#node-delete-confirm #edit-submit-1").hide();
	}
	else {
		$("#node-delete-confirm #edit-submit-1").show();
	}
	
	
	/*************************/
	/*  Input Form           */
	/*************************/
	//Swicher for Actual and Estimate Table
	show_current_table();
	$("#severity_level").bind("change",function(){
		show_current_table();
	});
	
	// Calculate values in Estimated Table
	recalculate_estimate();
	$(".estimated-day").bind("focus, click, keyup", function(){   //focus, click, keyup  (not put BLUR! code will work not correctly!)

		f = $(this).parents('td');
		ff = $(this).parents('tr');
		f1 = parseInt($(this).parents('tr').find('td').index(f) ) - 1 ;
		f2 = parseInt($(this).parents('table').find('tr').index(ff) ) ;
		
		calculate_estimate();
		
	});
	
	// Calculate values in Actual Table
	calculate_actual();
	$(".actual-patnt").bind("focus, click, keyup", function(){  
		calculate_actual();
	});
	
	$("#aspcm-ward-monthly-data-input-form #edit-hospital").bind("change",function(){
		$("#edit-Search").attr('disabled','disabled').addClass('disabled');
		$("#edit-search-click").click();
	});
	$("#aspcm-ward-monthly-data-input-form #edit-search, #aspcm-ward-monthly-data-input-form #edit-search-click").bind("click",function(){
		$.cookie(cookieHospital, $("#edit-hospital").val(), { path: '/' });
		$.cookie(cookieWard, $("#edit-ward").val(), { path: '/' });
	});
	// Graph Page Select All ward Control
	$("#aspcm-graph-form #select-all-wards").click(function(){
		//@author_lxl_change
		//$("#aspcm-graph-form #edit-ward option").attr("selected","selected");
		return false;
	});
	$("#aspcm-graph-form #reset-all-wards").click(function(){
		//@author_lxl_change
		//$("#aspcm-graph-form #edit-ward option").removeAttr("selected");
		return false;
	});
	
	// Disable Press Enter for non-button controls
	$('form:not(#user-login-form) input:not(.form-submit), select').keypress(function(event) {
		if (event.which == '13') {
			return false;
		}
	});

	
	// Validation for Input Form and View Form 
	
	$("#product-frm-input input, #Mandays, #total_patient, #estimated input:enabled, #actual input:enabled, #disease-list input, #aspcm-ward-annually-data-view #date, #aspcm-ward-annually-data-view-1 #date").numeric();
	$("#aspcm-ward-annually-data-import #date, #aspcm-ward-monthly-data-input-form #date").numeric({allow:"/"});
	$("#edit-field-aspcm-telephone-number-0-value-wrapper input").numeric({allow:"-"});
	
	// Input Page: Popup for confirmation
	$("#aspcm-ward-monthly-data-input-form .block-goback .link-back").bind("click",function(){
		showpopup("#popup-confirmation", "入力一覧画面に戻る", 340, 105);
		return false;
	});
	
	$("#calc-compliance-rate").bind('click', function(){
		$("#compliance-rate").text("Waiting...");
		$.post(
				"get/compliance-rate", 
				$("#aspcm-ward-monthly-data-input-form").serialize(),
                function(data){
					$("#compliance-rate").text(data);
                });
	});
	/*
	*Hospital management: Popup for product finding*
	*/
	// fields validation
	$('.custom-perpush-text').numeric({allow:"."});
	if( $("input[name='products_hidden']").length > 0 ){
		if( $("input[name='products_hidden']").val() != '' ){
			arr_savetemp = $("input[name='products_hidden']").val().split(';');
		}
	}
	if( ($('#add-products-button').length > 0) && ($.browser.msie) && ($.browser.version=="6.0") ){
		showpopup5("#product-finder-widget", "使用製品情報検索", 800, 590);
		$('.product-finder-popup').hide();
		$('.product-finder-popup').next('.ui-widget-overlay').hide();
	}
	
	//alert('arr_checked:'+arr_checked.join(';'));
	//alert('arr_unchecked:'+arr_unchecked.join(';'));
	//alert('arr_savetemp:'+arr_savetemp.join(';'));
	$("#add-products-button").bind("click", function() {
		if(($.browser.msie) && ($.browser.version=="6.0")){
			$('.product-finder-popup').show();  //.css('top', '50%')
			$('#background-popup-dialog').removeClass('hidden');
			$('#background-popup-dialog').css({"width": $(document).width()+"px","height": $(document).height()+"px"}).bgiframe();
			$("html").css("overflow","hidden");
			$('.product-finder-popup').center();
			//$('#ui-dialog-title-product-finder-widget').focus();
			return false;
		}else{
			showpopup5("#product-finder-widget", "使用製品情報検索", 800, 590);
			return false;
		}
	});
	//ajax for 'add product' search in the popup
	$("#product-finder-widget #edit-srch-submit").bind('click', function(){
	   //#edit-srch-is-jnj, #edit-srch-pgroup,
	   $("#edit-srch-cname, #edit-srch-pname, #edit-srch-submit, #edit-product-submit, .save-redirect, .redirect").addClass('disabled').attr('disabled', 'disabled');
		$(this).after('<img id="loader-img" class="product-search-loader" src="/sites/all/themes/asp_cm/images/ajax-loading-small.gif" width="16" height="16" title="loading" alt="loading" />');
	   //alert('data: '+ arr_savetemp.join(';'));
	   $.ajax({
		 type : "POST",
		 url : "/product/search/ajax",
		 dataType: "text",
		 data: {
			'srch_cname': $('#edit-srch-cname').val(),
			'selected_products': arr_savetemp.join(';')
		 },
		 success : function(result) {
			 //if(matches['status']){
			 $("#search-result-content, .custom-perpush-info, #no-search-result").remove();
			 $('#edit-product-submit').after(result);
			 $('#loader-img').remove();
			 $('.custom-perpush-text').numeric({allow: '.'});
			 $("#edit-srch-cname, #edit-srch-pname, #edit-srch-submit, #edit-product-submit, .redirect, .save-redirect").removeClass('disabled').removeAttr('disabled');
			 //}
			 return false;
		 },
		 error : function(msg) {
			return false;
		 }
	   });
	   return false;
	});
	$("#product-finder-widget #edit-product-submit").bind('click', function(){
	   //#edit-srch-is-jnj, #edit-srch-pgroup,
	   $("#edit-srch-cname, #edit-srch-pname, #edit-srch-submit, #edit-product-submit, .save-redirect, .redirect").addClass('disabled').attr('disabled', 'disabled');
	   $(this).after('<img id="loader-img" class="product-search-loader2" src="/sites/all/themes/asp_cm/images/ajax-loading-small.gif" width="16" height="16" title="loading" alt="loading" />');
		
	   //alert('data: '+ arr_savetemp.join(';'));
	   $.ajax({
		 type : "POST",
		 url : "/product/search/ajax",
		 dataType: "text",
		 data: {
			'srch_pname': $('#edit-srch-pname').val(),
			'selected_products': arr_savetemp.join(';')
		 },
		 success : function(result) {
			 //if(matches['status']){
			 $("#search-result-content, .custom-perpush-info, #no-search-result").remove();
			 $('#edit-product-submit').after(result);
			 $('#loader-img').remove();
			 $('.custom-perpush-text').numeric({allow: '.'});
			 $("#edit-srch-cname, #edit-srch-pname, #edit-srch-submit, #edit-product-submit, .redirect, .save-redirect").removeClass('disabled').removeAttr('disabled');
			 //}
			 return false;
		 },
		 error : function(msg) {
			return false;
		 }
	   });
	   return false;
	});
	//click event for checkboxes in product search popup
	$('#search-result-content .selprod-checkbox-group').live('click', function(){
		var v = $(this).attr('checked');
		var vv = $(this).val();
		var arr_idssave = [];
		var arr_idschk = [];
		var arr_idsunchk = [];
		var vvv = $(this).parent().parent().find('.custom-perpush-text').val();
		var vcon = vv + ':' + vvv;
		$.each(arr_savetemp, function(n,value){
			arr_idssave.push(value.split(':')[0]);
		});
		$.each(arr_checked, function(n,value){
			arr_idschk.push(value.split(':')[0]);
		});
		$.each(arr_unchecked, function(n,value){
			arr_idsunchk.push(value.split(':')[0]);
		});
		
		if(v == true || v == "checked"){
		    if( ($.inArray(vv, arr_idssave)==-1) && ($.inArray(vv, arr_idschk)==-1) ){  //not in array, add
				//alert('add to checked');
				arr_checked.push(vcon);
		    }
		}else{
			//alert('unchecked');
		    if( ($.inArray(vv, arr_idssave)!=-1) && ($.inArray(vv, arr_idsunchk)==-1) ){  
				//alert('add to unchecked');
				arr_unchecked.push(vcon);
		    }
			if($.inArray(vv, arr_idschk)!=-1){  //in array, remove
				//alert('remove from checked');
				arr_checked = $.grep(arr_checked, function(a){ return a.split(':')[0] != vv; });
		    }
		}
		//alert('arr_checked: ' + arr_checked.join(';'));
		//alert('arr_unchecked: ' + arr_unchecked.join(';'));
		//alert('arr_savetemp: ' + arr_savetemp.join(';'));
	});
	
	
	// Graph Form: Popups for confirmation
	$("#aspcm-graph-form #edit-save-confirm").bind("click", function(){
		//[validation was removed] if(check_date_unit()){
			showpopup3("#popup-confirmation-save", "設定の保存", 340, 105, "#hidden-buttons #edit-save");  //"#edit-generate-main"
		//}
		return false;
	});
	$("#aspcm-graph-form #edit-reset-confirm").bind("click", function(){
		showpopup3("#popup-confirmation-reset", "設定のリセット", 340, 105, "#hidden-buttons #edit-reset");
		return false;
	});

	//Logic for Product Master Page
	display_product_category();
	//$("#edit-field-aspcm-ref-product-category-nid-nid").bind("change",function(){
	$(".form-item-field-aspcm-ref-product-category-und").bind("change",function(){
		display_product_category();
	});	
	//$("#edit-field-aspcm-is-jnj-product-value-yes-wrapper input, #edit-field-aspcm-is-jnj-product-value-no-wrapper input").bind("click",function(){
	$("#edit-field-aspcm-is-jnj-product-und-yes,#edit-field-aspcm-is-jnj-product-und-no").bind("click",function(){
		display_product_category();
	});
	
	//Logic for Product Master Page about jnj element
	var hiddenfield = $("#edit-field-aspcm-is-jnj-product-und");
	hiddenfield.parents('.form-item').addClass("hidden");
	$("#edit-field-aspcm-company-und").bind("change",function(){
		display_jnj_item();
		display_product_category();
	});
	
	//View Page: JS for plus-minus items
	$("#form-view .clickable").toggle(
	 function(){
		 var p_id = $(this).parents('tr').attr('id');
		 $('.sub-'+p_id).removeClass("hidden");
	     $(this).removeClass("collapsed").addClass("expanded");
	 },
	 function(){
	     var p_id = $(this).parents('tr').attr('id');
		 $('.sub-'+p_id).addClass("hidden");
	     $(this).removeClass("expanded").addClass("collapsed");
	 }
	);
  
	function compare_graph_dept(name){
	    switch(name){
	      case 'compliance_rate':
	        $("#gov-dept").hide();
	        $("#clinical-dept").show();
	        break;
	      default:
	        $("#gov-dept").show();
	        $("#clinical-dept").hide();
	    }
	} 
  
	//[add] Product treeview with count of checkboxes (Hyper care) 
	var treeview_legend = $(".product-treeview fieldset.collapsible legend");
	check_treeview(false);
	$(".product-treeview .form-checkbox").bind('click', function(){
		 check_treeview(true);
	});
	
	//Input Page: Simulate tab for enter press in estamation table 
	var num_ed = $('.estimated-day').length;
	$('.estimated-day').keypress(function(event) {
		  if (event.which == '13') {
			if($(this).is(":focus")) {
				r = $(this).parents('table').find('.estimated-day').index($(this));
				if(num_ed == (r+1)){
					r = -1;
				}
				$("table .estimated-day:eq("+(r+1)+")").focus();
				return false;
			} 
		  }
	});
	
	/* Print feature */
	$("a.bg-print").bind("click",function(){
		window.print();
		return false;
	});
	/* Popups for Graph page */
	$("#major-link-block a").bind("click",function(){ 
		showpopup2("popup-major", "主軸", 'popup-major-axis'); 
		popup_create_event("popup-major");
		return false;
	});
	$("#minor-link-block a").bind("click",function(){
		showpopup2("popup-minor", "副軸", 'popup-minor-axis'); 
		popup_create_event("popup-minor");
		return false;
	});
	$("#horiz-link-block a").bind("click",function(){
		showpopup2("popup-horiz", "横軸", 'popup-horiz-axis');
		popup_create_event("popup-horiz"); 
		return false;
	});	  
	
	/* Logic for Popups in Graph Page */
	$("#edit-preseta").bind("click",function(){		
		$.cookie(cookiePreset, '1', { path: '/' });
	});
	$("#edit-presetb").bind("click",function(){		
		$.cookie(cookiePreset, '2', { path: '/' });
	});	
	$("#edit-presetc").bind("click",function(){	
		$.cookie(cookiePreset, '3', { path: '/' });
	});
	if($.cookie(cookiePreset)==null){ $.cookie(cookiePreset, '1', { path: '/' });}

	//cookie_popups();
	$("#edit-generate-main").bind("click",function(){
		// [removed validation] if(($("#aspcm-comparison-graph-form").length>0)||((check_date_unit())&&(($("#aspcm-graph-form").length>0)))){
		if(($("#aspcm-comparison-graph-form").length>0)||($("#aspcm-graph-form").length>0)){
				show_loading();
				return true;
		}else{ 
				return false;
		}
	});
	$("form").submit(function(){
		$("#autocomplete").remove();
		return true;
	});
	
	/* Loading Feature for submit buttons in forms */
	$("#preset-panel input, #edit-Previous-and-Save,#edit-Save,#edit-Next-and-Save, #edit-Search, #edit-search, #edit-search-1, #navlinks .loading-link").bind("click",function(){
		show_loading();
		return true;
	});
	
/*bind the loading for submit buttons in form except export csv 6-7*/
	$("#panel-buttons input, #edit-Previous-and-Save,#edit-Save,#edit-Next-and-Save, #edit-Search, #edit-search, #edit-search-1, #navlinks .loading-link").bind("click",function(){
	    if (this.id != "edit-export-csv")
		{
		  show_loading();
		  return true;
		}
	});
	
	/* Autocomplete control feature and fixes */
	if($("#aspcm-ward-annually-data-view").length>0){
		ws_page = "ward";
		ws_hosp_id = "#edit-hospital";
	}
	if($("#aspcm-ward-annually-data-view-1").length>0){
		ws_page = "ward1";
		ws_hosp_id = "#edit-hospital-1";
	}
	if($("#aspcm-graph-form").length>0){
		ws_page = "graph";
		ws_hosp_id = "#edit-hospital";
	}
	if($("#aspcm-comparison-graph-form").length>0){
		ws_page = "graph_comp";
		ws_hosp_id = "#edit-hospital";
	}
	if($("#user-register-form").length>0){
		ws_page = "user";
		ws_hosp_id = "#edit-asp-hospital-name-visiable";
	}
	if($("#user-profile-form").length>0){
		ws_page = "user_profile";
		ws_hosp_id = "#edit-asp-hospital-name-visiable-wrapper:not(.hospital-name-only) #edit-asp-hospital-name-visiable";
	}
	$(".search-autoc").bind('click', function(){
		ward_search(ws_hosp_id,ws_page);
		return false;
	});
	if($(".form-autocomplete:visible").length>0){
		fix_mouse_click(ws_page);
	}
	
	/* Call ajax for Autocomplete control */
	$("#aspcm-ward-annually-data-view #edit-hospital").bind('keyup', function(event){
		if (event.which == '13') { 
			//ward_search("#edit-hospital",'ward');
			fix_japanese_input("#edit-hospital",'ward');
			return false;
		}
	}).bind('keypress keydown', function(event){
		if (event.which == '13') {
			return false;
		};
	}).bind('change', function(event){
		return false;
	});
	$("#aspcm-ward-annually-data-view-1 #edit-hospital-1").bind('keyup', function(event){
		if (event.which == '13') { 
			//ward_search("#edit-hospital-1",'ward1');
			fix_japanese_input("#edit-hospital-1",'ward1');
			return false;
		}
	}).bind('keypress keydown', function(event){
		if (event.which == '13') {
			return false;
		};
	}).bind('change', function(event){
		return false;
	});
	
	$("#aspcm-graph-form #edit-hospital").bind('keyup', function(event){
		if (event.which == '13') { 
			fix_japanese_input("#edit-hospital",'graph');
		}
	}).bind('keypress keydown', function(event){
		if (event.which == '13') {
			return false;
		};
	}).bind('change', function(event){
		return false;
	});
	$("#aspcm-comparison-graph-form #edit-hospital").bind('keyup', function(event){
		if (event.which == '13') { 
			fix_japanese_input("#edit-hospital",'graph_comp');
		}
	}).bind('keypress keydown', function(event){
		if (event.which == '13') {
			return false;
		};
	}).bind('change', function(event){
		return false;
	});
	$("#user-profile-form #edit-asp-hospital-name-visiable").bind('keyup', function(event){
		if (event.which == '13') { 
			//ward_search("#edit-asp-hospital-name-visiable-wrapper:not(.hospital-name-only) #edit-asp-hospital-name-visiable",'user_profile');
			fix_japanese_input("#edit-asp-hospital-name-visiable-wrapper:not(.hospital-name-only) #edit-asp-hospital-name-visiable",'user_profile');
			return false;
		}
	}).bind('keypress keydown', function(event){
		if (event.which == '13') {
			return false;
		};
	}).bind('change', function(event){
		return false;
	});
	$("#user-register #edit-asp-hospital-name-visiable, #user-register #edit-asp-hospital-name-visiable-noajax").bind('keyup', function(event){
		if (event.which == '13') { 
			//ward_search("#edit-asp-hospital-name-visiable",'user');
			fix_japanese_input("#edit-asp-hospital-name-visiable",'user');
			return false;
		}
	}).bind('keypress keydown', function(event){
		if (event.which == '13') {
			return false;
		};
	}).bind('change', function(event){
		return false;
	});
	$("#aspcm-ward-annually-data-view .form-submit, #aspcm-graph-form .form-submit").keypress(function(event){
		if (event.which == '13') { 
			event.stopImmediatePropagation();
			return false;
		}
	});
  

	/* Footer Links Open/Close in new window */
	$("#footer .footer-links a, .contact-descript .ext-link-text, .contact-descript .ext-link").bind("click", function(){
		open_new_window($(this).attr('href') ,'form', 710);
		return false;
	});
	$("#above .contact-us, #user-login-form .frm-link").bind("click", function(){
		open_new_window($(this).attr('href') ,'contact', 780);
		return false;
	});
	$("#footer a.link-close").bind("click", function(){
		window.close();
		return false;
	});

	
	/* Operation of Severity Level */
	/* Author: Edwin */
	var lastindex = 0;
	$('#severity_nursing_page, #severity_result_page').hide();
	$(".page-severity_level input:radio").attr("checked", false); 
	$(".page-severity_level_normal input:radio").attr("checked", false); 
	$('a.next_to_nurse').bind("click", function(){
		$('a.back_to_severity, a.navi_to_result').addClass("current");
		$('a.next_to_nurse').removeClass("current");
		$('#severity_severity_page, #severity_result_page, .result-class, .severity-class').hide();
		$('#severity_nursing_page, .nursing-class').show();
		return false;
	});
	
	$('a.back_to_severity').bind("click",function(){
		$('#severity_nursing_page,#severity_result_page, .result-class, .nursing-class').hide();
		$('a.back_to_severity, a.navi_to_result').removeClass("current");
		$('a.next_to_nurse').addClass("current");
		$('#severity_severity_page, .severity-class').show();
		return false;
	});
	
	$('a.back_to_nurse').bind("click",function(){
		$('#severity_nursing_page, .nursing-class').show();
		$('#severity_result_page,#severity_severity_page, .result-class, .severity-class').hide();
		$('a.back_to_nurse').removeClass("current");
		$('a.navi_to_result, a.back_to_severity').addClass("current");
		return false;
	});
	
	$('a.navi_to_result').bind("click",function(){
		$('#severity_nursing_page,#severity_severity_page, .nursing-class, .severity-class').hide();
		$('#severity_result_page, .result-class').show();
		$('a.back_to_nurse').addClass("current");
		$('a.navi_to_result, a.back_to_severity').removeClass("current");
		//Calculation Logic for ICU
		var severity_mt_score = Number($('span.ascore').eq(0).text());
		var severity_cond_score = Number($('span.bscore').eq(0).text());
		var nursing_mt_score = Number($('span.nurascore').eq(0).text());
		var nursing_cond_score = Number($('span.nurbscore').eq(0).text());
		
		var result_part_1 = 0;
		var result_part_2 = 0;
		var result_part_2_tmp = 0;
		if(severity_mt_score >= 3 || severity_cond_score >= 3){
			if(nursing_cond_score >= 13){
				result_part_1 = 5;
			}else{
				result_part_1 = 4;
			}
		}
		if(result_part_1 == 0) {
			if(nursing_mt_score >= 3 || nursing_cond_score >= 7) {
				result_part_2_tmp = 3;
			}else {
				result_part_2_tmp = 0;
			}
		}else {
			result_part_2_tmp = 1;
		}
		if(result_part_2_tmp == 0){
			if(nursing_mt_score == 0 && nursing_cond_score == 0){
				result_part_2 = 1;
			}else {
				result_part_2 = 2;
			}
		}else {
			result_part_2 = 0;
		}
		result_part_2_tmp = result_part_2_tmp == 1 ? 0 : result_part_2_tmp;
		result = result_part_1 + result_part_2 + result_part_2_tmp;
		var index = Number(result);
		$('#final_classify').html(result);
		var index_highlight = 5 - index;
		$(".criterion_body tr").children("td").removeClass("highlighted");
		$(".criterion_body tr").eq(lastindex).children("td").removeClass("highlighted").end().end().eq(index_highlight).children("td").addClass("highlighted");
		return false;
	});

	$('input.severity_mt_radio').bind("click",function(){	
		add_score(this,'form#severity_mt input:radio:checked','.ascore','icu');
	});
	$('input.severity_cond_radio').bind("click",function(){	
		add_score(this,'form#severity_cond input:radio:checked','.bscore','icu');
	});
	$('input.nursing_mt_radio').bind("click",function(){	
		add_score(this,'form#nursing_mt input:radio:checked,this.value,','.nurascore','icu');
	});
	$('input.nursing_cond_radio').bind("click",function(){	
		add_score(this,'form#nursing_cond input:radio:checked','.nurbscore','icu');
	});
	$('input.normal_nursing_mt_radio').bind("click",function(){	
		add_score(this,'form#normal_nursing_treat input:radio:checked','.ascore','normal');
	});
	$('input.normal_nursing_cond_radio').bind("click",function(){	
		add_score(this,'form#normal_nursing_cond input:radio:checked','.bscore','normal');
	});
	
  /* hide some field for user regrist page */
  /* author: fox/yulia */
	var $role_hospital_input = $("#user-register-form .class-hospital").prev(),
		$role_ward_input = $('#user-register-form .class-ward').prev(),
		$role_admin_input = $('#user-register-form .class-admin').prev(),
		$role_sales_input = $('#user-register-form .class-sales').prev();
	
	//.form-item-asp-hospital-name-visiable, .form-item-asp-hospital-name-visiable-noajax, .form-item-asp-ward-name-visiable
	$role_admin_input.bind("click",function(){
		//JNJ Admin & JNJ Sales
		$('.form-item-asp-hospital-name-visiable, .form-item-asp-hospital-name-visiable-noajax, .form-item-asp-ward-name-visiable').hide();    
		$(this).attr("checked", true);
		$("#edit-name").focus();
		
	});
	$role_sales_input.bind("click",function(){
		//JNJ Admin & JNJ Sales
		$('.form-item-asp-hospital-name-visiable, .form-item-asp-hospital-name-visiable-noajax, .form-item-asp-ward-name-visiable').hide();    
		$(this).attr("checked", true);
		$("#edit-name").focus();
		
	});	
	$role_ward_input.bind("click",function(){
		// Regular user
		$('.form-item-asp-hospital-name-visiable-noajax').hide();
		$('.form-item-asp-hospital-name-visiable, .form-item-asp-ward-name-visiable').show();   
		$(this).attr("checked", true);
		$("#edit-name").focus();
	});
	$role_hospital_input.bind("click",function(){
		// Super user
		$('.form-item-asp-hospital-name-visiable-noajax').show();
		$('.form-item-asp-hospital-name-visiable, .form-item-asp-ward-name-visiable').hide(); 
		$(this).attr("checked", true);		
		$("#edit-name").focus();
	});	
	
	if(($role_admin_input.is(":checked"))||($role_sales_input.is(":checked"))){
		//JNJ Admin & JNJ Sales
		$('.form-item-asp-hospital-name-visiable, .form-item-asp-hospital-name-visiable-noajax, .form-item-asp-ward-name-visiable').hide();    
	}
	if($role_ward_input.is(":checked")){
		// Regular user
		$('.form-item-asp-hospital-name-visiable-noajax').hide();
		$('.form-item-asp-hospital-name-visiable, .form-item-asp-ward-name-visiable').show();   
	}
	if($role_hospital_input.is(":checked")){
		// Super user
		$('.form-item-asp-hospital-name-visiable-noajax').show();
		$('.form-item-asp-hospital-name-visiable, .form-item-asp-ward-name-visiable').hide();     
	}
	
	
//for contact-us  
	$(".form-radio").click(function() {
		var $choose_selectedvalue = $("input[name='choose']:checked").val();

		if ($choose_selectedvalue == 'yes') {
			$("#question-type-yes").removeClass('hidden');
			$("#question-type-no").addClass('hidden');
		}
		if ($choose_selectedvalue == 'no') {
			$("#question-type-no").removeClass('hidden');
			$("#question-type-yes").addClass('hidden');
		}
	});

    //wxm start
    //breadcrumb
    $("#navlinks ul.menu li").removeAttr("style");
    var bString = $(".breadcrumb");
    var breadcrumbNum = $(".breadcrumb a").length;
    var bText = $(".breadcrumb a:eq(1)").html();
    if(breadcrumbNum == 1){
            $(".breadcrumb a:eq(0)").addClass("b_three").css("margin-right","8px");
    }
    if(breadcrumbNum == 2){
        if(bText.indexOf("へルプ")>=0){
            $(".breadcrumb a:eq(0)").addClass("b_three").css("margin-right","0");
            $(".breadcrumb a:eq(1)").addClass("b_three").css("margin-right","8px");
        }else
            $(".breadcrumb a:eq(1)").addClass("b_two").css("margin-right","8px");
    }
    if(breadcrumbNum == 3){
        $(".breadcrumb a:eq(1)").addClass("b_two");
        $(".breadcrumb a:eq(2)").addClass("b_three").css("margin-right","8px");
    }
    if(breadcrumbNum > 0){
        bString.html(bString.html().replace(/»/g,""));
    }
    $("ul.primary li:eq(2)").hide();
    //wxm end

function add_score(input_class,form_id,target,is_normal){
	var result = 0;
	$(input_class).parent("td").addClass("highlighted").removeClass("normal").end().parent("td").
	prevAll("td").addClass("normal").removeClass("highlighted");
	$(input_class).parent("td").nextAll("td").addClass("normal").removeClass("highlighted");	
	$(form_id).each(function(){
		result += Number(this.value);
	});
	$(target).text(result);
	if(is_normal == "normal")
	{
		var partascore = Number($('span.ascore').eq(0).text());
		var partbscore = Number($('span.bscore').eq(0).text());
		var index = 0;
		var indexhighlight = 0;
		var lastindex = 0;
		//Calculation Logic for normal
		if(partascore >= 4 && partbscore >= 6)
		{
			index = 5;
		}
		else
		{
			if(partascore >= 2 && partbscore >= 3)
			{
				index = 4;
			}
			else
			{
				if(partascore >= 2 || partbscore >= 3)
				{
					index = 3;
				}
				else
				{
					if(partascore >= 1 || partbscore >= 1)
					{
						index = 2;
					}
					else
					{
						index = 1;
					}
				}
			}
		}
		$('#final_classify').html(index);
		indexhighlight = 5 - index;
		$(".criterion_body tr").children("td").removeClass("highlighted");
		$(".criterion_body tr").eq(indexhighlight).children("td").addClass("highlighted");
	}
};

(function($){
     $.fn.extend({
          center: function (options) {
               var options =  $.extend({ // Default values
                    inside:window, // element, center into window
                    transition: 0, // millisecond, transition time
                    minX:0, // pixel, minimum left element value
                    minY:0, // pixel, minimum top element value
                    vertical:true, // booleen, center vertical
                    withScrolling:true, // booleen, take care of element inside scrollTop when minX < 0 and window is small or when window is big
                    horizontal:true // booleen, center horizontal
               }, options);
               return this.each(function() {
                    var props = {position:'absolute'};
                    if (options.vertical) {
                         var top = ($(options.inside).height() - $(this).outerHeight()) / 2;
                         if (options.withScrolling) top += $(options.inside).scrollTop() || 0;
                         top = (top > options.minY ? top : options.minY);
                         $.extend(props, {top: top+'px'});
                    }
					
                    if (options.horizontal) {
                          var left = ($(options.inside).width() - $(this).outerWidth()) / 2;
                          if (options.withScrolling) left += $(options.inside).scrollLeft() || 0;
                          left = (left > options.minX ? left : options.minX);
                          $.extend(props, {left: left+'px'});
                    }
                    if (options.transition > 0) $(this).animate(props, options.transition);
                    else $(this).css(props);
                    return $(this);
               });
          }
     });
/*lxl_start*/
//remove class=b_two a
$('a.b_two').attr('href','javascript:;');
//ward-monthly-data-input readonly to disabled 
if($("form#aspcm-ward-monthly-data-input-form").length>0){
    $('input').each(function (i){
        if($(this).attr("readonly") == "readonly"){
            $(this).attr("disabled",true);
        }   
    });
}
//hide Preview trimmed version
if($("div.preview").length>0){
    if($("div.preview h3:first").next("div").attr('class') == 'node odd full-node node-type-aspcm_product' && $("div.preview h3:eq(1)").next("div").attr('class') == 'node even full-node node-type-aspcm_product'){
        $("div.preview h3:first").hide();
        $("div.preview h3:first").next("div").hide();
    }
}
/*lxl_end*/
})(jQuery);

function IsImageOk(img) {
// During the onload event, IE correctly identifies any images that
// weren’t downloaded as not complete. Others should too. Gecko-based
// browsers act like NS4 in that they report this incorrectly.
if (!img.complete) {
	return false;
}

// However, they do have two very useful properties: naturalWidth and
// naturalHeight. These give the true size of the image. If it failed
// to load, either of these should be zero.
if (typeof img.naturalWidth!= 'undefined' && img.naturalWidth == 0) {
	return false;
}

// No other way of checking: assume it’s ok.
return true;
}

function show_loading(){
	if(!IsImageOk($("#load-picture"))){
		$("#load-picture").attr('src','/sites/all/themes/asp_cm/images/loader/pure01.png');
	}
	if(!IsImageOk($(".loading-scroll"))){
		$(".loading-scroll").attr('src','/sites/all/themes/asp_cm/images/loader_horiz1s.gif');
	}
	
	var div_loader = $("#div-loader");
	var background_popup = $("#background-popup");
	var loader = $("#loader");

	background_popup.removeClass('hidden'); 
	div_loader.show();
	
	if($.browser.msie){
		loader.corner(settings5);
	}
	if($.browser.msie && $.browser.version=="6.0"){
		background_popup.css({"width": $(document).width()+"px","height": $(document).height()+"px"}).bgiframe();
        $("html").css("overflow","hidden");
		loader.css("top", (($(window).height() - $("#loader").outerHeight()) / 2) + $(window).scrollTop() + "px");
    } 
	var t = setTimeout(rotatepict,1650);
	countl ++;
}

function rotatepict(){
	countl ++;
	/*var img01 = new Image();
	img01.src = '/sites/all/themes/asp_cm/images/loader/pure0' + countl + '.png';
	img01.id = 'load-picture';
	*/
	
	$("#load-picture").attr('src','/sites/all/themes/asp_cm/images/loader/pure0' + countl + '.png');
	//if(IsImageOk(img01)){
	//	$("#load-picture").attr('src', img01.src);
    //}
	if(countl==10){  
		countl = 0; 
	}
	var t=setTimeout(rotatepict,1650);
}

function display_product_category() {
	var selected1 = $("#edit-field-aspcm-ref-product-category-und").val();
	var disabled1 = $("#edit-field-aspcm-product-type-und");
	var hiddenfield1 = $(".form-item-field-aspcm-product-type-und");
	if(disabled1.find("option:eq(0)").val() == '_none')
	{
		disabled1.find("option:eq(0)").text('');
	}
	if( $("#edit-field-aspcm-is-jnj-product-und-yes:checked").length > 0 )  //yes
	{
		if(selected1 == "35") {  //1
			disabled1.find("option").each(function(){
				if($(this).val() == '_none' || $(this).val() == '')
				{
					$(this).remove();
				}
			});
			hiddenfield1.removeClass("hidden");
		}
		if(selected1 == "34") {  //2
			disabled1.find("option").each(function(){ 			
				$(this).removeAttr("selected");
			});
			if(disabled1.find("option:eq(0)").val() != '_none')
			{	
				disabled1.prepend("<option>");
				disabled1.find("option:eq(0)").val('_none');
                                disabled1.find("option:eq(0)").attr("selected","selected");
			}
			hiddenfield1.addClass("hidden");	
			
		}
	}
	if( $("#edit-field-aspcm-is-jnj-product-und-no:checked").length > 0 )  //no
	{
		disabled1.find("option").each(function(){ 			
				$(this).removeAttr("selected");
		});
		if(disabled1.find("option:eq(0)").val() != '')
		{
			//disabled1.prepend("<option value='' selected='selected'></option>");
			disabled1.prepend("<option>");
			disabled1.find("option:eq(0)").val('_none').attr("selected", "selected").text('');
		}
		hiddenfield1.addClass("hidden");
	}
	
};

function display_jnj_item() {
	var selected1 = $("#edit-field-aspcm-company-und").val();
	var jnj_yes = $("#edit-field-aspcm-is-jnj-product-und-yes");
	var jnj_no = $("#edit-field-aspcm-is-jnj-product-und-no");

	if(selected1 == 'JnJ'){
		jnj_yes.attr("checked", "checked");
	}
	else {
		jnj_no.attr("checked", "checked");
	}
}

// [note: PO request to remove buttons that use this functionality]
var showpopup = function (popid, poptitle, popwidth, popheight) {
	$( popid ).dialog({
			resizable: false, 
			draggable: false,
			title: poptitle,
			dialogClass: 'popup-confirmation', 
			height: popheight,
			width: popwidth,
			modal: true,
			buttons: {
				"Yes": {
					text: 'はい',
					className: 'save-redirect', 
					'class': 'save-redirect', 
					click: function() {
						$( this ).dialog( "close" );
						window.location.href = $("#top-link-back a").attr('href');
						/*$("#edit-save-and-redirect").click();
						show_loading();*/
					}
				},
				"No": {
					text: 'いいえ',
					className: 'redirect', 
					click: function() {
							$( this ).dialog( "close" );
							
							
					}
				}
			},
			open: function(event, ui) { 
				if($.browser.msie && $.browser.version=="6.0"){
				makehover(".ui-dialog-buttonset button", "button-hover");
				makehover(".ui-dialog-buttonset .save-redirect", "save-redirect-button-hover");	}	
			}
		});	
};
function showpopup2( popid, poptitle, popclass) {

	$("#" + popid).dialog({
		resizable: false, 
		draggable: false,
		title: poptitle,
		dialogClass: popclass, 
		modal: true,
		width: 500,
		close: function(event, ui) {
			destroy_popup(popid);
			fixing_control_issues(popid);
		}
	});	
};
function showpopup3( popid, poptitle, popwidth, popheight, clickid) {
	$( popid ).dialog({
			resizable: false, 
			draggable: false,
			title: poptitle,
			dialogClass: 'popup-confirmation', 
			height: popheight,
			width: popwidth,
			modal: true,
			buttons: {
				"Yes": {
					text: 'はい',
					className: 'save-redirect', 
					'class': 'save-redirect', 
					click: function() {
							$( this ).dialog( "close" );
							show_loading();
							$(clickid).click();
							return false;
						}
				},
				"No": {
					text: 'いいえ',
					className: 'redirect', 
					click: function() {
							$( this ).dialog( "close" );
							return false;
						}
				}
			},
			open: function(event, ui) { 
				if($.browser.msie && $.browser.version=="6.0"){
				makehover(".ui-dialog-buttonset button", "button-hover");
				makehover(".ui-dialog-buttonset .save-redirect", "save-redirect-button-hover");	}	
			}
		});	
};
//add by Benny for ACMSUPP-13 20111130 begin
function showpopup4( popid, poptitle, popwidth, popheight) {
	if ($.browser.version=="6.0"){
		popwidth="575";
	} 
	
	$( popid ).dialog({
			resizable: false, 
			draggable: false,
			title: poptitle,
			dialogClass: 'popup-confirmation', 
			height: popheight,
			width: popwidth,
			modal: true,
			buttons: {
				"Yes": {
					text: '同意します',
					width: '117px',
					className: 'save-redirect', 
					click: function() {
							$( this ).dialog( "close" );
							//show_loading();
							//$(clickid).click();
							return false;
						}
				},
				"No": {
					text: '同意しません',
					width: '117px',
					className: 'redirect', 
					click: function() {
							$( this ).dialog( "close" );
							window.location.href="../../logout";
							return false;
						}
				}
			},
			open: function(event, ui) { 
				if($.browser.msie && $.browser.version=="6.0"){
				makehover(".ui-dialog-buttonset button", "button-hover");
				makehover(".ui-dialog-buttonset .save-redirect", "save-redirect-button-hover");	}	
			}
			
		});	
	$('a.ui-dialog-titlebar-close').hide();
};
//add by Benny for ACMSUPP-13 20111130 end

//[Products finder]
function showpopup5(popid, poptitle, popwidth, popheight) {
	
	$( popid ).dialog({
			resizable: false, 
			draggable: false,
			title: poptitle,
			dialogClass: 'popup-confirmation product-finder-popup', 
			height: popheight,
			width: popwidth,
			modal: true,
			open: function(event, ui) {   
				/*if($.browser.msie && $.browser.version=="6.0"){
					makehover(".ui-dialog-buttonset button", "button-hover");
					makehover(".ui-dialog-buttonset .save-redirect", "save-redirect-button-hover");	
				}*/
			},
			close: function(event, ui){
				if(($.browser.msie) && ($.browser.version=="6.0")){
					if($('#background-popup-dialog.hidden').length <= 0){
						$('#background-popup-dialog').addClass('hidden');
						$("html").css("overflow-y","auto");
					}
				}
				$('#search-result-content, .custom-perpush-info, #no-search-result').remove();
				arr_checked = [];
				arr_unchecked = [];
				//alert('arr_checked: ' + arr_checked.join(';'));
				//alert('arr_unchecked: ' + arr_unchecked.join(';'));
				//alert('arr_savetemp: ' + arr_savetemp.join(';'));
			},
			buttons: {
				"Save Current Page": {
					text: '一時保存',
					'class': 'save-redirect',
					click: function() {
							//#edit-srch-is-jnj, #edit-srch-pgroup, 
							$("#edit-srch-cname, #edit-srch-pname, #edit-srch-submit, #edit-product-submit, .save-redirect, .redirect").addClass('disabled').attr('disabled', 'disabled');
							$('.save-redirect').after('<img id="loader-img" src="/sites/all/themes/asp_cm/images/ajax-loading-small.gif" width="16" height="16" title="loading" alt="loading" />');
							
							save_temp();
							//alert('arr_checked: ' + arr_checked.join(';'));
							//alert('arr_unchecked: ' + arr_unchecked.join(';'));
							//alert('arr_savetemp: ' + arr_savetemp.join(';'));
							$("#edit-srch-cname, #edit-srch-pname, #edit-srch-submit, #edit-product-submit, .redirect, .save-redirect").removeClass('disabled').removeAttr('disabled');
							$('#loader-img').remove();
							return false;
						} // click  
				}, // Yes button
				"Submit": {
					text: '登録完了',
					'class': 'redirect',
					click: function() {
							$("#edit-srch-cname, #edit-srch-pname, #edit-srch-submit, #edit-product-submit, .save-redirect, .redirect").addClass('disabled').attr('disabled', 'disabled');
							$('.redirect').after('<img id="loader-img" src="/sites/all/themes/asp_cm/images/ajax-loading-small.gif" width="16" height="16" title="loading" alt="loading" />');
							save_temp();
							kod = arr_savetemp.join(';');
							$.ajax({
								type: 'post',
								url: '/product/save/ajax',
								dataType: 'text',
								data: {
									selected_products: kod
								},
								success : function(data) {
									//alert('kod:'+kod);
									$('.selected-products-table').remove();
									$("#add-products-wrapper").before(data);
									$('.custom-perpush-text').numeric({allow:"."});
									$("#edit-srch-cname, #edit-srch-pname, #edit-srch-submit, #edit-product-submit, .redirect, .save-redirect").removeClass('disabled').removeAttr('disabled');
									$('#loader-img').remove();
									return false;
								},
								error : function() {
								  // request error
								},
								complete : function(){
									//arr_checked = [];
									//arr_unchecked = [];
								}
							});
							$(this).dialog("close");
							return true;
						}
				}
			}
		});	
}
function save_temp(){
		var pid1 = val1 = '';
		var arr_ids = [];
		$.each(arr_checked, function(n,value){
			arr_savetemp.push(value);
		}); 
		$("#search-result-content input:checked").each(function(){
				//alert($(this).val());
				var pid = $(this).val();
				var val_pid = $(this).parent().parent().find('.custom-perpush-text').val(); //$("#custom-perpush-"+pid).val();
				var vcon = pid + ':' + val_pid;
				//alert('new val: '+val_pid);

				$.each(arr_savetemp, function(n,value){
						pid1 = value.split(':')[0];
						val1 = value.split(':')[1];
						//if(pid1 == pid){
							//alert('old val: '+val1);
						//}
						if( ( pid1 == pid) && (val1 != val_pid) ){
							//alert('replace');
							arr_savetemp = $.grep(arr_savetemp, function(a){ return a != value; });
							arr_savetemp.push(vcon);
						}
				});
			});
			$.each(arr_savetemp, function(n,value){
				arr_ids.push(value.split(':')[0]);
			});
			$.each(arr_unchecked, function(n,value){
				pid1 = value.split(':')[0];
				if( $.inArray(pid1, arr_ids)!=-1 ){
					arr_savetemp = $.grep(arr_savetemp, function(a){ return a.split(':')[0] != pid1; });
				}
			});
			arr_checked = [];
			arr_unchecked = [];
}
function makehover(id, hoverclass){
	$(id).hover(
		function(){
			$(this).addClass(hoverclass);
		},function(){
			$(this).removeClass(hoverclass);
		});
};
function show_current_table(){
			$("#estimated, #actual, .estimated-comment, .actual-comment").hide();
			$("#"+$("#severity_level").val() + ", ." + $("#severity_level").val() + "-comment").show();
}
function calculate_actual(){
		var a_sum =0;
		$(".actual-patnt").each(function(){
				if($(this).val()!=''){
					a_sum = a_sum + parseInt($(this).val());
				}
		});
		$("#actual_total").val(a_sum);
};
function calculate_estimate(){
	
		var t=0;
		var tt=0;
		var i=0;
		
		//by Columns
		$("#estimated tr").each(function(){
			$(this).find("td:eq("+ (f1+1) +") .estimated-day").each(function(){
				if($(this).val()==' '){$(this).val('');}
				if($(this).val()!=''){
					t = t + parseInt($(this).val());
				}
			});
		});
		//by Rows
		$("#estimated tr:eq("+ (f2) + ") .estimated-day").each(function(){
				if(($(this).val()!='')/*&&($(this).val()!=0)*/){
					tt = tt + parseInt($(this).val());
					i++;
				}
		});
		
		a = parseInt($("#edit-count-days").val());
		$("#estimated tr:last td:eq("+ (f1+1) +") input").val(t);
		$("#estimated tr:eq("+ f2 +") .estimated-total").val(tt);
		$("#estimated tr:eq("+ f2 +") .estimated-estimated-count").val(i);
		if(i==0){
			avg = 0;
		}else{
			avg = (tt/i)*a;
		}
		$("#estimated tr:eq("+ f2 +") .estimated-avg").val(Math.round(avg));
		//alert('tt='+tt+"i="+i+"a="+a);
		
		recalculate_total();
};
// Calculate from first load
function recalculate_estimate(){
	$(".estimated-day").each(function(){

		f = $(this).parents('td');
		ff = $(this).parents('tr');
		f1 = parseInt($(this).parents('tr').find('td').index(f) ) - 1 ;
		f2 = parseInt($(this).parents('table').find('tr').index(ff) ) ;
		
		calculate_estimate();
		
	});
};
//Calculate total values for last 3 columns
function recalculate_total(){
		var total = new Array(0,0,0);
		$("#estimated tr:not(:last)").each(function(){
			for(i=8;i<11;i++){
				$(this).find("td:eq("+ i +") input").each(function(){
					if($(this).val()!=''){
						total[i-8] = total[i-8] + parseInt($(this).val());
					}
				});
			}
			
		});
		for(i=8;i<11;i++){
			$("#estimated tr:last td:eq("+ i +") input").val(total[i-8]);
		}
}
function ajax_ward_js(hospit,n){
		ward_wrapper = $(".ward-field .form-item");
		ward_input = ward_wrapper.find("select");
		
		kod ='';
		kod += "hospital=" + encodeURI(hospit) + "&ajax=1";
		kod += "&date="+$("#date").val() + "&form_build_id="+$("input:hidden[name='form_build_id']").val() + "&form_id=aspcm_ward_annually_data_view";
		if( ward_input.val() > 0){ kod += "&ward=" + ward_input.val(); }
		if (n!=''){ kod +='&view=1'; }
		if($("#edit-aspcm-ward-annually-data-view"+n+"-form-token").length>0){ kod += "&form_token="+$("#edit-aspcm-ward-annually-data-view"+n+"-form-token").val(); }
		ward_input.attr("disabled","disabled");
		$("#edit-hospital"+n).css('background-position','100% -17px');
		$.ajax({
				type : "POST",
				url : "/ward/js/",
				dataType: "json",
				data: kod,
				success : function(matches) {
						if(matches['status']){
							//alert(encodeURI(hospit));
							ward_wrapper.replaceWith(matches['data']);
							//alert(matches['data']);
						}
						$("#edit-hospital"+n).val(hospit).css('background-position','100% 2px');
						//disable_search();
					return false;
				},
				complete : function(XMLHttpRequest, textStatus) {
				  return false;
				},
				error : function() {
				  // request error
				}
		});
}
function ajax_user_js(hospit){
		ward_wrapper = $("#user-register-form .form-item-asp-ward-name-visiable");
		kod ='';
		kod += "asp_hospital_name_visiable=" + encodeURI(hospit) + "&ajax=1";
		kod += "&form_build_id="+$("input:hidden[name='form_build_id']").val() + "&form_id=user_register_form";
		if($("input:hidden[name='form_token']").length>0){ kod += "&form_token="+$("input:hidden[name='form_token']").val(); }
		$("#edit-asp-hospital-name-visiable").addClass('throbbing');
		$.ajax({
				type : "POST",
				url : "/ward/js/",
				dataType: "json",
				data: kod,
				success : function(matches) {
						if(matches['status']){
							ward_wrapper.replaceWith(matches['data']);
						}
						$("#edit-asp-hospital-name-visiable").val(hospit).removeClass('throbbing');
					return false;
				},
				complete : function(XMLHttpRequest, textStatus) {
				  return false;
				},
				error : function() {
				  // request error
				  
				}
		});
}
function ajax_user_profile_js(hospit){
                ward_wrapper = $("#user-profile-form .form-item-asp-ward-name-visiable");
		kod ='';
		kod += "asp_hospital_name_visiable=" + encodeURI(hospit) + "&ajax=1";
		kod += "&form_build_id="+$("input:hidden[name='form_build_id']").val() + "&form_id=user_profile_form";
		if($("input:hidden[name='form_token']").length>0){ kod += "&form_token="+$("input:hidden[name='form_token']").val(); }
		$("#edit-asp-hospital-name-visiable").addClass('throbbing');
		$.ajax({
				type : "POST",
				url : "/ward/js/",
				dataType: "json",
				data: kod,
				success : function(matches) {
						if(matches['status']){
							ward_wrapper.replaceWith(matches['data']);
						}
						$("#edit-asp-hospital-name-visiable").val(hospit).removeClass('throbbing');
					return false;
				},
				complete : function(XMLHttpRequest, textStatus) {
				  return false;
				},
				error : function() {
				  // request error
				}
		});
}
function ajax_graph_js(hospit){

		kod ='';
		kod += "hospital=" + encodeURI(hospit) + "&ajax=1";
		kod += "&form_build_id="+$("input:hidden[name='form_build_id']").val() + "&form_id=aspcm_graph_form";
		//if(($("#edit-ward").val()!='')||($("#edit-ward").val()!=null)){ kod += "&ward[]=" + $("#edit-ward").val(); }
		
		ew = $("#edit-ward").val();
		//alert(ew);
		//alert(ew[0]);
		if(ew!=null){ kod += "&ward[]=" + ew[0];}
		
		if($("#edit-aspcm-graph-form-form-token").length>0){ kod += "&form_token="+$("#edit-aspcm-graph-form-form-token").val(); }
		$("#edit-ward").attr("disabled","disabled");
		$("#edit-hospital").css('background-position','100% -17px');
		$.ajax({
				type : "POST",
				url : "/ward/jsgraph/",
				dataType: "json",
				data: kod,
				success : function(matches) {
						if(matches['status']){
							//alert(matches['data']);
							$('#edit-ward-wrapper').replaceWith(matches['data']['ward']);
							//$('#edit-major-infection-rate-wrapper').replaceWith(matches['data']['major_diseases']);
							//$('#edit-minor-infection-rate-wrapper').replaceWith(matches['data']['minor_diseases']);
							//form-item-major-infection-rate  form-item-minor-infection-rate
							$('#form-item-major-infection-rate').replaceWith(matches['data']['major_diseases']);
							$('#form-item-minor-infection-rate').replaceWith(matches['data']['minor_diseases']);
						}
						$("#edit-hospital").val(hospit).css('background-position','100% 2px');
						$("#aspcm-graph-form #edit-ward option").attr("selected","selected");
                        /*@author_lxl_start lxl_graph_single-graph*/
                        if($("#edit-ward2").length>0){
                            $("#aspcm-graph-form #edit-ward option").attr("selected",false);
                            $("#aspcm-graph-form #edit-ward2 option").remove();
                        }
                        /*@author_lxl_end*/
					return false;
				},
				complete : function(XMLHttpRequest, textStatus) {
				  return false;
				},
				error : function() {
				  // request error
				  
				}
		});
}

function destroy_popup(pname){
		newSubForm = $("#"+pname).clone().removeAttr('class').removeAttr('style').hide();
		$("#"+pname).dialog("destroy").remove();
		$("form").append(newSubForm);
}
function popup_create_event(popupname){
	switch(popupname)
    {
        case 'popup-major': 
			$("#edit-generate-major").bind("click", function(){
					//[removed validation] if(($("#aspcm-comparison-graph-form").length>0)||((check_date_unit())&&(($("#aspcm-graph-form").length>0)))){
					if(($("#aspcm-comparison-graph-form").length>0)||($("#aspcm-graph-form").length>0)){
							destroy_popup("popup-major");
							fixing_control_issues("popup-major");
							$("#edit-generate-main").click();
					}
					return false;
					
			});
			break;
        case 'popup-minor': 
			$("#edit-generate-minor").bind("click", function(){
					// [removed validation] if(($("#aspcm-comparison-graph-form").length>0)||((check_date_unit())&&(($("#aspcm-graph-form").length>0)))){
					if(($("#aspcm-comparison-graph-form").length>0)||($("#aspcm-graph-form").length>0)){
							destroy_popup("popup-minor");
							fixing_control_issues("popup-minor");
							$("#edit-generate-main").click();
					}
					return false;
					
			});
			break;
        case 'popup-horiz':
			$("#edit-generate-horizontal").bind("click", function(){
					// [removed validation] if(($("#aspcm-comparison-graph-form").length>0)||((check_date_unit())&&(($("#aspcm-graph-form").length>0)))){
					if(($("#aspcm-comparison-graph-form").length>0)||($("#aspcm-graph-form").length>0)){
							destroy_popup("popup-horiz");
							fixing_control_issues("popup-horiz");
							$("#edit-generate-main").click();
					}
					return false;
					
			});
			break;
    }
	fixing_control_issues(popupname);
		  
	// Major & Minor Selects Disable-Enable

	$("#edit-minor-fixed-value, #edit-major-fixed-value").numeric();
	$("#edit-horizontal-start, #edit-horizontal-end").numeric({allow:"/"});
	
	$("#edit-major-fixed-value, #edit-minor-fixed-value").attr('disabled','disabled');
        
        //add by yueshuo 2016053
        if (jQuery('body').hasClass('not-front section-graph page-graph-single-graph'))  
            $("#edit-major-infection-rate, #edit-minor-infection-rate, #edit-major-wash-number, #edit-major-antisepic-volume, #edit-minor-wash-number, #major-axis-bars input, #minor-axis-bars input, #edit-minor-antisepic-volume, #aspcm-graph-form #edit-unit-of-period-displayed").attr('disabled','disabled');//modified by yueshuo 201604 backup
	else
          $("#edit-major-infection-rate, #edit-minor-infection-rate, #edit-major-wash-number, #edit-major-antisepic-volume, #major-axis-bars input, #minor-axis-bars input,  #aspcm-graph-form #edit-unit-of-period-displayed").attr('disabled','disabled');//modified by yueshuo 201604
      //end 
//        $("#edit-major-infection-rate, #edit-minor-infection-rate, #edit-major-wash-number, #edit-major-antisepic-volume, #edit-minor-wash-number, #major-axis-bars input, #minor-axis-bars input, #edit-minor-antisepic-volume, #aspcm-graph-form #edit-unit-of-period-displayed").attr('disabled','disabled');//modified by yueshuo 201604 backup
//	$("#edit-minor-value-none, #edit-major-value-none").removeAttr('disabled','disabled');
	$("#popup-major #major_"+$("#edit-major-value").val()).attr("checked","checked");
	
	if(($("#major-axis input:radio:checked").val() != 'hand_washing')&&($("#major-axis input:radio:checked").val() != 'antiseptic_used')&&($("#aspcm-graph-form").length>0)){
			$("#edit-major-single-bar-single").attr("checked","checked");
	}
	if(($("#minor-axis input:radio:checked").val() != 'hand_washing')&&($("#minor-axis input:radio:checked").val() != 'antiseptic_used')&&($("#aspcm-graph-form").length>0)){
			$("#edit-minor-single-line-single").attr("checked","checked");
	}

	popup_event_content('major','minor');
	popup_event_content('minor','major');
	if($("#horiz-axis input:radio:checked").val() == 'period'){
		$("#edit-unit-of-period-displayed").removeAttr('disabled');
	}
	if($("#edit-horizontal-axis-ward:checked").length>0){
		$("#edit-unit-of-period-displayed").attr('disabled','disabled');
	}
	if($("#edit-horizontal-axis-period:checked").length>0){
		$("#edit-unit-of-period-displayed").removeAttr('disabled');
	}

	$("#edit-horizontal-axis-ward").bind("click",function(){
		$("#edit-unit-of-period-displayed").attr('disabled','disabled');
		//$("#edit-horizontal-start, #edit-horizontal-end").datepicker("destroy");
	});
	$("#edit-horizontal-axis-period").bind("click",function(){
		$("#edit-unit-of-period-displayed").removeAttr('disabled');
	});
	
	// [removed validation] if($("#aspcm-graph-form").length>0) { check_date_unit(); }
	
}
function fixing_control_issues(popupname){
	
	// Fixing issue with Radioboxes (especialy for IE8)
	if($.browser.version>="8.0"){
		$("#" + popupname+ " input:radio").bind('click', function(){
				vv = $(this).val(); // get value
				popup_id = popupname;
				//alert($(this).parents(".form-item").attr("id"));
				pids[popup_id].push( $(this).parents(".form-item").attr("id") + " input:radio");  //put id to array
				vals[popup_id].push('checked'); // put value to array
				$(this).parents(".radio-box").find("input:radio").removeAttr('checked'); 
				$(this).attr('checked','checked');  //set checked
		});
	}
	// Fixing issue with Selectboxes (especialy for FF, Safari, Opera)
	//if(!($.browser.msie)){
		$("#" + popupname+ " select").bind('change', function(){
			popup_id = popupname;
			pids[popup_id].push( $(this).attr('id'));  //put id to array
			vv = $(this).val();
			vals[popup_id].push(vv); // value to array
			$(this).find("option").removeAttr('selected'); 
			$(this).find("option[value='"+vv+"']").attr('selected','selected'); 
			$(this).val(vv);  //set value
			//alert(vv);
		});
	//}		
	jQuery.each(pnames, function(ni, nval) { // loopfor all popups
			jQuery.each(pids[nval], function(i, val) {
				if(vals[nval][i]!='checked'){
					  $("#" + nval + " #" + val).val(vals[nval][i]); // set values
				}else{
					  $("#" + nval + " #" + val).attr('checked','checked');
					  //alert(val + vals[nval][i]);
				}
			});
	});
}
function check_addit_radio(popup_name,flag){
	var select_bar = $("#"+popup_name+"-axis select:enabled").val();
	var input_bar = $("#"+popup_name+"-axis-bars input");
	//alert(select_bar);
	if((select_bar=='all_patient')||(select_bar=='all_patient_1000')){
		input_bar.removeAttr('disabled','disabled');
		if(flag){
			$("#edit-" + popup_name + "-single-bar-multiple").attr('checked','checked');
			$("#edit-" + popup_name + "-single-line-multiple").attr('checked','checked');
		}
	}else{
			input_bar.attr('disabled','disabled');
			$("#edit-" + popup_name + "-single-bar-single").attr('checked','checked');
			$("#edit-" + popup_name + "-single-line-single").attr('checked','checked');
	}
}
//open a window for footer links && for popup to popup
function open_new_window(wUrl, op, width) {
  newWin = window.open(
          wUrl,
          op,
          "width=" + width + ",height=690,toolbar=no,menubar=no,directories=no,location=no,status=no,scrollbars=yes,resizable=yes");
  newWin.opener = self;
  newWin && newWin.focus();
};

function check_date_unit() {
	var mess_elms = $(".page-graph-single-graph .ui-dialog div.error, .main-mess");
	
	if($(".main-mess").length==0){ 
			$("#content-top").before('<div class="error messages main-mess hidden"></div>');
	}
	dstart_t = $(".page-graph-single-graph #edit-horizontal-start").val().split('/');
	dend_t = $(".page-graph-single-graph #edit-horizontal-end").val().split('/');
	
	var intYear = /^([1-2][0-9][0-9][0-9])$/;
	var intDay = /^([0-1][0-9])$/;
    if((!intYear.test(dstart_t[0]))||(!intDay.test(dstart_t[1]))||(!intYear.test(dend_t[0]))||(!intDay.test(dend_t[1]))||(dstart_t[1]>12)||(dend_t[1]>12)||(dstart_t[1]<1)||(dend_t[1]<1)) { 
		
		mess_elms.removeClass('hidden').text('期間の日付は正しいフォーマットで入力してください(YYYY/MM)。');
		return false;
	}else{
			dstart = new Date(dstart_t[0], (dstart_t[1]-1));
			dend = new Date(dend_t[0], (dend_t[1]-1));
			
			if(Date.parse(dstart)>Date.parse(dend)){
				mess_elms.removeClass('hidden').text("終了日付は開始日付より後の日付で入力してください。");
				return false;
			}else{
				return true;
			}
	}
	
}
function ward_search(hosp_id, flag){

			v = $("#autocomplete .selected div").text();
			if((v==null)||(v=='')){
				if(($(hosp_id).val()!=null)&&($(hosp_id).val()!='')){
					v = $(hosp_id).val();
					ward_search_switch(flag);
				}
			}else{
				ward_search_switch(flag);
			}
			$("#autocomplete").remove();
}
function ward_search_switch(flag){
					switch(flag)
					{
						case 'graph': 
							ajax_graph_js(v);
							break;
						case 'ward': 
							ajax_ward_js(v,'');
							break;
						case 'ward1': 
							ajax_ward_js(v,'-1');
							break;	
						case 'user': 
							ajax_user_js(v);
							break;
						case 'user_profile': 
							if($(".hospital-name-only").length==0) {
								ajax_user_profile_js(v);
							}
							break;
					}
}

function popup_event_content(popupname,popupname2){
	var axis_radio_checked = $("#" + popupname + "-axis input:radio:checked");
	var edit_fixed_value = $("#edit-" + popupname + "-fixed-value");
	var max_axis_radio = $("#max-" + popupname + "-axis input:radio");
	
	if($("#max-" + popupname + "-axis input:radio:checked").val()=='fixed'){
		edit_fixed_value.removeAttr('disabled');
	}
	
	if( axis_radio_checked.val() == 'hand_washing'){
		$("#edit-" + popupname + "-wash-number").removeAttr('disabled');
	}
	if( axis_radio_checked.val() == 'antiseptic_used'){
		$("#edit-" + popupname + "-antisepic-volume").removeAttr('disabled');
	}
	if( axis_radio_checked.val() == 'infection_rate'){
		$("#edit-" + popupname + "-infection-rate").removeAttr('disabled');
	}
	if($("#edit-" + popupname + "-value-none").attr('checked') == 'checked'){
		max_axis_radio.attr('disabled','disabled');
		edit_fixed_value.attr('disabled','disabled');
		$("#edit-" + popupname2 + "-value-none").attr('disabled','disabled').removeAttr('checked');
	}	
	
	check_addit_radio(popupname,false);
	
	$("#" + popupname + "-axis input:radio").bind("click",function(){
		$("#" + popupname + "-axis select").attr('disabled','disabled');
		max_axis_radio.removeAttr('disabled');
		
		if($(this).val() == 'hand_washing'){
			$("#edit-" + popupname + "-wash-number").removeAttr('disabled');
		}
		if($(this).val() == 'antiseptic_used'){
			$("#edit-" + popupname + "-antisepic-volume").removeAttr('disabled');
		}
		if($(this).val() == 'infection_rate'){
			$("#edit-" + popupname + "-infection-rate").removeAttr('disabled');
		}
		if(($(this).val() != 'hand_washing')&&($(this).val() != 'antiseptic_used')&&($("#aspcm-graph-form").length>0)){
			$("#edit-" + popupname + "-single-bar-single").attr("checked","checked");
		}
		if( max_axis_radio.filter(":checked").val()=='fixed'){
			edit_fixed_value.removeAttr('disabled');
		}
		check_addit_radio(popupname,true);		
	});

	$("#major-axis select, #minor-axis select").bind("change",function(){
		check_addit_radio('major',true);
		check_addit_radio('minor',true);
	});

	max_axis_radio.bind("click",function(){
		edit_fixed_value.attr('disabled','disabled');
		if($(this).val()=='fixed'){
			edit_fixed_value.removeAttr('disabled');
		}
	});
	
	$("#edit-" + popupname + "-value-none").bind("click",function(){
		max_axis_radio.attr('disabled','disabled'); 
		edit_fixed_value.attr('disabled','disabled');
	});	
}

function fix_japanese_input(hosp_id, name){
		if(flag2){
			Drupal.jsAC.prototype.onkeyup = function (input, e) {
			  flag2 = false;
			  if (!e) {
				e = window.event;
			  }
			  switch (e.keyCode) {
				case 33: // page up
				case 34: // page down
				case 37: // left arrow
				case 38: // up arrow
				case 39: // right arrow
				case 40: // down arrow
				  return true;
				case 13: if($("#autocomplete").length>0){  
							if($("#autocomplete .selected").length>0){
								this.hidePopup(e.keyCode); 
								if((input.id != 'edit-asp-hospital-name-visiable-noajax')&&(name!='graph_comp')){		
									//alert(input.id);
									ward_search(hosp_id, name);
								}
							} 
						}else{ 
							if (input.value.length > 0) 
								this.populatePopup();
						} 
						break;
				case 27: // esc
				  this.hidePopup(e.keyCode);
				  return true;

				default: // all other keys
				  if (input.value.length > 0){
					this.populatePopup();
				  }else{
					this.hidePopup(e.keyCode);
				  }
				  return true;
			  }
			};
		}
}
function fix_mouse_click(ws_page){

	/**
	 * Positions the suggestions popup and starts a search.
	 */
	Drupal.jsAC.prototype.populatePopup = function () {
	  var $input = $(this.input);
	  var position = $input.position();
	  // Show popup.
	  if (this.popup) {
		$(this.popup).remove();
	  }
	  this.selected = false;
	  this.popup = $('<div id="autocomplete"></div>')[0];
	  this.popup.owner = this;
	  $(this.popup).css({		
		marginTop: this.input.offsetHeight +'px',
		width: (this.input.offsetWidth - 4) +'px',
		display: 'none'
	  });
	  $input.before(this.popup);
	 
	  // Do search.
	  this.db.owner = this;
	  this.db.search(this.input.value);
	};

  Drupal.jsAC.prototype.found = function (matches) {
  // If no value in the textfield, do not show the popup.
  if (!this.input.value.length) {
    return false;
  }
  f_hosp = "#" + this.input.id;
  // Prepare matches.
  var ul = $('<ul></ul>');
  var ac = this;
  for (key in matches) {
    $('<li></li>')
      .html($('<div></div>').html(matches[key]))
      .mousedown(function () { ac.select(this); })
      .mouseover(function () { ac.highlight(this); })
      .mouseout(function () { ac.unhighlight(this); })
	  .click(function () { 
		if(f_hosp != '#edit-asp-hospital-name-visiable-noajax'){		
                    ward_search(f_hosp, ws_page);
		}
	  })
      .data('autocompleteValue', key)
      .appendTo(ul);
  }

  // Show popup with matches, if any.
  if (this.popup) {
    if (ul.children().length) {
      $(this.popup).empty().append(ul).show();
      $(this.ariaLive).html(Drupal.t('Autocomplete popup'));
	  //Hover issue for IE6
		if(($.browser.version=="6.0")&&((ws_page=='graph')||(ws_page=='graph_comp'))){
			$("#autocomplete").bgiframe();
		}
    }
    else {
      $(this.popup).css({ visibility: 'hidden' });
      this.hidePopup();
    }
  }
};
}
//[add] Product treeview with count of checkboxes (Hyper care) 
function check_treeview(flag){
		treeview_legend.each(function(){
				s = $(this).parent().find(".form-checkbox:checked").length;
				$(this).css("color",'red');
				
				if(flag){
					$(this).find(".count").text('('+s+')');
				}else{
					$(this).html( $(this).text() + ' <span class="count">('+s+')</span>' );
				}
		});
}

/**
 * Attach handlers to evaluate the strength of any password fields and to check
 * that its confirmation is correct.
 */
Drupal.behaviors.password = {
  attach: function (context, settings) {
    var translate = settings.password;
    $('input.password-field', context).once('password', function () {
      var passwordInput = $(this);
      var innerWrapper = $(this).parent();
      var outerWrapper = $(this).parent().parent();

      // Add identifying class to password element parent.
      innerWrapper.addClass('password-parent');

      // Add the password confirmation layer.
      $('input.password-confirm', outerWrapper).parent().prepend('<div class="password-confirm">' +' <span></span></div>').addClass('confirm-parent');
      var confirmInput = $('input.password-confirm', outerWrapper);
      var confirmResult = $('div.password-confirm', outerWrapper);
      var confirmChild = $('span', confirmResult);

      // Add the description box.
      var passwordMeter = '<div class="password-strength"><div class="password-strength-text" aria-live="assertive"></div><div class="password-strength-title">' + '</div><div class="password-indicator"><div class="indicator"></div></div></div>';
      $(confirmInput).parent().after('<div class="password-suggestions description"></div>');
      $(innerWrapper).prepend(passwordMeter);
      var passwordDescription = $('div.password-suggestions', outerWrapper).hide();

      // Check the password strength.
      var passwordCheck = function () {

        // Evaluate the password strength.
        var result = Drupal.evaluatePasswordStrength(passwordInput.val(), settings.password);

        // Update the suggestions for how to improve the password.
        if (passwordDescription.html() != result.message) {
          passwordDescription.html(result.message);
        }

        // Only show the description box if there is a weakness in the password.
        if (result.strength == 100) {
          passwordDescription.hide();
        }
        else {
          passwordDescription.show();
        }

        // Adjust the length of the strength indicator.
        $(innerWrapper).find('.indicator').css('width', result.strength + '%');

        // Update the strength indication text.

        $(innerWrapper).find('.password-strength-title').html(translate['strengthTitle']);
        var _arr_tips=new Array();
        _arr_tips[translate.weak]="error";
        _arr_tips[translate.fair]="warning";
        _arr_tips[translate.strong]="ok";
        $(innerWrapper).find('.password-strength-text').html(
        "<span class=" + _arr_tips[result.indicatorText] + ">" + result.indicatorText + "</span>");

		if(passwordInput.val() == ''){
			passwordDescription.hide();
			$(innerWrapper).find('.password-strength-title').html('');
			$(innerWrapper).find('.password-strength-text').html('');
		}
		
        passwordCheckMatch();
      };

      // Check that password and confirmation inputs match.
      var passwordCheckMatch = function () {

        if (confirmInput.val()) {
          var success = passwordInput.val() === confirmInput.val();

          // Show the confirm result.
          confirmResult.css({ visibility: 'visible' });

          // Remove the previous styling if any exists.
          if (this.confirmClass) {
            confirmChild.removeClass(this.confirmClass);
          }
          
          // Fill in the success message and set the class accordingly.
          $(outerWrapper).find('div.password-confirm').html(translate['confirmTitle'] + '<span></span>');
          var confirmClass = success ? 'ok' : 'error';
          $(outerWrapper).find('div.password-confirm span').html(translate['confirm' + (success ? 'Success' : 'Failure')]).addClass(confirmClass);
          this.confirmClass = confirmClass;
        }
        else {
          confirmResult.css({ visibility: 'hidden' });
        }
      };

      // Monitor keyup and blur events.
      // Blur must be used because a mouse paste does not trigger keyup.
      passwordInput.keyup(passwordCheck).focus(passwordCheck).blur(passwordCheck);
      confirmInput.keyup(passwordCheckMatch).blur(passwordCheckMatch);
    });
  }
};

})(jQuery);

function ward_select_change(){
	jQuery("#edit-search").trigger("click");
}
// @author_lxl_start
function comparison_graph_radio(radio_val){
	if( radio_val == 'radio_no' ){
		jQuery("div.line-spec").find("input[type=checkbox]").each(function(){
			jQuery(this).attr("checked",false);
			jQuery(this).attr("disabled",true);
			jQuery("#comparison_graph_grey").addClass('comparison_graph_grey');
		});
	}
	if( radio_val == 'radio_yes' ){
		jQuery("div.line-spec").find("input[type=checkbox]").each(function(){
			jQuery(this).attr("disabled",false);
			jQuery("#comparison_graph_grey").attr('class','');
		});
	}
}
// @author_lxl_end