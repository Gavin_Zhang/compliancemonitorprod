<div id="skip-nav"><a href="#main">Skip to Content</a></div>

<div id="bg1"><div id="bg2">

<div id="top_bg" class="page0">
<div class="sizer0">
<div id="topex" class="expander0">
<div id="top_left">
<div id="top_right">
<div id="headimg">

<?php if ($page['above']): ?>
	<div id="above" class="clearfix"><?php print render($page['above']); ?></div>
<?php endif; ?>

<div id="header-print" style="display:none;">
	<img src="/<?php print path_to_theme();?>/images/logo-print.gif" width="341" height="58" />
</div>
<div id="header" class="clearfix">
<?php if (($search_box)&&(function_exists('toplinks'))&&($banner)){ ?>
  <div id="top-elements">
    <?php if ($search_box): ?>
      <div id="search-box"><?php print $search_box; ?></div>
    <?php endif; ?>
    <?php if (function_exists('toplinks')): ?>
      <div id="toplinks"><?php print toplinks() ?></div>
    <?php endif; ?>
    <?php if ($banner): ?>
      <div id="banner"><?php print $banner; ?></div>
    <?php endif; ?> 
	
	<?php /*if ($logged_in) : ?>
		<div id="user_links"><?php print asp_cm_login() ?></div>
	<?php endif; */ ?>
	<div class="clear"></div>
  </div><!-- /top-elements -->
<?php } ?>
  <div id="logo">
  <?php if ($logo): ?>
    <a href="<?php print check_url($front_page); ?>" title="<?php print $site_name; ?>">
      <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" />
    </a>
  <?php endif; ?>
  </div> <!-- /logo -->
  <?php if (($site_name)||($site_slogan)) : ?>
	<!--<div id="name-and-slogan">
	<?php if ($site_name) : ?>
		<?php if ($is_front): ?>
			<h1 id="site-name"><a href="<?php print check_url($front_page); ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></h1>   
		<?php endif; ?> 
		<?php if (!$is_front): ?>
			<p id="site-name"><a href="<?php print check_url($front_page); ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></p>
		<?php endif; ?>
	<?php endif; ?>
	<?php if ($site_slogan): ?>
		<div id="site-slogan"><?php print $site_slogan; ?></div>
	<?php endif; ?>
	</div>--> <!-- /name-and-slogan -->
  <?php endif; ?>

<div class="brclear"></div>

<?php if ($page['header']): ?>
  <?php print render($page['header']); ?>
<?php endif; ?>

</div> <!-- /header -->



</div>
</div><!-- /top_right -->
</div><!-- /top_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->

<div id="body_bg" class="page0">
<div class="sizer0">
<div class="expander0">
<div id="body_left">
<div id="body_right">

	<?php if ((isset($main_menu)) && ($logged_in)) { ?>
	<?php if (theme_get_setting('menutype')== '0'): ?><div class="<?php print menupos() ?>"><?php print theme('links', $main_menu, array('class' =>'links', 'id' => 'navlist')); ?></div><?php endif; ?>
	<?php if (theme_get_setting('menutype')== '1'): ?><div id="navlinks" class="<?php print menupos() ?>"><?php print $primary_links_tree; ?></div><?php endif; ?>
	<?php } ?>

	<?php /*if (isset($secondary_links)) { ?>
	  <div class="<?php print menupos() ?>"><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')); ?></div>
	<?php } */?>

	
<?php if ($breadcrumb): ?>
  <div id="breadcrumb">
    <?php print $breadcrumb; ?>
  </div>
<?php endif; ?>

<?php
  foreach ($main_menu as $value) {
    if (isset($value['attributes']['class'])&&$title!='ユーザアカウント') $info = $value['title'];
  }
  if ($info=='マスタデータ')$logo_info='masters-mgmt';
  elseif ($info=='へルプ')$logo_info='content';
  elseif ($info=='重症度レベル判定')$logo_info='severity_level';
  elseif ($info=='病院情報管理')$logo_info='hospital-mgmt';
  elseif ($info=='グラフ')$logo_info='graph';
  elseif ($info=='データ入力')$logo_info='ward-data';

  if ($_GET['q'] == 'user/create') $logo_info = '';
?>
<?php if ($logo_info) {?><div id="title-main" class="<?php print $logo_info; ?>">
    <?php print $info; ?>
<?php print '</div>'; }?>
  
<div id="middlecontainer">
  <div id="wrapper">
  


    <div class="outer">
      <div class="float-wrap">
        <div class="colmain">
          <div id="main">
            <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
            <?php if ($title): { print '<h1 class="title"><span>'. $title .'</span></h1>'; } endif; ?>
            <?php // Remove File browser tab from user profile page
              $tab_name = t('File browser');
                $server_uri = $_SERVER['REQUEST_URI'];
                $server_uri_arr = explode('?', $server_uri);
                if(isset($server_uri_arr[1])) {
                  $query_str = '?' . $server_uri_arr[1];
                }
              $tabs = render($tabs);
              $tabs = preg_replace("/<li.*$tab_name<\/a><\/li>/", '', $tabs); 
              $path = drupal_get_path_alias($_GET['q']);
              if(preg_match('/user\/\d*\/edit$/', $path)) {
                $tabs = preg_replace('/<ul class=\"tabs secondary\">(?s).*?<\/ul>/', '', $tabs);
              }
              $path_str = explode('/', $path);
              if(count($path_str) == 2 && $path_str[0] == 'users') {
                $tabs = preg_replace('/href=\"(.*?)\"/', 'href="$1' .$query_str. '"', $tabs);
              }
            ?>

            <?php if ($tabs) { ?> <div class="tabs" id="tabs"><?php print $tabs ?></div> <?php } ?>
            <?php if ($content_top = render($page['content_top'])):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
            <?php print $help ?>
            <?php print $messages ?>
            <?php if ($node_middle = render($page['node_middle'])) { ?> <div id="node-middle"><?php print $node_middle; ?></div> <?php } ?>
            <?php print _asp_cm_user_profile(render($page['content']));?>
            <?php print $feed_icons;?>
			
          </div>
        </div> <!-- /colmain -->
        <?php if ($left = render($page['left'])) { ?>
          <div class="colleft">
            <div id="sidebar-left"><?php print $left ?></div>
          </div>
        <?php } ?>
        <br class="brclear" />
      </div> <!-- /float-wrap -->
      <?php if (($right = render($page['right'])||
              ($right_bottom = render($page['right_bottom']))))
              : ?>
        <div class="colright">
			<?php if ($right) : ?>
				<div id="sidebar-right"><?php print render($right); ?></div>
			<?php endif; ?>
			<?php if ($right_bottom) : ?>
				<div id="right-bottom"><?php print render($right_bottom); ?></div>
			<?php endif; ?>
        </div>
	  <?php endif; ?>
      <br class="brclear" />
    </div><!-- /outer -->
	<?php if ($content_bottom = render($page['content_bottom'])): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
  </div><!-- /wrapper -->
</div>

<div id="bar"></div>

<div id="bottom_bg" class="page0">
<div class="sizer0">
<div class="expander0">
<div id="bottom_left">
<div id="bottom_right">

<div id="footer-wrapper" class="clearfix">
  <div id="footer">
    <?php if ($below = render($page['below'])) { ?><div id="below"><?php print $below; ?></div><?php } ?>
    <div class="footer-links"><?php /* print $footer_message */ ?></div>
    <div class="footer-info">&copy; COPYRIGHT AIR LIQUIDE JAPAN 2015 <?php //print _get_copyright_year(); ?></div> 
  </div>
</div> <!-- /footer-wrapper -->

<div id="belowme">
</div>

</div><!-- /bottom_right -->
</div><!-- /bottom_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->


<?php /*if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist2')) ?><?php } */?>

</div><!-- /body_right -->
</div><!-- /body_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->

<div class="eopage">

</div>

</div></div><!-- /bg# -->
