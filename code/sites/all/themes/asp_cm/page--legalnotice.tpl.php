<div id="skip-nav"><a href="#main">Skip to Content</a></div>

<div id="bg1"><div id="bg2">

<div id="top_bg" class="page0">
<div class="sizer0">
<div id="topex" class="expander0">
<div id="top_left">
<div id="top_right">
<div id="headimg">

<?php if ($page['above']): ?>
	<div id="above" class="clearfix"><?php print render($page['above']); ?></div>
<?php endif; ?>

<div id="header" class="clearfix">
  <div id="logo">
  <?php if ($logo): ?>
      <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" />
  <?php endif; ?>
  </div>

<div class="brclear"></div>

<?php if ($page['header']): ?>
  <?php print render($page['header']); ?>
<?php endif; ?>

</div> <!-- /header -->



</div>
</div><!-- /top_right -->
</div><!-- /top_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->

<div id="body_bg" class="page0">
<div class="sizer0">
<div class="expander0">
<div id="body_left">
<div id="body_right">

<div id="middlecontainer">
  <div id="wrapper">
    <div class="outer">
      <div class="float-wrap">
        <div class="colmain">
          <div id="main">
            <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
            <?php if ($title): { print '<h1 class="title"><span>'. $title .'</span></h1>'; } endif; ?>
			<?php print $messages ?>
			<?php print $help ?>
            <?php print render($page['content']); ?>
            <?php print $feed_icons; ?>
			
          </div>
        </div> <!-- /colmain -->
        <?php if ($left = render($page['left'])) { ?>
          <div class="colleft">
            <div id="sidebar-left"><?php print $left ?></div>
          </div>
        <?php } ?>
        <br class="brclear" />
      </div> <!-- /float-wrap -->
      <?php if (($right = render($page['right'])||
              ($right_bottom = render($page['right_bottom']))))
              : ?>
        <div class="colright">
			<?php if ($right) : ?>
				<div id="sidebar-right"><?php print render($page['right']); ?></div>
			<?php endif; ?>
			<?php if ($right_bottom) : ?>
				<div id="right-bottom"><?php print print render($page['right_bottom']); ?></div>
			<?php endif; ?>
        </div>
	  <?php endif; ?>
      <br class="brclear" />
    </div><!-- /outer -->
	<?php if ($content_bottom = render($page['content_bottom'])): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
  </div><!-- /wrapper -->
</div>

<div id="bar"></div>

<div id="bottom_bg" class="page0">
<div class="sizer0">
<div class="expander0">
<div id="bottom_left">
<div id="bottom_right">

<div id="footer-wrapper" class="clearfix">
  <div id="footer">
    <?php if ($below = render($page['below'])) { ?><div id="below"><?php print $below; ?></div><?php } ?>
  </div>
</div> <!-- /footer-wrapper -->

<div id="belowme">
</div>

</div><!-- /bottom_right -->
</div><!-- /bottom_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->

</div><!-- /body_right -->
</div><!-- /body_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->

<div class="eopage">

</div>

</div>
  
</div>
