<div id="skip-nav"><a href="#main">Skip to Content</a></div>

<div id="bg1"><div id="bg2">

<div id="top_bg" class="page0">
<div class="sizer0">
<div id="topex" class="expander0">
<div id="top_left">
<div id="top_right">
<div id="headimg">

<?php if ($page['above']): ?>
	<div id="above" class="clearfix"><?php print render($page['above']); ?></div>
<?php endif; ?>

<div id="header-print" style="display:none;">
	<img src="/<?php print path_to_theme();?>/images/logo-print.gif" width="341" height="58" alt="<?php print $site_name; ?>" />
</div>
<div id="header" class="clearfix">
<!--search_box was removed from Drupal 7 -->  
<?php if (($search_box = FALSE) && (function_exists('toplinks')) && ($banner)){ ?>
  <div id="top-elements">
    <?php if (function_exists('toplinks')): ?>
      <div id="toplinks"><?php print toplinks() ?></div>
    <?php endif; ?>
    <?php if ($banner): ?>
      <div id="banner"><?php print $banner; ?></div>
    <?php endif; ?> 
	
	<?php /*if ($logged_in) : ?>
		<div id="user_links"><?php print asp_cm_login() ?></div>
	<?php endif; */ ?>
	<div class="clear"></div>
  </div><!-- /top-elements -->
<?php } ?>
  <div id="logo">
  <?php if ($logo): ?>
    <a href="<?php print check_url($front_page); ?>" title="<?php print $site_name; ?>">
      <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" />
    </a>
  <?php endif; ?>
  </div> <!-- /logo -->
  <?php if (($site_name)||($site_slogan)) : ?>
	<!--<div id="name-and-slogan">
	<?php if ($site_name) : ?>
		<?php if ($is_front): ?>
			<h1 id="site-name"><a href="<?php print check_url($front_page); ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></h1>   
		<?php endif; ?> 
		<?php if (!$is_front): ?>
			<p id="site-name"><a href="<?php print check_url($front_page); ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></p>
		<?php endif; ?>
	<?php endif; ?>
	<?php if ($site_slogan): ?>
		<div id="site-slogan"><?php print $site_slogan; ?></div>
	<?php endif; ?>
	</div>--> <!-- /name-and-slogan -->
  <?php endif; ?>

<div class="brclear"></div>

<?php if ($page['header']): ?>
  <?php print render($page['header']); ?>
<?php endif;?>

</div> <!-- /header -->



</div>
</div><!-- /top_right -->
</div><!-- /top_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->

<div id="body_bg" class="page0">
<div class="sizer0">
<div class="expander0">
<div id="body_left">
<div id="body_right">
<?php 
// add !empty($primary_links) 
?>
	<?php if ((isset($main_menu)) && ($logged_in)) { ?>
	<?php if (theme_get_setting('menutype')== '0'): ?><div class="<?php print menupos() ?>"><?php print theme('links', $main_menu, array('class' =>'links', 'id' => 'navlist')); ?></div><?php endif; ?>
	<?php if (theme_get_setting('menutype')== '1' && !empty($main_menu)): ?><div id="navlinks" class="<?php print menupos() ?>"><?php print $primary_links_tree; ?></div><?php endif; ?>
	<?php }?>

	<?php /*if (isset($secondary_links)) { ?>
	  <div class="<?php print menupos() ?>"><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')); ?></div>
	<?php } */?>

	
<?php if ($breadcrumb): ?>
  <div id="breadcrumb">
    <?php print $breadcrumb; ?>
  </div>
<?php endif; ?>

<?php
  $info = '';
  foreach ($main_menu as $key => $value) {
    if (preg_match('/active-trail/', $key) && $title!='ユーザアカウント') {
      $info = $value['title'];
    }
  }
  if ($info == 'マスタデータ') { $logo_info = 'masters-mgmt'; }
  elseif ($info == 'へルプ') { $logo_info = 'content'; }
  elseif ($info == '重症度レベル判定') { $logo_info = 'severity_level';}
  elseif ($info == '病院情報管理') { $logo_info = 'hospital-mgmt'; }
  elseif ($info == 'グラフ') { $logo_info = 'graph'; }
  elseif ($info == 'データ入力') { $logo_info = 'ward-data'; }
?>
<?php if (isset($logo_info)) {?><div id="title-main"><span class="<?php print $logo_info; ?>">
    <?php print $info; ?>
<?php print '</span></div>'; }?>
        
  <?php $tabs['#primary'][2]['#access']    =   false;//  remove  'トークン'  from $tabs .modified by yueshuo20160510?>
<div id="middlecontainer">
  <div id="wrapper">
    <div class="outer">
      <div class="float-wrap">
        <div class="colmain">
          <div id="main">
            <?php if ($highlighted = render($page['highlighted'])): ?><div id="highlighted"><?php print $highlighted; ?></div><?php endif; ?>
            <?php if ($title): { print '<h1 class="title"><span>'. $title .'</span></h1>'; } endif; ?>
            <?php if ($tabs = render($tabs)) { ?> <div class="tabs" id="tabs"><?php print $tabs ?></div> <?php } ?>
            <?php print $messages ?>
            <?php print render($page['help']) ?>
            <?php if ($content_top = render($page['content_top'])):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
            <?php if ($node_middle = render($page['node_middle'])) { ?> <div id="node-middle"><?php print $node_middle ?></div> <?php } ?>
            <?php 
            print render($page['content']); ?>
            <?php print $feed_icons; ?>
			
          </div>
        </div> <!-- /colmain -->
        <?php if ($left = render($page['left'])) { ?>
          <div class="colleft">
            <div id="sidebar-left"><?php print $left ?></div>
          </div>
        <?php } ?>
        <br class="brclear" />
      </div> <!-- /float-wrap -->
      <?php 
      $right = render($page['right']);
      $right_bottom = render($page['right_bottom']);
      if ($right || $right_bottom) : ?>
        <div class="colright">
			<?php if ($right) : ?>
				<div id="sidebar-right"><?php print $right; ?></div>
			<?php endif; ?>
			<?php if ($right_bottom) : ?>
				<div id="right-bottom"><?php print $right_bottom; ?></div>
			<?php endif; ?>
        </div>
	  <?php endif; ?>
      <br class="brclear" />
    </div><!-- /outer -->
	<?php if ($content_bottom = render($page['content_bottom'])): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
  </div><!-- /wrapper -->
</div>

<div id="bar"></div>

<div id="bottom_bg" class="page0">
<div class="sizer0">
<div class="expander0">
<div id="bottom_left">
<div id="bottom_right">

<div id="footer-wrapper" class="clearfix">
  <div id="footer">
    <?php if ($below = render($page['below'])) { ?><div id="below"><?php print $below; ?></div><?php } ?>
    <div class="footer-links"><?php /* print $footer_message */ ?></div>
    <div class="footer-info">&copy; COPYRIGHT AIR LIQUIDE JAPAN 2015 <?php //print _get_copyright_year(); ?></div> 
  </div>
</div> <!-- /footer-wrapper -->

<div id="belowme">
</div>

</div><!-- /bottom_right -->
</div><!-- /bottom_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->


<?php /*if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist2')) ?><?php } */?>

</div><!-- /body_right -->
</div><!-- /body_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->

<div class="eopage">

</div>

</div></div><!-- /bg# -->
<?php //if($show_loading_icon){ ?>
	<div id="div-loader">
			<div id="loader">
				<div id="div-load-picture"><img id="load-picture" src="/sites/all/themes/asp_cm/images/loader/pure01.png" alt="ロード中" /></div>
				<img class="loading-scroll" src="/sites/all/themes/asp_cm/images/loader_horiz1s.gif" alt="ロード中" />
			</div>
	</div>
	<div id='background-popup' class="hidden"></div>
	<div id='background-popup-dialog' class="hidden"></div>
	

<?php //} ?>
