<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

<head>
  <title><?php print $head_title; ?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=8" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if IE]>
  <?php print $ie_style; ?>
  <![endif]-->
  <!--[if lte IE 6]>
  <?php print $ie6_style; ?>
  <![endif]-->
  <!--[if IE 7]>
  <?php print $ie7_style; ?>
  <![endif]-->
  <!--[if IE 8]>
  <?php print $ie8_style; ?>
  <![endif]-->
  <link type="text/css" rel="stylesheet" media="print" href="/sites/all/themes/asp_cm/css/print.css" />
  <!--[if IE ]>
  <link type="text/css" rel="stylesheet" media="print" href="/sites/all/themes/asp_cm/css/print_ie.css" />
  <![endif]-->
  <script src="/sites/all/themes/asp_cm/js/jquery-1.7.2.min.js" type="text/javascript"></script>
  <?php print $scripts ?>
</head>
<body id="maintenance" class="<?php print $classes; ?>" <?php print $attributes;?>>
<div id="page">
<div id="skip-nav"><a href="#main">Skip to Content</a></div>

<div id="bg1">
  <div id="bg2">
  <div id="top_bg" class="page0">
  <div class="sizer0">
  <div id="topex" class="expander0">
  <div id="top_left">
  <div id="top_right">
  <div id="headimg">
  
    <?php 
    $block = module_invoke('block', 'block_view', '12');
    if ($block['content']) :?>
    <div id="above" class="clearfix">
    <?php print render($block['content']);?>
    </div>
    <?php endif;?>
  
  <div id="header" class="clearfix">
    <div id="logo">
    <?php if ($logo): ?>
        <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" />
    <?php endif; ?>
    </div>
  <div class="brclear"></div>
  </div> <!-- /header -->
  
  
  

  </div>
  </div><!-- /top_right -->
  </div><!-- /top_left -->
  </div><!-- /expander0 -->
  </div><!-- /sizer0 -->
  </div><!-- /page0 -->

  <div id="body_bg" class="page0">
    <div class="sizer0">
    <div class="expander0">
    <div id="body_left">
    <div id="body_right">

    <div id="middlecontainer">
      <div id="wrapper">
    <div class="outer">
      <div class="float-wrap">
        <div class="colmain">
          <div id="main">
          <?php if (!empty($title)): ?><h1 class="title" id="page-title"><span><?php print $title; ?></span></h1><?php endif; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
          <div id="content-content" class="clearfix">
            <?php print $content; ?>
          </div> <!-- /content-content -->

          </div>
         </div> <!-- /colmain -->
        <br class="brclear" />
      </div> <!-- /float-wrap -->
      <br class="brclear" />
    </div><!-- /outer -->
    </div><!-- /wrapper -->
    </div>
<div id="bar"></div>

<div id="bottom_bg" class="page0">
<div class="sizer0">
<div class="expander0">
<div id="bottom_left">
<div id="bottom_right">

<div id="footer-wrapper" class="clearfix">
  <div id="footer">
    <?php if ($footer_message) { ?><div id="below"><?php print $footer_message; ?></div><?php } ?>
  </div>
</div> <!-- /footer-wrapper -->

<div id="belowme">
</div>

</div><!-- /bottom_right -->
</div><!-- /bottom_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->

</div><!-- /body_right -->
</div><!-- /body_left -->
</div><!-- /expander0 -->
</div><!-- /sizer0 -->
</div><!-- /page0 -->
  
  </div>
</div>
</div>
</body>
    
</html>