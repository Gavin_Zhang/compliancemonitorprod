<div class="block-wrapper <?php print $block_zebra .' block_'. $block_id; ?>">
  <div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module ?> <?php if ($themed_block): ?>themed-block<?php endif; ?>">
    <?php if ($block->subject): ?>
      <?php if ($themed_block): ?>
    <div class="block-icon"></div>
      <?php endif; ?>
    <h2 class="title"><span class="title-border"><?php print $block->subject ?></span></h2>
      <?php endif; ?>
    <div class="content"><?php global $user, $base_path;
    module_load_include('inc', 'aspcm', 'includes/aspcm_common');
    if ($user->uid > 0) {
    $user = get_user_obj($user->uid);
      $content = str_replace('%USER_UID%', $user->uid, $content);
      $content = str_replace('%USER_NAME%', $user->asp_user_japanese_name.' さん', $content);
    }
    // Replace following placeholders in the block
    $patterns = array(
      '%BASE_PATH%' => $base_path, 
      '%BASE_FILES%' => file_directory_path() .'/',
      '%COPYRIGHT_YEAR%' => _get_copyright_year(),
    );
  
    $content = strtr($content, $patterns);
    print $content?></div>
  </div>
</div>